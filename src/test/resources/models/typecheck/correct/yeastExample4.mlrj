// Example 4

////////////////////////////////////////////////////////////////////////////////
//  Multi-level model describing the interplay between intracellular          //
//  dynamics, secreted pheromone molecules, and high-level states at cellular //
//  level. In addition, cells may switch their mating type when they divide.  //
////////////////////////////////////////////////////////////////////////////////

// PARAMETERS
dtot:1000;
k1:0.015*dtot;
k2:200;
k3:180;
k3prime:0.018;
k4:4.5;
k5:0.6;
k6:1.0;
k7:1e6;
k8:k7;
k9:k7;
k10:1.0;
k11:1.5;
k112:800;
k12:0.02;
k13:0.005;
k15:1e-4;
t7:250;
t8:70;
t9:20;
td:232;

// SPECIES DEFINITIONS
C(num,string,string,string)[];
Y();
Yp();
D();
Mi();
Ma();
Mr();
Fp();
Fm();
X();

// INITIAL SOLUTION
>>INIT[
  C(1.0,'G1','P','U')[(dtot-1) D + Ma] + 
  C(1.0,'G1','M','U')[(dtot-1) D + Ma]
];

// RULE SCHEMATA

// (1) cyclin synthesis 
C(v,p,t,w)[s?]:c -> C(v,p,t,w)[Y + s?] @ k1*#c;

// (2) formation of inactive MPF complex
Y:y + D:d -> Mi @ k2*#y*#d;

// (3) activation of MPF complex
Mi:i + Ma:a -> 2 Ma @ (k3prime+(k3*((#a/dtot)^(2))))*#i;

// (4) breakage of activated MPF complex
C(v,p,t,w)[Ma:a + s?]:c -> C(v,p,t,w)[Yp + D + s?] @ if (#a>1) then (k4/v)*#a*#c else 0;

// (5) cyclin degradation
Yp:y -> @ k5*#y;

// (6) cell growth
C(v,p,t,w)[s?]:c -> C(v+(1/td),p,t,w)[s?] @ if (p=='G1') || (p=='SG2') then k6*#c else 0;

// (7) cell cycle transition from G1->S/G2
C(v,'G1',t,w)[Mi:i + s?]:c -> C(v,'SG2',t,w)[Mi + s?] @ if (#i>t7) then k7*#c else 0;

// (8) cell cycle transition from S/G2->M
C(v,'SG2',t,w)[Ma:a + s?]:c -> C(v,'M',t,w)[Ma + s?] @ if (#a>t8) then k8*#c else 0;

// (9) cell division and mating type switching (transition from M->G1)
C(v,'M',t,'U')[Ma:a + s?]:c -> C(v/2,'G1',t,'S')[Ma + s?] + C(v/2,'G1',t,'U')[Ma + s?] @ if (#a<t9) then k9*#c else 0;
C(v,'M',t,'S')[Ma:a + s?]:c -> C(v/2,'G1',t,'S')[Ma + s?] + C(v/2,'G1',if (t=='P') then 'M' else 'P','U')[Ma + s?] @ if (#a<t9) then k9*#c else 0;

// (10) pheromone secretion
C(v,p,'P',w)[s?]:c -> C(v,p,'P',w)[s?] + Fp @ k10*#c;
C(v,p,'M',w)[s?]:c -> C(v,p,'M',w)[s?] + Fm @ k10*#c;

// (11) pheromone induced MPF inhibition
C(v,p,'M',w)[Mi:i + s?]:c + Fp:f -> C(v,p,'M',w)[Mr + s?] + Fp @ ((k11*(#f^(3)))/((k112^(3))+(#f^(3)))/(v^(2)))*#i*#c;
C(v,p,'P',w)[Mi:i + s?]:c + Fm:f -> C(v,p,'P',w)[Mr + s?] + Fm @ ((k11*(#f^(3)))/((k112^(3))+(#f^(3)))/(v^(2)))*#i*#c;

// (12) recovery of inactive MPF from inhibited MPF
Mr:r -> Mi @ k12*#r;

// (13) pheromone degradation (diffusion out of system)
Fp:f -> @ k13*#f;
Fm:f -> @ k13*#f;

// (10,13) Sxa2 turnover
C(v,p,'M',w)[s?]:c -> C(v,p,'M',w)[s?] + X @ (k10/10)*#c;
X:x -> @ k13*#x;

// (15) catalytic P-factor degradation
X:x + Fp:p -> X @ k15*#x*#p;

