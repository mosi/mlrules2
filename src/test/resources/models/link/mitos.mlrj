// get a random number in [0,1]
h :: num;
h = unif(0,1) * 1.0;

// create n mitochondria
g :: num -> sol;
g 0 = [];
g x = 1 Mito(false,free,h(),h())[(midNum) Midi] + g(x-1);

//species
Cell()[];
Mito(bool,link,num,num)[];
Fis();
Drpa();
Drpi();
Mff();
Midi();
Midm();

//parameters

mitoNum: 80; // number of mitochondria

minCoord: 0;
maxCoord: 1;

movement: 1;
rMovement: 1;

maxMff: 4;
maxMid: 4;
maxFis: 8;
maxDrpa: 4;
maxDrpi: 4;


fisRecThreshold: 2;
midRecThreshold: 2;
drpaRecThreshold: 2;
drpiRecThreshold: 2;


rMffRecruitment_free: 0.002; 
rMffRecruitment: 1;
rMidRecruitment_free: 0.002;
rMidRecruitment: 1;
rDrpaRecruitment: 1;
rDrpiRecruitment: 0.2;
rFreeDrpi: 1;
rFisRecruitment: 1;

rFusion: 0.1;
rFission: 0.1;

fisdrpFactor: 1;

mffNum: maxMff * mitoNum;
midNum: maxMid * mitoNum; 
drpaNum: mitoNum * maxDrpa * fisdrpFactor;
drpiNum: mitoNum * maxDrpi * fisdrpFactor;
fisNum: mitoNum * maxFis * fisdrpFactor;

//initial solution
>>INIT[(fisNum) Fis + (drpaNum) Drpa + (drpiNum) Drpi + g(mitoNum) + (mffNum) Mff];  

//reaction rules

// Attachment Mito-Tubule 
Mito(false,free,x,y)[sol?]:m
	-> Mito(true,free,x,y)[sol?] 
	@ 0.2*#m; 

// Detachment Mito-Tubule 
Mito(true,mito,x,y)[sol?]:m 
	-> Mito(false,mito,x,y)[sol?] 
	@ 0.2*#m;

// Mito Fusion
Mito(true,free,x1,y1)[sol1?]:m1 + Mito(tub,free,x2,y2)[sol2?]:m2 
	-> Mito(true,n,x1,y1)[sol1?] + Mito(tub,n,x2,y2)[sol2?]
	@ if ((dist(x1, y1, x2, y2)<=0.1) && (count(sol1?,'Mff') + count(sol2?,'Mff') < maxMff)  && (count(sol1?,'Midm') + count(sol2?,'Midm') < maxMid)) then rFusion else 0 
	where n = nu();

// Mff recruitment for free mitos
Mito(tub,free,x1,y1)[sol?]:mito + Mff:mff 
	-> Mito(tub,free,x1,y1)[Mff + sol?] 
	@ if (count(sol?,'Mff') < maxMff) then #mito*#mff*rMffRecruitment_free else 0;

// Midm recruitment for free Mitos
Mito(tub,free,x1,y1)[Midi:midi + sol?]:mito 
	-> Mito(tub,free,x1,y1)[Midm + sol?] 
	@ if ((count(sol?,'Mff') >= midRecThreshold) && (count(sol?,'Midm') < maxMid)) then rMidRecruitment_free * #mito * #midi else 0;

// Mff recruitment for binded mitos
Mito(tub1,mito,x1,y1)[sol1?] + Mito(tub2,mito,x2,y2)[sol2?] + Mff:mff 
	-> Mito(tub1,mito,x1,y1)[Mff + sol1?] + Mito(tub2,mito,x2,y2)[sol2?]
	@ if ((mito != free) && (count(sol1?,'Mff') + count(sol2?,'Mff') < maxMff)) then rMffRecruitment * #mff else 0;

// Fis Recruitment
Mito(tub1,mito,x1,y1)[sol1?] + Mito(tub2,mito,x2,y2)[sol2?] + 1 Fis:fis 
	-> Mito(tub1,mito,x1,y1)[sol1? + 1 Fis] + Mito(tub2,mito,x2,y2)[sol2?] 
	@ if ((mito != free) && (count(sol1?,'Fis') + count(sol2?,'Fis') < maxFis) && (count(sol1?,'Mff') + count(sol2?,'Mff') >= fisRecThreshold)) then rFisRecruitment * #fis else 0; 

// Mid Recruitment
Mito(tub1,mito,x1,y1)[Midi:midi + sol1?] + Mito(tub2,mito,x2,y2)[sol2?] 
	-> Mito(tub1,mito,x1,y1)[Midm + sol1?] + Mito(tub2,mito,x2,y2)[sol2?]
	@ if ((mito != free) && (count(sol1?,'Mff') + count(sol2?,'Mff') >= midRecThreshold) && (count(sol1?,'Midm') + count(sol2?,'Midm') < maxMid)) then rMidRecruitment * #midi else 0;

// Drpa recruitment
Mito(tub1,mito,x1,y1)[sol1?] + Mito(tub2,mito,x2,y2)[sol2?] + Drpa:drpa
	-> Mito(tub1,mito,x1,y1)[Drpa + sol1?] + Mito(tub2,mito,x2,y2)[sol2?]
	@ if ((mito != free) && (count(sol1?,'Midm') + count(sol2?,'Midm') >= drpaRecThreshold) && (count(sol1?,'Drpa') + count(sol2?,'Drpa') < maxDrpa)) then rDrpaRecruitment * #drpa else 0;

// Drpi recruitment
Mito(tub1,mito,x1,y1)[sol1?] + Mito(tub2,mito,x2,y2)[sol2?] + Drpi:drpi
	-> Mito(tub1,mito,x1,y1)[Drpi + sol1?] + Mito(tub2,mito,x2,y2)[sol2?]
	@ if ((mito != free) && (count(sol1?,'Midm') + count(sol2?,'Midm') >= drpiRecThreshold) && (count(sol1?,'Drpi') + count(sol2?,'Drpi') < maxDrpi)) then rDrpiRecruitment * #drpi else 0;

// Freeing Drpi
Mito(tub1,mito,x1,y1)[Drpi:drpi + sol1?] + Mito(tub2,mito,x2,y2)[sol2?]
	-> Mito(tub1,mito,x1,y1)[(midm) Midi + filter(sol1?,'Midm')] + Mito(tub2,mito,x2,y2)[sol2?] + Drpi
	@ if ((mito != free) && (midm + count(sol2?,'Midm') >= maxMid) && (count(sol1?,'Midi') + count(sol2?,'Midi') >= maxMid) && (count(sol1?,'Drpi') + count(sol2?,'Drpi') >= maxDrpi)) then rFreeDrpi else 0 
	where midm = count(sol1?,'Midm');
	
// Mito fission
Mito(tub1,mito,x1,y1)[sol1?] + Mito(tub2,mito,x2,y2)[sol2?]
	-> Mito(tub1,mito,x1,y1)[(midm1) Midi] + Mito(tub2,mito,x2,y2)[(midm2) Midi] + filter(sol1?,'Midm') + filter(sol2?,'Midm')
	@ if ((mito != free) && (count(sol1?,'Fis') + count(sol2?,'Fis') >= maxFis) && (count(sol1?,'Drpa') + count(sol2?,'Drpa') > maxDrpa) && (count(sol1?,'Mff') + count(sol2?,'Mff') >= maxMff) && (count(sol1?,'Midi') + count(sol2?,'Midi') >= maxMid) && (count(sol1?,'Midm') + count(sol2?,'Midm') >= maxMid)) then rFission else 0
	where midm1 = count(sol1?,'Midm'), midm2 = count(sol2?,'Midm');