package org.jamesii.mlrules.util.runtimeCompiling;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.math.AddNode;
import org.jamesii.core.math.parsetree.math.MinusNode;
import org.jamesii.core.math.parsetree.variables.Environment;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.debug.MyParserTest;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.parser.functions.Function;
import org.jamesii.mlrules.parser.nodes.FunctionCallNode;
import org.junit.AfterClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by tm219 on 4/5/17.
 */
public class DynamicNodeTest {
    private final String mlrjFile = getClass().getResource("/models/runtimeCompiling/default.mlrj").getPath();

    private INode createAddNode(){
        ValueNode<Double> left = new ValueNode<>(24.3545);
        ValueNode<Integer> right = new ValueNode<>(6D);
        return new AddNode(left,right);
    }

    @AfterClass
    public static void cleanupTest(){
        try {
            CompileTools.deleteGeneratedClasses();
        }catch (Exception e){
            assert(false);
        }
    }

    @Test
    public void testJavassistGeneration(){
        JavassistNodeFactory factory;
        try {
            factory = new JavassistNodeFactory();
        DynamicNode dynamicNode = factory.generateDynamicNode(createAddNode(), new Environment());
        System.out.println(((ValueNode)dynamicNode.calc(new Environment<>())).getValue());
        assertEquals(30.3545, ((ValueNode)dynamicNode.calc(new Environment<>())).getValue());
        }catch (Exception e){
            assert(false);
        }
    }

    @Test
    public void testJavassistGeneration2(){
        JavassistNodeFactory factory;
        try {
            factory = new JavassistNodeFactory();
        IEnvironment<String> environment = new Environment<>(new HashMap<>());
        environment.setValue("test", 15D);
        MinusNode root = new MinusNode(createAddNode(), new Identifier<>("test"));
        DynamicNode dynamicNode = factory.generateDynamicNode(root, environment);
        System.out.println(((ValueNode)dynamicNode.calc(environment)).getValue());
        assertEquals(15.3545D, (Double)((ValueNode)dynamicNode.calc(environment)).getValue(),0.00001);
        }catch (Exception e){
            assert(false);
        }
    }

    @Test
    public void testDefaultScript(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            INode randomStuffFunction = ((Function) model.getEnv().getGlobal().get("randomStuff")).getDefinitions().get(0).getFunction();
            JavassistNodeFactory factory = new JavassistNodeFactory();
            DynamicNode dynamicNode = factory.generateDynamicNode(randomStuffFunction, model.getEnv());
            model.getEnv().getGlobal().put("a", 10D);
            model.getEnv().getGlobal().put("b", 1D);
            //MyParserTest.run(model, 10);
            System.out.println(((ValueNode)dynamicNode.calc(model.getEnv())).getValue());
            assertEquals(16D, (Double)((ValueNode)dynamicNode.calc(model.getEnv())).getValue(),0.00000001D);
            model.getEnv().getGlobal().put("a", 5D);
            model.getEnv().getGlobal().put("b", 2D);
            System.out.println(((ValueNode)dynamicNode.calc(model.getEnv())).getValue());
            assertEquals(4.5D, (Double)((ValueNode)dynamicNode.calc(model.getEnv())).getValue(),0.00000001D);
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void functionTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            JavassistNodeFactory factory = new JavassistNodeFactory();
            Function dynamicFunction = CompileTools.compileFunction((Function) model.getEnv().getGlobal().get("randomStuff"), model.getEnv());
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(5D));
            arguments.add(new ValueNode<>(2D));
            FunctionCallNode functionCall = new FunctionCallNode(new ValueNode<>(dynamicFunction), arguments);
            System.out.println(((ValueNode)functionCall.calc(model.getEnv())).getValue());
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void functionTest2(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(5D));
            arguments.add(new ValueNode<>(2D));
            FunctionCallNode functionCall = Utils.generateDynamicFunctionCall("randomStuff2", arguments, model);
            System.out.println(((ValueNode)functionCall.calc(model.getEnv())).getValue());
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void constantTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            List<Node> arguments = new LinkedList<>();
            FunctionCallNode functionCall = Utils.generateDynamicFunctionCall("f", arguments, model);
            assertEquals(5.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void globalVariableTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(1.0));
            FunctionCallNode functionCall = Utils.generateDynamicFunctionCall("globalVarUse", arguments, model);
            assertEquals(1.1,((ValueNode)functionCall.calc(model.getEnv())).getValue());
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void simpleMultipleDefinitionsTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            String functionName = "g";
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(1.0));
            FunctionCallNode functionCall = new FunctionCallNode((Function)model.getEnv().getGlobal().get("g"), arguments);
            assertEquals(-1.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());


            arguments.clear();
            arguments.add(new ValueNode<>(1.0));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(-1.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(2D));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(2.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(200D));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(200.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(-200D));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(-200.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void negationTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            String functionName = "h";
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(1.0));
            FunctionCallNode functionCall = new FunctionCallNode((Function)model.getEnv().getGlobal().get(functionName), arguments);
            assertEquals(-1.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(1.0));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(-1.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(2D));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(-2.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(200D));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(-200.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

            arguments.clear();
            arguments.add(new ValueNode<>(-200));
            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            assertEquals(200.0,((ValueNode)functionCall.calc(model.getEnv())).getValue());

        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void recursionTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            String functionName = "recursion";
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(1.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 1.0, 0.0);

            arguments.clear();
            arguments.add(new ValueNode<>(2.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 2.0, 0.0);

            arguments.clear();
            arguments.add(new ValueNode<>(10D));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 10.0, 0.0);

            arguments.clear();
            arguments.add(new ValueNode<>(200D));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 200.0, 0.0);

        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void fibonacciTest(){
        try {
            Model model = MyParserTest.init(mlrjFile);
            String functionName = "fibonacci";
            List<Node> arguments = new LinkedList<>();
            arguments.add(new ValueNode<>(0.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 0, 0.0);

            arguments.clear();
            arguments.add(new ValueNode<>(1.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 1.0, 0.0);


            arguments.clear();
            arguments.add(new ValueNode<>(2.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 1.0, 0.0);


            arguments.clear();
            arguments.add(new ValueNode<>(3.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 2.0, 0.0);


            arguments.clear();
            arguments.add(new ValueNode<>(10.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 55.0, 0.0);


            arguments.clear();
            arguments.add(new ValueNode<>(20.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 6765.0, 0.0);

            arguments.clear();
            arguments.add(new ValueNode<>(30.0));
            Utils.doStandardTest(mlrjFile, functionName, arguments, 832040.0, 0.0);
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void multiplyBraketsTest(){
        List<Node> arguments = new LinkedList<>();
        Utils.doStandardTest(mlrjFile, "multBrakets", arguments, 7, 0.0);
    }

    @Test
    public void addBraketsTest(){
        List<Node> arguments = new LinkedList<>();
        Utils.doStandardTest(mlrjFile, "addBrakets", arguments, 9, 0.0);
    }
}
