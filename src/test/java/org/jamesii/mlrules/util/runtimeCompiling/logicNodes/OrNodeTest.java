package org.jamesii.mlrules.util.runtimeCompiling.logicNodes;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.mlrules.util.runtimeCompiling.CompileTools;
import org.jamesii.mlrules.util.runtimeCompiling.Utils;
import org.junit.AfterClass;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tm219 on 7/18/17.
 */
public class OrNodeTest {

    private final String mlrjFile = getClass().getResource("/models/runtimeCompiling/logicNodes/or.mlrj").getPath();

    @AfterClass
    public static void cleanupTest(){
        try {
            CompileTools.deleteGeneratedClasses();
        }catch (Exception e){
            assert(false);
        }
    }

    @Test
    public void constantFalseTest(){
        List<Node> arguments = new LinkedList<>();
        Utils.doStandardTest(mlrjFile, "constant1", arguments, false);
    }

    @Test
    public void constantTrueTest(){
        List<Node> arguments = new LinkedList<>();
        Utils.doStandardTest(mlrjFile, "constant2", arguments, true);
    }

    @Test
    public void leftvarFalseTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(false));
        Utils.doStandardTest(mlrjFile, "leftvar", arguments, false);
    }

    @Test
    public void leftvarTrueTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(true));
        Utils.doStandardTest(mlrjFile, "leftvar", arguments, true);
    }

    @Test
    public void rightvarFalseTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(false));
        Utils.doStandardTest(mlrjFile, "rightvar", arguments, false);
    }

    @Test
    public void rightvarTrueTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(true));
        Utils.doStandardTest(mlrjFile, "rightvar", arguments, true);
    }

    @Test
    public void variableFalseTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(false));
        arguments.add(new ValueNode<>(false));
        Utils.doStandardTest(mlrjFile, "variable", arguments, false);
    }

    @Test
    public void variableTrueTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(true));
        arguments.add(new ValueNode<>(false));
        Utils.doStandardTest(mlrjFile, "variable", arguments, true);
    }

}
