package org.jamesii.mlrules.util.runtimeCompiling;

import org.jamesii.mlrules.debug.MyParserTest;
import org.jamesii.mlrules.model.Model;
import org.junit.Test;

public class ConstantPropagationTest {
    private final String mlrjFile = getClass().getResource("/models/runtimeCompiling/constantPropagation.mlrj").getPath();
    @Test
    public void runTest() {
        try {
            Model model = MyParserTest.init(mlrjFile);
            System.out.println("Start with runtimeCompilation");
            long startTime = System.currentTimeMillis();
            MyParserTest.run(MyParserTest.SimulatorType.Standard, model, true, 5000);
            System.out.println(String.format("Finished in %d ms",System.currentTimeMillis()-startTime));
            model = MyParserTest.init(mlrjFile);
            System.out.println("Start unoptimized");
            startTime = System.currentTimeMillis();
            MyParserTest.run(MyParserTest.SimulatorType.Standard,model, false, 5000);
            System.out.println(String.format("Finished in %d ms",System.currentTimeMillis()-startTime));
        }catch (Exception e){
            assert(false);
        }
    }
}
