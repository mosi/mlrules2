package org.jamesii.mlrules.util.runtimeCompiling.arithmeticNodes;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.mlrules.util.runtimeCompiling.CompileTools;
import org.jamesii.mlrules.util.runtimeCompiling.Utils;
import org.junit.AfterClass;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tm219 on 7/18/17.
 */
public class AddNodeTest {

    private final String mlrjFile = getClass().getResource("/models/runtimeCompiling/arithmeticNodes/add.mlrj").getPath();

    @AfterClass
    public static void cleanupTest(){
        try {
            CompileTools.deleteGeneratedClasses();
        }catch (Exception e){
            assert(false);
        }
    }

    @Test
    public void constantTest(){
        List<Node> arguments = new LinkedList<>();
        Utils.doStandardTest(mlrjFile, "constant", arguments, 6.0, 0.0);
    }

    @Test
    public void leftvarTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(5D));
        Utils.doStandardTest(mlrjFile, "leftvar", arguments, 6.0, 0.0);
    }

    @Test
    public void rightvarTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(5D));
        Utils.doStandardTest(mlrjFile, "rightvar", arguments, 6.0, 0.0);
    }

    @Test
    public void variableTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(5D));
        arguments.add(new ValueNode<>(1D));
        Utils.doStandardTest(mlrjFile, "variable", arguments, 6.0, 0.0);
    }

    @Test
    public void recursiveTest(){
        List<Node> arguments = new LinkedList<>();
        arguments.add(new ValueNode<>(0D));
        Utils.doStandardTest(mlrjFile, "recursive", arguments, 50.0, 0.0);
    }

    @Test
    public void recursiveConstantTest(){
        List<Node> arguments = new LinkedList<>();
        Utils.doStandardTest(mlrjFile, "recursiveConstant", arguments, 50.0, 0.0);
    }


}
