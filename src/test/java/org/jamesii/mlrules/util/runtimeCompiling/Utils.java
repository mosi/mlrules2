package org.jamesii.mlrules.util.runtimeCompiling;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.mlrules.debug.MyParserTest;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.parser.functions.Function;
import org.jamesii.mlrules.parser.functions.FunctionDefinition;
import org.jamesii.mlrules.parser.nodes.FunctionCallNode;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by tm219 on 7/18/17.
 */
public class Utils {
    public static FunctionCallNode generateDynamicFunctionCall(String functionName, List<Node> arguments, Model model){
        Function dynamicFunction;
        try {
            dynamicFunction = CompileTools.compileFunction((Function) model.getEnv().getGlobal().get(functionName), model.getEnv());
        }catch (Exception e){
            assert (false);
            return null;
        }
        for(FunctionDefinition node : dynamicFunction.getDefinitions()){
            if(!(node.getFunction() instanceof DynamicNode)){
                assert (false);
            }
        }
        model.getEnv().getGlobal().put(functionName, dynamicFunction);
        return new FunctionCallNode(new ValueNode<>(dynamicFunction), arguments);
    }

    public static void doStandardTest(String mlrjFile, final String functionName, List<Node> arguments, double expected, double delta){
        try {
            Model model = MyParserTest.init(mlrjFile);

            Function function = (Function) model.getEnv().getGlobal().get(functionName);
            FunctionCallNode functionCall = new FunctionCallNode(new ValueNode<>(function), arguments);
            Double reference = (Double)((ValueNode)functionCall.calc(model.getEnv())).getValue();

            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            Double actual = (Double)((ValueNode)functionCall.calc(model.getEnv())).getValue();

            assertEquals(expected, actual,delta);
            assertEquals(reference, actual,delta);
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }

    public static void doStandardTest(String mlrjFile, final String functionName, List<Node> arguments, boolean expected){
        try {
            Model model = MyParserTest.init(mlrjFile);

            Function function = (Function) model.getEnv().getGlobal().get(functionName);
            FunctionCallNode functionCall = new FunctionCallNode(new ValueNode<>(function), arguments);
            Boolean reference = (Boolean)((ValueNode)functionCall.calc(model.getEnv())).getValue();

            functionCall = Utils.generateDynamicFunctionCall(functionName, arguments, model);
            Boolean actual = (Boolean)((ValueNode)functionCall.calc(model.getEnv())).getValue();

            assertEquals(expected, actual);
            assertEquals(reference, actual);
        }catch (Exception e){
            System.out.println("\n");
            e.printStackTrace();
            assert(false);
        }
    }
}
