/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.jamesii.mlrules.parser.exception.SemanticsException;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TypeCheckVisitorTest {

    @BeforeClass
    public static void setLogLevel() {
        Logger.getGlobal().setLevel(Level.WARNING);
    }


    private void checkModel(File model) throws IOException {
        CharStreams.fromFileName(model.getAbsolutePath());
        MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromFileName(model.getAbsolutePath()));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MLRulesParser parser = new MLRulesParser(tokens);
        TypeCheckVisitor typeChecker = new TypeCheckVisitor();
        typeChecker.doTypeCheck(parser.model());
    }

    @Test
    public void checkCorrectModels() throws IOException {
        URL typeCheckURL = this.getClass().getResource("/models/typecheck/correct");
        for (File model : (new File(typeCheckURL.getFile())).listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.isFile();
            }

        })) {
            checkModel(model);
        }
        URL modelURL = this.getClass().getResource("/models");
        for (File model : (new File(modelURL.getFile())).listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.isFile();
            }

        })) {
            checkModel(model);
        }
    }

    @Test(expected = SemanticsException.class)
    public void checkFailureModel1() throws IOException {
        URL url = this.getClass().getResource("/models/typecheck/failure/first.mlrj");
        checkModel(new File(url.getFile()));
    }

    @Test(expected = SemanticsException.class)
    public void checkFailureModel2() throws IOException {
        URL url = this.getClass().getResource("/models/typecheck/failure/second.mlrj");
        checkModel(new File(url.getFile()));
    }

    @Test(expected = SemanticsException.class)
    public void checkFailureModel3() throws IOException {
        URL url = this.getClass().getResource("/models/typecheck/failure/third.mlrj");
        checkModel(new File(url.getFile()));
    }

    @Test(expected = SemanticsException.class)
    public void checkFailureModelNoAttributes() throws IOException {
        URL url = this.getClass().getResource("/models/typecheck/failure/noAttributes.mlrj");
        checkModel(new File(url.getFile()));
    }

    @Test(expected = SemanticsException.class)
    public void checkFailureModelFunctionAsParameter() throws IOException {
        URL url = this.getClass().getResource("/models/typecheck/failure/functionAsParameter.mlrj");
        checkModel(new File(url.getFile()));
    }

}
