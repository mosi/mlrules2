/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.misc.CountListener;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.TimePointListObserver;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.factory.*;
import org.jamesii.mlrules.util.MLEnvironment;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

public class ObservationTest {

  @BeforeClass
  public static void setLogLevel() {
    Logger.getGlobal().setLevel(Level.WARNING);
  }


  private void executeModel(String path, SimulatorFactory factory) throws IOException, ModelNotSupportedException {
    String path2 = new File(this.getClass().getResource(path).getFile()).getAbsolutePath();


    MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromFileName(path2));

    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(Model.RNG, new MersenneTwister(System.currentTimeMillis()));
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
    PredefinedFunctions.addAll(env);

    MLRulesLexer typeLexer = new MLRulesLexer(CharStreams.fromFileName(path2));
    CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
    MLRulesParser typeParser = new MLRulesParser(typeTokens);
    TypeCheckVisitor typeChecker = new TypeCheckVisitor();
    typeChecker.doTypeCheck(typeParser.model());

    StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, new HashMap<>());
    Model m = visitor.create(parser.model());

    Observer o = new TimePointListObserver(Arrays.asList(0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0));
    CountListener l = new CountListener();
    o.register(l);

    Simulator simulator = factory.create(m);
    simulator.getObserver().add(o);
    double time = 10;
    while (Double.compare(simulator.getCurrentTime(), time) <= 0) {
      simulator.nextStep();
    }
    assertEquals(l.getCounter(), 11);

  }

  @Test
  public void testHybrid() throws IOException, ModelNotSupportedException {
    executeModel("/models/lotkavolterra.mlrj", new HybridSimulatorFactory(HybridSimulatorFactory.Integrator.DORMAND_PRINCE, 0.01, 0.1, 0.03, false,false));
  }
  
  @Test
  public void testSimple() throws IOException, ModelNotSupportedException {
    executeModel("/models/lotkavolterra.mlrj", new SimpleSimulatorFactory(true, false));
  }
  
  @Test
  public void testLink() throws IOException, ModelNotSupportedException {
    executeModel("/models/lotkavolterra.mlrj", new LinkSimulatorFactory(true, false));
  }
  
  @Test
  public void testStandard() throws IOException, ModelNotSupportedException {
    executeModel("/models/lotkavolterra.mlrj", new StandardSimulatorFactory(true, false));
  }

  @Test
  public void testTau() throws IOException, ModelNotSupportedException {
    executeModel("/models/lotkavolterra.mlrj", new TauLeapingSimulatorFactory(false));
  }

}
