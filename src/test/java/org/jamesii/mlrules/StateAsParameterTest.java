package org.jamesii.mlrules;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.exception.SemanticsException;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.util.MLEnvironment;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.fail;

public class StateAsParameterTest {

  private static String FILE = "/models/stateparameter/stateAsParameter.mlrj";

  private Model parse(String file, Map<String, Object> parameter) throws IOException {
    String path = new File(this.getClass().getResource(file).getFile()).getAbsolutePath();
    MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromFileName(path));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(Model.RNG,
            new MersenneTwister(System.currentTimeMillis()));
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
    PredefinedFunctions.addAll(env);

    MLRulesLexer typeLexer = new MLRulesLexer(CharStreams.fromFileName(path));
    CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
    MLRulesParser typeParser = new MLRulesParser(typeTokens);
    TypeCheckVisitor typeChecker = new TypeCheckVisitor();
    typeChecker.doTypeCheck(typeParser.model());

    StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, parameter);
    return visitor.create(parser.model());
  }


  @Test
  public void testWithoutParameter() {
    Logger.getGlobal().setLevel(Level.WARNING);
    try {
      Model model = parse(FILE, Collections.emptyMap());
      double amount = model.getSpecies().getSubLeavesStream().mapToDouble(LeafSpecies::getAmount).sum();
      assertEquals(200.0, amount);

    } catch (IOException e) {
      fail(e.toString());
    }
  }

  @Test
  public void testWithParameter() {
    Logger.getGlobal().setLevel(Level.WARNING);
    try {
      Map<String, Object> parameter = new HashMap<>();
      parameter.put("a","1000 A + 2000 B");
      Model model = parse(FILE, parameter);
      double amount = model.getSpecies().getSubLeavesStream().mapToDouble(LeafSpecies::getAmount).sum();
      assertEquals(3000.0, amount);

    } catch (IOException e) {
      fail(e.toString());
    }
  }

  @Test(expected=SemanticsException.class)
  public void testInvalidParameter() {
    Logger.getGlobal().setLevel(Level.WARNING);
    try {
      Map<String, Object> parameter = new HashMap<>();
      parameter.put("a","hello world");
      Model model = parse(FILE, parameter);
      double amount = model.getSpecies().getSubLeavesStream().mapToDouble(LeafSpecies::getAmount).sum();
      assertEquals(3000.0, amount);

    } catch (IOException e) {
      fail(e.toString());
    }
  }


}
