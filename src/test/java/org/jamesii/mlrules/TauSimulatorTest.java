package org.jamesii.mlrules;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.tauleaping.TauLeapingSimulator;
import org.jamesii.mlrules.util.MLEnvironment;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RunWith(Parameterized.class)
public class TauSimulatorTest {

  @BeforeClass
  public static void setLogLevel() {
    Logger.getGlobal().setLevel(Level.WARNING);
  }


  @Parameterized.Parameters
  public static List<Object[]> data() {
    return Arrays.asList(new Object[10][0]);
  }

  private void executeModel(String path) throws IOException {
    String path2 = new File(this.getClass().getResource(path).getFile()).getAbsolutePath();

    MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromFileName(path2));

    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(Model.RNG, new MersenneTwister(System.currentTimeMillis()));
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
    PredefinedFunctions.addAll(env);

    MLRulesLexer typeLexer = new MLRulesLexer(CharStreams.fromFileName(path2));
    CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
    MLRulesParser typeParser = new MLRulesParser(typeTokens);
    TypeCheckVisitor typeChecker = new TypeCheckVisitor();
    typeChecker.doTypeCheck(typeParser.model());

    StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, new HashMap<>());
    Model m = visitor.create(parser.model());

    Simulator simulator = new TauLeapingSimulator(m, 0.03, 2, 10, 1000);

    double time = 10;
    while (Double.compare(simulator.getCurrentTime(),time) <= 0) {
      simulator.nextStep();
    }
  }

  @Test
  public void testWNTModel() throws IOException {
    executeModel("/models/tau/wnt.mlrj");
  }

  @Test
  public void testWorking1Model() throws IOException {
    executeModel("/models/tau/working1.mlrj");
  }

  @Test
  public void testWorking2Model() throws IOException {
    executeModel("/models/tau/working2.mlrj");
  }

  @Test
  public void testWorkingAModel() throws IOException {
    executeModel("/models/tau/workingA.mlrj");
  }

  @Test
  public void testWorkingBModel() throws IOException {
    executeModel("/models/tau/workingB.mlrj");
  }

}
