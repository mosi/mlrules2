/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.postprocessing.RatePostProcessor;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.util.MLEnvironment;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RatePostProcessorFailingTest {

  @BeforeClass
  public static void setLogLevel() {
    Logger.getGlobal().setLevel(Level.WARNING);
  }

  @Test//(expected =IllegalArgumentException.class)
  public void testConstantReplacement() {
    Model model = createModel(
        "/models/functions/constantReplacementPostProcess.mlrj");
    RatePostProcessor postProcess = new RatePostProcessor();
    postProcess.postProcess(model);
  }

  private Model createModel(String modelPath) {
    Model result = null;
    try {
      String path2 = new File(this.getClass().getResource(modelPath).getFile()).getAbsolutePath();

      // type check
      MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromFileName(path2));
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      MLRulesParser parser = new MLRulesParser(tokens);

      MLEnvironment env = new MLEnvironment();
      env.setGlobalValue(Model.RNG, new MersenneTwister(1));
      env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
      PredefinedFunctions.addAll(env);

      MLRulesLexer typeLexer = new MLRulesLexer(CharStreams.fromFileName(path2));
      CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
      MLRulesParser typeParser = new MLRulesParser(typeTokens);
      TypeCheckVisitor typeChecker = new TypeCheckVisitor();
      typeChecker.doTypeCheck(typeParser.model());

      StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, new HashMap<>());
      result = visitor.create(parser.model());
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result;
  }

}
