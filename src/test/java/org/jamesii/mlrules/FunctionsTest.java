/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.types.Tuple;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.Nu;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class FunctionsTest {

  @BeforeClass
  public static void setLogLevel() {
    Logger.getGlobal().setLevel(Level.WARNING);
  }

  private MLEnvironment parse(String file) throws IOException {
    String path = new File(this.getClass().getResource(file).getFile()).getAbsolutePath();
    MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromFileName(path));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(Model.RNG,
            new MersenneTwister(System.currentTimeMillis()));
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
    PredefinedFunctions.addAll(env);

    MLRulesLexer typeLexer = new MLRulesLexer(CharStreams.fromFileName(path));
    CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
    MLRulesParser typeParser = new MLRulesParser(typeTokens);
    TypeCheckVisitor typeChecker = new TypeCheckVisitor();
    typeChecker.doTypeCheck(typeParser.model());

    StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, new HashMap<>());
    visitor.create(parser.model());

    return env;
  }

  @Test
  public void parseExpressionsWithoutSpecies() {
    Logger.getGlobal().setLevel(Level.WARNING);
    try {
      MLEnvironment env = parse("/models/functions/expressionsWithoutSpecies.mlrj");
      assertEquals(env.getValue("a1"), 3.0);
      assertEquals(env.getValue("a2"), 3.4);
      assertEquals(env.getValue("a3"), 1.5e+10 + 1.5E+10 + 5E+20);
      assertEquals(env.getValue("a4"), 6.4);
      assertEquals(env.getValue("a5"), "test");
      assertEquals(env.getValue("a6"), true);
      assertEquals(env.getValue("a7"), -3.0);
      assertEquals(env.getValue("a8"), 3 + 3 * (4 + 4) / 3 * 2 + 6.4);
      List<Double> values = Arrays.asList(1.0, 2D, 3D, 4D, 5., 6., 7., 8., 9., 10.);
      for (int i = 0; i < values.size(); ++i) {
        assertEquals(((Tuple) env.getValue("a9")).get(i), values.get(i));
      }
      assertEquals(((Tuple) env.getValue("a10")).size(), 0);
      assertEquals(env.getValue("a11"), 1.0);
      assertEquals(env.getValue("a15"), 1.0);
      assertEquals(env.getValue("a16"), 120.0);
      assertEquals(env.getValue("a17"), true);
      assertEquals(env.getValue("a18"), 5.0);
      assertEquals(env.getValue("a19"), 100.0);
      assertEquals(env.getValue("a20"), 1.0);
      assertEquals(env.getValue("a21"), 0.0);
      assertEquals(env.getValue("a22"), 5.0);
      assertEquals(env.getValue("a23"), 2.0);
    } catch (IOException e) {
      fail(e.toString());
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void parseExpressionsWithSpecies() {
    Logger.getGlobal().setLevel(Level.WARNING);
    try {
      MLEnvironment env = parse("/models/functions/expressionsWithSpecies.mlrj");
      assertEquals(env.getValue("a0"), 10.0);
      assertEquals(env.getValue("a5"), 3.0);
      assertEquals(env.getValue("a6"), "A1");
      assertEquals(env.getValue("a9"), 225.0);
      assertEquals(env.getValue("a18"), 10.0);
      Species s =
          ((Map<Species, Species>) env.getValue("a19")).keySet().iterator()
              .next();
      assertTrue(s.getAttribute(0) instanceof Nu);
      assertEquals(env.getValue("a20"), true);

    } catch (IOException e) {
      fail(e.toString());
    }
  }

}
