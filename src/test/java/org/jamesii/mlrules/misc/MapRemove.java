package org.jamesii.mlrules.misc;

import java.util.HashMap;
import java.util.Map;

/**
 * Values and Entry Sets are coupled with the map!
 */
public class MapRemove {

    public static void main(String[] args) {
        Map<String,Integer> m = new HashMap<>();
        m.put("A",1);
        m.put("B",2);
        m.put("C",1);

        //m.values().removeIf(x -> x == 1);

        System.out.println(m.values().stream().reduce(0, Integer::sum));

        System.out.println(m.toString());
    }

}
