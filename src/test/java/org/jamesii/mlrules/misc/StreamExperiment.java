/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.misc;

import java.util.stream.Stream;

public class StreamExperiment {

  public static void executeA(int num) {
    Stream.iterate(new int[] { 0, 1 }, x -> new int[] { x[1], x[0] + x[1] }).limit(num).map(a -> a[0]).skip(num - 1);
  }

  public static void executeB(int num) {
    Stream.iterate(new Pair(0, 1), x -> x.set(x.getB(), x.getA() + x.getB())).limit(num).map(a -> a.getA()).skip(num - 1);
  }

  public static void main(String[] args) {

    final int num = 100000;
    final int reps = 1000000;

    long start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeA(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeB(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeA(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeB(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeA(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeB(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeA(num);
    }
    System.out.println(System.currentTimeMillis() - start);
    
    start = System.currentTimeMillis();
    for (int i = 0; i < reps; ++i) {
      executeB(num);
    }
    System.out.println(System.currentTimeMillis() - start);

  }

}
