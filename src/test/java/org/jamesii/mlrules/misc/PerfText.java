/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.misc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PerfText {

  private static int functionFor(List<Integer> values) {
    int sum = 0;
    for (int v : values) {
      sum += v;
    }
    return sum;
  }
  
  private static int functionStreams(List<Integer> values) {
    return values.stream().mapToInt(Integer::intValue).sum();
  }
  
  public static void main(String[] args) throws IOException {
    List<Integer> values = new ArrayList<>();
    for (int i = 0; i < 10000000; ++i) {
      values.add(i);
    }
    long sumA = 0;
    long start = System.currentTimeMillis();
    for (int i = 0; i < 1000; ++i) {
      sumA += functionFor(values);
    }
    System.out.println(System.currentTimeMillis() - start);
    start = System.currentTimeMillis();
    long sumB = 0;
    for (int j = 0; j < 1000; ++j) {
      sumB += functionStreams(values);
    }
    System.out.println(System.currentTimeMillis() - start);
    System.out.println(sumA + " : " + sumB);
  }
  
}
