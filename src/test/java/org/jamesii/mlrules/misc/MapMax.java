package org.jamesii.mlrules.misc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tobi on 1/18/17.
 */
public class MapMax {


  public static void main(String[] args) {
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < 1000000; ++i) {
      list.add(i);
    }

    long start = System.currentTimeMillis();
    int min = Integer.MAX_VALUE;
    for (Integer i : list) {
      i = i + 1;
      if (min > i) {
        min = i;
      }
    }
    System.out.println(min);
    System.out.println(System.currentTimeMillis() - start);

    start = System.currentTimeMillis();
    System.out.println(list.stream().map(i -> i + 1).reduce(Integer.MAX_VALUE, Math::min));
    System.out.println(System.currentTimeMillis() - start);


    start = System.currentTimeMillis();
    min = Integer.MAX_VALUE;
    for (Integer i : list) {
      i = i + 1;
      if (min > i) {
        min = i;
      }
    }
    System.out.println(min);
    System.out.println(System.currentTimeMillis() - start);

    start = System.currentTimeMillis();
    System.out.println(list.stream().map(i -> i + 1).reduce(Integer.MAX_VALUE, Math::min));
    System.out.println(System.currentTimeMillis() - start);

  }

}
