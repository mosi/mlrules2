/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.misc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Random;
import java.util.TreeSet;

public class RandomTest {

  public static int NUM = 50000;
  
  public static void execute(int k) throws InterruptedException, IOException {
    TreeSet<Point> data = new TreeSet<Point>(new Comparator<Point>() {
      @Override
      public int compare(Point a, Point b) {
        int c = Double.compare(a.x, b.x);
        // confusing, but needed to save same values within the treeset
        return c == 0 ? c + 1 : c;
      }
    });
    Random ran = new Random();
    
    long start = System.currentTimeMillis();
    for (int i = 0; i < NUM; ++i){
      data.add(new Point(ran.nextDouble()));
    }
    FileWriter writer = new FileWriter("./" + k);
    BufferedWriter buffer = new BufferedWriter(writer);
    for (Point p : data) {
      buffer.write(p.x + "\n");
    }
    buffer.flush();
    buffer.close();
    writer.close();
    System.out.println(System.currentTimeMillis() - start);
  }
  
  public static void main(String[] args) throws InterruptedException, IOException {
    System.in.read();
    for (int i = 0; i < 20; ++i) {
      execute(i);
    }
  }
  
}
