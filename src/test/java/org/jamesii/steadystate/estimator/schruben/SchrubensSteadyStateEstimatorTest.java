package org.jamesii.steadystate.estimator.schruben;


import org.jamesii.core.math.random.distributions.NormalDistribution;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SchrubensSteadyStateEstimatorTest {

  @Test
  public void simpleLine() {
    SchrubensSteadyStateEstimator estimator = new SchrubensSteadyStateEstimator(8, 4, 0.05);
    List<Number> values = new ArrayList<>();
    for (int i = 0; i < 100; ++i) {
      values.add(100);
    }
    Assert.assertNotNull(estimator.estimateInitialBiasEnd(values));
  }

  @Test
  public void normalDistribution() {
    SchrubensSteadyStateEstimator estimator = new SchrubensSteadyStateEstimator(8, 4, 0.05);
    List<Number> values = new ArrayList<>();
    NormalDistribution norm = new NormalDistribution(new MersenneTwister(10), 1, 1);
    for (int i = 0; i < 1000; ++i) {
      values.add(norm.getRandomNumber());
    }
    Assert.assertNotNull(estimator.estimateInitialBiasEnd(values));
  }

  @Test
  public void tenNormalDistributions() {
    SchrubensSteadyStateEstimator estimator = new SchrubensSteadyStateEstimator(8, 4, 0.05);
    MersenneTwister rng = new MersenneTwister(1);
    List<Number> values = new ArrayList<>();
    for (int i = 0; i < 10; ++i) {
      NormalDistribution norm = new NormalDistribution(new MersenneTwister(10), rng.nextInt(1000), rng.nextInt(10));
      for (int j = 0; j < 100; ++j) {
        values.add(norm.getRandomNumber());
      }
    }
    Assert.assertTrue(estimator.estimateInitialBiasEnd(values) > 900);
  }

  @Test
  public void noise() {
    SchrubensSteadyStateEstimator estimator = new SchrubensSteadyStateEstimator(8, 4, 0.05);
    List<Number> values = new ArrayList<>();
    for (int i = 0; i < 100; ++i) {
      values.add(i);
    }
    Assert.assertNull(estimator.estimateInitialBiasEnd(values));
  }

}