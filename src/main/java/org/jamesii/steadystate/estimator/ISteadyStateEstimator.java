/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.steadystate.estimator;

/**
 * Interface for classes estimating steady-state statistics in time series.
 * Input is the time-series, outpur is the estimated statistic (e.g., mean).
 * 
 * @author Stefan Leye
 */
public interface ISteadyStateEstimator<S extends SteadyStateEstimatorState>
    {

}
