/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.steadystate.estimator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * State that holds all required information.
 *
 * @author Stefan Leye
 *
 */
public class SteadyStateEstimatorState {

  /**
   * The serialization id.
   */
  private static final long serialVersionUID = -6342381693850583055L;
  
  /**
   * The statistic, the estimator has calculated, e.g., the steady state mean.
   */
  Serializable statistic = null;
  
  /**
   * The point until which the time has been analyzed so far.
   */
  private int currentPosition = 0;
  
  /**
   * The problem time series.
   * It is extended in each iteration of problem solving.
   */
  List<Number> timeSeries = new ArrayList<>();

  /**
   * Set the statistic, the estimator has calculated.
   * 
   * @param result, the estimator has calculated.
   */
  public void setResult(Serializable result) {
    this.statistic = result;
  }
  
  /**
   * Get the point in the time series which has been analyzed so far.
   * 
   * @return the point in the time series
   */
  public int getCurrentPosition() {
    return currentPosition;
  }

  /**
   * Set the point in the time series which has been analyzed so far.
   * 
   * @param currentPosition the point in the time series
   */
  public void setCurrentPosition(int currentPosition) {
    this.currentPosition = currentPosition;
  }
  
  /**
   * Get the problem time series.
   * 
   * @return the problem time series
   */
  public List<Number> getTimeSeries() {
    return new ArrayList<>(timeSeries);
  }

  /**
   * Adds additional data to the time series.
   * 
   * @param data the data to be added
   */
  protected void addTimeSeriesData(List<? extends Number> data) {
    this.timeSeries.addAll(data);
  }

  public Serializable getCurrentResult() {
    return statistic;
  }

  public Boolean getNextRequest() {
    return statistic == null;
  }
  
}
