/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.steadystate.estimator.schruben;


import org.jamesii.steadystate.estimator.batchmean.BatchMeanSteadyStateEstimator;

import java.util.ArrayList;
import java.util.List;

/**
 * Estimates the steady state according to Schruben's method.
 * 
 * L. Schruben: Confidence Interval Estimation Using Standardized Time
 *      Series. Operations Research, 31(6), 1983 p. 1090 - 1108
 * 
 * 
 *      LICENCE: JAMESLIC
 * @author Stefan Leye
 * 
 */
public class SchrubensSteadyStateEstimator extends
        BatchMeanSteadyStateEstimator {

  /**
   * The size of the batches.
   */
  private Integer batchSize = 0;

  private List<Double> cumulativeSumMaxima = new ArrayList<>();

  private List<Double> cumulativeSumMinima = new ArrayList<>();

  private List<Integer> cumulativeSumMaxIndices = new ArrayList<>();

  private List<Integer> cumulativeSumMinIndices = new ArrayList<>();

  /**
   * Default constructor.
   * 
   * @param batchCount
   * @param groupBatchCount
   * @param allowedSignificance
   */
  public SchrubensSteadyStateEstimator(int batchCount, int groupBatchCount,
                                       Double allowedSignificance) {
    super(batchCount, groupBatchCount, allowedSignificance, 0.0);
  }

  @Override
  public Integer estimateInitialBiasEnd(List<Number> input) {
    Boolean result = false;
    List<Number> sampleSeries = new ArrayList<>();
    sampleSeries.addAll(getState().getTimeSeries());
    sampleSeries.addAll(input);
    for (int currentPos = 0; currentPos < sampleSeries.size(); currentPos +=
        this.batchSize) {
      this.batchSize =
          (sampleSeries.size() - currentPos) / getState().getBatchCount();
      calcCumulativeSumExtrema(sampleSeries, currentPos);
      result = getTestResult();
      if (result == null) {
        return null;
      } else if (result) {
        return currentPos + getState().getCurrentPosition() + 1;
      }
    }
    return null;
  }

  /**
   * Calculates the extrema of the cummulative sum of a trajectory, from a given
   * position.
   * 
   * @param sampleSeries
   *          the trajectory
   * @param currentPos
   *          the given position
   */
  private void calcCumulativeSumExtrema(List<Number> sampleSeries,
      int currentPos) {
    this.cumulativeSumMaxima = new ArrayList<>();
    this.cumulativeSumMinima = new ArrayList<>();
    this.cumulativeSumMaxIndices = new ArrayList<>();
    this.cumulativeSumMinIndices = new ArrayList<>();
    for (int j = 0; j < getState().getBatchCount(); j++) {
      double batchMean = 0.0;
      final List<Double> cummulativeBatchMeans = new ArrayList<>();
      for (int p = 0; p < this.batchSize; p++) {
        batchMean +=
            sampleSeries.get(currentPos + (j * this.batchSize) + p)
                .doubleValue() / this.batchSize;
        cummulativeBatchMeans.add(batchMean);
      }
      Double max = Double.NEGATIVE_INFINITY;
      Double min = Double.POSITIVE_INFINITY;
      int maxIndex = -1;
      int minIndex = -1;
      for (int p = 0; p < this.batchSize; p++) {
        final Double sjp = batchMean - cummulativeBatchMeans.get(p);
        if (max.compareTo(sjp) < 0) {
          max = sjp;
          maxIndex = p + 1;
        }
        if (min.compareTo(sjp) > 0) {
          min = sjp;
          minIndex = p + 1;
        }
      }
      this.cumulativeSumMaxima.add(maxIndex * max);
      this.cumulativeSumMaxIndices.add(maxIndex);
      this.cumulativeSumMinima.add(minIndex * min);
      this.cumulativeSumMinIndices.add(minIndex);
    }
  }

  /**
   * Performs f-tests on the cummulative maxima and minima.
   * 
   * @return true if test is successful
   */
  protected Boolean getTestResult() {
    Double varMax1 = 0.0;
    Double varMin1 = 0.0;
    int groupBatchCount = getState().getGroupBatchCount();
    int batchCount = getState().getBatchCount();
    for (int i = 0; i < groupBatchCount; i++) {
      final double max = this.cumulativeSumMaxima.get(i);
      final int maxIndex = this.cumulativeSumMaxIndices.get(i);
      final double min = this.cumulativeSumMinima.get(i);
      final int minIndex = this.cumulativeSumMinIndices.get(i);
      varMax1 +=
          (this.batchSize * max * max)
              / (maxIndex * (this.batchSize - maxIndex));
      varMin1 +=
          (this.batchSize * min * min)
              / (minIndex * (this.batchSize - minIndex));
    }
    varMax1 /= 3 * groupBatchCount;
    varMin1 /= 3 * groupBatchCount;
    Double varMax2 = 0.0;
    Double varMin2 = 0.0;
    for (int i = groupBatchCount; i < batchCount; i++) {
      final double max = this.cumulativeSumMaxima.get(i);
      final int maxIndex = this.cumulativeSumMaxIndices.get(i);
      final double min = this.cumulativeSumMinima.get(i);
      final int minIndex = this.cumulativeSumMinIndices.get(i);
      varMax2 +=
          (this.batchSize * max * max)
              / (maxIndex * (this.batchSize - maxIndex));
      varMin2 +=
          (this.batchSize * min * min)
              / (minIndex * (this.batchSize - minIndex));
    }
    varMax2 /= 3 * (batchCount - groupBatchCount);
    varMin2 /= 3 * (batchCount - groupBatchCount);
    final Double minRatio = varMin1 / varMin2;
    final Double maxRatio = varMax1 / varMax2;
    final double def1 = 3 * groupBatchCount;
    final double def2 = 3 * (batchCount - groupBatchCount);
    return checkResult(minRatio, maxRatio, def1, def2);
  }

}
