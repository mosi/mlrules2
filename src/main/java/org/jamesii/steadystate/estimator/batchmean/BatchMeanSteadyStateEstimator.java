/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.steadystate.estimator.batchmean;


import org.jamesii.core.math.statistics.BetaFunction;
import org.jamesii.core.util.misc.Quadruple;
import org.jamesii.steadystate.estimator.SteadyStateEstimator;

import java.util.ArrayList;
import java.util.List;

/**
 * Estimates steady state by performing f-test on two groups of batch means.
 * Cash, C. R.; Dippold, D. G.; Long, J. M. & Nelson, B. L. Evaluation of tests for initial-condition bias, Proceddings of the Winter Simulation Conference, 1992
 *
 * LICENCE: JAMESLIC
 * @author Stefan Leye
 *
 */
public class BatchMeanSteadyStateEstimator extends
        SteadyStateEstimator<BatchMeanSteadyStateEstimatorState> {

  /**
   * The serialization id.
   */
  private static final long serialVersionUID = -5539517478936568791L;

  /**
   * The absolute size of the batches.
   */
  Integer batchSize = 0;

  /**
   * Default constructor.
   * 
   * @param batchCount
   *          the overall count of batches
   * @param lowerBatchCount
   *          the count of batches in the lower group
   * @param allowedSignificance
   *          the allowed significance of the f-test
   * @param minimumbatchSize
   *          the minimum relative size of batches, if undercut, additional data is required.
   */
  public BatchMeanSteadyStateEstimator(int batchCount, int lowerBatchCount,
                                       Double allowedSignificance, Double minimumbatchSize) {
    super(new BatchMeanSteadyStateEstimatorState(batchCount, lowerBatchCount, allowedSignificance, minimumbatchSize));
  }
  
  public BatchMeanSteadyStateEstimatorState init() {
    int batchCount = getState().getBatchCount();
    int groupBatchCount = getState().getGroupBatchCount();
    double allowedSignificance = getState().getAllowedSignificance();
    double minimumBatchSize = getState().getMinimumbatchSize();
    return new BatchMeanSteadyStateEstimatorState(batchCount, groupBatchCount, 
        allowedSignificance, minimumBatchSize);
  }
  
  @Override
  public Integer estimateInitialBiasEnd(List<Number> input) {
    int currentPos = 0;
    final int size = input.size();
    Boolean finished = false;
    for (currentPos = 0; currentPos < input.size(); currentPos += this.batchSize) {
      this.batchSize = (size - currentPos) / getState().getBatchCount();
      
      if ((size - currentPos) < (getState().getMinimumbatchSize() * size)) {
        return null;
      }
      Quadruple<Double, List<Double>, Double, List<Double>> sums = calculateSums(input, currentPos);
      Boolean result = getTestResult(sums.getA(), sums.getB(), sums.getC(), sums.getD());
      if (result == null) {
        return null;
      } else if (result) {
        finished = true;
        break;
      }
    }
    currentPos = getState().getTimeSeries().size() + currentPos;
    getState().setCurrentPosition(currentPos);
    if (finished) {
      return currentPos;
    }
    return null;
  }

  /**
   * Calculates the means of the different batches and their sums.
   * 
   * @param sampleSeries the input series
   * @param currentPos the current starting position in the input series
   * @return quadruple containing 
   *               the sum of batch means in the lower group
   *               batch means in the lower group
   *               the sum of batch means in the upper group
   *               batch means in the upper group
   */
  protected Quadruple<Double, List<Double>, Double, List<Double>> calculateSums(List<Number> sampleSeries, int currentPos) {
    Double lowerSum = 0.0;
    List<Double> lowerMeans = new ArrayList<>();
    int groupBatchCount = getState().getGroupBatchCount();
    for (int j = 0; j < groupBatchCount-1; j++) {
      double batchMean = 0.0;
      for (int p = 0; p < this.batchSize; p++) {
        double doubleBatchSize = (double) this.batchSize;
        int index = currentPos + (j * this.batchSize) + p;
        batchMean += sampleSeries.get(index)
                .doubleValue() / doubleBatchSize;
      }
      lowerMeans.add(batchMean);
      lowerSum += batchMean;
    }
    Double upperSum = 0.0;
    List<Double> upperMeans = new ArrayList<>();
    for (int j = groupBatchCount-1; j < getState().getBatchCount()-1; j++) {
      double batchMean = 0.0;
      for (int p = 0; p < this.batchSize; p++) {
        double doubleBatchSize = (double) this.batchSize;
        int index = currentPos + (j * this.batchSize) + p;
        batchMean += sampleSeries.get(index)
                .doubleValue() / doubleBatchSize;
      }
      upperMeans.add(batchMean);
      upperSum += batchMean;
    }
    return new Quadruple<>(lowerSum, 
        lowerMeans, upperSum, upperMeans);
  }

  /**
   * Executes the test on the batch means.
   * 
   * @param lowerSum 
   *            the sum of the batch means of the lower group of batches
   * @param lowerMeans 
   *            the batch means of the lower group of batches
   * @param upperSum 
   *            the sum of the batch means of the upper group of batches
   * @param upperMeans
   *            the batch means of the upper group of batches
   * @return
   */
  protected Boolean getTestResult(Double lowerSum, List<Double> lowerMeans, 
      Double upperSum, List<Double> upperMeans) {
    Double varLower = 0.0;
    int groupBatchCount = getState().getGroupBatchCount();
    for (int i = 0; i < lowerMeans.size(); i++) {
      Double square = lowerMeans.get(i);
      square -= (1 / groupBatchCount) * lowerSum;
      square *= square;
      varLower += square;
    }
    varLower *= this.batchSize / (groupBatchCount - 1);
    Double varUpper = 0.0;
    final int count = getState().getBatchCount() - groupBatchCount;
    for (int i = 0; i < upperMeans.size(); i++) {
      Double square = upperMeans.get(i);
      square -= (1 / count) * upperSum;
      square *= square;
      varUpper += square;
    }
    varUpper *= this.batchSize / (count - 1);
    final double def1 = groupBatchCount - 1;
    final double def2 = count - 1;
    final Double ratio = varLower / varUpper;
    if (varLower.compareTo(varUpper) == 0) {
      return true;
    }
    return checkResult(Double.NaN, ratio, def1, def2);
  }
  
  /**
   * Performs the f test depending on the ratio between the samples and the degree of freedom.
   * 
   * @param minRatio
   *         ratio between upper and lower group
   * @param maxRatio
   *         ratio between lower and upper group
   * @param def1
   *         degree of freedom of the first group
   * @param def2
   *         degree of freedom of the second group
   * @return true if test is successful
   */
  protected Boolean checkResult(Double minRatio, Double maxRatio, double def1,
      double def2) {
    Double allowedSignificance = getState().getAllowedSignificance();
    if (maxRatio.isNaN() && minRatio.isNaN()) {
      return null;
    } else if (maxRatio.isNaN()) {
      if (allowedSignificance.compareTo(fTest(minRatio, def1, def2)) < 0.0) {
        return false;
      }
      return true;
    } else if (minRatio.isNaN()) {
      if (allowedSignificance.compareTo(fTest(maxRatio, def1, def2)) < 0.0) {
        return false;
      }
      return true;
    } else if ((allowedSignificance.compareTo(fTest(minRatio, def1, def2)) < 0.0)
        || (allowedSignificance.compareTo(fTest(maxRatio, def1, def2)) < 0.0)) {
      return false;
    }
    return true;
  }
  
  /**
   * Calculates the f-test.
   * 
   * @param ratio the ratio between the two samples
   * @param def1 the degree of freedom of the first sample
   * @param def2 the degree of freedom of the second sample
   * @return the f statistic
   */
  private static double fTest(double ratio, double def1, double def2) {
    final Double result =
        2 * (BetaFunction.iBeta(0.5 * def2, 0.5 * def1, def2
            / (def2 + (def1 * (ratio)))));
    return Math.abs(1 - result);
  }

}
