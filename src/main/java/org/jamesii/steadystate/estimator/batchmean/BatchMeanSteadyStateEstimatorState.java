/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.steadystate.estimator.batchmean;


import org.jamesii.steadystate.estimator.SteadyStateEstimatorState;

/**
 * Internal state of {@link BatchMeanSteadyStateEstimator}.
 *
 * LICENCE: JAMESLIC
 * @author Stefan Leye
 *
 */
public class BatchMeanSteadyStateEstimatorState extends SteadyStateEstimatorState {

  /**
   * The serialization id.
   */
  private static final long serialVersionUID = -1187642356999312266L;
  
  /**
   * The overall count of batches.
   */
  int batchCount;

  /**
   * The count of batches in the lower group.
   */
  int groupBatchCount;

  /**
   * The allowed significance of the f-test.
   */
  Double allowedSignificance;
  
  /**
   * The minimum relative size of batches, if undercut, additional data is required.
   */
  Double minimumbatchSize;
  
  /**
   * Default constructor.
   * 
   * @param batchCount
   *          the overall count of batches
   * @param groupBatchCount
   *          the count of batches in the lower group
   * @param allowedSignificance
   *          the allowed significance of the f-test
   * @param minimumbatchSize
   *          the minimum relative size of batches, if undercut, additional data is required.
   */
  public BatchMeanSteadyStateEstimatorState(int batchCount, int groupBatchCount,
                                            Double allowedSignificance, Double minimumbatchSize) {
    this.batchCount = batchCount;
    this.groupBatchCount = groupBatchCount;
    this.allowedSignificance = allowedSignificance;
    this.minimumbatchSize = minimumbatchSize;
  }

  /**
   * Get the overall count of batches.
   * 
   * @return the overall count of batches
   */
  public int getBatchCount() {
    return batchCount;
  }

  /**
   * Get the count of batches in the lower group.
   * 
   * @return the count of batches in the lower group
   */
  public int getGroupBatchCount() {
    return groupBatchCount;
  }

  /**
   * Get the allowed significance of the f-test.
   * 
   * @return the allowed significance of the f-test
   */
  public Double getAllowedSignificance() {
    return allowedSignificance;
  }

  /**
   * Get the minimum relative size of batches, if undercut, additional data is required.
   * 
   * @return the minimum relative size of batches, if undercut, additional data is required
   */
  public Double getMinimumbatchSize() {
    return minimumbatchSize;
  }

}