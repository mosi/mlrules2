/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.steadystate.estimator;


import java.util.List;

/**
 * Base class for steady state estimators.
 *
 * LICENCE: JAMESLIC
 * @author Stefan Leye
 */
public abstract class SteadyStateEstimator<S extends SteadyStateEstimatorState>
                  implements ISteadyStateEstimator<S> {

  /**
   * The state of this estimator.
   */
  private S state;

  /**
   * Default constructor.
   * 
   * @param state the state of this estimator
   */
  public SteadyStateEstimator(S state) {
    this.state = state;
  }
  
  /**
   * Estimates the end of the initial bias, i.e., the point from which on the steady state statistic shall be calculated.
   * Returns null if the end could not be found, i.e., additional data is required.
   * 
   * @return the index of the estimate end of the initial bias.
   */
  public abstract Integer estimateInitialBiasEnd(List<Number> input);

  /**
   * Utility method. Calculates the relative deviation between two values.
   * 
   * @param value1 first value
   * @param value2 second value
   * @return relative deviation
   */
  public static double calculateRelativeDeviation(double value1, double value2) {
    Double max = Math.max(Math.abs(value1), Math.abs(value2));
    Double dist = Math.abs(value1 - value2);
    if (dist.compareTo(0.0) == 0) {
      return dist;
    } 
    return dist / max;
  }
  
  public S getState() {
    return state;
  }
}
