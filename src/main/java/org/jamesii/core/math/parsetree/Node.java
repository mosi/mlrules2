/*
 * The general modelling and simulation framework JAMES II.
 * Copyright by the University of Rostock.
 * 
 * LICENCE: JAMESLIC
 */
package org.jamesii.core.math.parsetree;

import org.jamesii.core.math.parsetree.variables.IEnvironment;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Node. A node in a parse tree can be anything on which operations
 * might be executed.
 *
 * A node may have children or not, but a node can always be calculated (by
 * calculating its sub tree).
 *
 * @author Jan Himmelspach
 */
public abstract class Node implements INode, Cloneable {

  /**
   * The Constant serialVersionUID.
   */
  private static final long serialVersionUID = 2561004396538702179L;

 @Override
  public abstract <N extends INode> N calc(IEnvironment<?> cEnv);

  /**
   * Create an empty node.
   */
  public Node() {
    super();
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  /**
   * Get the children of the node, if any.
   *
   * @return the children
   */
  @Override
  public List<? extends INode> getChildren() {
    return new ArrayList<>();
  }

  /**
   * Gets the name. Human readable name of the node. Can be, e.g., ADD, or +, or
   * ... Has to be unique overall existing node classes (or empty).
   *
   * @return the name
   */
  @Override
  public String getName() {
    return "";
  }

  @Override
  public String toString() {
    return super.toString();
  }

}
