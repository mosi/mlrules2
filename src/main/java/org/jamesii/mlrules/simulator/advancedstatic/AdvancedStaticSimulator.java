/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.advancedstatic;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.parser.nodes.MLRulesAddNode;
import org.jamesii.mlrules.parser.nodes.SpeciesPatternNode;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.simple.SimpleReaction;
import org.jamesii.mlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.mlrules.simulator.simple.SimpleRule;
import org.jamesii.mlrules.simulator.simple.StaticRule;
import org.jamesii.mlrules.simulator.standard.*;
import org.jamesii.mlrules.simulator.tauleaping.NoReactionSelectedException;
import org.jamesii.mlrules.simulator.tauleaping.TauReaction;
import org.jamesii.mlrules.util.DoubleNumber;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.RestSolution;
import org.jamesii.mlrules.util.RestSolutionStatic;

import java.util.*;
import java.util.stream.Collectors;


/**
 * This simulator divides the rules into static and dynamic rules and basically does not update
 * the
 *
 * @author Tobias Helms
 */
public class AdvancedStaticSimulator extends Simulator {

  private final ExponentialDistribution expDist;

  private final ReactionCreator complexCreator = new ReactionCreator() {
    @Override
    protected boolean validAmount(Species species, Tree matching, Reactant reactant, MLEnvironment env) {
      return true;
    }

    @Override
    protected Reaction createReaction(ContextMatchings cm, Rule rule) {
      return new TauReaction(cm, rule);
    }

    @Override
    protected RestSolution createRestSolution(String name, Compartment context, Map<Species, Integer> removals) {
      return new RestSolutionStatic(name, context, removals);
    }

  };

  private final SimpleReactionCreator simpleCreator = new SimpleReactionCreator();

  private final Map<Compartment, List<Reaction>> complexReactions = new HashMap<>();

  private final Map<Compartment, List<SimpleReaction>> simpleReactions = new HashMap<>();

  private List<Rule> complexRules = new ArrayList<>();

  private List<SimpleRule> simpleRules = new ArrayList<>();

  private Map<Reactant, Reactant> reactantProducts = new HashMap<>();

  private final List<TimedRule> timedRules;

  private static void checkReactant(Reactant reactant) {
    if (!(reactant.getAmount() instanceof ValueNode<?>)) {
      throw new IllegalArgumentException("amount expressions are not allowed within reactants and products");
    }
    reactant.getSubReactants()
            .forEach(AdvancedStaticSimulator::checkReactant);
  }

  private void createReactants(MLRulesAddNode node, Set<String> variables, List<Reactant> reactants) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getLeft()).toReactant(getModel().getEnv());
      checkReactant(reactant);
      reactants.add(reactant);
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getRight()).toReactant(getModel().getEnv());
      checkReactant(reactant);
      reactants.add(reactant);
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getRight(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
  }

  private boolean emptySpecies(Node node) {
    if (node instanceof ValueNode<?>) {
      ValueNode<?> vn = (ValueNode<?>) node;
      if (vn.getValue() instanceof Map<?, ?>) {
        Map<?, ?> map = (Map<?, ?>) vn.getValue();
        return map.isEmpty();
      }
    }
    return false;
  }

  private List<Reactant> checkProduct(Node node, Set<String> variables) {
    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      Reactant product = spn.toReactant(getModel().getEnv());
      checkReactant(product);
      products.add(product);
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(addNode, variables, products);
    } else if (emptySpecies(node)) {
      // do nothing;
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (products.stream()
                .anyMatch(p -> p.getBoundTo()
                                .isPresent())) {
      throw new IllegalArgumentException("product species cannot be bound to variables");
    }

    return products;
  }

  private Optional<SimpleRule> getSimple(Rule rule, Map<Reactant, Reactant> reactantProducts) {
    try {
      Set<String> variables = new HashSet<>();
      for (Reactant reactant : rule.getReactants()) {
        checkReactant(reactant);
      }
      List<Reactant> products = checkProduct(rule.getProduct(), variables);

      if (rule.getAssignments()
              .stream()
              .anyMatch(a -> a.getNames()
                              .stream()
                              .anyMatch(n -> variables.contains(n)))) {
        throw new IllegalArgumentException(
                "Variables defined within the where part of a rule are not allowed to be used within the reactants or products.");
      }

      if (!StaticRule.isValidTauRule(rule.getReactants(), products, reactantProducts)) {
        throw new IllegalArgumentException("rules must not change the structure of the model");
      }

      return Optional.of(new SimpleRule(rule.getReactants(), products, rule.getRate(), rule.getOriginalRate(), rule.getAssignments()));
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  public void divideRules(List<Rule> rules) {
    for (Rule rule : rules) {
      Optional<SimpleRule> sr = getSimple(rule, reactantProducts);
      if (sr.isPresent()) {
        simpleRules.add(sr.get());
      } else {
        complexRules.add(rule);
      }
    }
  }

  public AdvancedStaticSimulator(Model model) {
    super(model);

    divideRules(model.getRules()
                     .getRules());

    timedRules = new ArrayList<>(model.getRules()
                                      .getTimedRules());
    timedRules.sort(RuleComparator.instance);

    simpleReactions.clear();
    simpleCreator.createInitialReactions(model.getSpecies(), simpleRules, simpleReactions, Collections.emptyList(),
            model.getEnv(), reactantProducts);

    expDist = new ExponentialDistribution((IRandom) model.getEnv()
                                                         .getValue(Model.RNG));

    complexCreator.createInitialReactions(model.getSpecies(), complexRules, complexReactions, Collections.emptyList(),
            model.getEnv(), true);
  }

  private Optional<Reaction> select(Map<Compartment, List<Reaction>> reactions, double pivot) {
    DoubleNumber sum = new DoubleNumber(0D);
    return reactions.values()
                    .stream()
                    .flatMap(Collection::stream)
                    .filter(r -> sum.addAndGet(r.getRate()) > pivot)
                    .findFirst();
  }

  private Optional<SimpleReaction> select(double pivot) {
    DoubleNumber sum = new DoubleNumber(0D);
    return simpleReactions.values()
                          .stream()
                          .flatMap(Collection::stream)
                          .filter(r -> sum.addAndGet(r.getCalculatedPropensity()) > pivot)
                          .findFirst();

  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
                                 Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getContextMatchings()
            .getMatchings()
            .stream()
            .anyMatch(m -> removedSpecies.contains(m.getSpecies())
                    || changedSpecies.getOrDefault(m.getSpecies()
                                                    .getType(), Collections.emptySet())
                                     .contains(m.getSpecies()));
  }

  private boolean removeSimpleReaction(SimpleReaction r, List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getUsedCompartments()
            .stream()
            .anyMatch(removedSpecies::contains) || r.getContext()
                                                    .getContextStream()
                                                    .anyMatch(c -> changedSpecies.getOrDefault(c.getType(), Collections.emptySet())
                                                                                 .contains(c));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    for (Species s : changed.getRemovedSpecies()) {
      if (s instanceof Compartment) {
        Compartment c = (Compartment) s;
        c.getSubCompartmentsRecursiveStream()
         .forEach(sub -> {
           complexReactions.remove(sub);
           simpleReactions.remove(sub);
         });
      }
    }
  }

  private void removeReactions(List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    complexReactions.getOrDefault(s, Collections.emptyList())
                    .removeIf(r -> removeReaction(r, removedSpecies, changedSpecies));

    Iterator<SimpleReaction> i = simpleReactions.getOrDefault(s, Collections.emptyList())
                                                .iterator();
    while (i.hasNext()) {
      SimpleReaction r = i.next();
      if (removeSimpleReaction(r, removedSpecies, changedSpecies)) {
        i.remove();
      }
    }
  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    Set<Species> current = new HashSet<>();
    for (Compartment c : selected.getContextMatchings()
                                 .getContext()
                                 .getContextStream()
                                 .collect(Collectors.toList())) {
      Map<SpeciesType, Set<Species>> changedSpecies;

      if (c == selected.getContextMatchings()
                       .getContext()) {
        changedSpecies = changed.getAllChangedSpecies();
      } else {
        changedSpecies = new HashMap<>();
        changedSpecies.put(current.iterator()
                                  .next()
                                  .getType(), current);
      }
      removeReactions(changed.getRemovedSpecies(), changedSpecies, c);
      complexCreator.createReactions(c, changedSpecies, complexRules, complexReactions, Collections.emptyList(),
              getModel().getEnv(), true);
      List<SimpleReaction> sr = simpleCreator.createReactions(c, changedSpecies, simpleRules, simpleReactions, Collections.emptyList(), getModel().getEnv(),
              reactantProducts);
      current = new HashSet<>();
      current.add(c);
    }
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies()
           .stream()
           .filter(s -> s instanceof Compartment)
           .map(s -> (Compartment) s)
           .forEach(c -> c.getSubCompartmentsRecursiveStream()
                          .forEach(sub -> {
                            complexCreator.createReactions(sub, null, complexRules, complexReactions, Collections.emptyList(),
                                    getModel().getEnv(), true);
                            simpleCreator.createReactions(sub, null, simpleRules, simpleReactions, Collections.emptyList(),
                                    getModel().getEnv(), reactantProducts);
                          }));
  }

  private void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
  }

  private void executeReaction(Reaction r) {
    ChangedSpecies changed = r.execute();
    getModel().getEnv()
              .setGlobalValue(Model.TIME, getNextTime());
    updateReactions(changed, r);
  }

  private void updateSimpleReaction(SimpleReaction sr) {
    sr.update();
    //sr.updateMaxFiringNumber();
  }

  private void executeReaction(SimpleReaction sr) {
    Set<Compartment> changedContexts = sr.execute();
    getModel().getEnv()
              .setGlobalValue(Model.TIME, getNextTime());
    for (Compartment changedContext : changedContexts) {
      for (SimpleReaction simpleReaction : simpleReactions.getOrDefault(changedContext, Collections.emptyList())) {
        /*if (simpleReaction.getReactantVector()
                          .keySet()
                          .stream()
                          .anyMatch(r -> sr.getReactantVector()
                                           .keySet()
                                           .contains(r) || sr.getProductVector()
                                                             .keySet()
                                                             .contains(r))) {*/
          simpleReaction.update();
        /*}*/
      }
      for (Reaction reaction : complexReactions.getOrDefault(changedContext, Collections.emptyList())) {
        if (reaction.getContextMatchings()
                    .getMatchings()
                    .stream()
                    .anyMatch(m -> isInfluenced(sr, m))) {
          reaction.update();
        }
      }
    }
  }

  private boolean isInfluenced(SimpleReaction sr, Matching m) {
    if (m.getSpecies() instanceof Compartment) {
      return m.getSubReactants()
              .stream()
              .anyMatch(sm -> isInfluenced(sr, sm));
    }
    return sr.getReactantVector()
             .containsKey(m.getSpecies()) || sr.getProductVector()
                                               .containsKey(m.getSpecies());
  }

  private void executeIndividualReaction(double simplePropSum, Map<Compartment, List<Reaction>> cr, double complexPropSum) {
    double pivot = ((IRandom) getModel().getEnv()
                                        .getValue(Model.RNG)).nextDouble() * (simplePropSum + complexPropSum);

    if (pivot > simplePropSum) {
      Optional<Reaction> selected = select(cr, pivot - simplePropSum);
      if (selected.isPresent()) {
        executeReaction(selected.get());
      } else if (timedRules.isEmpty()) {
        throw new NoReactionSelectedException();
      }
    } else {
      Optional<SimpleReaction> selected = select(pivot);
      if (selected.isPresent()) {
        executeReaction(selected.get());
      } else if (timedRules.isEmpty()) {
        throw new NoReactionSelectedException();
      }
    }

  }

  private Optional<Reaction> selectTimedReaction() {
    Map<Compartment, List<Reaction>> timedReactions = new HashMap<>();
    double time = timedRules.get(0)
                            .getNextTime();
    List<Rule> current = new ArrayList<>();

    for (TimedRule r : timedRules) {
      if (r.getNextTime() > time) {
        break;
      }
      current.add(r.getRule());
      r.updateNextTime();
    }
    timedRules.sort(RuleComparator.instance);

    complexCreator.createInitialReactions(getModel().getSpecies(), current, timedReactions, new ArrayList<>(),
            getModel().getEnv(), false);
    return getRandomElement(timedReactions);
  }

  /**
   * Return a random reaction of the given reactions without creating an
   * additional data structure containing all reactions (i.e. not flatmapping
   * them first).
   */
  private Optional<Reaction> getRandomElement(Map<Compartment, List<Reaction>> reactions) {
    if (reactions.isEmpty()) {
      return Optional.empty();
    }
    IRandom rng = (IRandom) getModel().getEnv()
                                      .getValue(Model.RNG);
    int i = rng.nextInt(reactions.size());
    int j = 0;
    for (List<Reaction> r : reactions.values()) {
      if (i == j) {
        return Optional.of(r.get(rng.nextInt(r.size())));
      }
      ++j;
    }
    return Optional.empty();
  }

  private double nextTimedReaction() {
    if (!timedRules.isEmpty()) {
      return timedRules.get(0)
                       .getNextTime();
    }
    return Double.POSITIVE_INFINITY;
  }

  private Optional<Reaction> checkTimedReactions() {
    double time = nextTimedReaction();
    if (!timedRules.isEmpty() && Double.compare(time, getNextTime()) <= 0) {
      Optional<Reaction> timedReaction = selectTimedReaction();
      if (timedReaction.isPresent()) {
        setNextTime(time);
        return timedReaction;
      }
    }
    return Optional.empty();
  }

  private double calcPropSumComplex() {
    return complexReactions.values()
                           .stream()
                           .flatMap(Collection::stream)
                           .map(Reaction::getRate)
                           .reduce(0D, Double::sum);
  }

  private double calcPropSumSimple() {
    return simpleReactions.values()
                          .stream()
                          .flatMap(Collection::stream)
                          .map(SimpleReaction::getCalculatedPropensity)
                          .reduce(0D, Double::sum);
  }

  private void executeSSAStep() {
    double propSum = calcPropSumSimple();
    double propSumComplex = calcPropSumComplex();
    double timeAdvance = expDist.getRandomNumber(1.0 / (propSum + propSumComplex));

    setNextTime(getCurrentTime() + timeAdvance);
    Optional<Reaction> timed = checkTimedReactions();
    getObserver().forEach(o -> o.update(this));
    if (timed.isPresent()) {
      executeReaction(timed.get());
    } else {
      try {
        executeIndividualReaction(propSum, complexReactions, propSumComplex);
        setSteps(getSteps() + 1);
        setCurrentTime(getNextTime());

      } catch (NoReactionSelectedException e) {
        setNextTime(getObserver().stream()
                                 .map(Observer::nextObservationPoint)
                                 .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY))
                                 .min()
                                 .orElse(Double.POSITIVE_INFINITY));
      }
    }
  }

  @Override
  public void nextStep() {
    executeSSAStep();
  }


}
