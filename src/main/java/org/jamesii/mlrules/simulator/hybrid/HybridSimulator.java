/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.hybrid;

import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.parser.nodes.MLRulesAddNode;
import org.jamesii.mlrules.parser.nodes.SpeciesPatternNode;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.simple.SimpleReaction;
import org.jamesii.mlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.mlrules.simulator.simple.SimpleRule;
import org.jamesii.mlrules.simulator.simple.StaticRule;
import org.jamesii.mlrules.simulator.standard.ChangedSpecies;
import org.jamesii.mlrules.simulator.standard.Reaction;
import org.jamesii.mlrules.simulator.standard.ReactionCreator;
import org.jamesii.mlrules.util.DoubleNumber;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * Prototypical implementation of a hybrid ML-Rules simulator using ODEs to
 * compute rules which can be mapped to {@link SimpleRule} and the SSA for the
 * remaining rules.
 *
 * @author Tobias Helms
 */
public class HybridSimulator extends Simulator implements FirstOrderDifferentialEquations {

  private final ExponentialDistribution expDist;

  private org.apache.commons.math3.distribution.ExponentialDistribution expDist2;

  private final ReactionCreator complexCreator = new ReactionCreator();

  private final SimpleReactionCreator simpleCreator = new SimpleReactionCreator();

  private final List<Reaction> infiniteReactions = new ArrayList<>();

  private final Map<Compartment, List<Reaction>> complexReactions = new HashMap<>();

  private final Map<Compartment, List<SimpleReaction>> simpleReactions = new HashMap<>();

  private final Map<Compartment, Double> complexRates = new HashMap<>();

  private double[] state;

  private List<Rule> complexRules = new ArrayList<>();

  private List<SimpleRule> simpleRules = new ArrayList<>();

  Map<Reactant, Reactant> reactantProducts = new HashMap<>();

  private final Map<LeafSpecies, Integer> speciesIndex = new IdentityHashMap<>();

  private final FirstOrderIntegrator integrator;

  private double nextComplexReaction = Double.POSITIVE_INFINITY;

  private PriorityQueue<Integer> freePositions = new PriorityQueue<>();

  private double quantil;

  private double epsilonMax;
  private double epsilonMin;

  private double oldSum = 0;

  private final List<TimedRule> timedRules;

  private final boolean fixedQuantil;

  @SuppressWarnings("unchecked")
  private void checkReactant(Reactant reactant, Set<String> variables) {
    if (!(reactant.getAmount() instanceof ValueNode<?>)) {
      throw new IllegalArgumentException("amount expressions are not allowed within reactants and products");
    }
//    for (Node att : reactant.getAttributeNodes()) {
//      if (att instanceof Identifier<?>) {
//        variables.add(((Identifier<String>) att).getIdent());
//      } else if (!(att instanceof ValueNode<?>)) {
//        throw new IllegalArgumentException("expressions are not allowed to calculate attribute values");
//      }
//    }
    reactant.getSubReactants()
            .forEach(sr -> checkReactant(sr, variables));
  }

  private void createReactants(MLRulesAddNode node, Set<String> variables, List<Reactant> reactants) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getLeft()).toReactant(getModel().getEnv());
      checkReactant(reactant, variables);
      reactants.add(reactant);
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getRight()).toReactant(getModel().getEnv());
      checkReactant(reactant, variables);
      reactants.add(reactant);
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getRight(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
  }

  private boolean emptySpecies(Node node) {
    if (node instanceof ValueNode<?>) {
      ValueNode<?> vn = (ValueNode<?>) node;
      if (vn.getValue() instanceof Map<?, ?>) {
        Map<?, ?> map = (Map<?, ?>) vn.getValue();
        return map.isEmpty();
      }
    }
    return false;
  }

  private List<Reactant> checkProduct(Node node, Set<String> variables) {
    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      Reactant product = spn.toReactant(getModel().getEnv());
      checkReactant(product, variables);
      products.add(product);
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(addNode, variables, products);
    } else if (emptySpecies(node)) {
      // do nothing;
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (products.stream()
                .anyMatch(p -> p.getBoundTo()
                                .isPresent())) {
      throw new IllegalArgumentException("product species cannot be bound to variables");
    }

    return products;
  }

  public Optional<SimpleRule> getSimple(Rule rule, Map<Reactant, Reactant> reactantProducts) {
    try {
      Set<String> variables = new HashSet<>();
      for (Reactant reactant : rule.getReactants()) {
        checkReactant(reactant, variables);
      }
      List<Reactant> products = checkProduct(rule.getProduct(), variables);

      if (rule.getAssignments()
              .stream()
              .anyMatch(a -> a.getNames()
                              .stream()
                              .anyMatch(n -> variables.contains(n)))) {
        throw new IllegalArgumentException(
                "Variables defined within the where part of a rule are not allowed to be used within the reactants or products.");
      }

      if (!StaticRule.isValidTauRule(rule.getReactants(), products, reactantProducts)) {
        throw new IllegalArgumentException("rules must not change the structure of the model");
      }

      return Optional.of(new SimpleRule(rule.getReactants(), products, rule.getRate(), rule.getOriginalRate(), rule.getAssignments()));
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  public void divideRules(List<Rule> rules) {
    for (Rule rule : rules) {
      Map<Reactant, Reactant> tmpReactantProducts = new HashMap<>();
      Optional<SimpleRule> sr = getSimple(rule, reactantProducts);
      if (sr.isPresent()) {
        simpleRules.add(sr.get());
        reactantProducts.putAll(tmpReactantProducts);
      } else {
        complexRules.add(rule);
      }
    }
  }

  public HybridSimulator(Model model, FirstOrderIntegrator integrator, double epsilonMin, double epsilonMax, double initialQuantil, boolean fixedQuantil) {
    super(model);

    divideRules(model.getRules()
                     .getRules());

    this.epsilonMin = epsilonMin;
    this.epsilonMax = epsilonMax;
    this.quantil = initialQuantil;
    this.fixedQuantil = fixedQuantil;

    timedRules = new ArrayList<>(model.getRules()
                                      .getTimedRules());
    timedRules.sort(RuleComparator.instance);
    this.integrator = integrator;

    simpleReactions.clear();
    simpleCreator.createInitialReactions(model.getSpecies(), simpleRules, simpleReactions, new ArrayList<>(),
            model.getEnv(), reactantProducts);
    collect(model.getSpecies(), new AtomicInteger(0));
    updateSimpleReactions();

    expDist = new ExponentialDistribution((IRandom) model.getEnv()
                                                         .getValue(Model.RNG));

    complexCreator.createInitialReactions(model.getSpecies(), complexRules, complexReactions, infiniteReactions,
            model.getEnv(), true);

    complexReactions.entrySet()
                    .stream()
                    .forEach(e -> {
                      if (!e.getValue()
                            .isEmpty()) {
                        complexRates.put(e.getKey(), e.getValue()
                                                      .stream()
                                                      .mapToDouble(r -> r.getRate())
                                                      .sum());
                      }
                    });
  }

  private void updateR() throws RejectException {
    if (!fixedQuantil && Double.compare(0D, oldSum) != 0) {
      double sum = 0D;
      for (Double d : complexRates.values()) {
        sum += d;
      }
      double relation = Math.abs(1 - (Double.compare(sum, oldSum) == 0 ? 1 : (oldSum / sum)));
      if (relation > epsilonMax && quantil > 0.001) {
        quantil = Math.max(0.0001, quantil / 2.0);
        throw new RejectException();
      } else {
        if (relation < epsilonMin) {
          quantil = Math.min(quantil * 2.0, 1);
        } else {
          quantil = Math.max(quantil / 2., 0.001);
        }
      }
    }
  }

  private void updateNextComplexReactionTime() {
    double sum = 0D;
    for (Double d : complexRates.values()) {
      sum += d;
    }
    if (Double.compare(sum, 0) > 0) {
      nextComplexReaction = getCurrentTime() + expDist.getRandomNumber(1.0 / sum);
    } else {
      nextComplexReaction = Double.POSITIVE_INFINITY;
    }
    expDist2 = new org.apache.commons.math3.distribution.ExponentialDistribution(null,
            1.0 / (Double.compare(sum, 0D) == 0 ? 0.0001 : sum));
    oldSum = sum;
  }

  private void updateSimpleReactions() {
    if (!freePositions.isEmpty()) {
      PriorityQueue<Entry<LeafSpecies, Integer>> pq = new PriorityQueue<Entry<LeafSpecies, Integer>>(
              freePositions.size(), new Comparator<Entry<LeafSpecies, Integer>>() {
        @Override
        public int compare(Entry<LeafSpecies, Integer> e1, Entry<LeafSpecies, Integer> e2) {
          return e1.getValue()
                   .compareTo(e2.getValue());
        }
      });
      for (Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
        pq.add(e);
        if (pq.size() > freePositions.size()) {
          pq.remove();
        }
      }
      for (Entry<LeafSpecies, Integer> e : pq) {
        if (freePositions.element() >= speciesIndex.size()) {
          break;
        }
        speciesIndex.put(e.getKey(), freePositions.remove());
      }
      freePositions.clear();
    }

    if (state == null || state.length != speciesIndex.size()) {
      state = new double[speciesIndex.size()];
    }
    for (Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
      state[e.getValue()] = e.getKey()
                             .getAmount();
    }
  }

  private void collect(Compartment compartment, AtomicInteger counter) {
    compartment.getSubCompartmentsStream()
               .forEach(s -> collect(s, counter));
    compartment.getSubLeavesStream()
               .forEach(s -> speciesIndex.put(s, counter.getAndIncrement()));
  }

  private Optional<Reaction> select(Map<Compartment, List<Reaction>> reactions, Map<Compartment, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return Optional.of(
              infiniteReactions.get(((IRandom) getModel().getEnv()
                                                         .getValue(Model.RNG)).nextInt(infiniteReactions.size())));
    }
    double rateSum = rates.values()
                          .stream()
                          .reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv()
                                        .getValue(Model.RNG)).nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return reactions.values()
                    .stream()
                    .flatMap(l -> l.stream())
                    .filter(r -> sum.addAndGet(r.getRate()) > pivot)
                    .findFirst();
  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
                                 Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getContextMatchings()
            .getMatchings()
            .stream()
            .anyMatch(m -> removedSpecies.contains(m.getSpecies())
                    || changedSpecies.getOrDefault(m.getSpecies()
                                                    .getType(), Collections.emptySet())
                                     .contains(m.getSpecies()));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    for (Species s : changed.getRemovedSpecies()) {
      if (s instanceof Compartment) {
        Compartment c = (Compartment) s;
        c.getSubLeavesStream()
         .forEach(sub -> {
           Integer i = speciesIndex.remove(sub);
           if (i != null) {
             freePositions.add(i);
           }
         });
        c.getSubCompartmentsRecursiveStream()
         .forEach(sub -> {
           complexReactions.remove(sub);
           simpleReactions.remove(sub);
           complexRates.remove(sub);
           Iterator<Reaction> i = infiniteReactions.iterator();
           while (i.hasNext()) {
             Reaction r = i.next();
             if (r.getContextMatchings()
                  .getContext() == s) {
               i.remove();
             }
           }
         });
      }
    }
  }

  private void removeReactions(List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    Iterator<Reaction> i;
    if (complexReactions.containsKey(s)) {
      i = complexReactions.get(s)
                          .iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
      i = infiniteReactions.iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
    }

    simpleReactions.getOrDefault(s, Collections.emptyList())
                   .clear();
  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    Set<Species> current = new HashSet<>();
    selected.getContextMatchings()
            .getContext()
            .getContextStream()
            .forEach(s -> {
              Map<SpeciesType, Set<Species>> changedSpecies;
              if (s == selected.getContextMatchings()
                               .getContext()) {
                changedSpecies = changed.getAllChangedSpecies();
              } else {
                changedSpecies = new HashMap<>();
                changedSpecies.put(current.iterator()
                                          .next()
                                          .getType(), current);
              }
              removeReactions(changed.getRemovedSpecies(), changedSpecies, s);
              complexRates.remove(s);
              complexCreator.createReactions(s, changedSpecies, complexRules, complexReactions, infiniteReactions,
                      getModel().getEnv(), true);
              for (Iterator<Entry<Compartment, List<Reaction>>> iterator = complexReactions.entrySet()
                                                                                           .iterator(); iterator
                           .hasNext(); ) {
                Entry<Compartment, List<Reaction>> e = iterator.next();
                if (e.getValue()
                     .isEmpty()) {
                  iterator.remove();
                }
              }
              simpleCreator.createReactions(s, null, simpleRules, simpleReactions, Collections.emptyList(), getModel().getEnv(),
                      reactantProducts);
              if (complexReactions.containsKey(s)) {
                complexRates.put(s, complexReactions.get(s)
                                                    .stream()
                                                    .mapToDouble(r -> r.getRate())
                                                    .sum());
              }
              current.clear();
              current.add(s);
            });
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies()
           .stream()
           .filter(s -> s instanceof Compartment)
           .map(s -> (Compartment) s)
           .forEach(c -> c.getSubCompartmentsRecursiveStream()
                          .forEach(sub -> {
                            complexCreator.createReactions(sub, null, complexRules, complexReactions, infiniteReactions,
                                    getModel().getEnv(), true);
                            simpleCreator.createReactions(sub, null, simpleRules, simpleReactions, Collections.emptyList(), getModel().getEnv(),
                                    reactantProducts);
                            if (complexReactions.containsKey(sub)) {
                              complexRates.put(sub, complexReactions.get(sub)
                                                                    .stream()
                                                                    .mapToDouble(r -> r.getRate())
                                                                    .sum());
                            }
                          }));
  }

  private void addSpeciesIndexValues(Species addedSpecies) {
    if (addedSpecies instanceof LeafSpecies) {
      int pos = freePositions.isEmpty() ? speciesIndex.size() : freePositions.remove();
      speciesIndex.put((LeafSpecies) addedSpecies, pos);
    } else {
      ((Compartment) addedSpecies).getAllSubSpeciesStream()
                                  .forEach(s -> addSpeciesIndexValues(s));
    }
  }

  protected void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
    changed.getAddedSpecies()
           .forEach(as -> addSpeciesIndexValues(as));
  }

  private void executeSSAStep() {
    Optional<Reaction> selected = select(complexReactions, complexRates);
    Optional<Reaction> reaction = selected;
    reaction = checkTimedReactions(reaction);
    if (reaction.isPresent()) {
      ChangedSpecies changed = reaction.get()
                                       .execute();

      getModel().getEnv()
                .setGlobalValue(Model.TIME, getNextTime());
      updateReactions(changed, reaction.get());

    } else if (timedRules.isEmpty()) {
      setNextTime(Double.POSITIVE_INFINITY);
    }
  }

  private void updateComplexReactionPropensities(Compartment context, List<Reaction> reactions) {
    double change = 0D;
    for (Reaction r : reactions) {
      r.updateRate();
      change += r.getRate();
    }
    complexRates.put(context, Math.max(0D, change));
  }

  private Optional<Reaction> selectTimedReaction() {
    Map<Compartment, List<Reaction>> timedReactions = new HashMap<>();
    double time = timedRules.get(0)
                            .getNextTime();
    List<Rule> current = new ArrayList<>();

    for (TimedRule r : timedRules) {
      if (r.getNextTime() > time) {
        break;
      }
      current.add(r.getRule());
      r.updateNextTime();
    }
    timedRules.sort(RuleComparator.instance);

    complexCreator.createInitialReactions(getModel().getSpecies(), current, timedReactions, new ArrayList<>(),
            getModel().getEnv(), false);
    return getRandomElement(timedReactions);
  }

  /**
   * Return a random reaction of the given reactions without creating an
   * additional data structure containing all reactions (i.e. not flatmapping
   * them first).
   */
  private Optional<Reaction> getRandomElement(Map<Compartment, List<Reaction>> reactions) {
    if (reactions.isEmpty()) {
      return Optional.empty();
    }
    IRandom rng = (IRandom) getModel().getEnv()
                                      .getValue(Model.RNG);
    int i = rng.nextInt(reactions.size());
    int j = 0;
    for (List<Reaction> r : reactions.values()) {
      if (i == j) {
        return Optional.of(r.get(rng.nextInt(r.size())));
      }
      ++j;
    }
    return Optional.empty();
  }

  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = timedRules.get(0)
                              .getNextTime();
      if (Double.compare(time, getNextTime()) <= 0) {
        Optional<Reaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  int numOfDiscreteEvents = 0;
  int numOfRejects = 0;

  @Override
  public void nextStep() {
    getObserver().forEach(o -> o.update(this));
    double min = Double.POSITIVE_INFINITY;
    for (Observer observer : getObserver()) {
      Optional<Double> next = observer.nextObservationPoint();
      min = Math.min(next.orElse(min), min);
    }

    double oldSumCopy = oldSum;
    Species copy = fixedQuantil ? null : getModel().getSpecies().copy();
    try {
      updateNextComplexReactionTime();
      double min1 = Math.min(min, getCurrentTime() + expDist2.inverseCumulativeProbability(quantil));
      min = Math.min(min1, nextComplexReaction);

      boolean timed = false;
      if (!timedRules.isEmpty()) {
        double time = timedRules.get(0)
                                .getNextTime();
        if (Double.compare(min, time) >= 0) {
          min = time;
          timed = true;
        }
      }


      if (Double.isFinite(min) && !simpleReactions.isEmpty()) {
        try {
          integrator.integrate(this, getCurrentTime(), state, min, state);

          // update dynamic propensities to adapt r or reject step
          for (Entry<Compartment, List<Reaction>> e : complexReactions.entrySet()) {
            updateComplexReactionPropensities(e.getKey(), e.getValue());
          }
          updateR();

        } catch (NumberIsTooSmallException e) {
          Logger.getGlobal()
                .info(String.format("skip continuous step due to too small time step from %s to %s",
                        getCurrentTime(), getNextTime()));
        }
        for (Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
          state[e.getValue()] = Math.max(0, state[e.getValue()]);
          e.getKey()
           .setAmount(state[e.getValue()]);
        }
      }
      setNextTime(min);

      if (timed || Double.compare(min1, nextComplexReaction) > 0) {
        ++numOfDiscreteEvents;
        executeSSAStep();
        updateSimpleReactions();
      }
      for (Entry<Compartment, List<Reaction>> e : complexReactions.entrySet()) {
        updateComplexReactionPropensities(e.getKey(), e.getValue());
      }

      setCurrentTime(getNextTime());
      setSteps(getSteps() + 1);
    } catch (RejectException exp) {
      ++numOfRejects;
      Logger.getGlobal().warning(numOfRejects + ": Execute rejection at step " + getSteps());
      infiniteReactions.clear();
      complexReactions.clear();
      simpleReactions.clear();
      complexRates.clear();
      state = null;
      speciesIndex.clear();
      freePositions.clear();

      oldSum = oldSumCopy;

      getModel().setSpecies((Compartment)copy);

      simpleCreator.createInitialReactions(getModel().getSpecies(), simpleRules, simpleReactions, new ArrayList<>(),
              getModel().getEnv(), reactantProducts);
      collect(getModel().getSpecies(), new AtomicInteger(0));
      updateSimpleReactions();

      complexCreator.createInitialReactions(getModel().getSpecies(), complexRules, complexReactions, infiniteReactions,
              getModel().getEnv(), true);

      complexReactions.entrySet()
                      .stream()
                      .forEach(e -> {
                        if (!e.getValue()
                              .isEmpty()) {
                          complexRates.put(e.getKey(), e.getValue()
                                                        .stream()
                                                        .mapToDouble(r -> r.getRate())
                                                        .sum());
                        }
                      });
    }
  }

  @Override
  public void computeDerivatives(double t, double[] y, double[] yDot) {
    for (Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
      e.getKey()
       .setAmount(Math.max(0, y[e.getValue()]));
      yDot[e.getValue()] = 0;
    }

    for (List<SimpleReaction> reactions : simpleReactions.values()) {
      for (SimpleReaction r : reactions) {
        r.updateConcentration();
        for (Entry<LeafSpecies, Integer> e : r.getChangeVectorNonZero()
                                              .entrySet()) {
          yDot[speciesIndex.get(e.getKey())] += e.getValue() * r.getCalculatedPropensity();
        }
      }
    }
  }

  @Override
  public int getDimension() {
    return speciesIndex.size();
  }

  public int getNumOfDiscreteEvents() {
    return numOfDiscreteEvents;
  }

}
