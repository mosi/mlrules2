package org.jamesii.mlrules.simulator.hybrid;

import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.parser.nodes.MLRulesAddNode;
import org.jamesii.mlrules.parser.nodes.SpeciesPatternNode;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.simple.SimpleReaction;
import org.jamesii.mlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.mlrules.simulator.simple.SimpleRule;
import org.jamesii.mlrules.simulator.simple.StaticRule;
import org.jamesii.mlrules.simulator.standard.*;
import org.jamesii.mlrules.util.DoubleNumber;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * In contrast to the {@link HybridSimulator}, this simulator also
 * approximates discrete reactions by using a Gamma distribution.
 *
 * @author Tobias Helms
 */
public class AdvancedHybridSimulator extends Simulator implements FirstOrderDifferentialEquations {

  // gamma distribution for leaps
  private org.apache.commons.math3.distribution.GammaDistribution gammaDist;

  private double jumpRelation;

  private final double minJumpRelation;

  private boolean isLeap = false;

  private double tauTime = Double.MAX_VALUE;

  private final ExponentialDistribution expDist;

  private org.apache.commons.math3.distribution.ExponentialDistribution expDist2;

  private final ReactionCreator complexCreator = new ReactionCreator();

  private final SimpleReactionCreator simpleCreator = new SimpleReactionCreator();

  private final List<Reaction> infiniteReactions = new ArrayList<>();

  private final Map<Compartment, List<Reaction>> complexReactions = new HashMap<>();

  private List<Reaction> allComplexReactions;
  private final Map<Compartment, List<SimpleReaction>> simpleReactions = new HashMap<>();

  private final Map<Compartment, Double> complexRates = new HashMap<>();

  private double[] state;

  private List<Rule> complexRules = new ArrayList<>();

  private List<SimpleRule> simpleRules = new ArrayList<>();

  Map<Reactant, Reactant> reactantProducts = new HashMap<>();

  private final Map<LeafSpecies, Integer> speciesIndex = new IdentityHashMap<>();

  private final FirstOrderIntegrator integrator;

  private PriorityQueue<Integer> freePositions = new PriorityQueue<>();

  private double quantil = 0.03;

  private final List<TimedRule> timedRules;

  private List<Reaction> selectedReactions;

  private Random random = new Random(((IRandom) getModel().getEnv()
                                                          .getValue(Model.RNG)).nextInt());

  private final boolean simple;

  private Species copy;

  private final double epsilonMin;

  private final double epsilonMax;

  private final double reject;

  private void checkReactant(Reactant reactant, Set<String> variables) {
    if (!(reactant.getAmount() instanceof ValueNode<?>)) {
      throw new IllegalArgumentException("amount expressions are not allowed within reactants and products");
    }
    reactant.getSubReactants()
            .forEach(sr -> checkReactant(sr, variables));
  }

  private void createReactants(MLRulesAddNode node, Set<String> variables, List<Reactant> reactants) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getLeft()).toReactant(getModel().getEnv());
      checkReactant(reactant, variables);
      reactants.add(reactant);
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getRight()).toReactant(getModel().getEnv());
      checkReactant(reactant, variables);
      reactants.add(reactant);
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getRight(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
  }

  private boolean emptySpecies(Node node) {
    if (node instanceof ValueNode<?>) {
      ValueNode<?> vn = (ValueNode<?>) node;
      if (vn.getValue() instanceof Map<?, ?>) {
        Map<?, ?> map = (Map<?, ?>) vn.getValue();
        return map.isEmpty();
      }
    }
    return false;
  }

  private List<Reactant> checkProduct(Node node, Set<String> variables) {
    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      Reactant product = spn.toReactant(getModel().getEnv());
      checkReactant(product, variables);
      products.add(product);
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(addNode, variables, products);
    } else if (emptySpecies(node)) {
      // do nothing;
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (products.stream()
                .anyMatch(p -> p.getBoundTo()
                                .isPresent())) {
      throw new IllegalArgumentException("product species cannot be bound to variables");
    }

    return products;
  }

  public Optional<SimpleRule> getSimple(Rule rule, Map<Reactant, Reactant> reactantProducts) {
    try {
      Set<String> variables = new HashSet<>();
      for (Reactant reactant : rule.getReactants()) {
        checkReactant(reactant, variables);
      }
      List<Reactant> products = checkProduct(rule.getProduct(), variables);

      if (rule.getAssignments()
              .stream()
              .anyMatch(a -> a.getNames()
                              .stream()
                              .anyMatch(n -> variables.contains(n)))) {
        throw new IllegalArgumentException(
                "Variables defined within the where part of a rule are not allowed to be used within the reactants or products.");
      }

      if (!StaticRule.isValidTauRule(rule.getReactants(), products, reactantProducts)) {
        throw new IllegalArgumentException("rules must not change the structure of the model");
      }

      return Optional.of(new SimpleRule(rule.getReactants(), products, rule.getRate(), rule.getAssignments()));
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  public void divideRules(List<Rule> rules) {
    for (Rule rule : rules) {
      Map<Reactant, Reactant> tmpReactantProducts = new HashMap<>();
      Optional<SimpleRule> sr = getSimple(rule, reactantProducts);
      if (sr.isPresent()) {
        simpleRules.add(sr.get());
        reactantProducts.putAll(tmpReactantProducts);
      } else {
        complexRules.add(rule);
      }
    }
  }

  public AdvancedHybridSimulator(Model model, FirstOrderIntegrator integrator, boolean simple, double jumpRelation, double minJumpRelation, double epsilonMin, double epsilonMax, double reject) {
    super(model);

    this.jumpRelation = jumpRelation;
    this.quantil = jumpRelation;
    this.minJumpRelation = minJumpRelation;
    this.simple = simple;
    this.epsilonMax = epsilonMax;
    this.epsilonMin = epsilonMin;
    this.reject = reject;

    divideRules(model.getRules()
                     .getRules());

    timedRules = new ArrayList<>(model.getRules()
                                      .getTimedRules());
    timedRules.sort(RuleComparator.instance);
    this.integrator = integrator;

    simpleReactions.clear();
    simpleCreator.createInitialReactions(model.getSpecies(), simpleRules, simpleReactions, new ArrayList<>(),
            model.getEnv(), reactantProducts);
    collect(model.getSpecies(), new AtomicInteger(0));
    updateSimpleReactions();

    expDist = new ExponentialDistribution((IRandom) model.getEnv()
                                                         .getValue(Model.RNG));

    complexCreator.createInitialReactions(model.getSpecies(), complexRules, complexReactions, infiniteReactions,
            model.getEnv(), true);

    complexReactions.entrySet()
                    .stream()
                    .forEach(e -> {
                      if (!e.getValue()
                            .isEmpty()) {
                        complexRates.put(e.getKey(), e.getValue()
                                                      .stream()
                                                      .mapToDouble(r -> r.getRate())
                                                      .sum());
                      }
                    });
  }

  private void updateSimpleReactions() {
    if (!freePositions.isEmpty()) {
      PriorityQueue<Map.Entry<LeafSpecies, Integer>> pq = new PriorityQueue<Map.Entry<LeafSpecies, Integer>>(
              freePositions.size(), new Comparator<Map.Entry<LeafSpecies, Integer>>() {
        @Override
        public int compare(Map.Entry<LeafSpecies, Integer> e1, Map.Entry<LeafSpecies, Integer> e2) {
          return e1.getValue()
                   .compareTo(e2.getValue());
        }
      });
      for (Map.Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
        pq.add(e);
        if (pq.size() > freePositions.size()) {
          pq.remove();
        }
      }
      for (Map.Entry<LeafSpecies, Integer> e : pq) {
        if (freePositions.element() >= speciesIndex.size()) {
          break;
        }
        speciesIndex.put(e.getKey(), freePositions.remove());
      }
      freePositions.clear();
    }

    if (state == null || state.length != speciesIndex.size()) {
      state = new double[speciesIndex.size()];
    }
    for (Map.Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
      state[e.getValue()] = e.getKey()
                             .getAmount();
    }
  }

  private void collect(Compartment compartment, AtomicInteger counter) {
    compartment.getSubCompartmentsStream()
               .forEach(s -> collect(s, counter));
    compartment.getSubLeavesStream()
               .forEach(s -> speciesIndex.put(s, counter.getAndIncrement()));
  }

  private Optional<Reaction> select(Map<Compartment, List<Reaction>> reactions, Map<Compartment, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return Optional.of(
              infiniteReactions.get(((IRandom) getModel().getEnv()
                                                         .getValue(Model.RNG)).nextInt(infiniteReactions.size())));
    }
    double rateSum = rates.values()
                          .stream()
                          .reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv()
                                        .getValue(Model.RNG)).nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return reactions.values()
                    .stream()
                    .flatMap(l -> l.stream())
                    .filter(r -> sum.addAndGet(r.getRate()) > pivot)
                    .findFirst();
  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
                                 Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getContextMatchings()
            .getMatchings()
            .stream()
            .anyMatch(m -> removedSpecies.contains(m.getSpecies())
                    || changedSpecies.getOrDefault(m.getSpecies()
                                                    .getType(), Collections.emptySet())
                                     .contains(m.getSpecies()));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    for (Species s : changed.getRemovedSpecies()) {
      if (s instanceof Compartment) {
        Compartment c = (Compartment) s;
        c.getSubLeavesStream()
         .forEach(sub -> {
           Integer i = speciesIndex.remove(sub);
           if (i != null) {
             freePositions.add(i);
           }
         });
        c.getSubCompartmentsRecursiveStream()
         .forEach(sub -> {
           complexReactions.remove(sub);
           simpleReactions.remove(sub);
           complexRates.remove(sub);
           Iterator<Reaction> i = infiniteReactions.iterator();
           while (i.hasNext()) {
             Reaction r = i.next();
             if (r.getContextMatchings()
                  .getContext() == s) {
               i.remove();
             }
           }
         });
      }
    }
  }

  private void removeReactions(List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    Iterator<Reaction> i;
    if (complexReactions.containsKey(s)) {
      i = complexReactions.get(s)
                          .iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
      i = infiniteReactions.iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
    }

    simpleReactions.getOrDefault(s, Collections.emptyList())
                   .clear();
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies()
           .stream()
           .filter(s -> s instanceof Compartment)
           .map(s -> (Compartment) s)
           .forEach(c -> c.getSubCompartmentsRecursiveStream()
                          .forEach(sub -> {
                            complexCreator.createReactions(sub, null, complexRules, complexReactions, infiniteReactions,
                                    getModel().getEnv(), true);
                            simpleCreator.createReactions(sub, null, simpleRules, simpleReactions, Collections.emptyList(), getModel().getEnv(),
                                    reactantProducts);
                            if (complexReactions.containsKey(sub)) {
                              complexRates.put(sub, complexReactions.get(sub)
                                                                    .stream()
                                                                    .mapToDouble(r -> r.getRate())
                                                                    .sum());
                            }
                          }));
  }

  private void addSpeciesIndexValues(Species addedSpecies) {
    if (addedSpecies instanceof LeafSpecies) {
      int pos = freePositions.isEmpty() ? speciesIndex.size() : freePositions.remove();
      speciesIndex.put((LeafSpecies) addedSpecies, pos);
    } else {
      ((Compartment) addedSpecies).getAllSubSpeciesStream()
                                  .forEach(s -> addSpeciesIndexValues(s));
    }
  }

  /**
   * update reactions sets
   *
   * @param changedContexts
   */
  private void buildReactionsForChangedSpecies(Set<Compartment> changedContexts) {

    for (Compartment s : changedContexts) {

      // clear reactions and rates for this context
      complexReactions.getOrDefault(s, Collections.emptyList())
                      .clear();
      simpleReactions.getOrDefault(s, Collections.emptyList())
                     .clear();
      complexRates.remove(s);

      // build set of complex reactions
      complexCreator.createReactions(s, null, complexRules, complexReactions,
              infiniteReactions, getModel().getEnv(), true);
      // remove empty entries
      complexReactions.entrySet()
                      .removeIf(e -> e.getValue()
                                      .isEmpty());

      // build set of simple reactions
      simpleCreator.createReactions(s, null, simpleRules, simpleReactions, Collections.emptyList(),
              getModel().getEnv(), reactantProducts);
      if (complexReactions.containsKey(s)) {
        complexRates.put(s, complexReactions.get(s)
                                            .stream()
                                            .mapToDouble(r -> r.getRate())
                                            .sum());
      }

    }
  }


  /**
   * update all reactions and species tree
   *
   * @param changed
   * @param changedContexts
   */
  protected void updateReactions(List<ChangedSpecies> changed, Set<Compartment> changedContexts) {
    for (ChangedSpecies cs : changed) {
      removeSubReactions(cs);
    }
    buildReactionsForChangedSpecies(changedContexts);
    for (ChangedSpecies cs : changed) {
      addReactionsToAddedSpecies(cs);
    }
    changed.forEach(cs -> cs.getAddedSpecies()
                            .forEach(as -> addSpeciesIndexValues(as)));
  }

  private void updateComplexReactionPropensities(Compartment context, List<Reaction> reactions) {
    double change = 0D;
    for (Reaction r : reactions) {
      r.updateRate();
      change += r.getRate();
    }
    complexRates.put(context, Math.max(0D, change));
  }

  private Optional<Reaction> selectTimedReaction() {
    Map<Compartment, List<Reaction>> timedReactions = new HashMap<>();
    double time = timedRules.get(0)
                            .getNextTime();
    List<Rule> current = new ArrayList<>();

    for (TimedRule r : timedRules) {
      if (r.getNextTime() > time) {
        break;
      }
      current.add(r.getRule());
      r.updateNextTime();
    }
    timedRules.sort(RuleComparator.instance);

    complexCreator.createInitialReactions(getModel().getSpecies(), current, timedReactions, new ArrayList<>(),
            getModel().getEnv(), false);
    return getRandomElement(timedReactions);
  }

  /**
   * Return a random reaction of the given reactions without creating an
   * additional data structure containing all reactions (i.e. not flatmapping
   * them first).
   */
  private Optional<Reaction> getRandomElement(Map<Compartment, List<Reaction>> reactions) {
    if (reactions.isEmpty()) {
      return Optional.empty();
    }
    IRandom rng = (IRandom) getModel().getEnv()
                                      .getValue(Model.RNG);
    int i = rng.nextInt(reactions.size());
    int j = 0;
    for (List<Reaction> r : reactions.values()) {
      if (i == j) {
        return Optional.of(r.get(rng.nextInt(r.size())));
      }
      ++j;
    }
    return Optional.empty();
  }

  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = timedRules.get(0)
                              .getNextTime();
      if (Double.compare(time, getNextTime()) <= 0) {
        Optional<Reaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }


  private void updateR(double oldSum) throws RejectException {
    for (Map.Entry<Compartment, List<Reaction>> e : complexReactions.entrySet()) {
      updateComplexReactionPropensities(e.getKey(), e.getValue());
    }
    if (Double.compare(0D, oldSum) != 0) {
      double sum = 0D;
      for (Double d : complexRates.values()) {
        sum += d;
      }
      double relation = Math.abs(1 - (Double.compare(sum, oldSum) == 0 ? 1 : (sum / oldSum)));
      if (simple) {
        if (relation > reject && quantil > minJumpRelation) {
          quantil = minJumpRelation;
          throw new RejectException();
        } else {
          if (relation < epsilonMin) {
            quantil = Math.min(quantil * 2.0, 1);
          } else if (relation > epsilonMax) {
            quantil = Math.max(quantil / 2., minJumpRelation);
          }
        }
      } else {
        if (relation > reject && jumpRelation > minJumpRelation) {
          jumpRelation = minJumpRelation;
          throw new RejectException();
        } else {
          if (relation < epsilonMin) {
            jumpRelation = Math.min(jumpRelation * 2.0, 1);
          } else  if(relation > epsilonMax){
            jumpRelation = Math.max(jumpRelation / 2., minJumpRelation);
          }
        }
      }
    }
  }


  double executedTauReactionsPerStep = 0D;
  int totalNumberOfStochasticReactions = 0;
  int numberOfTauSteps = 0;
  double averageTauLength = 0D;
  int numOfRejects = 0;
  double averageR = 0D;
  int sumN = 0;

  @Override
  public void nextStep() {

    copy = getModel().getSpecies()
                     .copy();
    boolean updated = false;
    try {

        // set propensity sum
        double a0 = getPropensitySum(complexRates);
      // currently in a leap
      if (isLeap()) {
          throw new RuntimeException("should never be true");
      }

      // currently not in a leap
      else {

        if (a0 > 0) {

          //get number of reactions
          Optional<Double> nextTime = selectTauReactions(a0);

          if (!selectedReactions.isEmpty()) {
            //set step size
            if (nextTime.isPresent()) {
              setTauTime(getCurrentTime() + nextTime.get());
            } else {
              getSizeOfTauStep(a0);
            }

            setIsLeap(true);

            // integration till t+tau// integration till t+tau
            setNextTime(getTauTime());
            integrateUntilNextTime(getNextTime());
            if (!simpleReactions.isEmpty()) {
              updateR(a0);
            }

            totalNumberOfStochasticReactions += selectedReactions.size();
            ++numberOfTauSteps;
            executedTauReactionsPerStep = ((numberOfTauSteps - 1) / (double) numberOfTauSteps) * executedTauReactionsPerStep + (1 / (double) numberOfTauSteps) * selectedReactions.size();
            averageTauLength = ((numberOfTauSteps - 1) / (double) numberOfTauSteps) * averageTauLength + (1 / (double) numberOfTauSteps) * (getTauTime() - getCurrentTime());

            executeTauReactions();

            //}
          } else {
            expDist2 = new org.apache.commons.math3.distribution.ExponentialDistribution(null,
                    1.0 / a0);
            setNextTime(getCurrentTime() + expDist2.inverseCumulativeProbability(simple ? quantil : jumpRelation));
            integrateUntilNextTime(getNextTime());
            updateR(a0);
          }

        }

        // end of simulation
        else {

          //more reactions to execute?
          if (!simpleReactions.isEmpty()) {
            integrateUntilNextTime(minObservationTime());
            for (Map.Entry<Compartment, List<Reaction>> e : complexReactions.entrySet()) {
              updateComplexReactionPropensities(e.getKey(), e.getValue());
            }
          } else {
            setNextTime(Double.POSITIVE_INFINITY);
            getObserver().forEach(o -> o.update(this));
          }

        }

      }

      // advance time
      getModel().getEnv()
                .setGlobalValue(Model.TIME, getNextTime());
      // set time
      setCurrentTime(getNextTime());
      // set steps
      averageR += simple ? quantil : jumpRelation;
      setSteps(getSteps() + 1);
    } catch (RejectException ex) {
      ++numOfRejects;
      Logger.getGlobal().info(numOfRejects + ": Execute rejection at step " + getSteps() + "(quantil: " + quantil + ", jump: " + jumpRelation + ")");
      isLeap = false;
      tauTime = Double.MAX_VALUE;
      selectedReactions.clear();
      infiniteReactions.clear();
      complexReactions.clear();
      simpleReactions.clear();
      complexRates.clear();
      state = null;
      speciesIndex.clear();
      freePositions.clear();

      getModel().setSpecies((Compartment)copy);

      simpleCreator.createInitialReactions(getModel().getSpecies(), simpleRules, simpleReactions, new ArrayList<>(),
              getModel().getEnv(), reactantProducts);
      collect(getModel().getSpecies(), new AtomicInteger(0));
      updateSimpleReactions();

      complexCreator.createInitialReactions(getModel().getSpecies(), complexRules, complexReactions, infiniteReactions,
              getModel().getEnv(), true);

      complexReactions.entrySet()
                      .stream()
                      .forEach(e -> {
                        if (!e.getValue()
                              .isEmpty()) {
                          complexRates.put(e.getKey(), e.getValue()
                                                        .stream()
                                                        .mapToDouble(r -> r.getRate())
                                                        .sum());
                        }
                      });

    }

  }

  private double minObservationTime() {
    return getObserver().stream().map(Observer::nextObservationPoint).mapToDouble(o -> o.orElse(Double.MIN_VALUE)).min().orElse(Double.NEGATIVE_INFINITY);
  }

  private void integrateUntilNextTime(double nextTime) {
    getObserver().forEach(o -> o.update(this));
    integrate(nextTime);
  }

  private void executeTauReactions() {
    List<ChangedSpecies> allChanges = new ArrayList<>();
    Set<Compartment> changedContexts = new HashSet<>();

    // execute and save changed species
    for (Reaction selected : selectedReactions) {
      ChangedSpecies cs = selected.execute();
      if (changedContexts.add(selected.getContextMatchings()
                                      .getContext())) {
        selected.getContextMatchings()
                .getContext()
                .getContextStream()
                .forEach(s -> changedContexts.add(s));
      }
      allChanges.add(cs);
    }


    // update reactions and species
    updateReactions(allChanges, changedContexts);

    // this leap finished
    setIsLeap(false);

    // update simple reactions
    updateSimpleReactions();
  }

  /**
   * set the size of the next tau step
   *
   * @param a0
   */
  private void getSizeOfTauStep(double a0) {

    // sample a value for tau from a gamma distribution GAMMA(number of complex reactions, propensity sum)
    double div = (double) 1 / a0;
    gammaDist = new org.apache.commons.math3.distribution.GammaDistribution(selectedReactions.size(), div);
    gammaDist.reseedRandomGenerator(((IRandom) getModel().getEnv()
                                                         .getValue(Model.RNG)).nextInt());
    double tau = gammaDist.sample();

    //save step size
    setTauTime(getCurrentTime() + tau);
    Logger.getGlobal()
          .info("Propensity discrete reactions: " + a0 + "; Selected: " + selectedReactions.size() + "; Tau: " + tau + "; Reaction Time: " + getTauTime());
  }


  private boolean checkCompartments(Reaction reaction, Set<Compartment> compartments) {
    Stream<Matching> compartmentMatchings = reaction.getContextMatchings()
                                                    .getMatchings()
                                                    .stream()
                                                    .filter(m -> m.getSpecies()
                                                                  .getType()
                                                                  .isCompartment());
    Stream<Compartment> reactionCompartments = compartmentMatchings.map(m -> (Compartment) m.getSpecies());
    return reactionCompartments.flatMap(Compartment::getSubCompartmentsRecursiveStream)
                               .noneMatch(compartments::contains);
  }

  private boolean checkContexts(Reaction reaction, Set<Compartment> compartments) {
    return reaction.getContextMatchings()
                   .getContext()
                   .getContextStream()
                   .filter(c -> c.getType() == SpeciesType.ROOT)
                   .noneMatch(compartments::contains);
  }

  /**
   * compute the number of complex reactions for the next tau step
   *
   * @param a0
   */
  private Optional<Double> selectTauReactions(double a0) {

    allComplexReactions = new ArrayList<>();
    selectedReactions = new LinkedList<>();

    // get flat list of all complex reactions with rate > 0
    allComplexReactions = complexReactions.values()
                                          .stream()
                                          .flatMap(l -> l.stream())
                                          .filter(r -> r.getRate() > 0)
                                          .collect(Collectors.toList());

    Set<Compartment> markedCompartments = new HashSet<>();

    int n = (int) (jumpRelation * allComplexReactions.size());
    Optional<Double> result = Optional.empty();
    if (simple || n == 0) {
      double nextTime = expDist.getRandomNumber(1.0 / a0);
      expDist2 = new org.apache.commons.math3.distribution.ExponentialDistribution(null,
              1.0 / a0);
      double q = expDist2.inverseCumulativeProbability(simple ? quantil : jumpRelation);
      if (nextTime < q) {
        n = 1;
        result = Optional.of(nextTime);
      } else {
        n = 0;
      }
    }

    sumN += n;

    // N samples
    for (int i = 0; i < n; i++) {
      // sample
      double pivot = random.nextDouble() * a0;
      double acc = 0;
      Reaction reaction = null;
      // find reaction
      for (Reaction r : allComplexReactions) {
        acc += r.getRate();
        if (acc >= pivot) {
          reaction = r;
          break;
        }
      }

      // reaction has already been used
      if (selectedReactions.contains(reaction) || !checkCompartments(reaction, markedCompartments) || !checkContexts(reaction, markedCompartments)) {
        continue;
      }

      markDependentSpecies(reaction.getContextMatchings(), markedCompartments);
      selectedReactions.add(reaction);

    }
    return result;
  }

  /**
   * mark all dependent species
   */
  private void markDependentSpecies(ContextMatchings matchings, Set<Compartment> markedCompartments) {
    for (Matching m : matchings.getMatchings()) {
      if (m.getSpecies()
           .getType()
           .isCompartment()) {
        Compartment comp = (Compartment) m.getSpecies();
        comp.getSubCompartmentsRecursiveStream()
            .forEach(markedCompartments::add);
      }
    }
  }

  /**
   * Integrate until nextTime
   *
   * @param nextTime
   */
  private void integrate(double nextTime) {

    if (Double.isFinite(nextTime)) {
      try {
        // continuous steps (simple reactions only)
        integrator.integrate(this, getCurrentTime(), state, getNextTime(), state);
      } catch (NumberIsTooSmallException e) {
        Logger.getGlobal()
              .info(String.format("skip continuous step due to too small time step from %s to %s",
                      getCurrentTime(), getNextTime()));
      }
      // update state
      for (Map.Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
        e.getKey()
         .setAmount(Math.max(0, state[e.getValue()]));
      }
    }

  }

  @Override
  public void computeDerivatives(double t, double[] y, double[] yDot) {
    for (Map.Entry<LeafSpecies, Integer> e : speciesIndex.entrySet()) {
      e.getKey()
       .setAmount(Math.max(0, y[e.getValue()]));
      yDot[e.getValue()] = 0;
    }

    for (List<SimpleReaction> reactions : simpleReactions.values()) {
      for (SimpleReaction r : reactions) {
        r.updateAlways();
        for (Map.Entry<LeafSpecies, Integer> e : r.getChangeVectorNonZero()
                                                  .entrySet()) {
          yDot[speciesIndex.get(e.getKey())] += e.getValue() * r.getCalculatedPropensity();
        }
      }
    }
  }

  @Override
  public int getDimension() {
    return speciesIndex.size();
  }

  protected void setIsLeap(boolean isLeap) {
    this.isLeap = isLeap;
  }

  public double getTauTime() {
    return tauTime;
  }

  public boolean isLeap() {
    return isLeap;
  }

  /**
   * Get rate sum of complex reactions
   */
  private double getPropensitySum(Map<Compartment, Double> rates) {
    return rates.values()
                .stream()
                .reduce(0.0, Double::sum);
  }

  protected void setTauTime(double time) {
    this.tauTime = time;
  }

  public double getExecutedTauReactionsPerStep() {
    return executedTauReactionsPerStep;
  }

  public double getAverageTauLength() {
    return averageTauLength;
  }

  public double getNumberOfTauSteps() {
    return numberOfTauSteps;
  }

  public int getTotalNumberOfStochasticReactions() {
    return totalNumberOfStochasticReactions;
  }

  public int getNumOfRejects() { return numOfRejects; }

  public double getAverageR() {return averageR / (double) getSteps(); }

  public int getSumN() {return sumN;}

  @Override
  public void finish() {
    StringJoiner joiner = new StringJoiner("\n","","");
    joiner.add("TauSteps: " + getNumberOfTauSteps() + "; TauLengh: " + getAverageTauLength());
    joiner.add("DynamicReactionsPerStep: " + getExecutedTauReactionsPerStep());
    joiner.add("TotalDynamicReactions: " + getTotalNumberOfStochasticReactions());
    joiner.add("AverageR: " + getAverageR());
    joiner.add("SumN: " + getSumN());
    joiner.add("Rejects:" + getNumOfRejects());
    Logger.getGlobal().info(joiner.toString());
  }

}
