/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator;

import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.observation.Observer;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Abstract class for all ML-Rules simulators containing basic information and corresponding getters.
 * 
 * @author Tobias Helms
 *
 */
public abstract class Simulator {

  /**
   * The model to be executed.
   */
  private final Model model;

  /**
   * The current simulation time of the model.
   */
  private double currentTime = 0D;

  /**
   * The simulation of the model after executing the next step.
   */
  private double nextTime = 0D;

  /**
   * The number of performed simulation steps.
   */
  private int steps = 0;

  /**
   * The observer available observing the model state.
   */
  private final Set<Observer> observer = new HashSet<>();

  public Simulator(Model model) {
    this.model = model;
  }

  public Set<Observer> getObserver() {
    return observer;
  }

  protected void setCurrentTime(double currentTime) {
    this.currentTime = currentTime;
  }

  protected void setNextTime(double nextTime) {
    this.nextTime = nextTime;
  }

  protected void setSteps(int steps) {
    this.steps = steps;
  }

  public abstract void nextStep();

  public Model getModel() {
    return model;
  }

  public void finish() {
    // do nothing per default
  }

  public int getSteps() {
    return steps;
  }

  public double getCurrentTime() {
    return currentTime;
  }

  public double getNextTime() {
    return nextTime;
  }

}
