/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.links;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.species.SpeciesType.AttributeType;
import org.jamesii.mlrules.util.Nu;

import java.util.*;

/**
 * The link model is tailored to models that extensively use links between pairs of species.
 * 
 * @author Tobias Helms
 *
 */
public class LinkModel {

  /**
   * This map contains for each rule all link variable names that must have a
   * non-free value. For example, "A(x) + B(x) -> ..." (assuming that both
   * attributes are link attributes) has the link variable name "x". "x" cannot
   * be free, because in this case the modeler should have written "free". In
   * contrast, the rule "A(x) + B(y) -> ..." does not have a link variable name,
   * because the value of the link variables do not matter. This distinction is
   * important, because it is possible that link attributes do not matter in
   * rules.
   */
  private final Map<Rule, Set<String>> linkVariables = new HashMap<>();
  
  private final Map<Reactant, Map<Integer, String>> reactantLinks =
      new HashMap<>();

  public LinkModel(Model model) {
    model.getRules().getRules().forEach(r -> {
      computeLinkVariables(r, r.getReactants());
      r.getReactants().forEach(reactant -> computeReactantLinks(r, reactant));
    });
  }
  
  private void computeLinkVariables(Rule rule, List<Reactant> reactants) {
    Map<Reactant, Set<String>> linkMap = new HashMap<>();
    for (Reactant r : reactants) {
      Set<String> linkSet = new HashSet<>();
      for (int i = 0; i < r.getType().getAttributesSize(); ++i) {
        if (r.getType().getType(i) != AttributeType.LINK) {
          continue;
        }
        Node n = r.getAttributeNodes().get(i);
        if (n instanceof ValueNode<?>) {
          ValueNode<?> vn = (ValueNode<?>) n;
          if (vn.getValue() != Nu.FREE) {
            throw new IllegalArgumentException(String.format(
                "invalid link value %s given in rule %s", vn.getValue(), rule));
          }
        } else if (n instanceof Identifier<?>) {
          Identifier<?> in = (Identifier<?>) n;
          if (in.getIdent() instanceof String) {
            String name = (String) in.getIdent();
            if (linkVariables.getOrDefault(rule, Collections.emptySet())
                .contains(name)) {
              throw new IllegalArgumentException(String.format(
                  "link variable %s cannot be used in more than two reactants",
                  name));
            }
            if (!linkSet.add(name)) {
              throw new IllegalArgumentException(
                  String
                      .format(
                          "link variable %s cannot be used twice within the same reactant",
                          name));
            }
            if (linkMap.values().stream().anyMatch(v -> v.contains(name))) {
              linkVariables.compute(rule,
                  (k, v) -> v == null ? new HashSet<>() : v).add(name);
            }
          }
        }
      }
      linkMap.put(r, linkSet);
      computeLinkVariables(rule, r.getSubReactants());
    }
  }
  
  @SuppressWarnings("unchecked")
  private void computeReactantLinks(Rule rule, Reactant reactant) {
    for (int i = 0; i < reactant.getType().getAttributesSize(); ++i) {
      if (reactant.getType().getType(i) != AttributeType.LINK) {
        continue;
      }
    
      if (reactant.getAttributeNodes().get(i) instanceof Identifier<?>) {
        if (linkVariables.getOrDefault(rule, Collections.emptySet()).contains(((Identifier<String>) reactant.getAttributeNodes().get(i)).getIdent())) {
        reactantLinks.computeIfAbsent(reactant, r -> new HashMap<>()).put(
            i,
            (String) ((Identifier<?>) reactant.getAttributeNodes().get(i))
                .getIdent());
        }
      }
    }
    reactant.getSubReactants().forEach(r -> computeReactantLinks(rule, r));
  }
  

  public Map<Reactant, Map<Integer, String>> getReactantLinks() {
    return reactantLinks;
  }
  
  public Map<Rule, Set<String>> getLinkVariables() {
    return linkVariables;
  }

}
