/*
 *   Copyright 2016 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.links;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.simulator.standard.ContextMatchings;
import org.jamesii.mlrules.simulator.standard.Matching;
import org.jamesii.mlrules.simulator.standard.ReactionCreator;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.Nu;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The {@link LinkReactionCreator} for the {@link LinkSimulator} explicitly uses
 * link (nu) values to improve the creation process for reactions with bindings
 * between two species.
 * 
 * @author Tobias Helms
 *
 */
public class LinkReactionCreator extends ReactionCreator {

  private final LinkModel model;

  private final Map<Nu, Map<Species, Species>> linkedSpecies;

  /**
   * Extend {@link Tree} to also save nu values.
   * 
   * @author Tobias Helms
   *
   */
  protected static class LinkTree extends Tree {

    private final Map<String, Nu> nus;

    public LinkTree(Matching current, LinkTree parent, Map<String, Nu> nus) {
      super(current, parent);
      this.nus = nus;
      if (parent != null) {
        this.nus.putAll(parent.getNus());
      }
    }

    public Map<String, Nu> getNus() {
      return nus;
    }

  }

  public LinkReactionCreator(LinkModel model, Map<Nu, Map<Species, Species>> linkedSpecies) {
    this.model = model;
    this.linkedSpecies = linkedSpecies;
  }

  protected boolean checkAttribute(Node attNode, Object left, Object right) {
    return !((left == Nu.FREE && attNode instanceof Identifier<?>) || !left.equals(right));
  }

  private Map<String, Nu> computeLinkValues(Map<Integer, String> linkVariables, Species species) {
    Map<String, Nu> result = new HashMap<>();
    linkVariables.entrySet().stream().filter(e -> species.getAttribute(e.getKey()) != Nu.FREE)
        .forEach(e -> result.put(e.getValue(), (Nu) species.getAttribute(e.getKey())));
    return result;
  }

  private Stream<Matching> createMatchings(Set<Species> candidates, Map<SpeciesType, Set<Species>> changedSpecies,
      Reactant reactant, Tree matchings, MLEnvironment env, boolean mustBeNonChanged) {
    Stream.Builder<Matching> builder = Stream.builder();
    List<Object> attributeValues = calcAttributeValues(reactant, env);
    candidates.forEach(s -> {
      if (!mustBeNonChanged || !changedSpecies.getOrDefault(s.getType(), Collections.emptySet()).contains(s)) {
        addMatching(builder, s, reactant, matchings, attributeValues, env);
      }
    });
    return builder.build();
  }

  private Set<Species> getSubSpeciesOfType(Compartment context, SpeciesType t) {
    if (t.isCompartment()) {
      return context.getSubCompartments().getOrDefault(t, Collections.emptySet()).stream().map(c -> (Species) c)
          .collect(Collectors.toSet());
    } else {
      return context.getSubLeaves().getOrDefault(t, Collections.emptyMap()).keySet().stream().map(l -> (Species) l)
          .collect(Collectors.toSet());
    }
  }

  protected void createContextMatchingsHelper(Stream.Builder<ContextMatchings> builder, Compartment context,
      Map<SpeciesType, Set<Species>> changedSpecies, List<Reactant> reactants, int i, Tree matchings, MLEnvironment env,
      int iteration) {
    LinkTree linkMatchings = (LinkTree) matchings;
    if (i == reactants.size()) {
      try {
        builder.add(new ContextMatchings(context, matchings.create(), env));
      } catch (Exception e) {
        Logger.getGlobal().info("Invalid context matching: " + e.getMessage());
      }
    } else {
      Reactant reactant = reactants.get(i);
      SpeciesType t = reactant.getType();

      Map<Integer, String> linkVariables = model.getReactantLinks().getOrDefault(reactant, Collections.emptyMap());
      List<Nu> nus = new ArrayList<>();
      linkVariables.values().forEach(v -> {
        if (linkMatchings.getNus().containsKey(v)) {
          nus.add(linkMatchings.getNus().get(v));
        }
      });

      Set<Species> linked = new HashSet<>();
      nus.forEach(n -> linked.addAll(linkedSpecies.getOrDefault(n, Collections.emptyMap()).keySet().stream()
          .filter(s -> s.getType() == t).collect(Collectors.toList())));

      Set<Species> candidates = (changedSpecies != null && i == 0)
          ? changedSpecies.getOrDefault(t, Collections.emptySet())
          : (nus.isEmpty() ? getSubSpeciesOfType(context, t) : linked);

      createMatchings(candidates, changedSpecies, reactant, matchings, env, i >= reactants.size() - iteration)
          .forEach(m -> createContextMatchingsHelper(builder, context, changedSpecies, reactants, i + 1,
              new LinkTree(m, linkMatchings, computeLinkValues(linkVariables, m.getSpecies())), m.getEnv(), iteration));
    }
  }

  /**
   * Reactants are moved to the end after finding all reactions for the current
   * order whereby the matching of the first reactant must be a changed species.
   * Consequently, only non-changed species are accepted for matchings of
   * already moved reactants. Otherwise, the same reactions would be calculated
   * several times.
   */
  protected void createContextMatchings(Stream.Builder<ContextMatchings> builder, Compartment context,
      Map<SpeciesType, Set<Species>> changedSpecies, List<Reactant> reactants, MLEnvironment env) {
    for (int i = 0; i < reactants.size(); ++i) {
      if ((changedSpecies == null || changedSpecies.keySet().contains(reactants.get(0).getType()))
          && reactants.stream().allMatch(t -> context.containsType(t.getType()))) {
        createContextMatchingsHelper(builder, context, changedSpecies, reactants, 0,
            new LinkTree(null, null, new HashMap<>()), env, i);
      }
      if (changedSpecies == null) {
        break;
      }
      reactants.add(reactants.remove(0));
    }
  }

}
