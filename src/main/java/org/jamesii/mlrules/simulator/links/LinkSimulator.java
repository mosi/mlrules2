/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.links;

import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.standard.ChangedSpecies;
import org.jamesii.mlrules.simulator.standard.Reaction;
import org.jamesii.mlrules.util.DoubleNumber;
import org.jamesii.mlrules.util.Nu;
import org.jamesii.mlrules.util.Pair;

import java.util.*;

/**
 * The {@link LinkSimulator} performs better if linked species are used
 * excessively. <br>
 * For example, when computing reactions for the rule A(x) + B(x) -> ...
 * (assuming that the attributes are link values), after matching the first
 * reactant, the second reactant is clearly determined. This simulator exploits
 * this observation and maintains a map of bound species to directly access the
 * partner. <br>
 * <br>
 * Further, the simulator uses a technique called <b>reactant swapping</b>. By
 * using this technique, the first matched reactant of a rule must be a changed
 * species (except no changed species are available). After finding all
 * matchings, the first reactant is moved to the end of the reactants and the
 * process starts again (whereby the last reactant now must be a nonchanged
 * species) and so on. For example, assume 100 A(link) and 100 B(link) (one A
 * always connected to one B, all individuals) are available and exactly 1
 * A(link) and 1 B(link) are changed. Only the rule A(x) + B(x) -> ... exists.
 * The normal simulator would iterate over all 100 A(link) for the first
 * reactant and check the second reactant for these (resulting in one reaction),
 * i.e., 100 checks are made. The LinkSimulator only considers the changed
 * A(link) for the first reactant, finding one matching B(link) directly for the
 * second reactant, then swapping the reactants and starting with the changed
 * B(link) resulting in no additional reaction (because no matching nonchanged
 * A(link) is found). Thus, only two checks are made. Nevertheless, applying
 * this technique forbids using variable expressions in reactants that depend on
 * each other, e.g., A(x) + B(x + 2) -> ... <br>
 * <br>
 * The <b>nu()</b> function is not allowed to be used within self-defined
 * functions!
 * 
 * 
 * @author Tobias Helms
 *
 */
public class LinkSimulator extends Simulator {

  private final LinkModel linkModel;

  private final Map<Nu, Map<Species, Species>> linkedSpecies = new HashMap<>();

  private final Map<Compartment, List<Reaction>> reactions = new HashMap<>();

  private final List<Reaction> infiniteReactions = new ArrayList<>();
  
  private final List<TimedRule> timedRules;

  private final Map<Compartment, Double> rates = new HashMap<>();

  private final ExponentialDistribution expDist;

  private final LinkReactionCreator creator;

  private final boolean useDependencyGraph;

  public LinkSimulator(Model model, boolean useDependencyGraph) {
    super(model);

    linkModel = new LinkModel(model);

    timedRules = new ArrayList<>(model.getRules().getTimedRules());
    timedRules.sort(RuleComparator.instance);
    
    this.useDependencyGraph = useDependencyGraph;
    expDist = new ExponentialDistribution((IRandom) model.getEnv().getValue(Model.RNG));

    creator = new LinkReactionCreator(linkModel, linkedSpecies);
    creator.createInitialReactions(model.getSpecies(), model.getRules().getRules(), reactions, infiniteReactions,
        model.getEnv(), false);
    reactions.entrySet().stream()
        .forEach(e -> rates.put(e.getKey(), e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
    getObserver().forEach(o -> o.update(this));
  }

  private void removeLinks(Species removed) {
    for (int i = 0; i < removed.getType().getAttributesSize(); ++i) {
      if (removed.getAttribute(i) instanceof Nu) {
        linkedSpecies.getOrDefault(removed.getAttribute(i), Collections.emptyMap()).remove(removed);
        if (linkedSpecies.getOrDefault(removed.getAttribute(i), Collections.emptyMap()).isEmpty()) {
          linkedSpecies.remove(removed.getAttribute(i));
        }
      }
    }
    if (removed instanceof Compartment) {
      ((Compartment) removed).getAllSubSpeciesStream().forEach(sub -> removeLinks(sub));
    }
  }

  private void addLinks(Species added) {
    for (int i = 0; i < added.getType().getAttributesSize(); ++i) {
      if (added.getAttribute(i) instanceof Nu && added.getAttribute(i) != Nu.FREE) {
        linkedSpecies.compute((Nu) added.getAttribute(i), (k, v) -> v == null ? new IdentityHashMap<>() : v).put(added,
            added);
      }
    }
    if (added instanceof Compartment) {
      ((Compartment) added).getAllSubSpeciesStream().forEach(sub -> addLinks(sub));
    }
  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
      Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getContextMatchings().getMatchings().stream().anyMatch(m -> removedSpecies.contains(m.getSpecies())
        || changedSpecies.getOrDefault(m.getSpecies().getType(), Collections.emptySet()).contains(m.getSpecies()));
  }

  private void removeReactions(List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    Iterator<Reaction> i;
    if (reactions.containsKey(s)) {
      i = reactions.get(s).iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
      i = infiniteReactions.iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
    }
  }

  private void removeSubReactionsFromCompartment(Compartment compartment) {
    reactions.remove(compartment);
    rates.remove(compartment);
    Iterator<Reaction> i = infiniteReactions.iterator();
    while (i.hasNext()) {
      Reaction r = i.next();
      if (r.getContextMatchings().getContext() == compartment) {
        i.remove();
      }
    }

    compartment.getSubCompartmentsStream().forEach(c -> removeSubReactionsFromCompartment(c));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    changed.getRemovedSpecies().stream().filter(s -> s instanceof Compartment)
        .forEach(s -> removeSubReactionsFromCompartment((Compartment) s));
  }

  private void updateChangedSpeciesOfContext(Compartment context, Optional<Compartment> changedSubCompartment,
      ChangedSpecies changed, Reaction selected) {
    Map<SpeciesType, Set<Species>> changedSpecies;
    if (!changedSubCompartment.isPresent()) {
      changedSpecies = changed.getAllChangedSpecies();
    } else {
      changedSpecies = new HashMap<>();
      Set<Species> set = new HashSet<>();
      set.add(changedSubCompartment.get());
      changedSpecies.put(changedSubCompartment.get().getType(), set);
    }
    removeReactions(changed.getRemovedSpecies(), changedSpecies, context);
    rates.remove(context);
    creator.createReactions(context, changedSpecies, getModel().getRules().getRules(), reactions, infiniteReactions,
        getModel().getEnv(), false);
    if (reactions.containsKey(context)) {
      rates.put(context, reactions.get(context).stream().mapToDouble(r -> r.getRate()).sum());
    }

    if (context.getContext() != Compartment.UNKNOWN) {
      updateChangedSpeciesOfContext(context.getContext(), Optional.of(context), changed, selected);
    }

  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    updateChangedSpeciesOfContext((Compartment) selected.getContextMatchings().getContext(), Optional.empty(), changed,
        selected);
  }

  private void addReactionsToAddedCompartment(Compartment compartment) {
    creator.createReactions(compartment, null, getModel().getRules().getRules(), reactions, infiniteReactions,
        getModel().getEnv(), false);
    if (reactions.containsKey(compartment)) {
      rates.put(compartment, reactions.get(compartment).stream().mapToDouble(r -> r.getRate()).sum());
    }

    compartment.getSubCompartmentsStream().forEach(c -> addReactionsToAddedCompartment(c));
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies().stream().filter(s -> s instanceof Compartment)
        .forEach(s -> addReactionsToAddedCompartment((Compartment) s));
  }

  protected void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
  }

  private Pair<Optional<Reaction>, Double> select(Map<Compartment, List<Reaction>> reactions, Map<Compartment, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return new Pair<>(Optional.of(
          infiniteReactions.get(((IRandom) getModel().getEnv().getValue(Model.RNG)).nextInt(infiniteReactions.size()))),
          0D);
    }
    double rateSum = rates.values().stream().reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv().getValue(Model.RNG)).nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return new Pair<>(reactions.values().stream().flatMap(l -> l.stream())
        .filter(r -> sum.addAndGet(r.getRate()) > pivot).findFirst(), rateSum);
  }

  private Optional<Reaction> selectTimedReaction() {
    Map<Compartment, List<Reaction>> timedReactions = new HashMap<>();
    double time = timedRules.get(0).getNextTime();
    List<Rule> current = new ArrayList<>();

    for (TimedRule r : timedRules) {
      if (r.getNextTime() > time) {
        break;
      }
      current.add(r.getRule());
      r.updateNextTime();
    }
    timedRules.sort(RuleComparator.instance);

    creator.createInitialReactions(getModel().getSpecies(), current, timedReactions, new ArrayList<>(),
        getModel().getEnv(), false);
    return getRandomElement(timedReactions);
  }

  /**
   * Return a random reaction of the given reactions without creating an
   * additional data structure containing all reactions (i.e. not flatmapping
   * them first).
   */
  private Optional<Reaction> getRandomElement(Map<Compartment, List<Reaction>> reactions) {
    if (reactions.isEmpty()) {
      return Optional.empty();
    }
    IRandom rng = (IRandom) getModel().getEnv().getValue(Model.RNG);
    int i = rng.nextInt(reactions.size());
    int j = 0;
    for (List<Reaction> r : reactions.values()) {
      if (i == j) {
        return Optional.of(r.get(rng.nextInt(r.size())));
      }
      ++j;
    }
    return Optional.empty();
  }

  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = timedRules.get(0).getNextTime();
      if (Double.compare(time, getNextTime()) <= 0) {
        Optional<Reaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  
  @Override
  public void nextStep() {
    Pair<Optional<Reaction>, Double> selected = select(reactions, rates);
    Optional<Reaction> reaction = selected.fst();
    if (!reaction.isPresent()) {
      setNextTime(getObserver().stream().map(o -> o.nextObservationPoint())
          .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY)).min().orElse(Double.POSITIVE_INFINITY));
    } else if (Double.compare(selected.snd(), 0D) != 0) {
      setNextTime(getCurrentTime() + expDist.getRandomNumber(1.0 / selected.snd()));
    }
    reaction = checkTimedReactions(reaction);
    getObserver().forEach(o -> o.update(this));
    if (reaction.isPresent()) {
      ChangedSpecies changed = reaction.get().execute();

      getModel().getEnv().setGlobalValue(Model.TIME, getNextTime());

      changed.getRemovedSpecies().forEach(r -> removeLinks(r));
      changed.getAddedSpecies().forEach(a -> addLinks(a));
      if (useDependencyGraph) {
        updateReactions(changed, reaction.get());
      } else {
        rates.clear();
        reactions.clear();
        creator.createInitialReactions(getModel().getSpecies(), getModel().getRules().getRules(), reactions,
            infiniteReactions, getModel().getEnv(), false);
        reactions.entrySet().stream()
            .forEach(e -> rates.put(e.getKey(), e.getValue().stream().mapToDouble(r -> r.getRate()).sum()));
      }
    }
    setSteps(getSteps() + 1);
    setCurrentTime(getNextTime());
  }

}
