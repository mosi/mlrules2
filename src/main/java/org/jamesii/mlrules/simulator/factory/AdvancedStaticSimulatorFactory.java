package org.jamesii.mlrules.simulator.factory;

import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.advancedstatic.AdvancedStaticSimulator;

/**
 * Factory for the {@link AdvancedStaticSimulator}.
 *
 * @author Tobias Helms
 *
 */
public class AdvancedStaticSimulatorFactory implements SimulatorFactory {

  @Override
  public Simulator create(Model model) {
    return new AdvancedStaticSimulator(model);
  }


}
