/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.factory;

import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerIntegrator;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.hybrid.HybridSimulator;

import java.util.logging.Logger;

import static org.jamesii.mlrules.util.runtimeCompiling.CompileTools.compileModel;

public class HybridSimulatorFactory implements SimulatorFactory {

  private final boolean useRuntimeCompilation;

  private final double epsilonMin;

  private final double epsilonMax;

  private final double initialQuantil;

  private final boolean fixedQuantil;

  public enum Integrator {
    DORMAND_PRINCE, GRAGG_BULIRSCH_STOER;
  }

  private final Integrator integrator;

  private FirstOrderIntegrator createIntegrator() {
    // always use default parameter
    double minStep = 1.0e-08;
    double maxStep = 10.0;
    double scaleAbsoluteTolerance = 1.0e-8;
    double scaleRelativeTolerance = 1.0e-8;
    switch(integrator) {
      case DORMAND_PRINCE:
        return new DormandPrince853Integrator(minStep,maxStep,scaleAbsoluteTolerance,scaleRelativeTolerance);
      case GRAGG_BULIRSCH_STOER:
        return new GraggBulirschStoerIntegrator(minStep, maxStep, scaleAbsoluteTolerance, scaleRelativeTolerance);
    }
    throw new RuntimeException("Unknown integrator: " + integrator.toString());
  }

  public HybridSimulatorFactory(Integrator integrator, double epsilonMin, double epsilonMax, double initialQuantil, boolean fixedQuantil, boolean useRuntimeCompilation) {
    this.integrator = integrator;
    this.epsilonMin = epsilonMin;
    this.epsilonMax = epsilonMax;
    this.initialQuantil = initialQuantil;
    this.fixedQuantil = fixedQuantil;
    this.useRuntimeCompilation = useRuntimeCompilation;
  }

  @Override
  public Simulator create(Model model) throws ModelNotSupportedException {
    if (useRuntimeCompilation) {
      try {
        compileModel(model);
      }catch (Exception e){
        Logger.getGlobal().severe("Cannot compile model for optimization");
      }
    }
    return new HybridSimulator(model, createIntegrator(), epsilonMin, epsilonMax, initialQuantil, fixedQuantil);
  }

}
