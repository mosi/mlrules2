package org.jamesii.mlrules.simulator.factory;

import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerIntegrator;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.hybrid.AdvancedHybridSimulator;

/**
 * @author Tobias Helms
 */
public class AdvancedHybridSimulatorFactory implements SimulatorFactory {

  private final double jumpRelation;

  private final double minJumpRelation;

  private final boolean simple;

  private final double epsilonMin;

  private final double epsilonMax;

  private final double reject;

  private final Integrator integrator;

  public enum Integrator {
    DORMAND_PRINCE, GRAGG_BULIRSCH_STOER;
  }

  private FirstOrderIntegrator createIntegrator() {
    // always use default parameter
    double minStep = 1.0e-08;
    double maxStep = 10.0;
    double scaleAbsoluteTolerance = 1.0e-8;
    double scaleRelativeTolerance = 1.0e-8;
    switch(integrator) {
      case DORMAND_PRINCE:
        return new DormandPrince853Integrator(minStep,maxStep,scaleAbsoluteTolerance,scaleRelativeTolerance);
      case GRAGG_BULIRSCH_STOER:
        return new GraggBulirschStoerIntegrator(minStep, maxStep, scaleAbsoluteTolerance, scaleRelativeTolerance);
    }
    throw new RuntimeException("Unknown integrator: " + integrator.toString());
  }

  public AdvancedHybridSimulatorFactory(Integrator integrator, boolean simple, double jumpRelation, double minJumpRelation, double epsilonMin, double epsilonMax, double reject) {
    this.integrator = integrator;
    this.simple = simple;
    this.jumpRelation = jumpRelation;
    this.minJumpRelation = minJumpRelation;
    this.epsilonMax = epsilonMax;
    this.epsilonMin = epsilonMin;
    this.reject = reject;
  }

  @Override
  public Simulator create(Model model) throws ModelNotSupportedException {
    return new AdvancedHybridSimulator(model, createIntegrator(),simple,jumpRelation, minJumpRelation, epsilonMin, epsilonMax, reject);
  }

}
