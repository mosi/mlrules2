/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.factory;

import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.links.LinkSimulator;

import java.util.logging.Logger;

import static org.jamesii.mlrules.util.runtimeCompiling.CompileTools.compileModel;

public class LinkSimulatorFactory implements SimulatorFactory {

  private final boolean useDependencyGraph;
  private final boolean useRuntimeCompilation;


  public LinkSimulatorFactory(boolean useDependencyGraph, boolean useRuntimeCompilation) {
    this.useDependencyGraph = useDependencyGraph;
    this.useRuntimeCompilation = useRuntimeCompilation;
  }

  @Override
  public Simulator create(Model model) throws ModelNotSupportedException {
    if (useRuntimeCompilation) {
      try {
        compileModel(model);
      }catch (Exception e){
        Logger.getGlobal().severe("Cannot compile model for optimization");
      }
    }
    return new LinkSimulator(model, useDependencyGraph);
  }

}
