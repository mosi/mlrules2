/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.factory;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Exception thrown if a simulator does not support a model. Typically, it is
 * thrown during the instantiation of the simulator.
 * 
 * @author Tobias Helms
 *
 */
public class ModelNotSupportedException extends Exception {

  private static final long serialVersionUID = 275197558967056955L;

  private final Exception e;

  public ModelNotSupportedException(Exception e) {
    this.e = e;
  }

  @Override
  public String getMessage() {
    return e.getMessage();
  }

  @Override
  public void printStackTrace() {
    e.printStackTrace();
  }

  @Override
  public void printStackTrace(PrintStream s) {
    e.printStackTrace(s);
  }

  @Override
  public void printStackTrace(PrintWriter s) {
    e.printStackTrace(s);
  }

  @Override
  public String toString() {
    return e.toString();
  }

  public Exception getException() {
    return e;
  }

}
