/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.simple;

import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.List;
import java.util.Map;

/**
 * The simple model has a static tree structure, i.e., no compartment species
 * are allowed to change. Further, it is not allowed to switch between infinite
 * and non-infinite rates within one rule.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleModel {

  private final Compartment species;

  private final Map<Compartment, List<SimpleReaction>> reactions;

  private final List<SimpleReaction> infiniteReactions;

  private final List<TimedRule> timedRules;

  private final Map<TimedRule, List<SimpleReaction>> timedReactions;

  private final MLEnvironment env;

  public SimpleModel(Compartment species, Map<Compartment, List<SimpleReaction>> reactions,
      List<SimpleReaction> infiniteReactions, List<TimedRule> timedRules,
      Map<TimedRule, List<SimpleReaction>> timedRulesReactions, MLEnvironment env) {
    this.species = species;
    this.reactions = reactions;
    this.infiniteReactions = infiniteReactions;
    this.timedRules = timedRules;
    timedRules.sort(RuleComparator.instance);
    this.timedReactions = timedRulesReactions;
    this.env = env;
  }

  @Override
  public String toString() {
    return species.toString() + "\n" + reactions.toString() + "\n" + timedReactions.toString() + "\n" + env.toString();
  }

  public MLEnvironment getEnv() {
    return env;
  }

  public Species getSpecies() {
    return species;
  }

  public Map<Compartment, List<SimpleReaction>> getReactions() {
    return reactions;
  }

  public List<TimedRule> getTimedRules() {
    return timedRules;
  }

  public Map<TimedRule, List<SimpleReaction>> getTimedReactions() {
    return timedReactions;
  }

  public List<SimpleReaction> getInfiniteReactions() {
    return infiniteReactions;
  }

}
