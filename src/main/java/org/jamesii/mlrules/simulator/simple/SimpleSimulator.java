/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.simple;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.parser.nodes.MLRulesAddNode;
import org.jamesii.mlrules.parser.nodes.SpeciesPatternNode;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.factory.ModelNotSupportedException;
import org.jamesii.mlrules.util.DoubleNumber;
import org.jamesii.mlrules.util.Pair;

import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The {@link SimpleSimulator} supports models with a fixed reaction set. Thus,
 * it is only computing the reactions once and reuses them. Only propensities
 * are updated. <br>
 * <br>
 * The {@link SimpleSimulator} does not support infinite rate reactions, since
 * they do not make sense when the set of reactions is static.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleSimulator extends Simulator {

  private final SimpleModel simpleModel;

  private double rateSum = 0D;

  private final ExponentialDistribution expDist;

  private final Map<SimpleReaction, Map<Species, Set<SimpleReaction>>> dependencies = new HashMap<>();

  private final Map<Species, Double> contextRates = new HashMap<>();

  private final boolean useDependencyGraph;

  @SuppressWarnings("unchecked")
  private void checkReactant(Reactant reactant, Set<String> variables) {
    if (!(reactant.getAmount() instanceof ValueNode<?>)) {
      throw new IllegalArgumentException("amount expressions are not allowed within reactants and products");
    }
    for (Node att : reactant.getAttributeNodes()) {
      if (att instanceof Identifier<?>) {
        variables.add(((Identifier<String>) att).getIdent());
      } else if (!(att instanceof ValueNode<?>)) {
        throw new IllegalArgumentException("expressions are not allowed to calculate attribute values");
      }
    }
    reactant.getSubReactants().forEach(sr -> checkReactant(sr, variables));
  }

  private void createReactants(MLRulesAddNode node, Set<String> variables, List<Reactant> reactants) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getLeft()).toReactant(getModel().getEnv());
      checkReactant(reactant, variables);
      reactants.add(reactant);
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getRight()).toReactant(getModel().getEnv());
      checkReactant(reactant, variables);
      reactants.add(reactant);
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getRight(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
  }

  private boolean emptySpecies(Node node) {
    if (node instanceof ValueNode<?>) {
      ValueNode<?> vn = (ValueNode<?>) node;
      if (vn.getValue() instanceof Map<?, ?>) {
        Map<?, ?> map = (Map<?, ?>) vn.getValue();
        return map.isEmpty();
      }
    }
    return false;
  }

  private List<Reactant> checkProduct(Node node, Set<String> variables) {
    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      Reactant product = spn.toReactant(getModel().getEnv());
      checkReactant(product, variables);
      products.add(product);
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(addNode, variables, products);
    } else if (emptySpecies(node)) {
      // do nothing;
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (products.stream().anyMatch(p -> p.getBoundTo().isPresent())) {
      throw new IllegalArgumentException("product species cannot be bound to variables");
    }

    return products;
  }

  private SimpleRule createSimpleRule(Rule rule, Map<Reactant, Reactant> reactantProducts) {
    Set<String> variables = new HashSet<>();
    for (Reactant reactant : rule.getReactants()) {
      checkReactant(reactant, variables);
    }
    List<Reactant> products = checkProduct(rule.getProduct(), variables);

    if (rule.getAssignments().stream().anyMatch(a -> a.getNames().stream().anyMatch(n -> variables.contains(n)))) {
      throw new IllegalArgumentException(
          "Variables defined within the where part of a rule are not allowed to be used within the reactants or products.");
    }

    if (!StaticRule.isValidTauRule(rule.getReactants(), products, reactantProducts)) {
      throw new IllegalArgumentException("rules must not change the structure of the model");
    }

    return new SimpleRule(rule.getReactants(), products, rule.getRate(), rule.getOriginalRate(), rule.getAssignments());
  }

  public SimpleModel createSimpleModel(Model model) {

    List<SimpleRule> simpleRules = new ArrayList<>();
    Map<TimedRule, List<SimpleReaction>> timedRules = new HashMap<>();
    Map<Reactant, Reactant> reactantProducts = new HashMap<>();

    for (Rule rule : model.getRules().getRules()) {
      simpleRules.add(createSimpleRule(rule, reactantProducts));
    }

    Map<TimedRule, SimpleRule> timedRuleToSimpleRule = new HashMap<>();
    for (TimedRule rule : model.getRules().getTimedRules()) {
      rule.getRule().setRate(new ValueNode<Double>(1D));
      timedRuleToSimpleRule.put(rule, createSimpleRule(rule.getRule(), reactantProducts));
    }

    SimpleReactionCreator creator = new SimpleReactionCreator();
    Map<Compartment, List<SimpleReaction>> reactions = new HashMap<>();
    List<SimpleReaction> infiniteReactions = new ArrayList<>();
    creator.createInitialReactions(model.getSpecies(), simpleRules, reactions, infiniteReactions, model.getEnv(),
        reactantProducts);

    for (Entry<TimedRule, SimpleRule> e : timedRuleToSimpleRule.entrySet()) {
      Map<Compartment, List<SimpleReaction>> timedReactions = new HashMap<>();
      creator.createInitialReactions(model.getSpecies(), Arrays.asList(e.getValue()), timedReactions, infiniteReactions,
          model.getEnv(), reactantProducts);
      timedRules.put(e.getKey(),
          timedReactions.values().stream().flatMap(l -> l.stream()).collect(Collectors.toList()));
    }
    for (Iterator<Entry<Compartment, List<SimpleReaction>>> i = reactions.entrySet().iterator(); i.hasNext();) {
      if (i.next().getValue().isEmpty()) {
        i.remove();
      }
    }

    infiniteReactions.forEach(SimpleReaction::updateAlways);

    return new SimpleModel(model.getSpecies(), reactions, infiniteReactions,
        new ArrayList<>(model.getRules().getTimedRules()), timedRules, model.getEnv());
  }

  public SimpleSimulator(Model model, boolean useDependencyGraph) throws ModelNotSupportedException {
    super(model);

    try {
      simpleModel = createSimpleModel(model);

      this.useDependencyGraph = useDependencyGraph;
      expDist = new ExponentialDistribution((IRandom) model.getEnv().getValue(Model.RNG));
      simpleModel.getReactions().entrySet().stream().forEach(e -> {
        final DoubleNumber rates = new DoubleNumber(0D);
        e.getValue().forEach(r -> rates.addAndGet(r.getCalculatedPropensity()));
        rateSum += rates.get();
        contextRates.put(e.getKey(), rates.get());
      });

      getObserver().forEach(o -> o.update(this));
      createDependencyGraph();

    } catch (Exception e) {
      throw new ModelNotSupportedException(e);
    }

  }

  /**
   * Put the species of the given reaction to the species reactions map (if not
   * already present) and add the given reaction to the corresponding reaction
   * lists.
   */
  private void addSpeciesReactions(SimpleReaction r, Map<LeafSpecies, Set<SimpleReaction>> speciesReactions) {
    r.getReactantVector().keySet()
        .forEach(s -> speciesReactions.computeIfAbsent(s, k -> new HashSet<>()).add(r));
    r.getProductVector().keySet()
        .forEach(s -> speciesReactions.computeIfAbsent(s, k -> new HashSet<>()).add(r));
  }

  private void addReactionDependencies(SimpleReaction r, Map<LeafSpecies, Set<SimpleReaction>> speciesReactions) {
    for (LeafSpecies s : r.getChangeVectorNonZero().keySet()) {
      Map<Species, Set<SimpleReaction>> reactions = dependencies.computeIfAbsent(r, reaction -> new HashMap<>());
      speciesReactions.getOrDefault(s, Collections.emptySet())
          .forEach(reaction -> reactions.computeIfAbsent(reaction.getContext(), rr -> new HashSet<>()).add(reaction));
    }
  }

  private void createDependencyGraph() {
    Map<LeafSpecies, Set<SimpleReaction>> speciesReactions = new IdentityHashMap<>();
    simpleModel.getReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addSpeciesReactions(r, speciesReactions));
    simpleModel.getReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addReactionDependencies(r, speciesReactions));
    simpleModel.getTimedReactions().values().stream().flatMap(l -> l.stream())
        .forEach(r -> addReactionDependencies(r, speciesReactions));
  }

  private void updatePropensitiesLoop(Species species, Collection<SimpleReaction> reactions) {
    if (reactions.isEmpty()) {
      return;
    }
    double change = 0D;
    for (SimpleReaction r : reactions) {
      change -= r.getCalculatedPropensity();
      r.update();
      change += r.getCalculatedPropensity();
    }
    rateSum += change;
    contextRates.put(species, contextRates.get(species) + change);
  }

  private void updatePropensities(SimpleReaction reaction) {
    Map<Species, Set<SimpleReaction>> reactions = dependencies.getOrDefault(reaction, Collections.emptyMap());
    if (useDependencyGraph) {
      for (Entry<Species, Set<SimpleReaction>> e : reactions.entrySet()) {
        updatePropensitiesLoop(e.getKey(), e.getValue());
      }
    } else {
      for (Entry<Compartment, List<SimpleReaction>> e : simpleModel.getReactions().entrySet()) {
        updatePropensitiesLoop(e.getKey(), e.getValue());
      }
    }

  }

  private Pair<Optional<SimpleReaction>, Double> select() {
    Optional<SimpleReaction> reaction = simpleModel.getInfiniteReactions().stream()
        .filter(r -> Double.isInfinite(r.getCalculatedPropensity()) && r.executable()).findAny();
    if (reaction.isPresent()) {
      return new Pair<>(reaction, 0D);
    }
    double pivot = ((IRandom) getModel().getEnv().getValue(Model.RNG)).nextDouble() * rateSum;

    final DoubleNumber sum = new DoubleNumber(0D);
    Optional<Entry<Species, Double>> context = contextRates.entrySet().stream()
        .filter(value -> sum.addAndGet(value.getValue()) > pivot).findFirst();
    if (!context.isPresent()) {
      return new Pair<>(Optional.empty(), 0D);
    }
    final DoubleNumber sum2 = new DoubleNumber(sum.get() - context.get().getValue());
    List<SimpleReaction> reactions = simpleModel.getReactions().get(context.get().getKey());
    return new Pair<>(reactions.stream().filter(r -> sum2.addAndGet(r.getCalculatedPropensity()) > pivot).findFirst(),
        rateSum);
  }

  private Optional<SimpleReaction> selectTimedReaction() {
    double time = simpleModel.getTimedRules().get(0).getNextTime();
    List<SimpleReaction> current = new ArrayList<>();

    for (TimedRule rule : simpleModel.getTimedRules()) {
      if (rule.getNextTime() > time) {
        break;
      }
      current.addAll(simpleModel.getTimedReactions().getOrDefault(rule, Collections.emptyList()).stream()
          .filter(r -> r.executable()).collect(Collectors.toList()));
      rule.updateNextTime();
    }
    simpleModel.getTimedRules().sort(RuleComparator.instance);

    if (current.isEmpty()) {
      return Optional.empty();
    }

    return Optional.of(current.get(((IRandom) getModel().getEnv().getValue(Model.RNG)).nextInt(current.size())));
  }

  private Optional<SimpleReaction> checkTimedReactions(Optional<SimpleReaction> reaction) {
    if (!simpleModel.getTimedReactions().isEmpty()) {
      double time = simpleModel.getTimedRules().get(0).getNextTime();
      if (Double.compare(time, getNextTime()) <= 0) {
        Optional<SimpleReaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  private void logStepInfo() {
    int numberOfCompartments = getModel().getSpecies().countAllCompartments();
    int numberOfSpecies = getModel().getSpecies().countAllLeafSpecies();
    int numberOfReactions = (int) simpleModel.getReactions().values().stream().flatMap(Collection::stream).count();

    StringJoiner joiner = new StringJoiner("\n", "","");
    joiner.add(String.format("#Step: %d, #Compartments: %d, #LeafSpecies: %d, #Reactions: %d, RateSum: %f", getSteps(), numberOfCompartments, numberOfSpecies, numberOfReactions, rateSum));
    joiner.add(String.format("State: %s", getModel().getSpecies().toString()));
    Logger.getGlobal().info(joiner.toString());
  }

  @Override
  public void nextStep() {
    if (Logger.getGlobal().isLoggable(Level.INFO)) {
      logStepInfo();
    }


    Pair<Optional<SimpleReaction>, Double> selected = select();
    Optional<SimpleReaction> reaction = selected.fst();
    if (!reaction.isPresent()) {
      setNextTime(getObserver().stream().map(o -> o.nextObservationPoint())
          .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY)).min().orElse(Double.POSITIVE_INFINITY));
    } else if (Double.compare(selected.snd(), 0D) != 0) {
      setNextTime(getCurrentTime() + expDist.getRandomNumber(1.0 / selected.snd()));
    }
    reaction = checkTimedReactions(reaction);
    getObserver().forEach(o -> o.update(this));
    if (reaction.isPresent()) {
      reaction.get().execute();
      updatePropensities(reaction.get());
      getModel().getEnv().setGlobalValue(Model.TIME, getNextTime());
    }
    setSteps(getSteps() + 1);
    setCurrentTime(getNextTime());
  }

}
