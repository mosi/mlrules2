/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.simple;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.simulator.standard.AbstractReactionCreator;
import org.jamesii.mlrules.simulator.standard.ContextMatchings;
import org.jamesii.mlrules.simulator.standard.Matching;
import org.jamesii.mlrules.util.Assignment;
import org.jamesii.mlrules.util.LazyInitialization;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.NodeHelper;

import java.util.*;
import java.util.stream.Stream;

/**
 * The {@link SimpleReactionCreator} ignores amount conditions of rules, i.e.,
 * the amount of a species is not considered when checking the validity of this
 * species for a reactant.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleReactionCreator extends AbstractReactionCreator {


  private class ImpossibleReactionException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
  }
  
  @Override
  protected boolean validAmount(Species species, Tree matching, Reactant reactant, MLEnvironment env) {
    return true;
  }

  private void checkMatching(Matching matching, Node rateNode, double rate,
      Map<LeafSpecies,Integer> reactantVector, Map<LeafSpecies, Integer> productVector, Map<Reactant, Reactant> reactantProducts, MLEnvironment env) {
    if (!matching.getReactant().isCompartment()) {
      reactantVector.merge((LeafSpecies) matching.getSpecies(), matching.getRemoval(), Integer::sum);
    } else {
      matching.getSubReactants().forEach(m -> checkMatching(m, rateNode, rate, reactantVector, productVector, reactantProducts, env));
      reactantProducts.get(matching.getReactant()).getSubReactants()
          .forEach(r -> checkProducts((Compartment) matching.getSpecies(), r, rateNode, rate, productVector, env));
    }
  }

  private void checkProducts(Compartment context, Reactant reactant, Node rateNode, double rate,
      Map<LeafSpecies, Integer> productVector, MLEnvironment env) {
    if (!reactant.isCompartment()) {
      Object[] attributes = new Object[reactant.getType().getAttributesSize()];
      for (int i = 0; i < attributes.length; ++i) {
        Node tmp = reactant.getAttributeNodes().get(i).calc(env);
        if (tmp instanceof ValueNode<?>) {
          attributes[i] = ((ValueNode<?>) tmp).getValue();
        } else {
          throw new IllegalArgumentException(
              String.format("could not compute the attribute value of a species %s", reactant.getType()));
        }
      }
      LeafSpecies species = new LeafSpecies(reactant.getType(),
          attributes, context, NodeHelper.getInt(reactant.getAmount(), env));
      if (context.getSubLeaves().getOrDefault(species.getType(), Collections.emptyMap()).get(species) == null) {
        if (Double.compare(0D, rate) == 0) {
          throw new ImpossibleReactionException();
        } else {
          throw new IllegalArgumentException(String.format(
              "the species %s is not available, i.e., all species must be added to the initial solution, even species with amount 0",
              species));
        }
      } else {
        productVector.merge(
            context.getSubLeaves().get(species.getType()).get(species),(int)species.getAmount(),Integer::sum);
      }
    }
  }

  private void computeChangeVector(ContextMatchings cm, SimpleRule rule, double rate,
      Map<Reactant, Reactant> reactantProducts, Map<LeafSpecies, Integer> reactantVector, Map<LeafSpecies, Integer> productVector) {
    cm.getMatchings().forEach(m -> checkMatching(m, rule.getRate(), rate, reactantVector, productVector, reactantProducts, cm.getEnv()));
    rule.getProducts().forEach(p -> checkProducts(cm.getContext(), p, rule.getRate(), rate, productVector, cm.getEnv()));
  }

  private void createReactions(Compartment context, Map<SpeciesType, Set<Species>> changedSpecies, SimpleRule rule, List<SimpleReaction> reactions, List<SimpleReaction> infiniteReactions,
                               MLEnvironment env, Map<Reactant, Reactant> reactantProducts) {
    Stream.Builder<ContextMatchings> builder = Stream.builder();
    createContextMatchings(builder, context, changedSpecies, rule.getReactants(), env);
    builder.build().forEach(cm -> {
      for (Assignment assign : rule.getAssignments()) {
        assign.getNames()
              .forEach(n -> cm.getEnv().setValue(n, new LazyInitialization(assign, cm.getEnv())));
      }
      double rate = NodeHelper.getDouble(rule.getRate(), cm.getEnv());
      try {
        Map<LeafSpecies, Integer> reactantVector = new IdentityHashMap<>();
        Map<LeafSpecies, Integer> productVector = new IdentityHashMap<>();
        computeChangeVector(cm,rule,rate,reactantProducts,reactantVector,productVector);
        SimpleReaction r = new SimpleReaction(context, reactantVector, productVector,
          rule.getRate(), rule, cm.getEnv());

      if (r.isPossible()) {
        if (Double.isInfinite(rate)) {
          infiniteReactions.add(r);
        } else {
          reactions.add(r);
        }
      }
      } catch (ImpossibleReactionException e) {
        // ignore reaction
      }
    });
  }

  public List<SimpleReaction> createReactions(Compartment species, Map<SpeciesType, Set<Species>> changedSpecies, List<SimpleRule> rules,
      Map<Compartment, List<SimpleReaction>> reactions, List<SimpleReaction> infiniteReactions, MLEnvironment env,
      Map<Reactant, Reactant> reactantProducts) {
    List<SimpleReaction> newReactions = new ArrayList<>();
    rules.forEach(r -> createReactions(species, changedSpecies, r, newReactions, infiniteReactions, env, reactantProducts));
    if (!newReactions.isEmpty()) {
      reactions.computeIfAbsent(species, s -> new ArrayList<>()).addAll(newReactions);
    }
    return newReactions;
  }

  public void createInitialReactions(Compartment species, List<SimpleRule> rules,
      Map<Compartment, List<SimpleReaction>> reactions, List<SimpleReaction> infiniteReactions, MLEnvironment env,
      Map<Reactant, Reactant> reactantProducts) {
    createReactions(species, null, rules, reactions, infiniteReactions, env, reactantProducts);
    species.getSubCompartmentsStream()
        .forEach(s -> createInitialReactions(s, rules, reactions, infiniteReactions, env, reactantProducts));
  }

}
