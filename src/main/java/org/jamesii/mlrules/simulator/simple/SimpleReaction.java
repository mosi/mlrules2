/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.simple;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.control.IfThenElseNode;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.parser.nodes.SpeciesAmountNode;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.NodeHelper;
import org.jamesii.mlrules.util.expressions.SimpleRateExpressions;
import org.jamesii.mlrules.util.runtimeCompiling.DynamicNode;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * A simple reaction has change vector and a propensity. The original rule of
 * this reaction is also saved. A simple reaction can be used to describe
 * reactions that do not change the structure of the model.
 *
 * @author Tobias Helms
 */
public class SimpleReaction {

  private final Compartment context;

  private final Set<Compartment> changedContexts = new HashSet<>();

  private final Set<Compartment> usedCompartments = new HashSet<>();

  private final Map<LeafSpecies, Integer> reactantVector;

  private final Map<LeafSpecies, Integer> productVector;

  private final Map<LeafSpecies, Integer> changeVectorNonZero;

  private Node propensity;

  private final SimpleRateExpressions simplePropensity;

  private Double calculatedPropensity;

  private final MLEnvironment env;

  private final SimpleRule rule;

  private boolean possible = true;

  private final int order;

  private int maxFiringNumber;

  @SuppressWarnings("unchecked")
  private SimpleRateExpressions createSimpleExpression(Node node, MLEnvironment env) {
    /*if (node instanceof MultNode) {
      MultNode mn = (MultNode) node;
      if (mn.getLeft() instanceof Identifier && mn.getRight() instanceof SpeciesAmountNode) {
        SpeciesAmountNode san = (SpeciesAmountNode) mn.getRight();

        Object o = env.getValue(san.getOtherName());
        Map<?, ?> list = (Map<?, ?>) o;
        Species s = (Species) list.entrySet()
                                  .iterator()
                                  .next()
                                  .getKey();

        double c = ((ValueNode<Double>) mn.getLeft()
                                          .calc(env)).getValue();
        return new SimpleRateExpressionOneSpecies(s, c);
      }
      if (mn.getLeft() instanceof MultNode && mn.getRight() instanceof SpeciesAmountNode) {
        MultNode mn2 = (MultNode) mn.getLeft();
        if (mn2.getLeft() instanceof Identifier && mn2.getRight() instanceof SpeciesAmountNode) {
          double c = ((ValueNode<Double>) mn2.getLeft()
                                             .calc(env)).getValue();

          SpeciesAmountNode san = (SpeciesAmountNode) mn.getRight();
          Object o = env.getValue(san.getOtherName());
          Map<?, ?> list = (Map<?, ?>) o;
          Species s1 = (Species) list.entrySet()
                                     .iterator()
                                     .next()
                                     .getKey();

          san = (SpeciesAmountNode) mn2.getRight();
          o = env.getValue(san.getOtherName());
          list = (Map<?, ?>) o;
          Species s2 = (Species) list.entrySet()
                                     .iterator()
                                     .next()
                                     .getKey();
          return new SimpleRateExpressionTwoSpecies(s1, s2, c);
        }
      }
    }*/
    if (node instanceof IfThenElseNode) {
      IfThenElseNode iten = (IfThenElseNode) node;
      if (((ValueNode<Boolean>) iten.getCondition()
                                    .calc(env)).getValue()) {
        /*if (iten.getThenStmt() instanceof MultNode) {
          MultNode mn = (MultNode) iten.getThenStmt();
          if (mn.getLeft() instanceof Identifier && mn.getRight() instanceof SpeciesAmountNode) {
            SpeciesAmountNode san = (SpeciesAmountNode) mn.getRight();

            Object o = env.getValue(san.getOtherName());
            Map<?, ?> list = (Map<?, ?>) o;
            Species s = (Species) list.entrySet()
                                      .iterator()
                                      .next()
                                      .getKey();

            double c = ((ValueNode<Double>) mn.getLeft()
                                              .calc(env)).getValue();
            return new SimpleRateExpressionOneSpecies(s, c);
          }
        }*/
      } else {
        if (!containsSpeciesAmountNode(iten.getCondition()) && iten.getElseStmt() instanceof ValueNode<?>
                && ((ValueNode<Double>) iten.getElseStmt()).getValue()
                                                           .equals(0D)) {
          possible = false;
        }
      }
    }
    return null;
  }

  private boolean containsSpeciesAmountNode(INode node) {
    if (node instanceof SpeciesAmountNode) {
      return true;
    } else {
      return node.getChildren()
                 .stream()
                 .anyMatch(this::containsSpeciesAmountNode);
    }
  }

  public SimpleReaction(Compartment context, Map<LeafSpecies, Integer> reactantVector, Map<LeafSpecies, Integer> productVector, Node propensity,
                        SimpleRule rule, MLEnvironment env) {
    this.context = context;
    Compartment c = context;
    do {
      changedContexts.add(c);
      c = c.getContext();
    } while (c != Compartment.UNKNOWN);
    reactantVector.keySet()
                  .forEach(s -> usedCompartments.add(s.getContext()));
    productVector.keySet()
                 .forEach(s -> usedCompartments.add(s.getContext()));
    changedContexts.addAll(usedCompartments);

    this.reactantVector = reactantVector;
    this.productVector = productVector;
    this.propensity = propensity;
    this.simplePropensity = createSimpleExpression(propensity, env);
    if ( rule.getRate() instanceof DynamicNode){
      try{
        this.propensity = ((DynamicNode)rule.getRate()).cloneSelf();
        if (rule.getOriginalRate() instanceof IfThenElseNode) {
          IfThenElseNode iten = (IfThenElseNode) rule.getOriginalRate();
          if (!((ValueNode<Boolean>) iten.getCondition()
                  .calc(env)).getValue()) {
            if (!containsSpeciesAmountNode(iten.getCondition()) && iten.getElseStmt() instanceof ValueNode<?>
                    && ((ValueNode<Double>) iten.getElseStmt()).getValue()
                    .equals(0D)) {
              possible = false;
            }
          }
        }
      }catch (Exception e){
        System.out.println("Warning: failed to clone Dynamic Node");
        this.propensity = rule.getRate();
      }
    }else {
      this.propensity = rule.getRate();
    }
    this.rule = rule;
    this.env = env;

    this.changeVectorNonZero = new IdentityHashMap<>();
    changeVectorNonZero.putAll(productVector);
    reactantVector.entrySet()
                  .forEach(e -> changeVectorNonZero.merge(e.getKey(), -e.getValue(), Integer::sum));
    changeVectorNonZero.values()
                       .removeIf(v -> v == 0);

    update();

    updateMaxFiringNumber();
    this.order = computeOrder();
  }

  public Node getPropensity() {
    return propensity;
  }

  public Compartment getContext() {
    return context;
  }

  public MLEnvironment getEnv() {
    return env;
  }

  /**
   * Iterate over the change vector and update all species. Return a set of all
   * compartments that contain changed species or compartments with changed
   * species etc.
   */
  public Set<Compartment> execute() {
    for (Entry<LeafSpecies, Integer> e : changeVectorNonZero.entrySet()) {
      e.getKey()
       .changeAmount(e.getValue());
    }

    return changedContexts;
  }

  /**
   * Return true if the reaction can be executed. It can happen that a reaction
   * is not executable due to violated amount conditions.
   */
  public boolean executable() {
    return changeVectorNonZero.entrySet()
                              .stream()
                              .allMatch(e -> Double.compare(e.getKey()
                                                             .getAmount(), -1 * e.getValue()) >= 0);
  }

  /**
   * Check whether the reaction is executable assuming species concentrations,
   * i.e., even 0.x species amount can activate a reaction.
   */
  public boolean executableConcetration() {
    return reactantVector.keySet().stream().allMatch(s -> Double.compare(s.getAmount(),0D) > 0);
  }

  public Double getCalculatedPropensity() {
    return Math.max(0, calculatedPropensity);
  }

  public void update() {
    if (executable()) {
      updateAlways();
    } else {
      calculatedPropensity = 0D;
    }
  }

  public void updateAlways() {
    if (simplePropensity != null) {
      calculatedPropensity = simplePropensity.calc();
    } else {
      env.resetEvaluatedRestSolution();
      calculatedPropensity = NodeHelper.getDouble(propensity, env);
    }
  }

  public void updateConcentration() {
    //if (executableConcetration()) {
      updateAlways();
    /*} else {
      calculatedPropensity = 0D;
    }*/
  }

  public SimpleRule getRule() {
    return rule;
  }

  public Map<LeafSpecies, Integer> getReactantVector() {
    return reactantVector;
  }

  public Map<LeafSpecies, Integer> getProductVector() {
    return productVector;
  }

  public boolean isPossible() {
    return possible;
  }

  public Map<LeafSpecies, Integer> getChangeVectorNonZero() {
    return changeVectorNonZero;
  }

  @Override
  public String toString() {
    return changeVectorNonZero.toString() + "@ " + propensity.toString();
  }

  private int computeOrder() {
    return reactantVector.values()
                         .stream()
                         .reduce(0, Integer::sum);
  }

  public int getOrder() {
    return order;
  }

  public int updateMaxFiringNumber() {
    if (reactantVector.keySet()
                      .stream()
                      .anyMatch(s -> Double.compare(s.getAmount(), 0D) == 0)) {
      maxFiringNumber = 0;
    } else {
      maxFiringNumber = changeVectorNonZero.entrySet()
                                           .stream()
                                           .filter(e -> e.getValue() < 0)
                                           .map(e -> (int) e.getKey()
                                                            .getAmount() / -e.getValue())
                                           .min(Integer::compare)
                                           .orElse(Integer.MAX_VALUE);
    }
    return maxFiringNumber;
  }

  public int getMaxFiringNumber() {
    return maxFiringNumber;
  }

  private double vectorMin(Map<LeafSpecies, Integer> vector) {
    return vector.keySet()
                 .stream()
                 .map(LeafSpecies::getAmount)
                 .min(Double::compare)
                 .orElse(Double.POSITIVE_INFINITY);
  }

  /**
   * Return the minimum of all reactant and product leaf species.
   */
  public double getMinimumSpeciesAmount() {
    return Math.min(vectorMin(productVector), vectorMin(reactantVector));
  }

  public Set<Compartment> getUsedCompartments() {
    return usedCompartments;
  }

}
