/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.simple;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.util.Assignment;

import java.util.List;

/**
 * In contrast to normal rules, simple rules do not allow to change attributes
 * of compartments, to create or remove compartments, to use functions to create
 * product species, and to use functions to calculate attribute values of
 * species.
 *
 * @author Tobias Helms
 */
public class SimpleRule {

    private final List<Reactant> reactants;

    private final List<Reactant> products;

    private final Node rate;

    private final Node originalRate;

    private final List<Assignment> assignments;

    public SimpleRule(List<Reactant> reactants, List<Reactant> products, Node rate, List<Assignment> assignments) {
        this(reactants, products, rate, null, assignments);
    }

    public SimpleRule(List<Reactant> reactants, List<Reactant> products, Node rate, Node originalRate, List<Assignment> assignments) {
        this.reactants = reactants;
        this.products = products;
        this.rate = rate;
        this.originalRate = originalRate;
        this.assignments = assignments;
    }

    public String toString() {
        return reactants.toString() + " -> " + products.toString() + "@" + rate + " where " + assignments.toString();
    }

    public List<Reactant> getReactants() {
        return reactants;
    }

    public List<Reactant> getProducts() {
        return products;
    }

    public Node getRate() {
        return rate;
    }

    public Node getOriginalRate() {
        return originalRate;
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }

}
