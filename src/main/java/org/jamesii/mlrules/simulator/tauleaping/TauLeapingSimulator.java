/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.tauleaping;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.distributions.PoissonDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.parser.nodes.MLRulesAddNode;
import org.jamesii.mlrules.parser.nodes.SpeciesPatternNode;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.simple.SimpleReaction;
import org.jamesii.mlrules.simulator.simple.SimpleReactionCreator;
import org.jamesii.mlrules.simulator.simple.SimpleRule;
import org.jamesii.mlrules.simulator.simple.StaticRule;
import org.jamesii.mlrules.simulator.standard.ChangedSpecies;
import org.jamesii.mlrules.simulator.standard.ContextMatchings;
import org.jamesii.mlrules.simulator.standard.Reaction;
import org.jamesii.mlrules.simulator.standard.ReactionCreator;
import org.jamesii.mlrules.util.DoubleNumber;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.RestSolution;
import org.jamesii.mlrules.util.RestSolutionStatic;

import java.util.*;
import java.util.stream.Collectors;


/**
 * A tau-leaping simulator for ML-Rules.
 *
 * @author Tobias Helms
 */
public class TauLeapingSimulator extends Simulator {

  private final ExponentialDistribution expDist;

  private final PoissonDistribution poisDist;

  private final ReactionCreator complexCreator = new ReactionCreator() {
    @Override
    protected boolean validAmount(Species species, Tree matching, Reactant reactant, MLEnvironment env) {
      return true;
    }

    @Override
    protected Reaction createReaction(ContextMatchings cm, Rule rule) {
      return new TauReaction(cm, rule);
    }

    @Override
    protected RestSolution createRestSolution(String name, Compartment context, Map<Species, Integer> removals) {
      return new RestSolutionStatic(name, context, removals);
    }
  };

  private final SimpleReactionCreator simpleCreator = new SimpleReactionCreator();

  private final Map<Compartment, List<Reaction>> complexReactions = new HashMap<>();

  private final Map<Compartment, List<SimpleReaction>> simpleReactions = new HashMap<>();

  private List<Rule> complexRules = new ArrayList<>();

  private List<SimpleRule> simpleRules = new ArrayList<>();

  private Map<Reactant, Reactant> reactantProducts = new HashMap<>();

  private final List<TimedRule> timedRules;

  private final TauComputation tauComputation;

  private Set<SimpleReaction> nonCritical = new HashSet<>();

  private Set<SimpleReaction> critical = new HashSet<>();

  private Map<LeafSpecies, List<SimpleReaction>> speciesReactions = new IdentityHashMap<>();

  private final double ALPHA;

  private final int NC;

  private boolean ssaMode = true;

  private int remainingSSASteps;

  private final int SSA_STEPS;

  private int ssaStepCounter = 0;

  private int tauStepCounter = 0;

  private int tauFiringCounter = 0;

  /**
   * Enum for individual reaction type during a tau leap
   */
  private enum IR {
    no, complex, timed;
  }

  private static void checkReactant(Reactant reactant) {
    if (!(reactant.getAmount() instanceof ValueNode<?>)) {
      throw new IllegalArgumentException("amount expressions are not allowed within reactants and products");
    }
    reactant.getSubReactants()
            .forEach(TauLeapingSimulator::checkReactant);
  }

  private void createReactants(MLRulesAddNode node, Set<String> variables, List<Reactant> reactants) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getLeft()).toReactant(getModel().getEnv());
      checkReactant(reactant);
      reactants.add(reactant);
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      Reactant reactant = ((SpeciesPatternNode) node.getRight()).toReactant(getModel().getEnv());
      checkReactant(reactant);
      reactants.add(reactant);
    } else if (node.getRight() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getRight(), variables, reactants);
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
  }

  private boolean emptySpecies(Node node) {
    if (node instanceof ValueNode<?>) {
      ValueNode<?> vn = (ValueNode<?>) node;
      if (vn.getValue() instanceof Map<?, ?>) {
        Map<?, ?> map = (Map<?, ?>) vn.getValue();
        return map.isEmpty();
      }
    }
    return false;
  }

  private List<Reactant> checkProduct(Node node, Set<String> variables) {
    List<Reactant> products = new ArrayList<>();
    if (node instanceof SpeciesPatternNode) {
      SpeciesPatternNode spn = (SpeciesPatternNode) node;
      Reactant product = spn.toReactant(getModel().getEnv());
      checkReactant(product);
      products.add(product);
    } else if (node instanceof MLRulesAddNode) {
      MLRulesAddNode addNode = (MLRulesAddNode) node;
      createReactants(addNode, variables, products);
    } else if (emptySpecies(node)) {
      // do nothing;
    } else {
      throw new IllegalArgumentException("only species are allowed in the products");
    }
    if (products.stream()
                .anyMatch(p -> p.getBoundTo()
                                .isPresent())) {
      throw new IllegalArgumentException("product species cannot be bound to variables");
    }

    return products;
  }

  private Optional<SimpleRule> getSimple(Rule rule, Map<Reactant, Reactant> reactantProducts) {
    try {
      Set<String> variables = new HashSet<>();
      for (Reactant reactant : rule.getReactants()) {
        checkReactant(reactant);
      }
      List<Reactant> products = checkProduct(rule.getProduct(), variables);

      if (rule.getAssignments()
              .stream()
              .anyMatch(a -> a.getNames()
                              .stream()
                              .anyMatch(n -> variables.contains(n)))) {
        throw new IllegalArgumentException(
                "Variables defined within the where part of a rule are not allowed to be used within the reactants or products.");
      }

      if (!StaticRule.isValidTauRule(rule.getReactants(), products, reactantProducts)) {
        throw new IllegalArgumentException("rules must not change the structure of the model");
      }

      return Optional.of(new SimpleRule(rule.getReactants(), products, rule.getRate(), rule.getOriginalRate(), rule.getAssignments()));
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  public void divideRules(List<Rule> rules) {
    for (Rule rule : rules) {
      Optional<SimpleRule> sr = getSimple(rule, reactantProducts);
      if (sr.isPresent()) {
        simpleRules.add(sr.get());
      } else {
        complexRules.add(rule);
      }
    }
  }

  public TauLeapingSimulator(Model model, double epsilon, double alpha, int nc, int ssaSteps) {
    super(model);

    ALPHA = alpha;
    NC = nc;
    SSA_STEPS = ssaSteps;
    remainingSSASteps = SSA_STEPS;

    tauComputation = new TauComputation(epsilon);

    divideRules(model.getRules()
                     .getRules());

    timedRules = new ArrayList<>(model.getRules()
                                      .getTimedRules());
    timedRules.sort(RuleComparator.instance);

    simpleReactions.clear();
    simpleCreator.createInitialReactions(model.getSpecies(), simpleRules, simpleReactions, Collections.emptyList(),
            model.getEnv(), reactantProducts);
    updateSpeciesReactionsMap(simpleReactions.values()
                                             .stream()
                                             .flatMap(Collection::stream)
                                             .collect(Collectors.toList()));

    expDist = new ExponentialDistribution((IRandom) model.getEnv()
                                                         .getValue(Model.RNG));
    poisDist = new PoissonDistribution((IRandom) model.getEnv()
                                                      .getValue(Model.RNG));

    complexCreator.createInitialReactions(model.getSpecies(), complexRules, complexReactions, Collections.emptyList(),
            model.getEnv(), true);
  }

  private void updateSpeciesReactionsMap(List<SimpleReaction> sr) {
    for (SimpleReaction r : sr) {
      int f = r.updateMaxFiringNumber();
      if (f < NC) {
        critical.add(r);
      } else {
        nonCritical.add(r);
      }
      r.getReactantVector()
       .keySet()
       .forEach(s -> speciesReactions.computeIfAbsent(s, sp -> new ArrayList<>())
                                     .add(r));
    }
  }

  private Optional<Reaction> select(Map<Compartment, List<Reaction>> reactions, double rateSum, double pivot) {
    DoubleNumber sum = new DoubleNumber(0D);
    return reactions.values()
                    .stream()
                    .flatMap(Collection::stream)
                    .filter(r -> sum.addAndGet(r.getRate()) > pivot)
                    .findFirst();
  }

  private Optional<SimpleReaction> select(Set<SimpleReaction> sr, double rateSum, double pivot) {
    DoubleNumber sum = new DoubleNumber(0D);
    return sr.stream()
             .filter(r -> sum.addAndGet(r.getCalculatedPropensity()) > pivot)
             .findFirst();

  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
                                 Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getContextMatchings()
            .getMatchings()
            .stream()
            .anyMatch(m -> removedSpecies.contains(m.getSpecies())
                    || changedSpecies.getOrDefault(m.getSpecies()
                                                    .getType(), Collections.emptySet())
                                     .contains(m.getSpecies()));
  }

  private boolean removeSimpleReaction(SimpleReaction r, List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getUsedCompartments()
            .stream()
            .anyMatch(removedSpecies::contains) || r.getContext()
                                                    .getContextStream()
                                                    .anyMatch(c -> changedSpecies.getOrDefault(c.getType(), Collections.emptySet())
                                                                                 .contains(c));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    for (Species s : changed.getRemovedSpecies()) {
      if (s instanceof Compartment) {
        Compartment c = (Compartment) s;
        c.getSubCompartmentsRecursiveStream()
         .forEach(sub -> {
           complexReactions.remove(sub);
           simpleReactions.remove(sub);
         });
      }
    }
  }

  private void removeReactions(List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    complexReactions.getOrDefault(s, Collections.emptyList())
                    .removeIf(r -> removeReaction(r, removedSpecies, changedSpecies));

    Iterator<SimpleReaction> i = simpleReactions.getOrDefault(s, Collections.emptyList())
                                                .iterator();
    while (i.hasNext()) {
      SimpleReaction r = i.next();
      if (removeSimpleReaction(r, removedSpecies, changedSpecies)) {
        i.remove();
        r.getReactantVector()
         .keySet()
         .forEach(reactant -> speciesReactions.getOrDefault(reactant, Collections.emptyList())
                                              .remove(r));
        if (r.getMaxFiringNumber() < NC) {
          critical.remove(r);
        } else {
          nonCritical.remove(r);
        }
      }
    }
  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    Set<Species> current = new HashSet<>();
    for (Compartment c : selected.getContextMatchings()
                                 .getContext()
                                 .getContextStream()
                                 .collect(Collectors.toList())) {
      Map<SpeciesType, Set<Species>> changedSpecies;

      if (c == selected.getContextMatchings()
                       .getContext()) {
        changedSpecies = changed.getAllChangedSpecies();
      } else {
        changedSpecies = new HashMap<>();
        changedSpecies.put(current.iterator()
                                  .next()
                                  .getType(), current);
      }
      removeReactions(changed.getRemovedSpecies(), changedSpecies, c);
      complexCreator.createReactions(c, changedSpecies, complexRules, complexReactions, Collections.emptyList(),
              getModel().getEnv(), true);
      List<SimpleReaction> sr = simpleCreator.createReactions(c, changedSpecies, simpleRules, simpleReactions, Collections.emptyList(), getModel().getEnv(),
              reactantProducts);
      updateSpeciesReactionsMap(sr);
      current = new HashSet<>();
      current.add(c);
    }
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies()
           .stream()
           .filter(s -> s instanceof Compartment)
           .map(s -> (Compartment) s)
           .forEach(c -> c.getSubCompartmentsRecursiveStream()
                          .forEach(sub -> {
                            complexCreator.createReactions(sub, null, complexRules, complexReactions, Collections.emptyList(),
                                    getModel().getEnv(), true);
                            List<SimpleReaction> sr = simpleCreator.createReactions(sub, null, simpleRules, simpleReactions, Collections.emptyList(), getModel().getEnv(),
                                    reactantProducts);
                            updateSpeciesReactionsMap(sr);
                          }));
  }

  private void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
  }

  private void executeReaction(Reaction r) {
    ChangedSpecies changed = r.execute();
    getModel().getEnv()
              .setGlobalValue(Model.TIME, getNextTime());
    updateReactions(changed, r);
  }

  private void executeReaction(SimpleReaction sr) {
    sr.execute();
    getModel().getEnv()
              .setGlobalValue(Model.TIME, getNextTime());
    simpleReactions.values()
                   .stream()
                   .flatMap(Collection::stream)
                   .forEach(SimpleReaction::update);
    complexReactions.values()
                    .stream()
                    .flatMap(Collection::stream)
                    .forEach(Reaction::update);

    for (LeafSpecies s : sr.getChangeVectorNonZero()
                           .keySet()) {
      for (SimpleReaction r : speciesReactions.getOrDefault(s, Collections.emptyList())) {
        int fOld = r.getMaxFiringNumber();
        int fNew = r.updateMaxFiringNumber();
        if (fOld < NC && fNew >= NC) {
          critical.remove(r);
          nonCritical.add(r);
        } else if (fOld >= NC && fNew < NC) {
          critical.add(r);
          nonCritical.remove(r);
        }
      }
    }
  }

  private void executeIndividualReaction(Set<SimpleReaction> sr, double simplePropSum, Map<Compartment, List<Reaction>> cr, double complexPropSum) {
    double pivot = ((IRandom) getModel().getEnv()
                                        .getValue(Model.RNG)).nextDouble() * (simplePropSum + complexPropSum);

    if (pivot > simplePropSum) {
      Optional<Reaction> selected = select(cr, complexPropSum, pivot - simplePropSum);
      if (selected.isPresent()) {
        executeReaction(selected.get());
      } else if (timedRules.isEmpty()) {
        throw new NoReactionSelectedException();
      }
    } else {
      Optional<SimpleReaction> selected = select(sr, simplePropSum, pivot);
      if (selected.isPresent()) {
        executeReaction(selected.get());
      } else if (timedRules.isEmpty()) {
        throw new NoReactionSelectedException();
      }
    }

  }

  private Optional<Reaction> selectTimedReaction() {
    Map<Compartment, List<Reaction>> timedReactions = new HashMap<>();
    double time = timedRules.get(0)
                            .getNextTime();
    List<Rule> current = new ArrayList<>();

    for (TimedRule r : timedRules) {
      if (r.getNextTime() > time) {
        break;
      }
      current.add(r.getRule());
      r.updateNextTime();
    }
    timedRules.sort(RuleComparator.instance);

    complexCreator.createInitialReactions(getModel().getSpecies(), current, timedReactions, new ArrayList<>(),
            getModel().getEnv(), false);
    return getRandomElement(timedReactions);
  }

  /**
   * Return a random reaction of the given reactions without creating an
   * additional data structure containing all reactions (i.e. not flatmapping
   * them first).
   */
  private Optional<Reaction> getRandomElement(Map<Compartment, List<Reaction>> reactions) {
    if (reactions.isEmpty()) {
      return Optional.empty();
    }
    IRandom rng = (IRandom) getModel().getEnv()
                                      .getValue(Model.RNG);
    int i = rng.nextInt(reactions.size());
    int j = 0;
    for (List<Reaction> r : reactions.values()) {
      if (i == j) {
        return Optional.of(r.get(rng.nextInt(r.size())));
      }
      ++j;
    }
    return Optional.empty();
  }

  private double nextTimedReaction() {
    if (!timedRules.isEmpty()) {
      return timedRules.get(0)
                       .getNextTime();
    }
    return Double.POSITIVE_INFINITY;
  }

  private Optional<Reaction> checkTimedReactions() {
    double time = nextTimedReaction();
    if (!timedRules.isEmpty() && Double.compare(time, getNextTime()) <= 0) {
      Optional<Reaction> timedReaction = selectTimedReaction();
      if (timedReaction.isPresent()) {
        setNextTime(time);
        return timedReaction;
      }
    }
    return Optional.empty();
  }

  /**
   * Execute the given simple reaction n times.
   */
  private void executeLeapForSimpleReaction(SimpleReaction sr, int n, Set<LeafSpecies> smallSpecies) {
    tauFiringCounter += n;
    for (Map.Entry<LeafSpecies, Integer> e : sr.getChangeVectorNonZero()
                                               .entrySet()) {
      e.getKey()
       .changeAmount(e.getValue() * n);
      if (e.getKey()
           .getAmount() < NC) {
        smallSpecies.add(e.getKey());
      }
    }
  }

  private double calcPropSumComplex() {
    return complexReactions.values()
                           .stream()
                           .flatMap(Collection::stream)
                           .map(Reaction::getRate)
                           .reduce(0D, Double::sum);
  }

  private void executeTauStep() {

    double tau = tauComputation.computeTau(nonCritical);
    double propSumNonCritical = nonCritical.stream()
                                           .map(SimpleReaction::getCalculatedPropensity)
                                           .reduce(0D, Double::sum);
    if (tau < ALPHA / propSumNonCritical) {
      ssaMode = true;
      ++ssaStepCounter;
      executeSSAStep();
    } else {
      double propSumCritical = critical.stream()
                                       .map(SimpleReaction::getCalculatedPropensity)
                                       .reduce(0D, Double::sum);
      double propSumComplex = calcPropSumComplex();


      double tauCritical = expDist.getRandomNumber(1.0 / (propSumCritical + propSumComplex));
      IR ir = IR.no;
      double nextTimed = nextTimedReaction() - getCurrentTime();
      if (tau > tauCritical && tauCritical < nextTimed) {
        ir = IR.complex;
        tau = tauCritical;
      } else if (tau > nextTimed) {
        ir = IR.timed;
        tau = nextTimed;
      }

      setNextTime(getCurrentTime() + tau);
      getObserver().forEach(o -> o.update(this));

      Set<LeafSpecies> smallSpecies = Collections.newSetFromMap(new IdentityHashMap<>());
      for (SimpleReaction r : nonCritical) {
        poisDist.setLambda(r.getCalculatedPropensity() * tau);
        int numOfFirings = Double.valueOf(poisDist.getRandomNumber())
                                 .intValue();
        if (numOfFirings > 0) {
          executeLeapForSimpleReaction(r, numOfFirings, smallSpecies);
        }
      }

      switch (ir) {
        case no:
          nonCritical.forEach(SimpleReaction::updateAlways);
          critical.forEach(SimpleReaction::update);
          complexReactions.values()
                          .stream()
                          .flatMap(Collection::stream)
                          .forEach(Reaction::update);
          break;
        case complex:
          executeIndividualReaction(critical, propSumCritical, complexReactions, propSumComplex);
          break;
        case timed:
          executeReaction(selectTimedReaction().get());
          break;
        default:
          throw new IllegalArgumentException();
      }

      for (LeafSpecies s : smallSpecies) {
        for (SimpleReaction r : speciesReactions.getOrDefault(s, Collections.emptyList())) {
          int fOld = r.getMaxFiringNumber();
          int fNew = r.updateMaxFiringNumber();
          if (fOld < NC && fNew >= NC) {
            critical.remove(r);
            nonCritical.add(r);
          } else if (fOld >= NC && fNew < NC) {
            critical.add(r);
            nonCritical.remove(r);
          }
        }
      }

      setCurrentTime(getNextTime());
      setSteps(getSteps() + 1);
    }

  }

  private void executeSSAStep() {
    --remainingSSASteps;
    Set<SimpleReaction> sr = simpleReactions.values()
                                            .stream()
                                            .flatMap(Collection::stream)
                                            .collect(Collectors.toSet());
    double propSum = sr.stream()
                       .map(SimpleReaction::getCalculatedPropensity)
                       .reduce(0D, Double::sum);
    double propSumComplex = calcPropSumComplex();

    double timeAdvance = expDist.getRandomNumber(1.0 / (propSum + propSumComplex));

    setNextTime(getCurrentTime() + timeAdvance);
    Optional<Reaction> timed = checkTimedReactions();
    getObserver().forEach(o -> o.update(this));
    if (timed.isPresent()) {
      executeReaction(timed.get());
    } else {
      try {
        executeIndividualReaction(sr, propSum, complexReactions, propSumComplex);
        setSteps(getSteps() + 1);
        setCurrentTime(getNextTime());

      } catch (NoReactionSelectedException e) {
        setNextTime(getObserver().stream()
                                 .map(Observer::nextObservationPoint)
                                 .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY))
                                 .min()
                                 .orElse(Double.POSITIVE_INFINITY));
      }
    }
  }

  @Override
  public void nextStep() {
    if (remainingSSASteps == 0) {
      remainingSSASteps = SSA_STEPS;
      ssaMode = false;
    }

    if (ssaMode) {
      ++ssaStepCounter;
      executeSSAStep();
    } else {
      ++tauStepCounter;
      executeTauStep();
    }

  }

  public int getSsaStepCounter() {
    return ssaStepCounter;
  }

  public int getTauStepCounter() {
    return tauStepCounter;
  }

  public int getTauFiringCounter() {
    return tauFiringCounter;
  }

}
