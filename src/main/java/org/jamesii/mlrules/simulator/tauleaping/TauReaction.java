package org.jamesii.mlrules.simulator.tauleaping;

import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.simulator.standard.ContextMatchings;
import org.jamesii.mlrules.simulator.standard.Matching;
import org.jamesii.mlrules.simulator.standard.Reaction;
import org.jamesii.mlrules.util.NodeHelper;

import java.util.List;
import java.util.Set;

/**
 * In contrast to a {@link Reaction}, a {@link TauReaction} does not remove
 * {@link LeafSpecies} when the amount becomes zero.
 */
public class TauReaction extends Reaction {

  public TauReaction(ContextMatchings matchings, Rule rule) {
    super(matchings, rule);
  }

  protected void removeSpecies(Compartment context, List<Species> removedSpecies, Set<Species> changedSpecies) {
    for (Matching m : getContextMatchings().getMatchings()) {
      if (m.getSpecies() instanceof LeafSpecies) {
        LeafSpecies ls = (LeafSpecies) m.getSpecies();
        ls.setAmount(ls.getAmount() - NodeHelper.getDouble(m.getReactant()
                                                            .getAmount(), m.getEnv()));
        changedSpecies.add(m.getSpecies());
      } else {
        context.remove(m.getSpecies());
        removedSpecies.add(m.getSpecies());
      }
    }
  }

}
