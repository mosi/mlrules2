package org.jamesii.mlrules.simulator.tauleaping;

import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.simulator.simple.SimpleReaction;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This tau computation uses the method described in <a
 * href="http://http://dx.doi.org/10.1063/1.2159468"
 * >"Efficient step size selection for the tau-leaping simulation method"</a>
 * for the tau computation. Thus, the computed tau depends on the dynamics on
 * the system and it is chosen so that the leap condition is fulfilled, i.e. the
 * changes of the reaction propensities are sufficiently low during the leaps.
 *
 * @author Tobias Helms
 */
public class TauComputation {

  private class SpeciesData {
    private double amount;
    private double mean;
    private double variance;
    private int order;
    private int maxAmount;
  }

  /**
   * The error parameter epsilon.
   */
  private double epsilon;

  public TauComputation(double epsilon) {
    this.epsilon = epsilon;
  }

  /**
   * Compute the tau value for the given species.
   */
  private double computeSpeciesTau(double amount, double g, double mean,
                                   double variance) {
    double enumerator = Math.max(epsilon * amount / g, 1D);
    return Math.min(enumerator / Math.abs(mean), Math.pow(enumerator, 2D)
            / Math.abs(variance));
  }

  /**
   * Compute the tau values for all species and return the minimum of them.
   */
  public double computeTau(Set<SimpleReaction> reactions) {
    Map<LeafSpecies, SpeciesData> speciesData = new IdentityHashMap<>();
    reactions.forEach(r -> computeSpeciesData(speciesData, r));

    double tau = Double.POSITIVE_INFINITY;
    for (SpeciesData sd : speciesData.values()) {
      double speciesTau = computeSpeciesTau(sd.amount, computeG(sd.amount, sd.order, sd.maxAmount), sd.mean, sd.variance);
      tau = Math.min(tau, speciesTau);
    }

    return tau;
  }

  /**
   * Check firstly if the reaction's order is higher than the previous values
   * and secondly if max amount is greater (only if this reaction has the
   * maximum order)
   */
  private void computeSpeciesData(
          Map<LeafSpecies, SpeciesData> data, SimpleReaction reaction) {
    int order = reaction.getOrder();
    for (Map.Entry<LeafSpecies, Integer> e : reaction.getReactantVector().entrySet()) {
      SpeciesData old = data.get(e.getKey());
      if (old == null) {
        SpeciesData sd = new SpeciesData();
        sd.order = order;
        sd.maxAmount = e.getValue();
        sd.amount = e.getKey().getAmount();
        sd.mean = e.getValue() * reaction.getCalculatedPropensity();
        sd.variance = e.getValue() * sd.mean;
        data.put(e.getKey(), sd);
      } else {
        old.mean += e.getValue() * reaction.getCalculatedPropensity();
        old.variance += e.getValue() * e.getValue() * reaction.getCalculatedPropensity();
        // only update amount for highest order reactions
        if (old.order < order) {
          old.order = order;
          if (old.maxAmount < e.getValue()) {
            old.maxAmount = e.getValue();
          }
        }
      }
    }
  }


  /**
   * Compute the g value for one species: g_i(x) = h(i) + (h(i)/n(i)) *
   * sum^(n(i)-1)_(j=1){j/(x_i-j)}
   * <p>
   * h(i) = highest order of reactions in which S_i appears as reactant<br>
   * n(i) = max number of S_i required by any of these reactions<br>
   * x = current state (x_i = amount of S_i)
   */
  private double computeG(double x, int h, int n) {
    if (n < 2) {
      return h;
    }

    double sumValue = 0;
    for (int j = 1; j < n; ++j) {
      sumValue += j / (x - j);
    }
    return h + (h / (double) n * sumValue);
  }

}
