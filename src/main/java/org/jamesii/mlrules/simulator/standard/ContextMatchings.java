/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.standard;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.NodeHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link ContextMatchings} contains all {@link Matching}s for a
 * {@link Reaction}, i.e., is a container for these matchings.
 * 
 * @author Tobias Helms
 * 
 */
public class ContextMatchings {

  private List<Matching> matchings = new ArrayList<>();

  private Compartment context;

  private MLEnvironment env;

  private Map<Species, Integer> computeRemovals() {
    Map<Species, Integer> removals = new HashMap<>();
    for (Matching m : matchings) {
      removals.compute(m.getSpecies(),
          (k, v) -> (v == null ? 0 : v) + NodeHelper.getInt(m.getReactant().getAmount(), env));
    }
    return removals;
  }

  public ContextMatchings(Compartment context, List<Matching> matchedReactants, MLEnvironment env) {
    this.context = context;
    this.matchings = matchedReactants;
    this.env = env;
  }

  public List<Matching> getMatchings() {
    return matchings;
  }

  public MLEnvironment getEnv() {
    return env;
  }

  public Compartment getContext() {
    return context;
  }

  public Map<Species, Integer> getRemovals() {
    return computeRemovals();
  }

}
