/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.standard;

import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * The {@link ReactionCreator} can be used for the {@link StandardSimulator} to
 * create {@link Reaction}s from {@link Rule}s.
 *
 * @author Tobias Helms
 */
public class ReactionCreator extends AbstractReactionCreator {

  protected Reaction createReaction(ContextMatchings cm, Rule rule) {
    return new Reaction(cm, rule);
  }

  private void createReactions(Compartment context, Map<SpeciesType, Set<Species>> changedSpecies, Rule rule,
                               List<Reaction> reactions, List<Reaction> infiniteReactions, MLEnvironment env,
                               boolean keepZeroPropensityReactions) {
    Stream.Builder<ContextMatchings> builder = Stream.builder();
    createContextMatchings(builder, context, changedSpecies, rule.getReactants(), env);
    builder.build()
           .forEach(cm -> {
             Reaction r = createReaction(cm, rule);
             if (Double.isInfinite(r.getRate())) {
               infiniteReactions.add(r);
             } else if (keepZeroPropensityReactions || r.getRate() > 0) {
               // } else {
               if (cm.getEnv().containsIdent("validCoords")) {
                 if ((Boolean)cm.getEnv().getValue("validCoords")) {
                   reactions.add(r);
                 }
               } else {
                 reactions.add(r);
               }
             }
           });
  }

  public void createReactions(Compartment species, Map<SpeciesType, Set<Species>> changedSpecies, List<Rule> rules,
                              Map<Compartment, List<Reaction>> reactions, List<Reaction> infiniteReactions, MLEnvironment env,
                              boolean keepZeroPropensityReactions) {
    List<Reaction> speciesReactions = reactions.computeIfAbsent(species, s -> new ArrayList<>());
    rules.forEach(r -> createReactions(species, changedSpecies, r, speciesReactions, infiniteReactions, env,
            keepZeroPropensityReactions));
    if (speciesReactions.isEmpty()) {
      reactions.remove(species);
    }
  }

  public void createInitialReactions(Compartment species, List<Rule> rules, Map<Compartment, List<Reaction>> reactions,
                                     List<Reaction> infiniteReactions, MLEnvironment env, boolean keepZeroPropensityReactions) {
    createReactions(species, null, rules, reactions, infiniteReactions, env, keepZeroPropensityReactions);
    species.getSubCompartmentsStream()
           .forEach(s -> createInitialReactions(s, rules, reactions, infiniteReactions, env, keepZeroPropensityReactions));
  }

}
