/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.standard;

import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.NodeHelper;

import java.util.List;

/**
 * A {@link Matching} is a concrete mapping of a reactant to a species entity
 * including {@link Matching}s for sub reactants.
 * 
 * @author Tobias Helms
 * 
 */
public class Matching {

  private final Species species;

  private final List<Matching> subMatchings;

  private final Reactant reactant;

  private final MLEnvironment env;

  /**
   * The concrete amount value of entities that are bound to this
   * {@link Matching}, i.e., if the {@link Reaction} of this mapping is
   * executed, {@link Matching#removal} species entities would be removed from the
   * context.
   */
  private final int removal;

  public Matching(Reactant reactant, Species species, List<Matching> subMatchings, MLEnvironment env) {
    this.reactant = reactant;
    this.species = species;
    this.subMatchings = subMatchings;
    this.env = env;
    this.removal = NodeHelper.getInt(reactant.getAmount(), env);
  }

  public Species getSpecies() {
    return species;
  }

  public Reactant getReactant() {
    return reactant;
  }

  public List<Matching> getSubReactants() {
    return subMatchings;
  }

  public MLEnvironment getEnv() {
    return env;
  }

  public int getRemoval() {
    return removal;
  }

}
