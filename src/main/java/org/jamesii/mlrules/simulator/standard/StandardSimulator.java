/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.standard;

import org.jamesii.core.math.random.distributions.ExponentialDistribution;
import org.jamesii.core.math.random.generators.IRandom;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.util.DoubleNumber;
import org.jamesii.mlrules.util.Pair;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@link StandardSimulator} realizes a basic stochastic simulation
 * algorithm (SSA) for ML-Rules. Additionally, it is possible to consider
 * {@link TimedRule}s, that are fired at concrete moments in time.
 *
 * @author Tobias Helms
 */
public class StandardSimulator extends Simulator {

  private final Map<Compartment, List<Reaction>> reactions = new HashMap<>();

  private final List<Reaction> infiniteReactions = new ArrayList<>();

  private final Map<Species, Double> rates = new HashMap<>();

  private final ExponentialDistribution expDist;

  private final List<TimedRule> timedRules;

  private final ReactionCreator creator = new ReactionCreator();

  private final boolean useDependencyGraph;

  public StandardSimulator(Model model, boolean useDependencyGraph) {
    super(model);
    this.useDependencyGraph = useDependencyGraph;
    expDist = new ExponentialDistribution((IRandom) model.getEnv()
                                                         .getValue(Model.RNG));

    timedRules = new ArrayList<>(model.getRules()
                                      .getTimedRules());
    timedRules.sort(RuleComparator.instance);

    creator.createInitialReactions(model.getSpecies(), model.getRules()
                                                            .getRules(), reactions, infiniteReactions,
            model.getEnv(), false);
    reactions.entrySet()
             .stream()
             .forEach(e -> rates.put(e.getKey(), e.getValue()
                                                  .stream()
                                                  .mapToDouble(r -> r.getRate())
                                                  .sum()));
    getObserver().forEach(o -> o.update(this));
  }

  private boolean removeReaction(Reaction r, List<Species> removedSpecies,
                                 Map<SpeciesType, Set<Species>> changedSpecies) {
    return r.getContextMatchings()
            .getMatchings()
            .stream()
            .anyMatch(m -> removedSpecies.contains(m.getSpecies())
                    || changedSpecies.getOrDefault(m.getSpecies()
                                                    .getType(), Collections.emptySet())
                                     .contains(m.getSpecies()));
  }

  private void removeReactions(List<Species> removedSpecies, Map<SpeciesType, Set<Species>> changedSpecies, Species s) {
    Iterator<Reaction> i;
    if (reactions.containsKey(s)) {
      i = reactions.get(s)
                   .iterator();
      while (i.hasNext()) {
        Reaction r = i.next();
        if (removeReaction(r, removedSpecies, changedSpecies)) {
          i.remove();
        }
      }
    }
    i = infiniteReactions.iterator();
    while (i.hasNext()) {
      Reaction r = i.next();
      if (removeReaction(r, removedSpecies, changedSpecies)) {
        i.remove();
      }
    }
  }

  private void removeSubReactionsFromCompartment(Compartment compartment) {
    reactions.remove(compartment);
    rates.remove(compartment);
    Iterator<Reaction> i = infiniteReactions.iterator();
    while (i.hasNext()) {
      Reaction r = i.next();
      if (r.getContextMatchings()
           .getContext() == compartment) {
        i.remove();
      }
    }

    compartment.getSubCompartmentsStream()
               .forEach(c -> removeSubReactionsFromCompartment(c));
  }

  private void removeSubReactions(ChangedSpecies changed) {
    changed.getRemovedSpecies()
           .stream()
           .filter(s -> s instanceof Compartment)
           .forEach(s -> removeSubReactionsFromCompartment((Compartment) s));
  }

  private void updateChangedSpeciesOfContext(Compartment context, Optional<Compartment> changedSubCompartment,
                                             ChangedSpecies changed, Reaction selected) {
    Map<SpeciesType, Set<Species>> changedSpecies;
    if (!changedSubCompartment.isPresent()) {
      changedSpecies = changed.getAllChangedSpecies();
    } else {
      changedSpecies = new HashMap<>();
      Set<Species> set = new HashSet<>();
      set.add(changedSubCompartment.get());
      changedSpecies.put(changedSubCompartment.get()
                                              .getType(), set);
    }
    removeReactions(changed.getRemovedSpecies(), changedSpecies, context);
    rates.remove(context);
    creator.createReactions(context, changedSpecies, getModel().getRules()
                                                               .getRules(), reactions, infiniteReactions,
            getModel().getEnv(), false);
    if (reactions.containsKey(context)) {
      rates.put(context, reactions.get(context)
                                  .stream()
                                  .mapToDouble(r -> r.getRate())
                                  .sum());
    }

    if (context.getContext() != Compartment.UNKNOWN) {
      updateChangedSpeciesOfContext(context.getContext(), Optional.of(context), changed, selected);
    }

  }

  private void updateChangedSpecies(ChangedSpecies changed, Reaction selected) {
    updateChangedSpeciesOfContext((Compartment) selected.getContextMatchings()
                                                        .getContext(), Optional.empty(), changed,
            selected);
  }

  private void addReactionsToAddedCompartment(Compartment compartment) {
    creator.createReactions(compartment, null, getModel().getRules()
                                                         .getRules(), reactions, infiniteReactions,
            getModel().getEnv(), false);
    if (reactions.containsKey(compartment)) {
      rates.put(compartment, reactions.get(compartment)
                                      .stream()
                                      .mapToDouble(r -> r.getRate())
                                      .sum());
    }

    compartment.getSubCompartmentsStream()
               .forEach(c -> addReactionsToAddedCompartment(c));
  }

  private void addReactionsToAddedSpecies(ChangedSpecies changed) {
    changed.getAddedSpecies()
           .stream()
           .filter(s -> s instanceof Compartment)
           .forEach(s -> addReactionsToAddedCompartment((Compartment) s));
  }

  protected void updateReactions(ChangedSpecies changed, Reaction selected) {
    removeSubReactions(changed);
    updateChangedSpecies(changed, selected);
    addReactionsToAddedSpecies(changed);
  }

  private Pair<Optional<Reaction>, Double> select(Map<Compartment, List<Reaction>> reactions, Map<Species, Double> rates) {
    if (!infiniteReactions.isEmpty()) {
      return new Pair<>(Optional.of(
              infiniteReactions.get(((IRandom) getModel().getEnv()
                                                         .getValue(Model.RNG)).nextInt(infiniteReactions.size()))),
              0D);
    }

    double rateSum = rates.values()
                          .stream()
                          .reduce(0.0, Double::sum);
    double pivot = ((IRandom) getModel().getEnv()
                                        .getValue(Model.RNG)).nextDouble() * rateSum;

    DoubleNumber sum = new DoubleNumber(0D);
    return new Pair<>(reactions.values()
                               .stream()
                               .flatMap(l -> l.stream())
                               .filter(r -> sum.addAndGet(r.getRate()) > pivot)
                               .findFirst(), rateSum);
  }

  private Optional<Reaction> selectTimedReaction() {
    Map<Compartment, List<Reaction>> timedReactions = new HashMap<>();
    double time = timedRules.get(0)
                            .getNextTime();
    List<Rule> current = new ArrayList<>();

    for (TimedRule r : timedRules) {
      if (r.getNextTime() > time) {
        break;
      }
      current.add(r.getRule());
      r.updateNextTime();
    }
    timedRules.sort(RuleComparator.instance);

    creator.createInitialReactions(getModel().getSpecies(), current, timedReactions, new ArrayList<>(),
            getModel().getEnv(), false);
    return getRandomElement(timedReactions);
  }

  /**
   * Return a random reaction of the given reactions without creating an
   * additional data structure containing all reactions (i.e. not flatmapping
   * them first).
   */
  private Optional<Reaction> getRandomElement(Map<Compartment, List<Reaction>> reactions) {
    if (reactions.isEmpty()) {
      return Optional.empty();
    }
    IRandom rng = (IRandom) getModel().getEnv()
                                      .getValue(Model.RNG);
    int i = rng.nextInt(reactions.size());
    int j = 0;
    for (List<Reaction> r : reactions.values()) {
      if (i == j) {
        return Optional.of(r.get(rng.nextInt(r.size())));
      }
      ++j;
    }
    return Optional.empty();
  }

  private Optional<Reaction> checkTimedReactions(Optional<Reaction> reaction) {
    if (!timedRules.isEmpty()) {
      double time = timedRules.get(0)
                              .getNextTime();
      if (Double.compare(time, getNextTime()) <= 0) {
        Optional<Reaction> timedReaction = selectTimedReaction();
        if (timedReaction.isPresent()) {
          setNextTime(time);
          return timedReaction;
        }
      }
    }
    return reaction;
  }

  private void logStepInfo() {
      int numberOfCompartments = getModel().getSpecies().countAllCompartments();
      int numberOfSpecies = getModel().getSpecies().countAllLeafSpecies();
      int numberOfReactions = (int) reactions.values().stream().flatMap(Collection::stream).count();
    double rateSum = rates.values()
                          .stream()
                          .reduce(0.0, Double::sum);
      StringJoiner joiner = new StringJoiner("\n", "","");
      joiner.add(String.format("#Step: %d, #Compartments: %d, #LeafSpecies: %d, #Reactions: %d, RateSum: %f", getSteps(), numberOfCompartments, numberOfSpecies, numberOfReactions, rateSum));
      joiner.add(String.format("State: %s", getModel().getSpecies().toString()));
      Logger.getGlobal().info(joiner.toString());
  }

  @Override
  public void nextStep() {
    if (Logger.getGlobal().isLoggable(Level.INFO)) {
      logStepInfo();
    }
    Pair<Optional<Reaction>, Double> selected = select(reactions, rates);
    Optional<Reaction> reaction = selected.fst();
    if (!reaction.isPresent()) {
      setNextTime(getObserver().stream()
                               .map(o -> o.nextObservationPoint())
                               .mapToDouble(p -> p.orElse(Double.POSITIVE_INFINITY))
                               .min()
                               .orElse(Double.POSITIVE_INFINITY));
    } else if (Double.compare(selected.snd(), 0D) != 0) {
      setNextTime(getCurrentTime() + expDist.getRandomNumber(1.0 / selected.snd()));
    }
    reaction = checkTimedReactions(reaction);
    getObserver().forEach(o -> o.update(this));
    if (reaction.isPresent()) {
      ChangedSpecies changed = reaction.get()
                                       .execute();

      getModel().getEnv()
                .setGlobalValue(Model.TIME, getNextTime());

      if (useDependencyGraph) {
        updateReactions(changed, reaction.get());
      } else {
        rates.clear();
        reactions.clear();
        creator.createInitialReactions(getModel().getSpecies(), getModel().getRules()
                                                                          .getRules(), reactions,
                infiniteReactions, getModel().getEnv(), false);
        reactions.entrySet()
                 .stream()
                 .forEach(e -> rates.put(e.getKey(), e.getValue()
                                                      .stream()
                                                      .mapToDouble(r -> r.getRate())
                                                      .sum()));
      }
    }
    setSteps(getSteps() + 1);
    setCurrentTime(getNextTime());
  }
}
