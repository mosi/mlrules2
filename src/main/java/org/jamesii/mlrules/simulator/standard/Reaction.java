/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.standard;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.Assignment;
import org.jamesii.mlrules.util.LazyInitialization;
import org.jamesii.mlrules.util.NodeHelper;
import org.jamesii.mlrules.util.runtimeCompiling.DynamicNode;

import java.util.*;

/**
 * A {@link Reaction} is an instantiation of a {@link Rule}, i.e., reactants are
 * assigned to concrete species entities and variables are assigned to concrete
 * values.
 * 
 * @author Tobias Helms
 *
 */
public class Reaction {

  private final ContextMatchings matchings;

  private final Rule rule;
  private Node rateNode;

  private double rate;

  public Reaction(ContextMatchings matchings, Rule rule) {
    this.matchings = matchings;
    if ( rule.getRate() instanceof DynamicNode){
      try{
        rateNode = ((DynamicNode)rule.getRate()).cloneSelf();
      }catch (Exception e){
        System.out.println("Warning: failed to clone Dynamic Node");
        rateNode = rule.getRate();
      }
    }else {
      rateNode = rule.getRate();
    }
    this.rule = rule;
    for (Assignment assign : rule.getAssignments()) {
      assign.getNames()
          .forEach(n -> matchings.getEnv().setValue(n, new LazyInitialization(assign, matchings.getEnv())));
    }

    update();
  }

  public ContextMatchings getContextMatchings() {
    return matchings;
  }

  public Rule getRule() {
    return rule;
  }

  public double getRate() {
    return rate;
  }

  private boolean checkSubMatchings(Matching m) {
    if (m.getSpecies() instanceof LeafSpecies) {
      LeafSpecies ls = (LeafSpecies) m.getSpecies();
      return (Double.compare(ls.getAmount() - NodeHelper.getDouble(m.getReactant()
                                                                .getAmount(), m.getEnv()), 0D) >= 0);
    } else {
      return m.getSubReactants().stream().allMatch(this::checkSubMatchings);
    }
  }

  /**
   * Also check whether the reaction is possible (0 rate in case it is not)
   */
  public void update() {
    if (matchings.getMatchings().stream().allMatch(this::checkSubMatchings)) {
      rate = NodeHelper.getDouble(rateNode, matchings.getEnv());
    } else {
      rate = 0D;
    }
  }

  public void updateRate() {
    rate = NodeHelper.getDouble(rateNode, matchings.getEnv());
  }

  protected void removeSpecies(Compartment context, List<Species> removedSpecies, Set<Species> changedSpecies) {
    for (Matching m : matchings.getMatchings()) {
      if (m.getSpecies() instanceof LeafSpecies) {
        LeafSpecies ls = (LeafSpecies) m.getSpecies();
        ls.setAmount(ls.getAmount() - NodeHelper.getDouble(m.getReactant().getAmount(), m.getEnv()));
        if (Double.compare(ls.getAmount(), 0D) <= 0) {
          context.remove(m.getSpecies());
          removedSpecies.add(m.getSpecies());
        } else {
          changedSpecies.add(m.getSpecies());
        }
      } else {
        context.remove(m.getSpecies());
        removedSpecies.add(m.getSpecies());
      }
    }
  }

  private void addSpecies(Compartment context, List<Species> addedSpecies, Set<Species> changedSpecies) {
    @SuppressWarnings("unchecked")
    Map<Species, Species> products = ((ValueNode<Map<Species, Species>>) rule.getProduct().calc(matchings.getEnv()))
        .getValue();

    for (Species s : products.keySet()) {
      s.setContext(context);
      if (s instanceof LeafSpecies) {
        Optional<Species> original = context.getSpecies(s);
        if (!original.isPresent()) {
          context.add(s);
          addedSpecies.add(s);
        } else {
          LeafSpecies ls = (LeafSpecies) s;
          ((LeafSpecies) original.get()).setAmount(original.get().getAmount() + ls.getAmount());
          if (!changedSpecies.contains(original.get())) {
            changedSpecies.add(original.get());
          }
        }
      } else {
        context.add(s);
        addedSpecies.add(s);
      }
    }
  }

  public ChangedSpecies execute() {
    Compartment context = matchings.getContext();

    List<Species> removedSpecies = new ArrayList<>();
    Set<Species> changedSpecies = new HashSet<>();
    List<Species> addedSpecies = new ArrayList<>();

    removeSpecies(context, removedSpecies, changedSpecies);
    addSpecies(context, addedSpecies, changedSpecies);

    return new ChangedSpecies(changedSpecies, addedSpecies, removedSpecies);
  }



}
