/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.simulator.standard;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.NodeHelper;
import org.jamesii.mlrules.util.RestSolution;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * The {@link AbstractReactionCreator} is able to compute all possible
 * {@link Reaction}s based on a given {@link Compartment} and a given set of
 * {@link Rule}s. <br>
 * <br>
 * <b>TODO</b> Tobias: Currently, {@link Stream}s and lambda functions are
 * somehow used excessively. Maybe it should be refactored.
 * 
 * @author Tobias Helms
 *
 */
public abstract class AbstractReactionCreator {

  /**
   * A {@link Tree} is a recursive structure containing the current
   * {@link Matching} of a {@link Reactant}, the parent {@link Tree}, and a
   * consumption map of all species including the consumption of the parent
   * tree.
   * 
   * @author Tobias Helms
   *
   */
  protected static class Tree {

    private final Matching current;

    private final Tree parent;

    private final Map<Species, Integer> consumption;

    private void getContexts(List<Matching> result) {
      if (parent != null) {
        result.add(current);
        parent.getContexts(result);
      }
    }

    public Tree(Matching current, Tree parent) {
      this.current = current;
      this.parent = parent;
      consumption = new HashMap<>(parent != null ? parent.getConsumption() : Collections.emptyMap());
      if (current != null) {
        consumption.compute(current.getSpecies(), (k, v) -> (v == null ? 0 : v) + current.getRemoval());
      }
    }

    public Matching getCurrent() {
      return current;
    }

    public List<Matching> create() {
      List<Matching> result = new ArrayList<>();
      getContexts(result);
      return result;
    }

    public Map<Species, Integer> getConsumption() {
      return consumption;
    }

  }

  protected List<Object> calcAttributeValues(Reactant reactant, MLEnvironment env) {
    try {
      List<Object> values = new ArrayList<>(reactant.getType().getAttributesSize());
      for (int i = 0; i < reactant.getType().getAttributesSize(); ++i) {
        if (reactant.getAttributeNodes().get(i) instanceof Identifier<?>) {
          @SuppressWarnings("unchecked")
          String var = ((Identifier<String>) reactant.getAttributeNodes().get(i)).getIdent();
          Object o = env.getValue(var);
          if (o != null) {
            values.add(o);
          } else {
            values.add(reactant.getAttributeNodes().get(i));
          }
        } else {
          Node node = reactant.getAttributeNodes().get(i).calc(env);
          if (node instanceof ValueNode<?>) {
            values.add(((ValueNode<?>) node).getValue());
          }
        }
      }
      return values;
    } catch (Exception e) {
      Logger.getGlobal().info(String.format("Could not compute attribute values of %s", reactant));
    }
    return Collections.emptyList();
  }

  /**
   * Check the amount condition of the given reactant species.
   */
  protected boolean validAmount(Species species, Tree matching, Reactant reactant, MLEnvironment env) {
    return Double.compare(species.getAmount(),
        matching.getConsumption().getOrDefault(species, 0) + NodeHelper.getDouble(reactant.getAmount(), env)) >= 0;
  }

  protected boolean checkAttribute(Node attNode, Object left, Object right) {
    return left.equals(right);
  }

  /**
   * Return true if the species type and the attributes of the given species
   * match the type and the attributes of the given reactant. Sub reactants are
   * not checked in this method.
   */
  private boolean match(Species species, Reactant reactant, List<Object> reactantValues, Tree matching,
      MLEnvironment env) {
    if (species.getType().getAttributesSize() != reactantValues.size()) {
      return false;
    }
    for (int i = 0; i < species.getType().getAttributesSize(); ++i) {
      if (reactantValues.get(i) instanceof Identifier<?>) {
        @SuppressWarnings("unchecked")
        String var = ((Identifier<String>) reactant.getAttributeNodes().get(i)).getIdent();
        env.setValue(var, species.getAttribute(i));
        continue;
      }
      if (!checkAttribute(reactant.getAttributeNodes().get(i), species.getAttribute(i), reactantValues.get(i))) {
        return false;
      }
    }
    if (!validAmount(species, matching, reactant, env)) {
      return false;
    }

    if (reactant.getBoundTo().isPresent()) {
      /*Map<Species, Species> map = new HashMap<>();
      map.put(species, species);
      env.setValue(reactant.getBoundTo().get(), map);*/
      env.setValue(reactant.getBoundTo().get(), species);
    }
    return true;
  }

  protected RestSolution createRestSolution(String name, Compartment context, Map<Species, Integer> removals) {
    return new RestSolution(name, context, removals);
  }

  protected void addMatching(Stream.Builder<Matching> builder, Species species, Reactant reactant, Tree matchings,
      List<Object> attributeValues, MLEnvironment env) {
    MLEnvironment e = env.copy();
    if (match(species, reactant, attributeValues, matchings, e)) {
      if (!reactant.getSubReactants().isEmpty()) {
        Stream.Builder<ContextMatchings> builder2 = Stream.builder();
        createContextMatchings(builder2, (Compartment) species, null, reactant.getSubReactants(), e);
        builder2.build().forEach(cm -> {
          if (reactant.getRest().isPresent()) {
            cm.getEnv().setValue(reactant.getRest().get(),
                createRestSolution(reactant.getRest().get(), cm.getContext(), cm.getRemovals()));
          }
          builder.add(new Matching(reactant, (Compartment) species, cm.getMatchings(), cm.getEnv()));
        });
      } else {
        if (reactant.getRest().isPresent()) {
          e.setValue(reactant.getRest().get(),
              createRestSolution(reactant.getRest().get(), (Compartment) species, Collections.emptyMap()));
        }
        builder.add(new Matching(reactant, species, Collections.emptyList(), e));
      }
    }
  }

  protected Stream<Matching> createMatchings(Stream<Species> candidates, Reactant reactant, Tree matchings,
      MLEnvironment env) {
    Stream.Builder<Matching> builder = Stream.builder();
    candidates.forEach(s -> addMatching(builder, s, reactant, matchings, calcAttributeValues(reactant, env), env));
    return builder.build();
  }

  protected static boolean isLast(Set<SpeciesType> types, List<Reactant> reactants, int i) {
    for (int j = i + 1; j < reactants.size(); ++j) {
      if (types.contains(reactants.get(j).getType())) {
        return false;
      }
    }
    return true;
  }

  protected void createContextMatchingsHelper(Stream.Builder<ContextMatchings> builder, Compartment context,
      Map<SpeciesType, Set<Species>> changedSpecies, List<Reactant> reactants, int i, Boolean isValid, Tree matchings,
      MLEnvironment env) {
    if (i == reactants.size()) {
      try {
        builder.add(new ContextMatchings(context, matchings.create(), env));
        return;
      } catch (Exception e) {
        Logger.getGlobal().info("Invalid context matching: " + e.getMessage());
        return;
      }
    }
    Reactant reactant = reactants.get(i);
    SpeciesType t = reactant.getType();
    if (changedSpecies != null && matchings.getCurrent() != null
        && changedSpecies.getOrDefault(matchings.getCurrent().getSpecies().getType(), Collections.emptySet())
            .contains(matchings.getCurrent().getSpecies())) {
      isValid = true;
    }
    final boolean nowValid = isValid;
    createMatchings(
        (!isValid && changedSpecies != null && isLast(changedSpecies.keySet(), reactants, i))
            ? changedSpecies.get(t).stream()
            : (t.isCompartment()
                ? context.getSubCompartments().getOrDefault(t, Collections.emptySet()).stream().map(cs -> (Species) cs)
                : context.getSubLeaves().getOrDefault(t, Collections.emptyMap()).keySet().stream()
                    .map(ls -> (Species) ls)),
        reactant, matchings, env)
            .forEach(m -> createContextMatchingsHelper(builder, context, changedSpecies, reactants, i + 1, nowValid,
                new Tree(m, matchings), m.getEnv()));
  }

  protected void createContextMatchings(Stream.Builder<ContextMatchings> builder, Compartment context,
      Map<SpeciesType, Set<Species>> changedSpecies, List<Reactant> reactants, MLEnvironment env) {
    if ((changedSpecies == null || reactants.stream().anyMatch(t -> changedSpecies.keySet().contains(t.getType())))
        && reactants.stream().allMatch(t -> context.containsType(t.getType()))) {
      createContextMatchingsHelper(builder, context, changedSpecies, reactants, 0, new Boolean(false),
          new Tree(null, null), env);
    }
  }

}
