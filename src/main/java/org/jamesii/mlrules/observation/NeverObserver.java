package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.simulator.Simulator;

import java.util.Optional;

/**
 * The {@link NeverObserver} never observes something and is therefore never notifying any listener.
 */
public class NeverObserver extends Observer {

  @Override
  public void update(Simulator simulator) {
    // do nothing
  }

  @Override
  public Optional<Double> nextObservationPoint() {
    return Optional.empty();
  }
}
