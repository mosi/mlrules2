package org.jamesii.mlrules.observation.visualization;

import info.monitorenter.gui.chart.TracePoint2D;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.visualization.chart.PlotWindow;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.TreeMap;

/**
 * Created by Florian Thonig on 10.01.2017.
 */

/**
 * This class handles all the traces in the plotWindow and adds new points
 */
public class JChart2DListener implements Listener {

    private TreeMap<String, Trace2DLtd> traces;
    private TreeMap<Trace2DLtd, TracePoint2D> old;
    private ASimulationDataHandler dataHandler;
    private int count;

    protected PlotWindow plotWindow;

    boolean active;

    public JChart2DListener(double endTime, boolean attributes, boolean showReals, boolean hierarchy, double interval,
                            long timeStamp) {
        plotWindow = new PlotWindow("Trajectory", "Time", "Species Amount", endTime, hierarchy,
                showReals, attributes, timeStamp);
        plotWindow.disable_side_panel();
        active = true;
        dataHandler = new SimpleSimulationDataHandler();
        dataHandler.setEndTime(endTime);
        plotWindow.setDataHandler(dataHandler);
        plotWindow.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        plotWindow.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                active = false;
                dataHandler.clean();
            }
        });
        traces = new TreeMap<>();
        old = new TreeMap<>();
        count = 0;
        plotWindow.clear_all();
        plotWindow.setInterval(interval);
    }

    @Override
    public void notify(Observer observer) {
        if (!(observer instanceof VisObserver)) {
            throw new IllegalArgumentException("The JChart2DListener can only handle VisObserver observer!");
        }

        VisObserver o = (VisObserver) observer;

        // using new algorithm
        plotWindow.build_traces(count, o.getTime());
        plotWindow.null_values();
        plotWindow.get_value(o.getRoot(), "");
        plotWindow.build_traces(count++, o.getTime());

        // add dataHandler for later usage
        dataHandler.addData(o.getTime(), o.getRoot());
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void finish(StopCondition stopCondition) {
        dataHandler.finished();
        plotWindow.finish();
    }
}
