package org.jamesii.mlrules.observation.visualization.histogram;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;
import org.jamesii.mlrules.observation.visualization.SimulationPoint;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import static org.jamesii.mlrules.model.species.SpeciesType.AttributeType.*;

/**
 * @author Eric Büchner
 */

public class HistogramInitFrame extends JFrame {

    private double steps;
    private HistogramInitFrame histogramInitFrame;
    private ASimulationDataHandler dataHandler;
    private JPanel panel;
    private long timeStamp;
    private HashMap<String, Enum[]> availableSpecies;

    public HistogramInitFrame(ASimulationDataHandler dataHandler, long timeStamp) {
        super("Pick Attribute - " + timeStamp);
        this.setResizable(true);
        this.setSize(300, 500);
        this.dataHandler = dataHandler;
        this.timeStamp = timeStamp;
        histogramInitFrame = this;

        Container pane = histogramInitFrame.getContentPane();
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        availableSpecies = new HashMap<>();

        steps = dataHandler.getNumSimPoints();
        for (int i = 0; i < steps; i++) {
            SimulationPoint tempRoot = dataHandler.get_next();
            findLiveSpecies(tempRoot.getRoot());
        }
        dataHandler.reset();

        generateChoosers();
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);

        pane.add(scrollPane, BorderLayout.CENTER);
        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    private void findLiveSpecies(Species species) {
        // search for unique species and save them in a map
        if (species.getType().getAttributesSize() > 0) {
            if (!availableSpecies.containsKey(species)) {
                availableSpecies.put(species.getType().getName(), identifyAttr(species));
            }
        }
        // recursion in case of compartment
        if (species instanceof Compartment) {
            Compartment c = (Compartment) species;
            if (!c.getAllSpecies().isEmpty())
                c.getAllSubSpeciesStream().forEach(this::findLiveSpecies);
        }
    }

    private Enum[] identifyAttr(Species species) {
        Enum[] arr = new Enum[species.getType().getAttributesSize()];
        for (int i = 0; i < species.getType().getAttributesSize(); i++) {
            if (species.getAttribute(i) instanceof Double) {
                arr[i] = NUM;

            } else if (species.getAttribute(i) instanceof Boolean) {
                arr[i] = BOOL;

            } else if (species.getAttribute(i) instanceof String) {
                arr[i] = STRING;

            } else {
                arr[i] = LINK;

            }
        }
        return arr;
    }

    private void generateChoosers() {
        Set keys = availableSpecies.keySet();
        Object[] temp = keys.toArray(new String[keys.size()]);
        Arrays.sort(temp);

        for (Object o : temp) {
            HistogramInitChooser chooser = new HistogramInitChooser(o.toString(), availableSpecies.get(o),
                    histogramInitFrame, dataHandler, timeStamp);
            panel.add(chooser);

        }
    }

    public double getSteps() {
        return steps;
    }
}
