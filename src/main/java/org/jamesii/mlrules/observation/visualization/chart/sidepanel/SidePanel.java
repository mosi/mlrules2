package org.jamesii.mlrules.observation.visualization.chart.sidepanel;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;


/**
 * @author Eric Büchner
 */
public class SidePanel extends JPanel {

    private TracePanel tracePanel;

    public SidePanel() {
        super();
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        setPreferredSize(new Dimension(235, 900));

        // generate elements for parentPanel
        tracePanel = new TracePanel();

        JScrollPane traceLegendSP = new JScrollPane();
        traceLegendSP.setBorder(BorderFactory.createEmptyBorder());
        traceLegendSP.setViewportView(tracePanel);
        traceLegendSP.setAlignmentX(LEFT_ALIGNMENT);
        traceLegendSP.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
        traceLegendSP.getVerticalScrollBar().setUnitIncrement(15);
        traceLegendSP.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);

        CheckboxSwitchPanel switchPanel = new CheckboxSwitchPanel(tracePanel);

        add(traceLegendSP, BorderLayout.CENTER);
        add(switchPanel, BorderLayout.PAGE_START);
    }

    public TracePanel getTracePanel() {
        return tracePanel;
    }

    public void enable_all() {
        //visualswitch.enable_all();
        tracePanel.enable_all();
    }

    public void disable_all() {
        //visualswitch.disable_all();
        tracePanel.disable_all();
    }
}
