package org.jamesii.mlrules.observation.visualization.histogram;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;
import org.jamesii.mlrules.observation.visualization.SimulationPoint;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.SimpleHistogramBin;
import org.jfree.data.statistics.SimpleHistogramDataset;

import javax.swing.*;
import java.awt.*;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.HashSet;
import java.util.Set;

import static org.jamesii.mlrules.model.species.SpeciesType.AttributeType.*;

/**
 * @author Eric Büchner
 */
class HistogramFrame extends JFrame {

    private SimpleHistogramDataset dataSet;
    private double intervalValue;
    private double minRangeValue;
    private double maxRangeValue;
    private NumberAxis xAxis;
    private double steps;
    private ASimulationDataHandler simulationDataHandler;
    private int attributeNum;
    private String speciesName;

    HistogramFrame(double min, double max, double interval, ASimulationDataHandler dataHandler,
                             HistogramInitFrame histogramInitFrame, String speciesName, Enum attrType, int attrNum,
                             long timeStamp) {
        super("Histogram - " + timeStamp);
        HistogramFrame histogramFrame = this;
        this.minRangeValue = min;
        this.maxRangeValue = max;
        this.intervalValue = interval;
        this.steps = histogramInitFrame.getSteps();
        this.speciesName = speciesName;
        this.attributeNum = attrNum;
        this.simulationDataHandler = dataHandler;
        getContentPane().setLayout(new BorderLayout());
        this.setMinimumSize(new Dimension(400, 300));
        setSize(800, 600);

        dataSet = new SimpleHistogramDataset("key");
        dataSet.setAdjustForBinSize(false);

        if (attrType == STRING) {
            maxRangeValue = findStringCount();
        }

        // generate appropriate bins
        this.createBins(minRangeValue, maxRangeValue, intervalValue);

        // using old theme because the newer version has an annoying visual effect
        ChartFactory.setChartTheme(StandardChartTheme.createLegacyTheme());

        // create the plotWindow with integer axis labels
        JFreeChart chart = ChartFactory.createHistogram("",
                "Species/Size", "Amount", dataSet, PlotOrientation.VERTICAL,
                false, false, false);

        XYPlot plot = (XYPlot) chart.getPlot();

        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setShadowVisible(false);

        // changing the xAxis labels
        if (attrType == NUM) {
            xAxis = new NumberAxis();
            xAxis.setNumberFormatOverride(new NumberFormat() {

                @Override
                public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                    if (number == minRangeValue)
                        return new StringBuffer(String.valueOf(minRangeValue));
                    else if (number < minRangeValue)
                        return new StringBuffer("< " + minRangeValue);
                    else if (number == maxRangeValue)
                        return new StringBuffer(">= " + maxRangeValue);
                    else if (number > maxRangeValue)
                        return new StringBuffer("");
                    else if (number < minRangeValue - intervalValue)
                        return new StringBuffer("");
                    else
                        return new StringBuffer(String.format("" + Double.toString(number)));
                }

                @Override
                public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
                    return new StringBuffer(String.format("%s", number));
                }

                @Override
                public Number parse(String source, ParsePosition parsePosition) {
                    return null;
                }
            });
        } else {
            xAxis = new AlignedNumberAxis();
        }
        //(AlignedNumberAxis) plotWindow.getXYPlot().getDomainAxis();
        chart.getXYPlot().setDomainAxis(xAxis);

        // set custom label at histogram bottom
        String xAxisLabel = "species: " + speciesName + " type: " + attrType.toString();
        xAxis.setLabel(xAxisLabel);

        // allow only relevant ticks
        xAxis.setTickUnit(new NumberTickUnit(intervalValue));

        // over normal tick labeling on xAxis
        // for some reason this blocks the rescaling of xAxis, which is exactly what was desired

        // initiate data handler, normalize the data and get a constant value for the YAxis
        HistogramDataHandler histogramData = new HistogramDataHandler(simulationDataHandler, histogramInitFrame, histogramFrame, speciesName,
                attrType, attrNum);
        histogramData.normDataList(minRangeValue, maxRangeValue, intervalValue);
        Double maxY = histogramData.findMaxY(minRangeValue, maxRangeValue, intervalValue);

        // setting color, AxisRange n stuff
        plot.getRenderer().setSeriesPaint(0, Color.DARK_GRAY);
        NumberAxis yAxis = (NumberAxis) chart.getXYPlot().getRangeAxis();
        yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        yAxis.setRange(0, maxY);

        // start with first dataList element
        dataSet.addObservations(histogramData.getDataList().get(0));

        // adding elements to frame
        ChartPanel panel = new ChartPanel(chart);
        HistogramFrameBottomPanel controlPanel = new HistogramFrameBottomPanel(histogramData, dataSet);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(controlPanel, BorderLayout.PAGE_END);
        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    private void createBins(double min, double max, double interval) {
        // create the desired amount of bins with one extra bin to the left and right
        double currentLeft = min - interval;
        for (int i = 0; i < ((max - min) / interval) + 2; i++) {
            double currentRight = currentLeft + interval;
            dataSet.addBin(new SimpleHistogramBin(currentLeft, currentRight, true, false));
            currentLeft = currentRight;
        }
    }

    void modifyRangeAxis(String[] val) {
        xAxis.setNumberFormatOverride(new NumberFormat() {

            @Override
            public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                int count = 0;
                for (int i = 0; i < val.length; i++) {
                    if (number == i)
                        return new StringBuffer(val[i]);
                    count++;
                }

                if (number > count)
                    return new StringBuffer("");
                else if (number < 0)
                    return new StringBuffer("");
                else
                    return new StringBuffer(String.format(""));
            }

            @Override
            public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
                return new StringBuffer(String.format(""));
            }

            @Override
            public Number parse(String source, ParsePosition parsePosition) {
                return null;
            }
        });
    }

    private void findStrings(Species species, Set set) {
        // search for unique species and save them in a map
        if (species.getType().getName().equals(speciesName)) {
            String str = species.getAttribute(attributeNum).toString();
            set.add(str);
        }
        // recursion in case of compartment
        if (species instanceof Compartment) {
            Compartment c = (Compartment) species;
            if (!c.getAllSpecies().isEmpty())
                c.getAllSubSpeciesStream().forEach((Species s) -> findStrings(s, set));
        }
    }

    private int findStringCount() {
        int count;
        HashSet set = new HashSet();
        for (int i = 0; i < steps; i++) {
            SimulationPoint tempRoot = simulationDataHandler.get_next();
            findStrings(tempRoot.getRoot(), set);
        }
        simulationDataHandler.reset();

        count = set.size();
        return count;
    }

}
