/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.visualization.chart.MLRulesChartFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

/**
 * In contrast to the {@VisListener}, this {@link Listener} shows a loading bar
 * during the calculation of a simulation run and draws the line plotWindow only once
 * at the end, i.e., after calling {@link Listener#finish}. This is done due to
 * performance reasons (frequently redrawing the line plotWindow with
 * {@link JFreeChart} is expensive).
 * 
 * @author Andreas Ruscheinski, Tobias Helms
 *
 */
@Deprecated
public class DelayedVisListener extends VisListener {

  private class DelayedJDialog extends JDialog {

    public DelayedJDialog() {
      super(null, "", Dialog.ModalityType.MODELESS);
    }
  }

  private File directory = null;

  private JDialog progessDialog = null;

  private JProgressBar progressBar = null;

  public DelayedVisListener(File directory) {
    this.directory = directory;
    progessDialog = new DelayedJDialog();
    progessDialog.setTitle("Calculation Progress");
    progessDialog.setSize(400, 200);

    progressBar = new JProgressBar(0, 100);
    progressBar.setValue(0);
    progressBar.setIndeterminate(true);
    progressBar.setStringPainted(true);
    progessDialog.add(progressBar);

    progessDialog.setVisible(true);

    progessDialog.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        isActive.set(false);
      }
    });
  }

  @Override
  public void notify(Observer observer) {
    if (observer instanceof VisObserver) {
      VisObserver allObserver = (VisObserver) observer;
      allObserver.getSpecies().forEach((k, v) -> {
        if (!entries.containsKey(k)) {
          XYSeries series = new XYSeries(k);
          dataset.addSeries(series);
          entries.put(k, series);
        }
      });
      entries.forEach((k, v) -> {
        v.add(allObserver.getTime(), v.isEmpty() ? (Number) 0 : (Number) v.getDataItem(v.getItemCount() - 1).getY());
        v.add(allObserver.getTime(), allObserver.getSpecies().getOrDefault(k, 0.0));
      });
      double endTime = allObserver.getEndTime();
      double currentTime = allObserver.getCurrentTime();
      Double percent = (100 * currentTime) / endTime;
      this.progressBar.setValue(percent.intValue());
    }
  }

  @Override
  public void finish(StopCondition stopCondition) {
    this.progessDialog.dispose();
    this.chart = ChartFactory.createXYLineChart("Trajectory", "Time", "Species Amount", dataset,
        PlotOrientation.VERTICAL, true, false, false);
    frame = new MLRulesChartFrame("Demo", chart, entries, directory);
    frame.pack();
    frame.setVisible(true);
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        isActive.set(false);
      }
    });
  }

}
