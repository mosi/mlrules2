/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation.visualization.chart;

import org.jamesii.core.data.runtime.CSVFormatter;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * This class extends {@link ChartPanel} to add the opportunity to save results
 * as CSV.
 * 
 * @author Tobias Helms
 *
 */
@Deprecated
public class MLRulesChartPanel extends ChartPanel {

  private static final long serialVersionUID = 1L;

  private static final String SAVE_AS_CSV_COMMAND = "SAVE_AS_CSV";

  private final Map<String, XYSeries> entries;
  
  private final File directory;

  public MLRulesChartPanel(JFreeChart chart, Map<String, XYSeries> entries, File directory) {
    super(chart);
    this.entries = entries;
    this.directory = directory;
  }

  public void save(String filename) throws IOException {
    if (entries.isEmpty()) {
      return;
    }

    FileWriter file = new FileWriter(filename);
    BufferedWriter writer = new BufferedWriter(file);

    String[] keys = entries.keySet().toArray(new String[] {});
    String headline = "time," + CSVFormatter.formatCSV(keys, ',') + "\n";
    writer.write(headline);
    Map<XYSeries, Integer> positions = new HashMap<>();
    Set<Double> times = new TreeSet<>();
    entries.values().forEach(xy -> {
      for (int i = 0; i < xy.getItemCount(); ++i) {
        times.add(xy.getX(i).doubleValue());
      }
    });
    for (Double time : times) {
      StringJoiner joiner = new StringJoiner(",", "", "\n");
      joiner.add(String.valueOf(time));
      for (XYSeries xy : entries.values()) {
            int pos = positions.getOrDefault(xy, 0);
            while (time.compareTo(xy.getX(pos).doubleValue()) > 0) {
              ++pos;
            }
            if (time.compareTo(xy.getX(pos).doubleValue()) == 0) {
              joiner.add(String.valueOf(xy.getY(pos + 1)));
            } else {
              joiner.add("0");
            }
            positions.put(xy, pos);
      }          
      writer.write(joiner.toString());    
    }

    writer.flush();
    writer.close();
    file.close();
  }
  
  @Override
  public void actionPerformed(ActionEvent event) {
    String command = event.getActionCommand();
    if (command.equals(SAVE_AS_CSV_COMMAND)) {
      try {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(directory);
        FileNameExtensionFilter filter =
            new FileNameExtensionFilter("csv", "csv");
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setFileFilter(filter);

        int option = fileChooser.showSaveDialog(this);
        if (option == JFileChooser.APPROVE_OPTION) {
          String filename = fileChooser.getSelectedFile().getPath();
          if (isEnforceFileExtensions()) {
            if (!filename.endsWith(".csv")) {
              filename = filename + ".csv";
            }
          }
          save(filename);
        }
      } catch (IOException e) {
        JOptionPane.showMessageDialog(this, "I/O error occurred.",
            "Save As PNG", JOptionPane.WARNING_MESSAGE);
      }

    } else {
      super.actionPerformed(event);
    }

  }

  @Override
  protected JPopupMenu createPopupMenu(boolean properties, boolean copy,
      boolean save, boolean print, boolean zoom) {
    JPopupMenu result =
        super.createPopupMenu(properties, copy, save, print, zoom);
    if (save) {
      for (Component sub : result.getComponents()) {
        if (sub instanceof JMenu) {
          JMenu menu = (JMenu) sub;
          if (menu.getText() == localizationResources.getString("Save_as")) {
            JMenuItem csvItem = new JMenuItem("CSV...");
            csvItem.setActionCommand(SAVE_AS_CSV_COMMAND);
            csvItem.addActionListener(this);
            menu.add(csvItem);
          }
        }
      }

    }

    return result;
  }

}
