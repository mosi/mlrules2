package org.jamesii.mlrules.observation.visualization.chart;

import org.jamesii.mlrules.util.JCheckBoxTree;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Created by Florian Thonig on 27.03.2017.
 */
public class SpecOptAttribute {
    private final int number;
    private DefaultMutableTreeNode node;
    private SpeciesOptions belongsTo;
    private boolean checked;
    private JCheckBoxTree.CheckChangeEventListener listener;


    public SpecOptAttribute(SpeciesOptions belongsTo, int number, boolean selected) {
        this.belongsTo = belongsTo;
        this.number = number;
        checked = selected;
    }

    public DefaultMutableTreeNode getNode() {
        return node;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getNumber() {
        return number;
    }

    public JCheckBoxTree.CheckChangeEventListener getListener() {
        return listener;
    }

    public void setListener(JCheckBoxTree.CheckChangeEventListener listener) {
        this.listener = listener;
    }

    public SpeciesOptions getBelongsTo() {
        return belongsTo;
    }
}
