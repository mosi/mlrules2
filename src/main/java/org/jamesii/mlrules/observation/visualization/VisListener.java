/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.visualization.chart.MLRulesChartFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 
 * A {@link Listener} that draws simulation results to a {@link JFreeChart} line
 * plotWindow.
 * 
 * @author Tobias Helms
 *
 */

public class VisListener implements Listener {

  protected final XYSeriesCollection dataset = new XYSeriesCollection();

  protected Map<String, XYSeries> entries = new TreeMap<>();

  protected JFreeChart chart = null;

  protected MLRulesChartFrame frame;

  protected AtomicBoolean isActive = new AtomicBoolean(true);

  public VisListener() {
  }


  public VisListener(File directory) {
    this.chart = ChartFactory.createXYLineChart("Trajectory", "Time", "Species Amount", dataset,
        PlotOrientation.VERTICAL, true, false, false);
    frame = new MLRulesChartFrame("Demo", chart, entries, directory);
    frame.pack();
    frame.setVisible(true);
    isActive = new AtomicBoolean(true);
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        isActive.set(false);
      }
    });
  }

  @Override
  public void notify(Observer observer) {
    // TODO: remove the "instance of"-checking
    if (observer instanceof VisObserver) {
      VisObserver allObserver = (VisObserver) observer;

      allObserver.getSpecies().forEach((k, v) -> {
        if (!entries.containsKey(k)) {
          XYSeries series = new XYSeries(k);
          dataset.addSeries(series);
          entries.put(k, series);
        }
      });
      // TODO: cleanup
      entries.forEach((k, v) -> {
        v.add(allObserver.getTime(), v.isEmpty() ? (Number) 0 : (Number) v.getDataItem(v.getItemCount() - 1).getY());
        v.add(allObserver.getTime(), allObserver.getSpecies().getOrDefault(k, 0.0));
      });

      return;
    }

    throw new IllegalArgumentException("The VisListener can only handle VisObserver observer!");
  }

  @Override
  public boolean isActive() {
    return isActive.get();
  }

  @Override
  public void finish(StopCondition stopCondition) {
    // nothing to do when the simulation is finished
  }

}
