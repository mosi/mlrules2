package org.jamesii.mlrules.observation.visualization.chart;

import info.monitorenter.gui.chart.traces.Trace2DLtd;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.JCheckBoxTree;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Florian Thonig on 26.03.2017.
 */
public class SpeciesOptions {

    private final Species species;
    private JCheckBoxTree tree;

    private JCheckBoxTree.CheckChangeEventListener listener;
    private PlotWindow chart;

    private String name;
    private double actual_value;
    private Trace2DLtd trace;
    private Color color;
    private DefaultMutableTreeNode node;
    private Vector<SpecOptAttribute> specOptAttributes;
    private boolean shown;
    private boolean checked;

    private boolean isChecked(JCheckBoxTree tree, int i) {
        Map<TreePath, JCheckBoxTree.CheckedNode> nodes = tree.getNodesCheckingState();
        for (Map.Entry<TreePath, JCheckBoxTree.CheckedNode> e : nodes.entrySet()) {
            TreePath k = e.getKey();
            if (k.getParentPath() != null && k.getParentPath().getParentPath() != null) {
                Integer att = (Integer) ((DefaultMutableTreeNode) (k.getLastPathComponent())).getUserObject();
                String name = (String) ((DefaultMutableTreeNode) (k.getParentPath().getLastPathComponent())).getUserObject();

                String respect = (String) ((DefaultMutableTreeNode) (k.getParentPath().getParentPath().getLastPathComponent())).getUserObject();

                if (att.equals(i) && name.equals(species.getType().getName()) && respect.equals("respect Attributes")) {
                    return e.getValue().isSelected();
                }
            }
        }
        return false;
    }

    public SpeciesOptions(Species s, Color color, JCheckBoxTree tree, PlotWindow chart) {
        species = s;
        specOptAttributes = new Vector<>();
        for (int i = 0; i < s.getType().getAttributesSize(); i++) {
            SpecOptAttribute spec = new SpecOptAttribute(this, i, isChecked(tree, i));
            specOptAttributes.add(spec);
        }
        this.color = color;
        this.name = s.getType().getName();
        this.tree = tree;
        checked = true;
        this.chart = chart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getActual_value() {
        return actual_value;
    }

    public void setActual_value(double actual_value) {
        this.actual_value = actual_value;
    }

    public void addToActual_value(double value) {
        this.actual_value += value;
    }

    public Trace2DLtd getTrace() {
        return trace;
    }

    public void setTrace(Trace2DLtd trace) {
        this.trace = trace;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        trace.setColor(color);
    }

    public DefaultMutableTreeNode getNode() {
        return node;
    }

    public void setNode(DefaultMutableTreeNode node) {
        this.node = node;
    }

    public Vector<SpecOptAttribute> getSpecOptAttributes() {
        return specOptAttributes;
    }

    public boolean isShown() {
        return shown;
    }

    public void setShown(boolean shown) {
        this.shown = shown;
    }

    public Species getSpecies() {
        return species;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void createNode(DefaultMutableTreeNode treenode){
        if (specOptAttributes.size() > 0) {
            node = new DefaultMutableTreeNode(species.getType().getName());
            listener = new JCheckBoxTree.CheckChangeEventListener() {
                @Override
                public void checkStateChanged(JCheckBoxTree.CheckChangeEvent event) {
                    if (((TreePath) event.getSource()).getLastPathComponent().equals(node)) {
                        if (checked) {
                            for (SpecOptAttribute i : specOptAttributes) {
                                i.setChecked(false);
                            }
                            checked = false;
                        } else {
                            for (SpecOptAttribute i : specOptAttributes) {
                                i.setChecked(true);
                            }
                            checked = true;
                        }
                        chart.recalculate();
                    }
                }
            };
            tree.addCheckChangeEventListener(listener);
            for (SpecOptAttribute i : specOptAttributes){
                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(i.getNumber());
                i.setListener(new JCheckBoxTree.CheckChangeEventListener() {
                    @Override
                    public void checkStateChanged(JCheckBoxTree.CheckChangeEvent event) {
                        if (((TreePath) event.getSource()).getLastPathComponent().equals(newNode)) {
                            if (i.isChecked()) {
                                i.setChecked(false);
                                checkCheck();
                            } else {
                                i.setChecked(true);
                                setChecked(true);
                            }
                        }
                        chart.recalculate();
                    }
                });

                tree.addCheckChangeEventListener(i.getListener());
                node.add(newNode);
            }
            treenode.add(node);
        }
    }

    private void checkCheck() {
        for (SpecOptAttribute i : specOptAttributes) {
            if (i.isChecked())
                return;
        }
        checked = false;
    }

    /**
     * clears everything and sets shown to false and name to its real name, but remembers color
     */
    public void clearAll() {
        setActual_value(0);
        setShown(false);
        setTrace(null);
    }

    /**
     * builds the trace for this entry
     */
    public Trace2DLtd buildTrace(double xVal) {
        trace.setColor(color);
        trace.addPoint(xVal, actual_value);
        return trace;
    }
}
