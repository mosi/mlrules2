package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.observation.Observer;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Florian Thonig on 30.01.17.
 */
public class DelayedJChart2DListener extends VisListener {

    private JDialog progressDialog = null;
    private JProgressBar progressBar = null;
    private ASimulationDataHandler dataHandler;
    private VisChooserPopup visChooser;
    private double endTime;
    private boolean active;
    private boolean respectAttributes;
    private boolean respectHierarchy;
    private boolean showReals;
    private long timeStamp;
    private String model;

    public DelayedJChart2DListener(double endTime, boolean respectAttributes, boolean showReals,
                                   boolean respectHierarchy, long timeStamp, String model) {
        dataHandler = new SimpleSimulationDataHandler();
        this.endTime = endTime;
        dataHandler.setEndTime(endTime);
        active = true;

        this.respectAttributes = respectAttributes;
        this.showReals = showReals;
        this.respectHierarchy = respectHierarchy;
        this.timeStamp = timeStamp;
        this.model = model;

        progressDialog = new JDialog();
        progressDialog.setTitle("Calculation Progress");
        progressDialog.setSize(400, 200);

        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(true);
        progressDialog.add(progressBar);

        progressDialog.setVisible(true);

        progressDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                progressDialog.dispose();
                isActive.set(false);
                active = false;
                dataHandler.finished();

                visChooser = new VisChooserPopup(endTime, respectHierarchy, showReals, respectAttributes,
                        dataHandler, timeStamp, model);

                visChooser.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        active = false;
                        dataHandler.clean();
                    }
                });
            }
        });
    }

    @Override
    public void notify(Observer observer) {
        if (!(observer instanceof VisObserver)) {
            throw new IllegalArgumentException("The JChart2DListener can only handle VisObserver observer!");
        }
        VisObserver o = (VisObserver) observer;
        dataHandler.addData(o.getTime(), o.getRoot());

        double endTime = o.getEndTime();
        double currentTime = o.getTime();
        Double percent = (100 * currentTime) / endTime;
        progressBar.setValue(percent.intValue());
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void finish(StopCondition stopCondition) {
        progressDialog.dispose();
        if (visChooser == null)
            visChooser = new VisChooserPopup(endTime, respectHierarchy, showReals, respectAttributes,
                    dataHandler, timeStamp, model);

        visChooser.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                active = false;
                dataHandler.clean();
            }
        });

        active = true;
        dataHandler.finished();

    }
}
