package org.jamesii.mlrules.observation.visualization.chart.sidepanel;

import org.jamesii.mlrules.observation.visualization.chart.PlotWindow;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author Eric Büchner
 */
public class VisualSwitchPanel extends JPanel {

    private JCheckBox showRealsCBox;
    private JButton attributeOptionButton;
    private PlotWindow chart_window;

    public VisualSwitchPanel(PlotWindow plotWindow) {
        super();
        chart_window = plotWindow;
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Visuals:"));

        ItemListener attribute_listener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (ItemEvent.SELECTED == e.getStateChange()) {
                    plotWindow.setRespectAttributes(true);
                    showRealsCBox.setEnabled(true);
                    attributeOptionButton.setEnabled(true);
                } else {
                    plotWindow.setRespectAttributes(false);
                    showRealsCBox.setEnabled(false);
                    showRealsCBox.setSelected(false);
                    plotWindow.setShowReals(false);
                    attributeOptionButton.setEnabled(false);
                    chart_window.attributeTree.setVisible(false);
                }
                plotWindow.recalculate();
            }
        };

        ItemListener hierarchy_listener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (ItemEvent.SELECTED == e.getStateChange()) {
                    plotWindow.setRespectHierarchy(true);
                } else {
                    plotWindow.setRespectHierarchy(false);
                }
                plotWindow.recalculate();
            }
        };

        ItemListener real_listener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (ItemEvent.SELECTED == e.getStateChange()) {
                    plotWindow.setShowReals(true);
                } else {
                    plotWindow.setShowReals(false);
                }
                plotWindow.recalculate();
            }
        };

        attributeOptionButton = new JButton("Attribute Options");
        attributeOptionButton.setMinimumSize(new Dimension(180, 27));
        attributeOptionButton.setMaximumSize(new Dimension(180, 27));
        attributeOptionButton.setEnabled(false);
        attributeOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chart_window.attributeTree.setVisible(true);
            }
        });

        JCheckBox respectAttributesCBox = new JCheckBox("respect Attributes");
        respectAttributesCBox.setSelected(plotWindow.isRespectAttributes());
        respectAttributesCBox.addItemListener(attribute_listener);

        JCheckBox respectHierarchyCBox = new JCheckBox("respect Hierarchy");
        respectHierarchyCBox.setSelected(plotWindow.isRespectHierarchy());
        respectHierarchyCBox.addItemListener(hierarchy_listener);

        showRealsCBox = new JCheckBox("show Reals");
        showRealsCBox.setSelected(plotWindow.isShowReals());
        showRealsCBox.addItemListener(real_listener);
        if (!plotWindow.isRespectAttributes()) {
            showRealsCBox.setEnabled(false);
            attributeOptionButton.setEnabled(false);
        }

        add(respectAttributesCBox);
        add(respectHierarchyCBox);
        add(showRealsCBox);
        add(Box.createRigidArea(new Dimension(0, 5)));
        add(attributeOptionButton);
    }

}
