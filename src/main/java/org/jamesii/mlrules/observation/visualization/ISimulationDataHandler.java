package org.jamesii.mlrules.observation.visualization;


/**
 * Created by Florian Thonig on 16.01.17.
 */

import org.jamesii.mlrules.model.species.Species;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Interface for simulation data handling
 */
public interface ISimulationDataHandler extends Serializable {

    /**
     * Added endTime, because it is needed for opening plot
     */

    public double getEndTime();

    public void setEndTime(double endTime);

    /**
     * Addind data as Simulation point to the DataHandler
     * @param time
     * @param root
     */
    public void addData(double time, Species root);

    /**
     * function to get all Data
     * @return Iterator of Simulation points
     */
    public SimulationPoint get_next();

    /**
     * function to reset the file input
     */
    void reset();


    /**
     * function called, when simulation is over and all data is collected
     */
    public void finished();

    /**
     * function called when simulation is closed, to remove the temporary file
     */
    public void clean();
}
