package org.jamesii.mlrules.observation.visualization.histogram;

import org.jfree.chart.axis.*;
import org.jfree.chart.event.AxisChangeEvent;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.ValueAxisPlot;
import org.jfree.chart.util.ParamChecks;
import org.jfree.data.Range;
import org.jfree.data.RangeType;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.MarkerAxisBand;
import org.jfree.chart.axis.NumberTick;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickType;
import org.jfree.chart.axis.TickUnit;
import org.jfree.chart.axis.TickUnitSource;

/**
 * @author Eric Büchner
 *
 * Mandatory, to align names on the x-Axis.
 */
public class AlignedNumberAxis extends NumberAxis {

        private static final long serialVersionUID = 2805933088476185789L;
        public static final NumberTickUnit DEFAULT_TICK_UNIT = new NumberTickUnit(1.0D, new DecimalFormat("0"));
        private RangeType rangeType;
        private boolean autoRangeIncludesZero;
        private boolean autoRangeStickyZero;
        private NumberTickUnit tickUnit;
        private NumberFormat numberFormatOverride;
        private MarkerAxisBand markerBand;

        public AlignedNumberAxis() {
            this((String)null);
        }

        public AlignedNumberAxis(String label) {
            super();
            this.rangeType = RangeType.FULL;
            this.autoRangeIncludesZero = true;
            this.autoRangeStickyZero = true;
            this.tickUnit = DEFAULT_TICK_UNIT;
            this.numberFormatOverride = null;
            this.markerBand = null;
        }

        public RangeType getRangeType() {
            return this.rangeType;
        }

        public void setRangeType(RangeType rangeType) {
            ParamChecks.nullNotPermitted(rangeType, "rangeType");
            this.rangeType = rangeType;
            this.notifyListeners(new AxisChangeEvent(this));
        }

        public boolean getAutoRangeIncludesZero() {
            return this.autoRangeIncludesZero;
        }

        public void setAutoRangeIncludesZero(boolean flag) {
            if(this.autoRangeIncludesZero != flag) {
                this.autoRangeIncludesZero = flag;
                if(this.isAutoRange()) {
                    this.autoAdjustRange();
                }

                this.notifyListeners(new AxisChangeEvent(this));
            }

        }

        public boolean getAutoRangeStickyZero() {
            return this.autoRangeStickyZero;
        }

        public void setAutoRangeStickyZero(boolean flag) {
            if(this.autoRangeStickyZero != flag) {
                this.autoRangeStickyZero = flag;
                if(this.isAutoRange()) {
                    this.autoAdjustRange();
                }

                this.notifyListeners(new AxisChangeEvent(this));
            }

        }

        public NumberTickUnit getTickUnit() {
            return this.tickUnit;
        }

        public void setTickUnit(NumberTickUnit unit) {
            this.setTickUnit(unit, true, true);
        }

        public void setTickUnit(NumberTickUnit unit, boolean notify, boolean turnOffAutoSelect) {
            ParamChecks.nullNotPermitted(unit, "unit");
            this.tickUnit = unit;
            if(turnOffAutoSelect) {
                this.setAutoTickUnitSelection(false, false);
            }

            if(notify) {
                this.notifyListeners(new AxisChangeEvent(this));
            }

        }

        public NumberFormat getNumberFormatOverride() {
            return this.numberFormatOverride;
        }

        public void setNumberFormatOverride(NumberFormat formatter) {
            this.numberFormatOverride = formatter;
            this.notifyListeners(new AxisChangeEvent(this));
        }

        public MarkerAxisBand getMarkerBand() {
            return this.markerBand;
        }

        public void setMarkerBand(MarkerAxisBand band) {
            this.markerBand = band;
            this.notifyListeners(new AxisChangeEvent(this));
        }

        public void configure() {
            if(this.isAutoRange()) {
                this.autoAdjustRange();
            }

        }

        protected void autoAdjustRange() {
            Plot plot = this.getPlot();
            if(plot != null) {
                if(plot instanceof ValueAxisPlot) {
                    ValueAxisPlot vap = (ValueAxisPlot)plot;
                    Range r = vap.getDataRange(this);
                    if(r == null) {
                        r = this.getDefaultAutoRange();
                    }

                    double upper = r.getUpperBound();
                    double lower = r.getLowerBound();
                    if(this.rangeType == RangeType.POSITIVE) {
                        lower = Math.max(0.0D, lower);
                        upper = Math.max(0.0D, upper);
                    } else if(this.rangeType == RangeType.NEGATIVE) {
                        lower = Math.min(0.0D, lower);
                        upper = Math.min(0.0D, upper);
                    }

                    if(this.getAutoRangeIncludesZero()) {
                        lower = Math.min(lower, 0.0D);
                        upper = Math.max(upper, 0.0D);
                    }

                    double range = upper - lower;
                    double fixedAutoRange = this.getFixedAutoRange();
                    if(fixedAutoRange > 0.0D) {
                        lower = upper - fixedAutoRange;
                    } else {
                        double minRange = this.getAutoRangeMinimumSize();
                        if(range < minRange) {
                            double expand = (minRange - range) / 2.0D;
                            upper += expand;
                            lower -= expand;
                            if(lower == upper) {
                                double adjust = Math.abs(lower) / 10.0D;
                                lower -= adjust;
                                upper += adjust;
                            }

                            if(this.rangeType == RangeType.POSITIVE) {
                                if(lower < 0.0D) {
                                    upper -= lower;
                                    lower = 0.0D;
                                }
                            } else if(this.rangeType == RangeType.NEGATIVE && upper > 0.0D) {
                                lower -= upper;
                                upper = 0.0D;
                            }
                        }

                        if(this.getAutoRangeStickyZero()) {
                            if(upper <= 0.0D) {
                                upper = Math.min(0.0D, upper + this.getUpperMargin() * range);
                            } else {
                                upper += this.getUpperMargin() * range;
                            }

                            if(lower >= 0.0D) {
                                lower = Math.max(0.0D, lower - this.getLowerMargin() * range);
                            } else {
                                lower -= this.getLowerMargin() * range;
                            }
                        } else {
                            upper += this.getUpperMargin() * range;
                            lower -= this.getLowerMargin() * range;
                        }
                    }

                    this.setRange(new Range(lower, upper), false, false);
                }

            }
        }

        public double valueToJava2D(double value, Rectangle2D area, RectangleEdge edge) {
            Range range = this.getRange();
            double axisMin = range.getLowerBound();
            double axisMax = range.getUpperBound();
            double min = 0.0D;
            double max = 0.0D;
            if(RectangleEdge.isTopOrBottom(edge)) {
                min = area.getX();
                max = area.getMaxX();
            } else if(RectangleEdge.isLeftOrRight(edge)) {
                max = area.getMinY();
                min = area.getMaxY();
            }

            return this.isInverted()?max - (value - axisMin) / (axisMax - axisMin) * (max - min):min + (value - axisMin) / (axisMax - axisMin) * (max - min);
        }

        public double java2DToValue(double java2DValue, Rectangle2D area, RectangleEdge edge) {
            Range range = this.getRange();
            double axisMin = range.getLowerBound();
            double axisMax = range.getUpperBound();
            double min = 0.0D;
            double max = 0.0D;
            if(RectangleEdge.isTopOrBottom(edge)) {
                min = area.getX();
                max = area.getMaxX();
            } else if(RectangleEdge.isLeftOrRight(edge)) {
                min = area.getMaxY();
                max = area.getY();
            }

            return this.isInverted()?axisMax - (java2DValue - min) / (max - min) * (axisMax - axisMin):axisMin + (java2DValue - min) / (max - min) * (axisMax - axisMin);
        }

        protected double calculateLowestVisibleTickValue() {
            double unit = this.getTickUnit().getSize();
            double index = Math.ceil(this.getRange().getLowerBound() / unit);
            return index * unit;
        }

        protected double calculateHighestVisibleTickValue() {
            double unit = this.getTickUnit().getSize();
            double index = Math.floor(this.getRange().getUpperBound() / unit);
            return index * unit;
        }

        protected int calculateVisibleTickCount() {
            double unit = this.getTickUnit().getSize();
            Range range = this.getRange();
            return (int)(Math.floor(range.getUpperBound() / unit) - Math.ceil(range.getLowerBound() / unit) + 1.0D);
        }

        public AxisState draw(Graphics2D g2, double cursor, Rectangle2D plotArea, Rectangle2D dataArea, RectangleEdge edge, PlotRenderingInfo plotState) {
            AxisState state;
            if(!this.isVisible()) {
                state = new AxisState(cursor);
                java.util.List ticks = this.refreshTicks(g2, state, dataArea, edge);
                state.setTicks(ticks);
                return state;
            } else {
                state = this.drawTickMarksAndLabels(g2, cursor, plotArea, dataArea, edge);
                if(this.getAttributedLabel() != null) {
                    state = this.drawAttributedLabel(this.getAttributedLabel(), g2, plotArea, dataArea, edge, state);
                } else {
                    state = this.drawLabel(this.getLabel(), g2, plotArea, dataArea, edge, state);
                }

                this.createAndAddEntity(cursor, state, dataArea, edge, plotState);
                return state;
            }
        }

        protected double estimateMaximumTickLabelHeight(Graphics2D g2) {
            RectangleInsets tickLabelInsets = this.getTickLabelInsets();
            double result = tickLabelInsets.getTop() + tickLabelInsets.getBottom();
            Font tickLabelFont = this.getTickLabelFont();
            FontRenderContext frc = g2.getFontRenderContext();
            result += (double)tickLabelFont.getLineMetrics("123", frc).getHeight();
            return result;
        }

        protected double estimateMaximumTickLabelWidth(Graphics2D g2, TickUnit unit) {
            RectangleInsets tickLabelInsets = this.getTickLabelInsets();
            double result = tickLabelInsets.getLeft() + tickLabelInsets.getRight();
            if(this.isVerticalTickLabels()) {
                FontRenderContext fm = g2.getFontRenderContext();
                LineMetrics range = this.getTickLabelFont().getLineMetrics("0", fm);
                result += (double)range.getHeight();
            } else {
                FontMetrics fm1 = g2.getFontMetrics(this.getTickLabelFont());
                Range range1 = this.getRange();
                double lower = range1.getLowerBound();
                double upper = range1.getUpperBound();
                NumberFormat formatter = this.getNumberFormatOverride();
                String lowerStr;
                String upperStr;
                if(formatter != null) {
                    lowerStr = formatter.format(lower);
                    upperStr = formatter.format(upper);
                } else {
                    lowerStr = unit.valueToString(lower);
                    upperStr = unit.valueToString(upper);
                }

                double w1 = (double)fm1.stringWidth(lowerStr);
                double w2 = (double)fm1.stringWidth(upperStr);
                result += Math.max(w1, w2);
            }

            return result;
        }

        protected void selectAutoTickUnit(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
            if(RectangleEdge.isTopOrBottom(edge)) {
                this.selectHorizontalAutoTickUnit(g2, dataArea, edge);
            } else if(RectangleEdge.isLeftOrRight(edge)) {
                this.selectVerticalAutoTickUnit(g2, dataArea, edge);
            }

        }

        protected void selectHorizontalAutoTickUnit(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
            double tickLabelWidth = this.estimateMaximumTickLabelWidth(g2, this.getTickUnit());
            TickUnitSource tickUnits = this.getStandardTickUnits();
            TickUnit unit1 = tickUnits.getCeilingTickUnit(this.getTickUnit());
            double unit1Width = this.lengthToJava2D(unit1.getSize(), dataArea, edge);
            double guess = tickLabelWidth / unit1Width * unit1.getSize();
            NumberTickUnit unit2 = (NumberTickUnit)tickUnits.getCeilingTickUnit(guess);
            double unit2Width = this.lengthToJava2D(unit2.getSize(), dataArea, edge);
            tickLabelWidth = this.estimateMaximumTickLabelWidth(g2, unit2);
            if(tickLabelWidth > unit2Width) {
                unit2 = (NumberTickUnit)tickUnits.getLargerTickUnit(unit2);
            }

            this.setTickUnit(unit2, false, false);
        }

        protected void selectVerticalAutoTickUnit(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
            double tickLabelHeight = this.estimateMaximumTickLabelHeight(g2);
            TickUnitSource tickUnits = this.getStandardTickUnits();
            TickUnit unit1 = tickUnits.getCeilingTickUnit(this.getTickUnit());
            double unitHeight = this.lengthToJava2D(unit1.getSize(), dataArea, edge);
            double guess = unit1.getSize();
            if(unitHeight > 0.0D) {
                guess = tickLabelHeight / unitHeight * unit1.getSize();
            }

            NumberTickUnit unit2 = (NumberTickUnit)tickUnits.getCeilingTickUnit(guess);
            double unit2Height = this.lengthToJava2D(unit2.getSize(), dataArea, edge);
            tickLabelHeight = this.estimateMaximumTickLabelHeight(g2);
            if(tickLabelHeight > unit2Height) {
                unit2 = (NumberTickUnit)tickUnits.getLargerTickUnit(unit2);
            }

            this.setTickUnit(unit2, false, false);
        }

        public java.util.List refreshTicks(Graphics2D g2, AxisState state, Rectangle2D dataArea, RectangleEdge edge) {
            Object result = new ArrayList();
            if(RectangleEdge.isTopOrBottom(edge)) {
                result = this.refreshTicksHorizontal(g2, dataArea, edge);
            } else if(RectangleEdge.isLeftOrRight(edge)) {
                result = this.refreshTicksVertical(g2, dataArea, edge);
            }

            return (java.util.List)result;
        }

        protected java.util.List refreshTicksHorizontal(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
            ArrayList result = new ArrayList();
            Font tickLabelFont = this.getTickLabelFont();
            g2.setFont(tickLabelFont);
            if(this.isAutoTickUnitSelection()) {
                this.selectAutoTickUnit(g2, dataArea, edge);
            }

            NumberTickUnit tu = this.getTickUnit();
            double size = tu.getSize();
            int count = this.calculateVisibleTickCount();
            double lowestTickValue = this.calculateLowestVisibleTickValue();
            if(count <= 500) {
                int minorTickSpaces = this.getMinorTickCount();
                if(minorTickSpaces <= 0) {
                    minorTickSpaces = tu.getMinorTickCount();
                }

                int i;
                double currentTickValue;
                for(i = 1; i < minorTickSpaces; ++i) {
                    currentTickValue = lowestTickValue - size * (double)i / (double)minorTickSpaces;
                    if(this.getRange().contains(currentTickValue)) {
                        result.add(new NumberTick(TickType.MINOR, currentTickValue, "", TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
                    }
                }
                //major ticks
                for(i = 0; i < count; ++i) {
                    currentTickValue = lowestTickValue + (double)i * size;
                    NumberFormat formatter = this.getNumberFormatOverride();
                    String tickLabel;
                    if(formatter != null) {
                        tickLabel = formatter.format(currentTickValue);
                    } else {
                        tickLabel = this.getTickUnit().valueToString(currentTickValue);
                    }

                    double angle = 0.0D;
                    TextAnchor anchor;
                    TextAnchor rotationAnchor;
                    if(this.isVerticalTickLabels()) {
                        anchor = TextAnchor.CENTER_RIGHT;
                        rotationAnchor = TextAnchor.CENTER_RIGHT;
                        if(edge == RectangleEdge.TOP) {
                            angle = 1.5707963267948966D;
                        } else {
                            angle = -1.5707963267948966D;
                        }
                    } else if(edge == RectangleEdge.TOP) {
                        anchor = TextAnchor.BOTTOM_CENTER;
                        rotationAnchor = TextAnchor.BOTTOM_CENTER;
                    } else {
                        anchor = TextAnchor.TOP_CENTER;
                        rotationAnchor = TextAnchor.TOP_CENTER;
                    }

                    NumberTick tick = new NumberTick((new Double(currentTickValue)) + (size/2), tickLabel, anchor, rotationAnchor, angle);
                    result.add(tick);
                    double nextTickValue = lowestTickValue + (double)(i + 1) * size;

                    for(int minorTick = 1; minorTick < minorTickSpaces; ++minorTick) {
                        double minorTickValue = currentTickValue + (nextTickValue - currentTickValue) * (double)minorTick / (double)minorTickSpaces;
                        if(this.getRange().contains(minorTickValue)) {
                            result.add(new NumberTick(TickType.MINOR, minorTickValue +1, "", TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
                        }
                    }
                }
            }

            return result;
        }

        protected java.util.List refreshTicksVertical(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {
            ArrayList result = new ArrayList();
            result.clear();
            Font tickLabelFont = this.getTickLabelFont();
            g2.setFont(tickLabelFont);
            if(this.isAutoTickUnitSelection()) {
                this.selectAutoTickUnit(g2, dataArea, edge);
            }

            NumberTickUnit tu = this.getTickUnit();
            double size = tu.getSize();
            int count = this.calculateVisibleTickCount();
            double lowestTickValue = this.calculateLowestVisibleTickValue();
            if(count <= 500) {
                int minorTickSpaces = this.getMinorTickCount();
                if(minorTickSpaces <= 0) {
                    minorTickSpaces = tu.getMinorTickCount();
                }

                int i;
                double currentTickValue;
                for(i = 1; i < minorTickSpaces; ++i) {
                    currentTickValue = lowestTickValue - size * (double)i / (double)minorTickSpaces;
                    if(this.getRange().contains(currentTickValue)) {
                        result.add(new NumberTick(TickType.MINOR, currentTickValue, "", TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
                    }
                }

                for(i = 0; i < count; ++i) {
                    currentTickValue = lowestTickValue + (double)i * size;
                    NumberFormat formatter = this.getNumberFormatOverride();
                    String tickLabel;
                    if(formatter != null) {
                        tickLabel = formatter.format(currentTickValue);
                    } else {
                        tickLabel = this.getTickUnit().valueToString(currentTickValue);
                    }

                    double angle = 0.0D;
                    TextAnchor anchor;
                    TextAnchor rotationAnchor;
                    if(this.isVerticalTickLabels()) {
                        if(edge == RectangleEdge.LEFT) {
                            anchor = TextAnchor.BOTTOM_CENTER;
                            rotationAnchor = TextAnchor.BOTTOM_CENTER;
                            angle = -1.5707963267948966D;
                        } else {
                            anchor = TextAnchor.BOTTOM_CENTER;
                            rotationAnchor = TextAnchor.BOTTOM_CENTER;
                            angle = 1.5707963267948966D;
                        }
                    } else if(edge == RectangleEdge.LEFT) {
                        anchor = TextAnchor.CENTER_RIGHT;
                        rotationAnchor = TextAnchor.CENTER_RIGHT;
                    } else {
                        anchor = TextAnchor.CENTER_LEFT;
                        rotationAnchor = TextAnchor.CENTER_LEFT;
                    }

                    NumberTick tick = new NumberTick(new Double(currentTickValue), tickLabel, anchor, rotationAnchor, angle);
                    result.add(tick);
                    double nextTickValue = lowestTickValue + (double)(i + 1) * size;

                    for(int minorTick = 1; minorTick < minorTickSpaces; ++minorTick) {
                        double minorTickValue = currentTickValue + (nextTickValue - currentTickValue) * (double)minorTick / (double)minorTickSpaces;
                        if(this.getRange().contains(minorTickValue)) {
                            result.add(new NumberTick(TickType.MINOR, minorTickValue, "", TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
                        }
                    }
                }
            }

            return result;
        }

        public int hashCode() {
            return super.hashCode();
        }
    }


