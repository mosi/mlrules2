/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.simulator.Simulator;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.logging.Logger;

/**
 * 
 * This observer saves all information of the simulator for each step.
 * 
 * @author Tobias Helms
 *
 */
public class VisObserver extends Observer {

	private int counter = 0;

	private final double interval;

	private Map<String, Double> species = new HashMap<>();

	private double time;

	private double currentTime;

	private final double endTime;

	private final boolean withAttributes;

	private final boolean withHierarchy;

	private final boolean shortenReals;

	private Species root;

	public VisObserver(Model model, double endTime, double interval, boolean withAttributes, boolean withHierarchy,
	    boolean shortenReals) {
		super();
		this.endTime = endTime;
		this.withAttributes = withAttributes;
		this.withHierarchy = withHierarchy;
		this.shortenReals = shortenReals;
		this.interval = interval;
	}

	public String toSimpleString(Species s, boolean shortenReals) {
		StringBuilder builder = new StringBuilder();
		builder.append(s.getType().getName());
		if (s.getType().getAttributesSize() > 0) {
			StringJoiner joiner = new StringJoiner(",", "(", ")");
			for (int i = 0; i < s.getType().getAttributesSize(); ++i) {
				if (shortenReals && s.getAttribute(i) instanceof Double) {
					joiner.add("real");
				} else {
					joiner.add(s.getAttribute(i).toString());
				}
			}
			builder.append(joiner.toString());
		}
		return builder.toString();
	}

	private String createKey(Species s, Species parent) {
		return withAttributes ? toSimpleString(s, shortenReals) : s.getType().getName();
	}

	private void createName(Species sub, String upwardKey, Species parent) {
		String key = (withHierarchy
		    ? upwardKey + (sub.getType() != SpeciesType.ROOT ? (parent.getType() == SpeciesType.ROOT ? "" : "/") : "") : "")
		    + (sub.getType() != SpeciesType.ROOT ? createKey(sub, parent) : "");
		if (!key.isEmpty()) {
			species.put(key, sub.getAmount() + species.getOrDefault(key, 0D));
		}
		if (sub instanceof Compartment) {
			createNames(key, (Compartment) sub);
		}
	}

	private void createNames(String upwardKey, Compartment parent) {
		parent.getAllSubSpeciesStream().forEach(s -> createName(s, upwardKey, parent));
	}

	@Override
	public void update(Simulator simulator) {
		// TODO: to extra function
		while ((Double.compare(simulator.getCurrentTime(), counter * interval) == 0
		    || Double.compare(simulator.getNextTime(), counter * interval) > 0)
		    && Double.compare(counter * interval, endTime) <= 0) {
			createNames("", simulator.getModel().getSpecies());
			root = simulator.getModel().getSpecies();
			time = counter * interval;
			notifyListener();
			++counter;
			species.clear();
			this.currentTime = simulator.getCurrentTime();
			Logger.getGlobal().info("steps " + simulator.getSteps() + ", time " + simulator.getCurrentTime() + ", state "
			    + simulator.getModel().getSpecies().toString());
			if (Double.isInfinite(simulator.getNextTime())) {
				break;
			}
		}
	}

	public Map<String, Double> getSpecies() {
		return species;
	}

	public double getTime() {
		return time;
	}

	@Override
	public Optional<Double> nextObservationPoint() {
		double result = counter * interval;
		return result > endTime ? Optional.empty() : Optional.of(result);
	}

	public double getCurrentTime() {
		return currentTime;
	}

	public double getEndTime() {
		return endTime;
	}

	public double getInterval() {
		return interval;
	}

	public Species getRoot() {
		return root;
	}
}
