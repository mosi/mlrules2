package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.model.species.Species;

import java.io.Serializable;

/**
 * Created by Florian Thonig on 16.01.17.
 */
public class SimulationPoint implements Serializable {
    double time;
    Species root;

    public SimulationPoint(double time, Species root) {
        this.time = time;
        this.root = root;
    }

    public double getTime() {
        return time;
    }

    public Species getRoot() {
        return root;
    }
}
