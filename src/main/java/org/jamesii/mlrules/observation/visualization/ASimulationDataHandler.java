package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.model.species.Species;

/**
 * Created by Florian Thonig on 01.04.2017.
 */
public abstract class ASimulationDataHandler implements ISimulationDataHandler {

    private int numSimPoints;

    ASimulationDataHandler() {
        numSimPoints = 0;
        endTime = 0;
    }

    /**
     * @return the number of simulation points inside the handler
     */
    public int getNumSimPoints() {
        return numSimPoints;
    }

    public void setNumSimPoints(int numSimPoints) {
        this.numSimPoints = numSimPoints;
    }

    /**
     * Added endTime, because it is needed for opening plot
     */
    private double endTime;


    @Override
    public double getEndTime() {
        return endTime;
    }

    @Override
    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    /**
     * Addind data as Simulation point to the DataHandler
     *
     * @param time
     * @param root
     */
    public void addData(double time, Species root) {}

    /**
     * function to get all Data
     *
     * @return Iterator of Simulation points
     */
    public SimulationPoint get_next() {return null;}

    /**
     * function to reset the file input
     */
    public void reset() {}

    /**
     * function called, when simulation is over and all data is collected
     */
    public void finished() {}

    /**
     * function called when simulation is closed, to remove the temporary file
     */
    public void clean() {}
}
