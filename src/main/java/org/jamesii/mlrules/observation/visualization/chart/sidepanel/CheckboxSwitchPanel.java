package org.jamesii.mlrules.observation.visualization.chart.sidepanel;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

/**
 * @author Eric Büchner
 */
class CheckboxSwitchPanel extends JPanel {

    CheckboxSwitchPanel(TracePanel legend) {
        this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Controls:"));
        this.setLayout(new GridLayout(0, 2));

        JLabel visLabel = new JLabel("Visible:");
        JLabel highlightLabel = new JLabel("Highlighted:");

        JButton enableVisibleButton = new JButton("enable all");
        enableVisibleButton.addActionListener(e -> {
            for (Trace t : legend.getTraces()) {
                t.getVisibleCheckbox().setSelected(true);
            }
        });

        JButton disableVisibleButton = new JButton("disable all");
        disableVisibleButton.addActionListener(e -> {
            for (Trace t : legend.getTraces()) {
                t.getVisibleCheckbox().setSelected(false);
            }
        });

        JButton enableHighlightsButton = new JButton("enable all");
        enableHighlightsButton.addActionListener(e -> {
            for (Trace t : legend.getTraces()) {
                t.getHighlightCheckbox().setSelected(true);
            }
        });

        JButton disableHighlightsButton = new JButton("disable all");
        disableHighlightsButton.addActionListener(e -> {
            for (Trace t : legend.getTraces()) {
                t.getHighlightCheckbox().setSelected(false);
            }
        });

        // layout setup
        add(visLabel);
        add(highlightLabel);
        add(enableVisibleButton);
        add(enableHighlightsButton);
        add(disableVisibleButton);
        add(disableHighlightsButton);

    }
}
