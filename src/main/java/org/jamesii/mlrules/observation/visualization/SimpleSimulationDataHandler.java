package org.jamesii.mlrules.observation.visualization;

import org.jamesii.mlrules.model.species.Species;

import java.util.Vector;

/**
 * Created by Florian Thonig on 31.01.2017.
 */
public class SimpleSimulationDataHandler extends ASimulationDataHandler {

    private Vector<SimulationPoint> data;
    private int idx;


    public SimpleSimulationDataHandler() {
        super();
        this.data = new Vector<>();
        idx = 0;
    }

    public SimpleSimulationDataHandler(int capacity) {
        super();
        this.data = new Vector<>(capacity);
        idx = 0;
    }

    /**
     * Addind data as Simulation point to the DataHandler
     *
     * @param time
     * @param root
     */
    @Override
    public void addData(double time, Species root) {
        data.add(new SimulationPoint(time, root.copy()));
        setNumSimPoints(getNumSimPoints() + 1);
    }

    /**
     * function to get all Data
     *
     * @return Iterator of Simulation points
     */
    @Override
    public SimulationPoint get_next() {
        if (idx < data.size())
            return data.get(idx++);
        else
            return null;
    }

    /**
     * function to reset the file input
     */
    @Override
    public void reset() {
        idx = 0;
    }

    /**
     * function called, when simulation is over and all data is collected
     */
    @Override
    public void finished() {
    }

    /**
     * function called when simulation is closed, to remove the temporary file
     */
    @Override
    public void clean() {
    }
}
