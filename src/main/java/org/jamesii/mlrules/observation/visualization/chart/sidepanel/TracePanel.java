package org.jamesii.mlrules.observation.visualization.chart.sidepanel;

import org.jamesii.mlrules.observation.visualization.chart.SpeciesOptions;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.util.Comparator;
import java.util.Vector;

/**
 * @author Eric Büchner
 */
public class TracePanel extends JPanel {

    private Vector<Trace> traces;

    TracePanel() {
        super();
        traces = new Vector<>();
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Traces:"));
    }

    public void addTrace(SpeciesOptions so) {
        Trace new_trace = new Trace(so);
        traces.add(new_trace);
        // sort ascending
        traces.sort(new Comparator<Trace>() {
            @Override
            public int compare(Trace o1, Trace o2) {
                if (o1 == null || o2 == null)
                    return -1;
                // compare names
                return (o1.getName().compareTo(o2.getName()));
            }
        });
        add(new_trace, traces.indexOf(new_trace));

        updateUI();
    }

    public void removeAllTraces() {
        removeAll();
        traces.clear();
        updateUI();
    }

    void disable_all() {
        for (Trace i : traces)
            i.disable_all();
    }

    void enable_all() {
        for (Trace i : traces)
            i.enable_all();
    }

    Vector<Trace> getTraces() {
        return traces;
    }
}
