package org.jamesii.mlrules.observation.visualization.chart;

import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.ZoomableChart;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;
import org.jamesii.mlrules.observation.visualization.SimpleSimulationDataHandler;
import org.jamesii.mlrules.observation.visualization.SimulationPoint;
import org.jamesii.mlrules.observation.visualization.chart.sidepanel.AttributePickerPopup;
import org.jamesii.mlrules.observation.visualization.chart.sidepanel.SidePanel;
import org.jamesii.mlrules.observation.visualization.chart.sidepanel.VisualSwitchPanel;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Florian Thonig on 10.01.2017.
 */

/**
 * This class is the plotWindow window and only contains the plotWindow itself
 */
public class PlotWindow extends JFrame {
    private ZoomableChart chart;
    private ASimulationDataHandler dataHandler;
    private SidePanel uipanel;
    public AttributePickerPopup attributeTree;

    private Map<String, SpeciesOptions> species = new HashMap<>();
    private Map<String, SpeciesOptions> workingSet = new HashMap<>();

    private boolean respectAttributes;
    private boolean respectHierarchy;
    private boolean showReals;

    private double endTime;

    private double interval;

    // colors = red, green, blue, gold/yellow, turquoise, violet, followed with a lighter and darker set afterwards
    public final Color[] colorArray =   {new Color(255,0,0), new Color(0,255,0), new Color(0,0,255), new Color(255,215,0), new Color(64,224,208), new Color(238,130,238),
                                         new Color(240,79,72), new Color(144,238,144), new Color(173,216,230), new Color(255,255,0), new Color(175,238,238), new Color(255,128,255),
                                         new Color(139,0,0), new Color(0,100,0), new Color(0,0,139), new Color(255,192,0), new Color(0,206,209), new Color(148,0,211)};
    private int colorStepper;

    public PlotWindow(String title, String xAxisLabel, String yAxisLabel, double endTime, boolean isHierarchy,
                      boolean isReals, boolean isAttributes, long timeStamp) {
        super(title + " - " + timeStamp);
        dataHandler = new SimpleSimulationDataHandler();
        chart = new ZoomableChart();
        chart.getAxisX().setAxisTitle(new IAxis.AxisTitle(xAxisLabel));
        chart.getAxisX().setRangePolicy(new RangePolicyFixedViewport(new Range(0.0, endTime)));
        chart.getAxisY().setAxisTitle(new IAxis.AxisTitle(yAxisLabel));
        chart.getAxisX().setPaintGrid(true);
        chart.getAxisY().setPaintGrid(true);
        chart.setGridColor(Color.GRAY);
        chart.setBackground(Color.LIGHT_GRAY);
        colorStepper = 0;
        respectHierarchy = isHierarchy;
        respectAttributes = isAttributes;
        showReals = isReals;
        this.endTime = endTime;

        attributeTree = new AttributePickerPopup();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // layout setup
        BorderLayout layout = new BorderLayout(0,0);
        setLayout(layout);

        uipanel = new SidePanel();
        add(uipanel, BorderLayout.LINE_END);

        // adding the plotWindow to the frame
        ModifiedChartPanel chart_panel = new ModifiedChartPanel(this);

        VisualSwitchPanel visualswitch = new VisualSwitchPanel(this);
        add(visualswitch, BorderLayout.PAGE_END);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, chart_panel, uipanel);
        splitPane.setDividerLocation(920);
        add(splitPane, BorderLayout.CENTER);
        Dimension minimumSize = new Dimension(50, 900);
        chart_panel.setMinimumSize(minimumSize);
        uipanel.setMinimumSize(minimumSize);

        setSize(1200,900);
        setVisible(false);
    }

    public void clear_all() {
        // clear plotWindow
        chart.removeAllTraces();
        // clear the legend
        uipanel.getTracePanel().removeAllTraces();
        for (SpeciesOptions i : species.values())
            i.clearAll();
        workingSet.clear();
        species.clear();
    }

    public void recalculate() {
        interval = endTime / (dataHandler.getNumSimPoints() - 1);
        clear_all();

        // go through all simulation points and build traces
        // according to the config
        SimulationPoint i;
        int count = 0;
        while ((i = dataHandler.get_next()) != null) {
            build_traces(count, i.getTime());
            // set all to null
            null_values();
            get_value(i.getRoot(), "");
            build_traces(count++, i.getTime());
        }

        this.validate();
        dataHandler.reset();
    }

    public void build_traces(int count, double simTime) {
        for (SpeciesOptions i : species.values()) {
            if (i.getTrace() == null) {
                int num_points = (int)(2*(endTime / interval + 1 - count));
                Trace2DLtd trace = new Trace2DLtd(num_points, "");
                trace.setColor(i.getColor());
                chart.addTrace(trace);
                i.setTrace(trace);
            }
            i.buildTrace(simTime);
        }
    }

    public void finish() {
        attributeTree.tree.checkSubTree(attributeTree.tree.getPathForRow(0), true);
        uipanel.enable_all();
        build_tree();
    }

    public void disable_side_panel() {
        uipanel.disable_all();
    }

    public ZoomableChart getChart() {
        return chart;
    }

    public void setDataHandler(ASimulationDataHandler data) {
        this.dataHandler = data;
    }

    public boolean isRespectAttributes() {
        return respectAttributes;
    }

    public void setRespectAttributes(boolean respectAttributes) {
        this.respectAttributes = respectAttributes;
    }

    public boolean isRespectHierarchy() {
        return respectHierarchy;
    }

    public void setRespectHierarchy(boolean respectHierarchy) {
        this.respectHierarchy = respectHierarchy;
    }

    public boolean isShowReals() {
        return showReals;
    }

    public void setShowReals(boolean showReals) {
        this.showReals = showReals;
    }

    private Object lock = new Object();

    public void get_value(Species species, String key) {
        synchronized(lock) {
            // build unique name
            String name = key + ((species.getType()
                                         .getName()
                                         .compareTo("$$ROOT$$") == 0) ? "" : species.getType()
                                                                                    .getName());
            String real_name = build_attributes_string(species, name);
            // looking for the attributes
            if (isRespectAttributes()) {
                if (name.compareTo(real_name) != 0) {
                    modifySpecOpt(real_name, species);
                } else if (!name.isEmpty()) {
                    modifySpecOpt(name, species);
                }
            } else if (!name.isEmpty()) {
                modifySpecOpt(name, species);
            }
            // sub-compartments
            if ((species instanceof Compartment)) {
                Compartment compartment = (Compartment) species;
                if (!compartment.getAllSpecies()
                                .isEmpty()) {
                    compartment.getAllSubSpeciesStream()
                               .forEach((Species s) -> {
                                   if (isRespectAttributes()) {
                                       get_value(s, (isRespectHierarchy()) ? (real_name + ((name.isEmpty()) ? "" : "/")) : "");
                                   } else {
                                       get_value(s, (isRespectHierarchy()) ? (name + ((name.isEmpty()) ? "" : "/")) : "");
                                   }
                               });
                } else if (!compartment.getSubCompartments()
                                       .isEmpty()) {
                    compartment.getSubCompartmentsStream()
                               .forEach((Compartment c) -> {
                                   if (isRespectAttributes()) {
                                       get_value(c, (isRespectHierarchy()) ? (real_name + ((name.isEmpty()) ? "" : "/")) : "");
                                   } else {
                                       get_value(c, (isRespectHierarchy()) ? (name + ((name.isEmpty()) ? "" : "/")) : "");
                                   }
                               });
                }
            }
        }
    }

    private String build_attributes_string(Species species, String name) {
       String attributes = "";
       SpeciesOptions so = getNodeSpeciesOptions(name);
       for (int i = 0; i < species.getType().getAttributesSize(); i++) {
           if (so == null) {
               so = new SpeciesOptions(species, getNewColor(), attributeTree.tree, this);
           }
               if (!so.getSpecOptAttributes().elementAt(i).isChecked()) {
                   attributes += (attributes.isEmpty()) ? "_" : (", _");
                   continue;
               }
           if (!isShowReals() && species.getAttribute(i) instanceof Double)
               attributes += (attributes.isEmpty()) ? "real" : (", real");
           else
               attributes += (attributes.isEmpty()) ? species.getAttribute(i).toString() : (", " + species.getAttribute(i).toString());
       }
       return (attributes.isEmpty())? name : name + "(" + attributes + ")";
    }

    private SpeciesOptions getNodeSpeciesOptions(String name) {
        for (Map.Entry<String, SpeciesOptions> i : species.entrySet()) {
            if (i.getValue().getSpecies().getType().getName().compareTo(name) == 0)
                return i.getValue();
        }
        return null;
    }

    public void null_values() {
        for (SpeciesOptions i : species.values()) {
            i.setActual_value(0.0);
        }
    }

    public void build_tree() {
        for (SpeciesOptions i : species.values()) {
            if (!attributeTree.hasChild(i.getSpecies().getType().getName())){
                i.createNode(attributeTree.root);
            }
        }
        attributeTree.refresh();
        attributeTree.tree.checkSubTree(attributeTree.tree.getPathForRow(0), true);
    }

    public void setInterval(double interval) {
        this.interval = interval;
    }

    public Color getNewColor() {
        return colorArray[colorStepper++%colorArray.length];
    }

    private void modifySpecOpt(String name, Species species) {
        SpeciesOptions specopts = this.species.get(name);
        if (specopts == null) {
            specopts = new SpeciesOptions(species, getNewColor(), attributeTree.tree, this);
            specopts.setName(name);
            this.species.put(name, specopts);
        }
        if (specopts.isShown() == false) {
            uipanel.getTracePanel().addTrace(specopts);
            this.validate();
            specopts.setShown(true);
        }
        specopts.addToActual_value(species.getAmount());
    }

    public ASimulationDataHandler getDataHandler() {
        return dataHandler;
    }

    public double getEndTime() {
        return endTime;
    }

    public double getInterval() {
        return interval;
    }

    public Map<String, SpeciesOptions> getSpecies() {
        return species;
    }
}