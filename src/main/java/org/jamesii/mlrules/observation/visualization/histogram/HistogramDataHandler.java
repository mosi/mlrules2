package org.jamesii.mlrules.observation.visualization.histogram;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;
import org.jamesii.mlrules.observation.visualization.SimulationPoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import static org.jamesii.mlrules.model.species.SpeciesType.AttributeType.*;

/**
 * @author Eric Büchner
 */
class HistogramDataHandler {

    private ArrayList<double[]> dataList;
    private HashMap<String, HashMap> stringMap;
    private ArrayList<Double> rawList;
    private int stepCounter;
    private int maxY;
    private double steps;
    private String speciesName;
    private int attributeNum;

    HistogramDataHandler(ASimulationDataHandler dataHandler, HistogramInitFrame histogramInitFrame,
                         HistogramFrame histogramFrame, String speciesName, Enum attributeType, int attrNum) {
        super();
        this.speciesName = speciesName;
        this.attributeNum = attrNum;
        this.steps = histogramInitFrame.getSteps();

        dataList = new ArrayList<>();
        stringMap = new HashMap<>();

        for (int i = 0; i < steps; i++) {
            stepCounter = i;
            rawList = new ArrayList<>();
            SimulationPoint tempRoot = dataHandler.get_next();

            if (attributeType == NUM) {
                fillDataSetNUM(tempRoot.getRoot());
                buildArray(rawList);
            } else if (attributeType == BOOL) {
                fillDataSetBOOL(tempRoot.getRoot());
                buildArray(rawList);
                histogramFrame.modifyRangeAxis(new String[]{"false", "true"});
            } else if (attributeType == STRING) {
                fillDataSetSTRING(tempRoot.getRoot());
            } else if (attributeType == LINK) {
                fillDataSetLINK(tempRoot.getRoot());
                buildArray(rawList);
                histogramFrame.modifyRangeAxis(new String[]{"free", "linked"});
            }

        }
        dataHandler.reset();

        if (attributeType == STRING) {

            buildStringArray(stringMap);

            Set keys = stringMap.keySet();
            Object[] arr = keys.toArray(new String[keys.size()]);
            Arrays.sort(arr);
            String[] stringArray = Arrays.copyOf(arr, arr.length + 2, String[].class);
            stringArray[stringArray.length - 2] = "";
            stringArray[stringArray.length - 1] = "";

            histogramFrame.modifyRangeAxis(stringArray);
        }

    }

    ArrayList<double[]> getDataList() {
        return dataList;
    }

    double findMaxY(double min, double max, double interval) {

        // account for additional bin on the left and right
        min = min - interval;
        max = max + interval;
        int binCount = (int) Math.ceil((max - min) / interval);

        // go over all arrays from the dataList, emulate the bin sorting process of JFreeChart, and search for the
        // biggest bin, then check for the next array and update maxY if its bigger
        for (int i = 0; i < dataList.size(); i++) {

            double[] temp = dataList.get(i);
            int[] counter = new int[binCount];

            // emulate bin sorting
            for (int j = 0; j < temp.length; j++) {

                for (int k = 1; k <= binCount; k++) {
                    // checks if the value is inside of the currently simulated interval
                    if (temp[j] < min + (k * interval) && temp[j] >= min + ((k - 1) * interval)) {
                        counter[k - 1] = counter[k - 1] + 1;
                    }
                }

                // check if a bin is bigger than the old maximum
                for (int l = 0; l < counter.length; l++) {
                    if (this.maxY < counter[l]) {
                        this.maxY = counter[l];
                    }

                }
            }
        }
        // adding a constant to make sure there is some "air" between the bar and the end of the plotWindow panel
        return this.maxY + 10;
    }

    void normDataList(double min, double max, double interval) {

        // normalizes the dataList and its arrays, so that values that exceed the user given min and max values are
        // gathered into the additional bin on the far left and right
        for (int j = 0; j < dataList.size(); j++) {

            double[] set = dataList.get(j);
            for (int i = 0; i < set.length; i++) {
                if (set[i] < min) {
                    set[i] = min - (interval / 2);
                } else if (set[i] >= max) {
                    set[i] = max;

                }
            }
            dataList.set(j, set);
        }

    }

    private void fillDataSetNUM(Species species) {
        // search for correct species and attribute and add them to set
        if (species.getType().getName().compareTo(speciesName) == 0) {
            for (int i = 0; i < species.getAmount(); i++) {
                if (species.getAttribute(attributeNum) instanceof Double) {
                    rawList.add((double) species.getAttribute(attributeNum));
                }
            }
        }
        // search through all compartments
        if (species instanceof Compartment) {
            Compartment c = (Compartment) species;
            if (!c.getAllSpecies().isEmpty())
                c.getAllSubSpeciesStream().forEach(this::fillDataSetNUM);
        }
    }

    private void fillDataSetBOOL(Species species) {
        // search for correct species and attribute and add them to set
        if (species.getType().getName().compareTo(speciesName) == 0) {
            for (int i = 0; i < species.getAmount(); i++) {
                if ((Boolean) species.getAttribute(attributeNum)) {
                    rawList.add(1.0d);
                } else {
                    rawList.add(0.0d);
                }
            }
        }
        // search through all compartments
        if (species instanceof Compartment) {
            Compartment c = (Compartment) species;
            if (!c.getAllSpecies().isEmpty())
                c.getAllSubSpeciesStream().forEach(this::fillDataSetBOOL);
        }
    }

    private void fillDataSetSTRING(Species species) {
        // search for unique species and save them in a map
        if (species.getType().getName().equals(speciesName)) {

            String str = species.getAttribute(attributeNum).toString();

            if (!stringMap.containsKey(str)) {
                HashMap<Integer, Double> tempMap = new HashMap<>();
                tempMap.put(stepCounter, species.getAmount());
                stringMap.put(str, tempMap);
            } else {
                if (stringMap.get(str).containsKey(stepCounter)) {
                    HashMap<Integer, Double> tempMap = stringMap.get(str);
                    tempMap.put(stepCounter, (double) stringMap.get(str).get(stepCounter) + species.getAmount());
                    stringMap.put(str, tempMap);
                } else {
                    HashMap<Integer, Double> tempMap = stringMap.get(str);
                    tempMap.put(stepCounter, species.getAmount());
                    stringMap.put(str, tempMap);
                }
            }
        }
        // recursion in case of compartment
        if (species instanceof Compartment) {
            Compartment c = (Compartment) species;
            if (!c.getAllSpecies().isEmpty())
                c.getAllSubSpeciesStream().forEach(this::fillDataSetSTRING);
        }
    }

    private void fillDataSetLINK(Species species) {
        // search for correct species and attribute and add them to set
        if (species.getType().getName().compareTo(speciesName) == 0) {
            for (int i = 0; i < species.getAmount(); i++) {
                if (species.getAttribute(attributeNum).toString().equals("link")) {
                    rawList.add(1.0d);
                } else if (species.getAttribute(attributeNum).toString().equals("free")) {
                    rawList.add(0.0d);
                }
            }
        }
        // search through all compartments
        if (species instanceof Compartment) {
            Compartment c = (Compartment) species;
            if (!c.getAllSpecies().isEmpty())
                c.getAllSubSpeciesStream().forEach(this::fillDataSetLINK);
        }
    }

    private void buildArray(ArrayList<Double> list) {
        // changes rawDataSet into Array and adds them as element to the final ArrayList because JFreeChart needs Arrays
        double arr[] = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            arr[i] = list.get(i);
        }
        dataList.add(arr);
    }

    private void buildStringArray(HashMap<String, HashMap> map) {
        Set keys = map.keySet();
        Object[] temp = keys.toArray(new String[keys.size()]);
        String[] strTemp = Arrays.copyOf(temp, temp.length, String[].class);
        Arrays.sort(strTemp);

        // for each simulation stepCounter
        for (int i = 0; i < steps; i++) {
            rawList = new ArrayList<>();
            // for each unique found string attribute
            for (int j = 0; j < strTemp.length; j++) {
                String str = strTemp[j];
                // when it occurs at stepCounter, add respective double values
                if (map.get(str).containsKey(i)) {
                    Object amt = map.get(str).get(i);
                    Double dbl = (Double) amt;
                    int x = dbl.intValue();
                    for (int k = 0; k < x; k++) {
                        rawList.add(0.0d + j);
                    }
                }

            }

            double arr[] = new double[rawList.size()];
            for (int j = 0; j < rawList.size(); j++) {
                arr[j] = rawList.get(j);
            }
            dataList.add(arr);
        }
    }

}

