package org.jamesii.mlrules.observation.visualization.chart.sidepanel;

import org.jamesii.mlrules.observation.visualization.chart.SpeciesOptions;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author Eric Büchner
 */
public class Trace extends JPanel {

    private JCheckBox visibleCheckbox;
    private JButton colorButton;
    private JCheckBox highlightCheckbox;
    private JLabel nameLabel;

    Trace(SpeciesOptions so) {
        super();
        setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        this.setPreferredSize(new Dimension(200, 55));
        setMinimumSize(new Dimension(250, 55));
        setMaximumSize(new Dimension(1000, 55));

        visibleCheckbox = new JCheckBox("Visible");
        visibleCheckbox.setSelected(true);
        visibleCheckbox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (ItemEvent.SELECTED == e.getStateChange()) {
                    so.getTrace().setVisible(true);
                } else {
                    so.getTrace().setVisible(false);
                }
            }
        });

        colorButton = new JButton();
        colorButton.setPreferredSize(new Dimension(40, 16));
        colorButton.setMinimumSize(new Dimension(40, 16));
        colorButton.setMaximumSize(new Dimension(40, 16));
        colorButton.setBackground(so.getColor());
        colorButton.setEnabled(true);
        colorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color colorPick = JColorChooser.showDialog(null, "ColorPick", so.getColor());
                so.setColor(colorPick);
                colorButton.setBackground(colorPick);
            }
        });

        highlightCheckbox = new JCheckBox("Highlight");
        highlightCheckbox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (ItemEvent.SELECTED == e.getStateChange()) {
                    so.getTrace().setStroke(new BasicStroke(3));
                } else {
                    so.getTrace().setStroke(new BasicStroke());
                }
            }
        });

        this.nameLabel = new JLabel(so.getName());
        this.nameLabel.setToolTipText(so.getName());

        // layout setup
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);

        GridBagConstraints c = new GridBagConstraints();

        // colorPicker
        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(0, 7, 0, 0);
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        this.add(colorButton, c);

        // visible checkbox
        c.gridx = 1;
        c.gridy = 1;
        this.add(visibleCheckbox, c);

        // highlight checkbox
        c.gridx = 2;
        c.gridy = 1;
        this.add(highlightCheckbox, c);

        // nameLabel
        c.insets = new Insets(0, 7, 2, 0);
        c.weightx = 0.5;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 0;
        this.add(nameLabel, c);
    }

    @Override
    public String getName() {
        return nameLabel.getText();
    }

    void disable_all() {
        colorButton.setEnabled(false);
    }

    void enable_all() {
        colorButton.setEnabled(true);
    }

    JCheckBox getVisibleCheckbox() {
        return visibleCheckbox;
    }

    JCheckBox getHighlightCheckbox() {
        return highlightCheckbox;
    }
}
