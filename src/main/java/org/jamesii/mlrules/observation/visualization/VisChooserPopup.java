package org.jamesii.mlrules.observation.visualization;


import org.jamesii.mlrules.observation.visualization.chart.PlotWindow;
import org.jamesii.mlrules.observation.visualization.histogram.HistogramInitFrame;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

/**
 * @author Eric Büchner
 */
class VisChooserPopup extends JFrame {

    private boolean isModelVisible = false;
    private JPanel panel;

    VisChooserPopup(double endTime, boolean respectHierarchy, boolean showReals, boolean respectAttributes,
                           ASimulationDataHandler dataHandler, long timeStamp, String model) {
        super("Choose Visualisation - " + timeStamp);
        this.setPreferredSize(new Dimension(400, 370));
        this.setMinimumSize(new Dimension(400, 168));
        this.setResizable(false);
        this.setVisible(true);

        // button to start plot view
        JButton plotButton = new JButton("New Line Plot");
        plotButton.setPreferredSize(new Dimension(180, 27));
        plotButton.addActionListener(e -> {
            PlotWindow plotWindow = new PlotWindow("Line Plot", "Time", "Species Amount",
                    endTime, respectHierarchy, showReals, respectAttributes, timeStamp);
            plotWindow.setDataHandler(dataHandler);
            plotWindow.recalculate();
            plotWindow.finish();
            plotWindow.setVisible(true);
        });

        // button to start histogram view
        JButton histogramButton = new JButton("New Histogram");
        histogramButton.setPreferredSize(new Dimension(180, 27));
        histogramButton.addActionListener(e -> new HistogramInitFrame(dataHandler, timeStamp));

        JTextArea modelTextArea = new JTextArea(model);
        modelTextArea.setEditable(false);
        JScrollPane modelScrollPane = new JScrollPane(modelTextArea);
        modelScrollPane.setPreferredSize(new Dimension(366, 330));

        // make model text available
        JButton showModelButton = new JButton("Toggle Model");
        showModelButton.setPreferredSize(new Dimension(180, 27));
        showModelButton.addActionListener(e -> {
            if (isModelVisible) {
                this.setSize(400, 168);
                panel.remove(modelScrollPane);
                panel.revalidate();
                panel.repaint();
                isModelVisible = false;
            } else {
                this.setSize(400, 500);
                panel.add(modelScrollPane);
                panel.revalidate();
                panel.repaint();
                isModelVisible = true;
            }
        });

        // setup layout
        JPanel mainPanel = new JPanel();
        BorderLayout layout = new BorderLayout();
        mainPanel.setLayout(layout);
        mainPanel.setPreferredSize(new Dimension(400, 370));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(400, 70));
        buttonPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Visuals:"));
        buttonPanel.add(plotButton);
        buttonPanel.add(histogramButton);

        mainPanel.add(buttonPanel, BorderLayout.PAGE_START);

        panel = new JPanel();
        new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Model:"));
        panel.add(showModelButton);

        mainPanel.add(panel, BorderLayout.CENTER);

        this.getContentPane().add(mainPanel);
        this.revalidate();
        this.repaint();

    }
}
