package org.jamesii.mlrules.observation.visualization.histogram;

import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * @author Eric Büchner
 */
class HistogramInitChooser extends JPanel {

    private HistogramInitFrame initFrame;
    private String speciesName;
    private HistogramInitFrame frame;
    private ASimulationDataHandler dataHandler;
    private long timeStamp;

    HistogramInitChooser(String sName, Enum[] attributes, HistogramInitFrame frame,
                                   ASimulationDataHandler dataHandler, long timeStamp) {
        super();
        this.speciesName = sName;
        this.initFrame = frame;
        this.frame = frame;
        this.dataHandler = dataHandler;
        this.timeStamp = timeStamp;
        this.setMaximumSize(new Dimension(3000, 68));
        TitledBorder border;
        border = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), speciesName);
        this.setBorder(border);

        generateButtons(attributes);

    }

    private void generateButtons(Enum[] attr) {
        for (int i = 0; i < attr.length; i++) {
            Enum attributeType = attr[i];
            String buttonLabel = attributeType.toString();
            int idx = i;
            JButton btn = new JButton(idx + " " + buttonLabel);
            if (buttonLabel.equals("NUM")) {
                btn.addActionListener(e -> {
                    new HistogramInitIntervalChooser(dataHandler, frame, speciesName, attributeType, idx, timeStamp);
                    initFrame.setVisible(false);
                });

            } else {
                btn.addActionListener(e -> {
                    new HistogramFrame(0.0d, 2.0d, 1.0d, dataHandler,
                            frame, speciesName, attributeType, idx, timeStamp);
                    initFrame.dispose();
                });

            }
            this.add(btn);
        }
    }
}
