package org.jamesii.mlrules.observation.visualization.chart.sidepanel;

import org.jamesii.mlrules.util.JCheckBoxTree;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.util.Enumeration;

/**
 * @author Eric Büchner
 */
public class AttributePickerPopup extends JFrame {
    public JCheckBoxTree tree;
    public DefaultMutableTreeNode root;

    public AttributePickerPopup() {
        super("Showed Attributes");
        setResizable(true);
        this.setSize(250, 500);

        this.getContentPane().setLayout(new BorderLayout());

        // checkbox structure
        tree = new JCheckBoxTree();
        root = new DefaultMutableTreeNode("respect Attributes");
        tree.setModel(new DefaultTreeModel(root));

        this.getContentPane().add(tree);

    }

    public void refresh() {
        tree.setModel(new DefaultTreeModel(root));
    }

    public boolean hasChild(String str) {
        for (Enumeration<TreeNode> i = root.children(); i.hasMoreElements(); ) {
            if (((String) ((DefaultMutableTreeNode)i.nextElement()).getUserObject()).compareTo(str) == 0) {
                return true;
            }
        }
        return false;

    }

}
