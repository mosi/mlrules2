package org.jamesii.mlrules.observation.visualization.chart;

import info.monitorenter.gui.chart.ITracePoint2D;
import info.monitorenter.gui.chart.TracePoint2D;
import info.monitorenter.gui.chart.events.PopupListener;
import info.monitorenter.gui.chart.views.ChartPanel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * @author Eric Büchner
 */
public class ModifiedChartPanel extends ChartPanel {
    private JPopupMenu popup;
    private JMenuItem csvItem;

    public ModifiedChartPanel(PlotWindow window) {
        super(window.getChart());
        csvItem = new JMenuItem("save as CSV");
        csvItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();

                FileNameExtensionFilter filter =
                        new FileNameExtensionFilter("csv", "csv");
                fc.addChoosableFileFilter(filter);
                fc.setFileFilter(filter);

                if (fc.showSaveDialog(csvItem) == JFileChooser.APPROVE_OPTION) {
                    try {
                        // set up file
                        FileWriter fw = new FileWriter(fc.getSelectedFile().getAbsolutePath() + ".csv");
                        BufferedWriter bw = new BufferedWriter(fw);

                        Map<String, Vector<ITracePoint2D>> traces_values = generatePoints(window);

                        // write headline
                        bw.write("time");
                        for (String i : traces_values.keySet()) {
                            bw.write(",\"" + i + "\""); // escape the names
                        }
                        /*  finding a vector of points, that existed at the simulation start point
                            to get all the "real" x - values
                         */
                        Vector<ITracePoint2D> initial = find_global_min(traces_values);
                        if (initial == null) {
                            JOptionPane.showMessageDialog(null, "Can't find global minimum.", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        //write value
                        String line;
                        int count = 0;
                        for (ITracePoint2D super_point : initial) {
                            line = Double.toString((count++) * window.getInterval());
                            for (Vector<ITracePoint2D> i : traces_values.values()) {
                                line += "," + Double.toString(getValueAt(super_point.getX(), i));
                            }
                            bw.newLine();
                            bw.write(line);
                        }
                        bw.flush();
                        bw.close();
                        fw.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        MouseListener[] listeners = window.getChart().getMouseListeners();

        for (MouseListener listener : listeners) {
            if (listener instanceof PopupListener) {
                PopupListener popupListener = (PopupListener) listener;
                popup = popupListener.getPopup();
                popup.add(csvItem);

            }
        }
    }

    private Map<String, Vector<ITracePoint2D>> generatePoints(PlotWindow window) {
        Map<String, Vector<ITracePoint2D>> traces_values = new HashMap<>();
        for (SpeciesOptions so : window.getSpecies().values()) {
            if (so.isShown()) { // only put the "active"/shown traces into the csv
                Vector<ITracePoint2D> points = new Vector<>();
                Iterator<ITracePoint2D> i = so.getTrace().iterator();
                // putting all the points in
                while (i.hasNext()) {
                    points.add(i.next());
                }
                // now sort...
                points.sort(new Comparator<ITracePoint2D>() {
                    @Override
                    public int compare(ITracePoint2D o1, ITracePoint2D o2) {
                        return Double.compare(o1.getX(), o2.getX());
                    }
                });

                // now filter out all the points, that were added to achieve that "stairstep" plot
                Vector<ITracePoint2D> filter = new Vector<>();
                for (int j = 0; j < points.size(); j++) {
                    if (j != 0)
                        if (points.elementAt(j).getX() == points.elementAt(j - 1).getX())
                            continue;
                    filter.add(points.elementAt(j));
                }
                traces_values.put(so.getName(), filter);
            }
        }
        return traces_values;
    }

    private double getValueAt(double xVal, Vector<ITracePoint2D> vec) {
        int idx = (Collections.binarySearch(vec, new TracePoint2D(xVal, 0.0), new Comparator<ITracePoint2D>() {
            @Override
            public int compare(ITracePoint2D o1, ITracePoint2D o2) {
                return Double.compare(o1.getX(), o2.getX());
            }
        }));
        return (idx >= 0) ? vec.elementAt(idx).getY() : 0.0;
    }

    private Vector<ITracePoint2D> find_global_min(Map<String, Vector<ITracePoint2D>> traces_values) {
        for (Vector<ITracePoint2D> i : traces_values.values()) {
            if (getValueAt(0.0, i) != 0.0) {
                return i;
            }
        }
        return null;
    }
}