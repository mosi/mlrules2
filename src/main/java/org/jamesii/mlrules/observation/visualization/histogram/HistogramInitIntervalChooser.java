package org.jamesii.mlrules.observation.visualization.histogram;

import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;

import javax.swing.*;
import java.awt.*;


/**
 * @author Eric Büchner
 */
class HistogramInitIntervalChooser extends JFrame {
    private JTextField interval;
    private JTextField maxRange;
    private JTextField minRange;
    private double intervalValue;
    private double minRangeValue;
    private double maxRangeValue;

    HistogramInitIntervalChooser(ASimulationDataHandler dataHandler, HistogramInitFrame histogramInitFrame,
                                 String speciesName, Enum attrType, int attrNum, long timeStamp) {
        super("Choose Interval - " + timeStamp);
        this.setMinimumSize(new Dimension(300, 180));
        HistogramInitIntervalChooser intervalChooser = this;
        GridBagLayout gridBag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        this.setLayout(gridBag);
        c.fill = GridBagConstraints.HORIZONTAL;

        JLabel label1 = new JLabel("min value:");
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(5, 5, 0, 5);
        gridBag.setConstraints(label1, c);
        this.add(label1);

        minRange = new JTextField(String.valueOf(0.0d), 4);
        minRange.setHorizontalAlignment(JTextField.CENTER);
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        gridBag.setConstraints(minRange, c);
        this.add(minRange);

        JLabel label2 = new JLabel("max value (exclusive):");
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        gridBag.setConstraints(label2, c);
        this.add(label2);

        maxRange = new JTextField(String.valueOf(10.0d), 4);
        maxRange.setHorizontalAlignment(JTextField.CENTER);
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 1;
        gridBag.setConstraints(maxRange, c);
        this.add(maxRange);

        JLabel label3 = new JLabel("interval step:");
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 2;
        gridBag.setConstraints(label3, c);
        this.add(label3);

        interval = new JTextField(String.valueOf(1.0d), 4);
        interval.setHorizontalAlignment(JTextField.CENTER);
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        gridBag.setConstraints(interval, c);
        this.add(interval);

        JButton start = new JButton("generate");
        start.addActionListener(e -> {
            try {
                intervalValue = Double.parseDouble(interval.getText());
                minRangeValue = Double.parseDouble(minRange.getText());
                maxRangeValue = Double.parseDouble(maxRange.getText());

                double absLength = maxRangeValue - minRangeValue;

                if (absLength / intervalValue == Double.NaN) { // workaround because double values and modulo are no friends
                    JOptionPane.showMessageDialog(null, "Interval does not fit into observed range.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    throw new RuntimeException("");
                }

                if (minRangeValue >= maxRangeValue) {
                    JOptionPane.showMessageDialog(null, "Minimum cannot exceed maximum.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    throw new RuntimeException("");
                }

                new HistogramFrame(minRangeValue, maxRangeValue, intervalValue, dataHandler, histogramInitFrame,
                        speciesName, attrType, attrNum, timeStamp);
                histogramInitFrame.dispose();
                intervalChooser.dispose();

            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Values have to be numeric.", "Error",
                        JOptionPane.ERROR_MESSAGE);
            } catch (NegativeArraySizeException e2) {
                JOptionPane.showMessageDialog(null, "Interval cannot be negative.", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }

        });

        c.weightx = 0.0;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 3;
        c.insets = new Insets(5, 5, 5, 5);
        gridBag.setConstraints(start, c);
        this.add(start);

        JButton back = new JButton("cancel");
        back.addActionListener(e -> {
            histogramInitFrame.setVisible(true);
            intervalChooser.dispose();
        });

        c.weightx = 0.0;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 3;
        c.insets = new Insets(5, 5, 5, 5);
        gridBag.setConstraints(back, c);
        this.add(back);

        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

}
