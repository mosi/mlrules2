package org.jamesii.mlrules.observation.visualization.histogram;

import org.jfree.data.statistics.SimpleHistogramDataset;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Eric Büchner
 */

class HistogramFrameBottomPanel extends JPanel {

    private JSlider slider;
    private JButton autoPlay;
    private JTextField speedField;
    private java.util.Timer timer;
    private TimerTask task;
    private float speed;
    private boolean isRunning = false;
    private boolean isFinished = false;
    private int dataListSize;
    private HistogramFrameBottomPanel hPanel;

    HistogramFrameBottomPanel(HistogramDataHandler histogramData, SimpleHistogramDataset dataSet) {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

        hPanel = this;
        dataListSize = histogramData.getDataList().size();

        // histogram-slider
        slider = new JSlider();
        slider.setMinimum(0);
        slider.setMaximum(dataListSize - 1);
        slider.setMajorTickSpacing(5);
        slider.setMinorTickSpacing(1);
        slider.createStandardLabels(1);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setValue(0);
        slider.addChangeListener(e -> {
            // empty bins and sort another dataSet
            dataSet.clearObservations();
            dataSet.addObservations(histogramData.getDataList().get(slider.getValue()));
        });


        timer = new Timer();
        autoPlay = new JButton("auto");
        autoPlay.setPreferredSize(new Dimension(80, 25));
        autoPlay.addActionListener(e -> {
            // check if autoPlay is already running, either start a new TimerTask and move the slider step by step
            // or cancel the currently running TimerTask, set the isRunning flag accordingly
            // if the autoPlay sequence is finished, a restart is offered
            try {
                if (!isRunning) {
                    task = new TimerTask() {
                        @Override
                        public void run() {
                            if (slider.getValue() != dataListSize - 1) {
                                slider.setValue(slider.getValue() + 1);
                                isRunning = true;
                                autoPlay.setText("pause");
                                hPanel.speed = Float.parseFloat(speedField.getText());
                                speedField.setEnabled(false);

                            } else if (isFinished) {
                                slider.setValue(0);
                                isRunning = true;
                                isFinished = false;
                                autoPlay.setText("pause");
                                hPanel.speed = Float.parseFloat(speedField.getText());
                                speedField.setEnabled(false);

                            } else {
                                cancel();
                                isRunning = false;
                                isFinished = true;
                                autoPlay.setText("restart");
                                speedField.setEnabled(true);

                            }
                        }
                    };
                    speed = Float.parseFloat(speedField.getText());
                    timer.schedule(task, 0, (int) Math.ceil(1000 * speed));

                } else {
                    task.cancel();
                    isRunning = false;
                    autoPlay.setText("auto");
                    speedField.setEnabled(true);
                }
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(null, "Delay has to be number.", "Error",
                        JOptionPane.ERROR_MESSAGE);

            } catch (IllegalArgumentException e2) {
                JOptionPane.showMessageDialog(null, "Delay has to be positive.", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });

        speedField = new JTextField(String.valueOf(1.0f), 4);
        speedField.setHorizontalAlignment(JFormattedTextField.CENTER);
        speedField.setPreferredSize(new Dimension(25, 25));
        speedField.setMaximumSize(new Dimension(25, 25));
        speedField.setEnabled(true);

        JLabel label = new JLabel("delay:");
        label.setToolTipText("delay:");
        label.setPreferredSize(new Dimension(40, 25));

        this.add(Box.createRigidArea(new Dimension(10, 0)));
        this.add(slider);
        this.add(Box.createRigidArea(new Dimension(5, 0)));
        this.add(label);
        this.add(Box.createRigidArea(new Dimension(5, 0)));
        this.add(speedField);
        this.add(Box.createRigidArea(new Dimension(5, 0)));
        this.add(autoPlay);
        this.add(Box.createRigidArea(new Dimension(10, 0)));

    }
}
