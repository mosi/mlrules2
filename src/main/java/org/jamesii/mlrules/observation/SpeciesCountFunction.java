package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;

import java.util.stream.Stream;

public class SpeciesCountFunction extends AbstractStateTraversal<Double> {

	public SpeciesCountFunction(Compartment root) {
		super(root);
	}

	@Override
	protected Double aggregate(Stream<Double> matches) {
		return matches.mapToDouble(d -> d).sum();
	}

	@Override
	protected Double processLeaf(LeafSpecies leaf) {
		return leaf.getAmount();
	}

	@Override
	protected Double processCompartment(Compartment compartment) {
		return 1.0;
	}
}
