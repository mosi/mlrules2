/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.util.Pair;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

/**
 * @author Tom Warnke
 */
public class SpeciesCountListener implements Listener {

	private final String species;

	private final Consumer<Pair<Double, Double>> callback;

	private Compartment root = null;

	private SpeciesCountFunction function = null;

	public SpeciesCountListener(String species, Consumer<Pair<Double, Double>> callback) {
		this.species = species;
		this.callback = callback;
	}

	@Override
	public void notify(Observer o) {

		if (o instanceof TimeTriggeredObserver) {

			TimeTriggeredObserver observer = (TimeTriggeredObserver) o;

			// root and function have to be set each step since the root can change due to rejections
			root = observer.getSpecies();
			function = new SpeciesCountFunction(root);

			Double result = function.apply(species);
			callback.accept(new Pair<>(observer.getTime(),result));
		}
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public void finish(StopCondition stopCondition) {
		// do nothing
	}

}
