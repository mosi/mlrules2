/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

/**
 * Searches the given model state for entities of the given species and collects
 * the values of the n-th attribute of these entities in a map of entity ID to attribute value.
 * The map is then fed to a callback.
 * The IDs are not guaranteed to remain the same across observation time points.
 *
 * @author Tom Warnke
 */
public class AttributeListener implements Listener {

  private final String species;

  private final int attribute;

  private final Consumer<Pair<Double, Map<String, Object>>> callback;

  private Compartment root = null;

  private AttributeCollectionFunction function = null;

  public AttributeListener(String species, int attribute, Consumer<Pair<Double, Map<String, Object>>> callback) {
    this.species = species;
    this.attribute = attribute;
    this.callback = callback;
  }

  @Override
  public void notify(Observer o) {

    if (o instanceof TimeTriggeredObserver) {

      TimeTriggeredObserver observer = (TimeTriggeredObserver) o;

      if (root == null) {
        root = observer.getSpecies();
        function = new AttributeCollectionFunction(root, attribute);
      }

      Map<String, Object> result = function.apply(species);
      callback.accept(new Pair<>(observer.getTime(), result));
    }
  }

  public boolean isActive() {
    return true;
  }

  public void finish(StopCondition stopCondition) {

  }
}
