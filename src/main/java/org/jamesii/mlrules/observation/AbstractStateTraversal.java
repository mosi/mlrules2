package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;

import java.util.function.Function;
import java.util.stream.Stream;

/**
 * <p>
 * An abstract function for traversing the species tree of an ML-Rules state.
 * It is only evaluated on the nodes that match the filter string which is given as an argument.
 * Subclasses must override the methods {@link #processLeaf(LeafSpecies)}, {@link #processCompartment(Compartment)}, and
 * {@link #aggregate(Stream<T>)}.
 * {@link #processLeaf(LeafSpecies)} and {@link #processCompartment(Compartment)} are invoked on every species in the
 * tree that are targeted by the filter string.
 * {@link #aggregate(Stream<T>)} is invoked to combine the results of processing the individual nodes.
 * </p>
 *
 * <p>
 * The filter string can describe nested entities, where nested levels are separated with a slash {@code /}.
 * An asterisk {@code *} can be used as a placeholder for path sections.
 * Attributes of a species can be given in parentheses.
 * An underscore {@code _} can be used as a placeholder for attributes.
 * If no attributes are given for an attributed species, all attribute combinations will pass the filter.
 * For example:
 * <ul>
 *     <li>
 *         {@code Cell/A} matches all {@code A}s in {@code Cell}s
 *     </li>
 *     <li>
 *         {@code Cell(_)/A} matches all {@code A}s in {@code Cell}s
 *     </li>
 *     <li>
 *         {@code Cell(1.0)/A} matches all {@code A}s in {@code Cell}s with an attribute value of {@code 1.0}
 *     </li>
 *     <li>
 *         {@code *}{@code /A} matches all {@code A}s everywhere
 *     </li>
 *     <li>
 *         {@code Cell/*}{@code /A} matches all {@code A}s everywhere inside {@code Cell}s
 *     </li>
 *     <li>
 *         {@code *}{@code /A(1.0)} matches all {@code A}s with an attribute value of 1.0 everywhere
 *     </li>
 * </ul>
 * </p>
 *
 * @param <T> the type of the result of applying the function
 *
 * @author Tom Warnke
 */
public abstract class AbstractStateTraversal<T> implements Function<String, T> {

	public static final Character pathSeparator = '/';
	public static final Character speciesWildCard = '*';
	public static final String attributeWildCard = "_";

	private final Compartment root;

	AbstractStateTraversal(Compartment root) {
		this.root = root;
	}

	/**
	 * Aggregate the results for all the individual nodes in the tree to the overall result.
	 * @param matches the result for all matching nodes
	 * @return the overall result
	 */
	protected abstract T aggregate(Stream<T> matches);

	/**
	 * Compute the result for a leaf entity.
	 */
	protected abstract T processLeaf(LeafSpecies leaf);

	/**
	 * Compute the result for a compartmental entity.
	 */
	protected abstract T processCompartment(Compartment compartment);

	@Override
	public T apply(final String t) {
		Stream<T> matches = checkCompartment(root, t);
		return aggregate(matches);
	}

	private Stream<T> checkCompartment(Compartment context, String t) {
		Stream<T> compartments = context.getSubCompartmentsStream().flatMap(c -> handleCompartment(c, t));
		Stream<T> leaves = context.getSubLeavesStream().flatMap(l -> handleLeaf(l, t));
		return Stream.concat(compartments, leaves);
	}

	private Stream<T> handleLeaf(LeafSpecies leaf, String filterExpression) {
		if (filterExpression.startsWith(String.valueOf(speciesWildCard) + String.valueOf(pathSeparator))) {
			// global quantifier, check this node again without wildcard and path separator
			return handleLeaf(leaf, filterExpression.substring(filterExpression.indexOf(pathSeparator) + 1));
		} else if (filterExpression.contains(String.valueOf(pathSeparator))) {
			// leaf species have no children
			return Stream.empty();
		} if (speciesMatchesFilterExpression(leaf, filterExpression)) {

			return Stream.of(processLeaf(leaf));
		}
		return Stream.empty();
	}

	private Stream<T> handleCompartment(Compartment context, String filterExpression) {
		if (filterExpression.startsWith(String.valueOf(speciesWildCard) + String.valueOf(pathSeparator))) {
			// global quantifier, check this node and children
			Stream<T> here = handleCompartment(context,
					filterExpression.substring(filterExpression.indexOf(pathSeparator) + 1));
			Stream<T> children = checkCompartment(context, filterExpression);
			return Stream.concat(here, children);
		} else if (filterExpression.contains(String.valueOf(pathSeparator))) {
			// this is a path, traverse down the species tree
			String thisLevelFilter = filterExpression.substring(0, filterExpression.indexOf(pathSeparator));
			if (speciesMatchesFilterExpression(context, thisLevelFilter)) {
				return checkCompartment(context, filterExpression.substring(filterExpression.indexOf(pathSeparator) + 1));
			}
			return Stream.empty();
		} else {
			// reached last level of filter expression
			if (speciesMatchesFilterExpression(context, filterExpression)) {
				return Stream.of(processCompartment(context));
			}
			return Stream.empty();
			// no need to go to sub species
		}
	}

	private static boolean speciesMatchesFilterExpression(Species species, String filterExpression) {

		if (filterExpression.startsWith("*" + String.valueOf(pathSeparator))) {
			return speciesMatchesFilterExpression(species, filterExpression.substring(filterExpression.indexOf(pathSeparator) + 1));

		}

		if (filterExpression.contains(String.valueOf(pathSeparator))
				|| (filterExpression.contains("(") && !filterExpression.contains(")" + ""))
				|| (!filterExpression.contains("(") && filterExpression.contains(")"))) {
			throw new IllegalArgumentException("Illegal filter expression");
		}

		if (filterExpression.contains("(")) {
			// has attributes - check species name
			String filterSpeciesName = filterExpression.substring(0,
					filterExpression.indexOf('('));

			if (!species.getType()
					.getName()
					.equals(filterSpeciesName)) {
				// wrong species name
				return false;
			}

			// check attributes
			String attributeString = filterExpression.substring(filterExpression.indexOf('(') + 1,
					filterExpression.indexOf(')'));
			String[] attributes = attributeString.split(",");
			if (species.getType()
					.getAttributesSize() != attributes.length) {
				// wrong number of attributes
				return false;
			}
			for (int i = 0; i < attributes.length; i++) {
				if (attributes[i].trim()
						.equals(attributeWildCard)) {
					continue;
				}
				if (!attributes[i].trim()
						.equals(String.valueOf(species.getAttribute(i)))) {
					// attribute value does not match
					return false;
				}
			}

		} else if (!species.getType()
				.getName()
				.equals(filterExpression)) {
			// no attributes, wrong species name
			return false;
		}

		return true;
	}
}
