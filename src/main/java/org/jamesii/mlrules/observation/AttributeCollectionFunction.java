package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A function that collects the values of the n-th attribute of certain entities.
 * The entities to consider are filtered by an AbstractStateTraversal.
 *
 * @author Tom Warnke
 */
public class AttributeCollectionFunction extends AbstractStateTraversal<Map<String, Object>> {

	private final int attribute;

	AttributeCollectionFunction(Compartment root, int attribute) {
		super(root);
		this.attribute = attribute;
	}

	@Override
	protected Map<String, Object> aggregate(Stream<Map<String, Object>> matches) {
		Stream<Map.Entry<String, Object>> entryStream = matches.flatMap(map -> map.entrySet().stream());
		Map<String, Object> result = entryStream.collect(Collectors.toMap(
				Map.Entry::getKey,
				Map.Entry::getValue
		));
		return result;
	}

	@Override
	protected Map<String, Object> processLeaf(LeafSpecies leaf) {
		HashMap<String, Object> result = new HashMap<>();
		for (int i = 0; i < leaf.getAmount(); i++) {
			String id = String.valueOf(System.identityHashCode(leaf)) + "_" + i;
			result.put(id, leaf.getAttribute(attribute));
		}
		return result;
	}

	@Override
	protected Map<String, Object> processCompartment(Compartment compartment) {
		String id = String.valueOf(System.identityHashCode(compartment));
		Object value = compartment.getAttribute(attribute);
		return Collections.singletonMap(id, value);
	}
}
