/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation.save;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.TimeTriggeredObserver;
import org.jamesii.mlrules.util.Pair;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;

/**
 * 
 * Save all observed data in a set of files; one file per species type. Every
 * line contains a time value, the amount and the attribute values of the
 * considered species.
 * 
 * @author Tobias Helms
 *
 */
public class SaveAllListener implements Listener {

  private final String DELIMITER = ",";

  private final File directory;

  private final String prefix;

  private final Map<String, Pair<FileWriter, BufferedWriter>> writer = new HashMap<>();

  public SaveAllListener(File directory, String prefix) {
    this.directory = directory;
    this.prefix = prefix;
  }

  private Pair<FileWriter, BufferedWriter> getNewWriter(SpeciesType type) {
    try {
      FileWriter fw = new FileWriter(directory.getAbsolutePath() + "/" + prefix + "---" + type.getName() + ".csv");
      BufferedWriter bw = new BufferedWriter(fw);

      bw.write("time");
      bw.write(DELIMITER);
      bw.write("amount");
      StringJoiner joiner = new StringJoiner(DELIMITER, DELIMITER, "");
      for (int i = 0; i < type.getAttributesSize(); ++i) {
        joiner.add(Integer.toString(i));
      }
      bw.write(joiner.toString());
      bw.newLine();
      bw.flush();

      return new Pair<>(fw, bw);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Failed to create file writer");
    }
  }

  private void save(double time, Map<String, Pair<Species, Double>> amounts) {
    try {
      for (Entry<String, Pair<Species, Double>> e : amounts.entrySet()) {
        Pair<FileWriter, BufferedWriter> fwbw = writer.computeIfAbsent(e.getValue().fst().getType().getName(),
            n -> getNewWriter(e.getValue().fst().getType()));
        fwbw.snd().write(Double.toString(time));
        fwbw.snd().write(DELIMITER);
        fwbw.snd().write(Double.toString(e.getValue().snd()));
        if (e.getValue().fst().getType().getAttributesSize() > 0) {
          StringJoiner joiner = new StringJoiner(DELIMITER, DELIMITER, "");
          for (int i = 0; i < e.getValue().fst().getType().getAttributesSize(); ++i) {
            joiner.add(e.getValue().fst().getAttribute(i).toString());
          }
          fwbw.snd().write(joiner.toString());
        }
        fwbw.snd().newLine();
        fwbw.snd().flush();
      }
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(String.format("Could not save simulation results of species at time %s.", time));
    }
  }

  public String toSimpleString(Species s) {
    StringBuilder builder = new StringBuilder();
    builder.append(s.getType().getName());
    if (s.getType().getAttributesSize() > 0) {
      StringJoiner joiner = new StringJoiner(",", "(", ")");
      for (int i = 0; i < s.getType().getAttributesSize(); ++i) {
        joiner.add(s.getAttribute(i).toString());
      }
      builder.append(joiner.toString());
    }
    return builder.toString();
  }

  private void handleSpecies(Species species, Map<String, Pair<Species, Double>> amounts) {
    if (species.getType() != SpeciesType.ROOT) {
      String name = toSimpleString(species);
      Pair<Species, Double> amount = amounts.computeIfAbsent(name, n -> new Pair<>(species, 0D));
      amount.setSnd(amount.snd() + species.getAmount());
    }
    if (species instanceof Compartment) {
      ((Compartment) species).getAllSubSpeciesStream().forEach(sub -> handleSpecies(sub, amounts));
    }
  }

  @Override
  public void notify(Observer observer) {
    if (observer instanceof TimeTriggeredObserver) {
      TimeTriggeredObserver allObserver = (TimeTriggeredObserver) observer;
      Map<String, Pair<Species, Double>> amounts = new HashMap<>();
      handleSpecies(allObserver.getSpecies(), amounts);
      save(allObserver.getTime(), amounts);
    } else {
      throw new IllegalArgumentException("The SaveAllListener can only handle TimeTriggeredObserver observer!");
    }
  }

  @Override
  public boolean isActive() {
    return true;
  }

  @Override
  public void finish(StopCondition stopCondition) {
    writer.values().forEach(p -> {
      try {
        p.snd().flush();
        p.snd().close();
        p.fst().close();
      } catch (Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Could not save simulation results.");
      }
    });
  }

}
