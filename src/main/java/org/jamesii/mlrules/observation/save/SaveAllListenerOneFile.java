/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation.save;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.TimeTriggeredObserver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * 
 * Save all observed data in one file. For each distinguished species (depends
 * on attribute and hierarchy settings), one column is used to save the results.
 * The first column is always always used for the simulation time. Since all
 * possible species are only known at the end, the whole file is written at the
 * end of a simulation run, i.e., by calling {@link Listener#finish}.
 * 
 * @author Tobias Helms
 *
 */
public class SaveAllListenerOneFile implements Listener {

  private final String DELIMITER = ",";

  private final File directory;

  private final String prefix;

  private final boolean hierarchyAware;

  private Map<Double, Map<String, Double>> results = new TreeMap<>();

  public SaveAllListenerOneFile(boolean hierarchyAware, File directory, String prefix) {
    this.hierarchyAware = hierarchyAware;
    this.directory = directory;
    this.prefix = prefix;
  }

  public String getContextNames(Species s) {
    if (s.getType() == SpeciesType.ROOT) {
      return "";
    } else {
      String contexts = getContextNames(s.getContext());
      return contexts + s.getType().getName() + "/";
    }
  }

  public String toSimpleString(Species s) {
    StringBuilder builder = new StringBuilder();
    if (hierarchyAware) {
      builder.append(getContextNames(s.getContext()));
    }
    builder.append(s.getType().getName());
    if (s.getType().getAttributesSize() > 0) {
      StringJoiner joiner = new StringJoiner(";", "(", ")");
      for (int i = 0; i < s.getType().getAttributesSize(); ++i) {
        joiner.add(s.getAttribute(i).toString());
      }
      builder.append(joiner.toString());
    }
    return builder.toString();
  }

  private void handleSpecies(Species species, Map<String, Double> amounts) {
    if (species.getType() != SpeciesType.ROOT) {
      String name = toSimpleString(species);
      amounts.compute(name, (k, v) -> (v == null ? 0 : v) + species.getAmount());
    }
    if (species instanceof Compartment) {
      ((Compartment) species).getAllSubSpeciesStream().forEach(sub -> handleSpecies(sub, amounts));
    }
  }

  @Override
  public void notify(Observer observer) {
    if (observer instanceof TimeTriggeredObserver) {
      TimeTriggeredObserver allObserver = (TimeTriggeredObserver) observer;
      Map<String, Double> amounts = new HashMap<>();
      handleSpecies(allObserver.getSpecies(), amounts);
      results.put(allObserver.getTime(), amounts);
    } else {
      throw new IllegalArgumentException("The SaveAllListenerOneFile can only handle TimeTriggeredObserver observer!");
    }
  }

  @Override
  public boolean isActive() {
    return true;
  }

  private String writeLine(double time, Set<String> allNames, Map<String, Double> values) {
    StringJoiner joiner = new StringJoiner(DELIMITER, "", "");
    joiner.add(Double.toString(time));
    allNames.forEach(n -> joiner.add(Double.toString(values.getOrDefault(n, 0D))));
    return joiner.toString();
  }

  @Override
  public void finish(StopCondition stopCondition) {
    try {
      Set<String> speciesNames = new TreeSet<>();
      results.values().forEach(m -> speciesNames.addAll(m.keySet()));

      FileWriter fw = new FileWriter(directory.getAbsolutePath() + "/" + prefix + ".csv");
      BufferedWriter bw = new BufferedWriter(fw);

      bw.write("time");
      StringJoiner joiner = new StringJoiner(DELIMITER, DELIMITER, "");
      speciesNames.forEach(sn -> joiner.add(sn));
      bw.write(joiner.toString());
      bw.newLine();

      for (Entry<Double, Map<String, Double>> e : results.entrySet()) {
        bw.write(writeLine(e.getKey(), speciesNames, e.getValue()));
        bw.newLine();
      }

      bw.flush();

      bw.close();
      fw.close();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Failed to create file writer");
    }
  }

}
