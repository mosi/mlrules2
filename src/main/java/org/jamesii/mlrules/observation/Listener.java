/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.experiment.stop.StopCondition;

/**
 * A listener can register at an {@link Observer} and will be notified if new
 * data is available. The listener is responsible for doing something with the
 * data the observer is extracting from the simulator.
 * 
 * @author Tobias Helms
 *
 */
public interface Listener {

  /**
   * Process new data available in the given observer.
   */
  void notify(Observer observer);

  /**
   * Return false, if the listener stopped working, i.e., it will not process
   * any data any more. For example, if the listener is a diagram and the
   * diagram has been closed, the listener is inactive.
   */
  boolean isActive();

  /**
   * Call this method after observing the last data set, e.g., at the end of a
   * simulation run. Then, the listener can finish its calculation. For example,
   * the listener can close open file readers.
   */
  void finish(StopCondition stopCondition);

}
