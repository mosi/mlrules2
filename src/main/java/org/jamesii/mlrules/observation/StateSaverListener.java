package org.jamesii.mlrules.observation;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.model.species.Compartment;

public class StateSaverListener implements Listener {

  private final Compartment root;

  public StateSaverListener(Compartment root) {
    this.root = root;
  }

  @Override
  public void notify(Observer observer) {
    // do nothing
  }

  @Override
  public boolean isActive() {
    return true;
  }

  @Override
  public void finish(StopCondition stop) {
    // do nothing
  }

  public Compartment getState() {
    return root;
  }

}
