package org.jamesii.mlrules.experiment;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tom Warnke
 */
public class SimpleJob extends Job {

	public SimpleJob(int id, Map<String, Object> parameter) {
		super(id, parameter);
	}

	@Override
	protected void onSuccess(SimulationRun run) {
		// no reaction necessary here
	}

	@Override
	protected void onFailure(Throwable t) {
		Logger.getGlobal().log(Level.SEVERE, "Exception in Job " + getID() + ":", t);
	}
}
