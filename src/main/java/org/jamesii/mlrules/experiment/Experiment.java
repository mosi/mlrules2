/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.experiment;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.FileUtils;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.Instrumenter;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.factory.ModelNotSupportedException;
import org.jamesii.mlrules.simulator.factory.SimulatorFactory;
import org.jamesii.mlrules.util.MLEnvironment;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This basic experiment class allows to execute replications of ML-Rules
 * simulations in parallel. The model location, model parameters, observers,
 * stopping conditions, the number of replications as well as the number of used
 * cores can be set.
 *
 * @author Tobias Helms
 */
public class Experiment {

  private final String modelLocation;

  private final SimulatorFactory simulatorFactory;

  protected final Instrumenter instrumenter;

  private final StopConditionFactory stopCondition;

  private final ExecutorService executor;

  private SimulationRun lastCreatedRun = null;

  public Experiment(String modelLocation, SimulatorFactory simulatorType, Instrumenter instrumenter,
      StopConditionFactory stopCondition, int cores) {
    this.modelLocation = modelLocation;
    this.simulatorFactory = simulatorType;
    this.instrumenter = instrumenter;
    this.stopCondition = stopCondition;
    this.executor = Executors.newFixedThreadPool(cores);
  }

  protected Model createModel(Map<String, Object> parameter) throws IOException {
    File file = new File(modelLocation);
    if (!file.exists()) {
      throw new IllegalArgumentException(String.format("Could not find file %s", modelLocation));
    }
    MLRulesLexer lexer = new MLRulesLexer(new ANTLRInputStream(FileUtils.readFileToString(file)));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    MLRulesParser parser = new MLRulesParser(tokens);

    MLRulesLexer typeLexer = new MLRulesLexer(new ANTLRInputStream(FileUtils.readFileToString(file)));
    CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
    MLRulesParser typeParser = new MLRulesParser(typeTokens);

    MLEnvironment env = new MLEnvironment();
    env.setGlobalValue(Model.RNG, new MersenneTwister(obtainSeed(parameter)));
    PredefinedFunctions.addAll(env);
    env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);

    TypeCheckVisitor typeChecker = new TypeCheckVisitor();
    typeChecker.doTypeCheck(typeParser.model());

    StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, parameter);
    return visitor.create(parser.model());
  }

  private long obtainSeed(Map<String, Object> parameters) {
    if (parameters.containsKey(Model.RNG)) {
      Object seedObject = parameters.get(Model.RNG);
      if (seedObject instanceof Long) {
        return (Long) seedObject;
      } else {
        throw new IllegalArgumentException(
                "If you set the seed value parameter " + Model.RNG + ", set it to a value of type Long");
      }
    } else {
      // no seed in parameter map - use default
      return System.currentTimeMillis();
    }
  }

  /**
	 * Adds a new job for execution. The job will be delegated to a thread
	 * pool and executed asynchronously.
   *
	 * @param job a job to execute
   */
	public boolean addJob(Job job) throws IOException, ModelNotSupportedException {

      lastCreatedRun = null;
      Model model = createModel(job.getParameter());
      Simulator simulator = simulatorFactory.create(model);
      simulator.getObserver().addAll(instrumenter.create(job, model));
      SimulationRun run = new SimulationRun(job, simulator, stopCondition);
      executor.execute(run);
      lastCreatedRun = run;
      return true;

  }

  public SimulationRun getLastCreatedRun() {
	  return lastCreatedRun;
  }

  public ExecutorService getExecutor() {
	  return executor;
  }

  /**
   * Stop accepting new jobs.
   */
  public void finish() {
    executor.shutdown();
  }

}
