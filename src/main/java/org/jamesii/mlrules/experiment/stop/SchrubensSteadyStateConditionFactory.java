package org.jamesii.mlrules.experiment.stop;

import org.jamesii.mlrules.observation.IntervalObserver;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.SpeciesCountFunction;
import org.jamesii.mlrules.simulator.Simulator;

import java.util.function.Function;

public class SchrubensSteadyStateConditionFactory implements StopConditionFactory {

  private final static int BATCH_COUNT = 8;

  private final static int GROUP_BATCH_COUNT = 4;

  private final static double ALLOWED_SIGNIFICANCE = 0.05;

  private final int batchCount;

  private final int groupBatchCount;

  private final double allowedSignificance;

  private final double interval;

  private final Function<SpeciesCountFunction, Double> function;

  public SchrubensSteadyStateConditionFactory(Function<SpeciesCountFunction, Double> function, double interval) {
    this(function, interval, BATCH_COUNT, GROUP_BATCH_COUNT, ALLOWED_SIGNIFICANCE);
  }

   public SchrubensSteadyStateConditionFactory(Function<SpeciesCountFunction, Double> function, double interval, int batchCount, int groupBatchCount, double allowedSignificance) {
    this.function = function;
    this.interval = interval;
    this.batchCount = batchCount;
    this.groupBatchCount = groupBatchCount;
    this.allowedSignificance = allowedSignificance;
  }

  @Override
  public StopCondition create(Simulator simulator) {
    SchrubensSteadyStateCondition stopCondition = new SchrubensSteadyStateCondition(simulator,
            function, interval, batchCount, groupBatchCount,
            allowedSignificance);
    IntervalObserver observer = new IntervalObserver(simulator.getModel(), interval);
    Listener listener = new Listener() {
      @Override
      public void notify(Observer observer) {
        stopCondition.updateByObserver();
      }

      @Override
      public boolean isActive() {
        return true;
      }

      @Override
      public void finish(StopCondition stopCondition) {
      }
    };
    observer.register(listener);
    simulator.getObserver().add(observer);
    return stopCondition;
    }

}
