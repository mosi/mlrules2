package org.jamesii.mlrules.experiment.stop;

import org.jamesii.mlrules.observation.SpeciesCountFunction;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.steadystate.estimator.schruben.SchrubensSteadyStateEstimator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.DoubleStream;

public class SchrubensSteadyStateCondition extends AbstractStopCondition implements StopCondition {

  private final Simulator simulator;

  private final double INTERVAL;

  private SpeciesCountFunction speciesCountFunction;

  private final Function<SpeciesCountFunction, Double> function;

  private final List<Number> values = new ArrayList<>();

  private final SchrubensSteadyStateEstimator estimator;

  private Integer initialBiasEnd = null;

  public SchrubensSteadyStateCondition(Simulator simulator, Function<SpeciesCountFunction, Double> function, double
          interval, int batchCount, int groupBatchCount, double allowedSignificance) {
    this.simulator = simulator;
    this.INTERVAL = interval;
    this.function = function;
    this.speciesCountFunction = new SpeciesCountFunction(simulator.getModel().getSpecies());
    this.estimator = new SchrubensSteadyStateEstimator(batchCount, groupBatchCount, allowedSignificance);
  }

  @Override
  public void update() {
  }

  void updateByObserver() {
    if (simulator.getNextTime() > values.size() * INTERVAL) {
      values.add(function.apply(speciesCountFunction));
      initialBiasEnd = estimator.estimateInitialBiasEnd(values);
    }
    stop = initialBiasEnd != null;
  }

  public double getSteadyStateMean() {
    if (initialBiasEnd == null) {
      throw new IllegalStateException("no steady state found yet");
    }
    if (initialBiasEnd >= values.size()) {
      throw new IllegalStateException("invalid value for initialBiasEnd");
    }
    List<Number> steadyStateValues = values.subList(initialBiasEnd, values.size());
    DoubleStream steadyStateValueStream = steadyStateValues.stream().mapToDouble(Number::doubleValue);
    return steadyStateValueStream.summaryStatistics().getAverage();
  }

  public double getStopTime() {
    return values.size() * INTERVAL;
  }
}
