/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.experiment.stop;

import java.util.List;

/**
 * 
 * Conjunction stop condition for simulation runs realizing a logical <b>and</b>.
 * 
 * @author Tobias Helms
 *
 */
public class AndStopCondition extends AbstractStopCondition implements StopCondition {

  private final List<StopCondition> conditions;
  
  public AndStopCondition(List<StopCondition> conditions) {
    this.conditions = conditions;
  }

  public List<StopCondition> getConditions() {
    return conditions;
  }

  @Override
  public void update() {
    stop = true;
    // always check all conditions, since they might use the simulator to calculate some values
    for (StopCondition condition : conditions) {
      condition.update();
      if (!condition.stop()) {
        stop = false;
      }
    }
  }

}
