package org.jamesii.mlrules.experiment.stop;

/**
 * This abstract class captures the internal state of a stop condition and makes sure that {@code stop()} can be
 * called repeatedly without side effects.
 *
 * @author Tom Warnke
 */
abstract class AbstractStopCondition implements StopCondition {

	protected boolean stop;

	@Override
	public final boolean stop() {
		return stop;
	}
}
