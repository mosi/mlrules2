/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.experiment.stop;

import org.jamesii.mlrules.experiment.SimulationRun;
import org.jamesii.mlrules.simulator.Simulator;

/**
 * 
 * Interface for all stop conditions for {@link SimulationRun}s.
 * 
 * @author Tobias Helms
 *
 */
public interface StopCondition {

  /**
   * This method triggers the stop condition to update its internal state.
   */
  public void update();

  /**
   * This method decides whether to stop the simulation based on the internal state of the stop condition.
   * Has no side-effects and yields the same results between calls of {@code update()}.
   */
  public boolean stop();

}
