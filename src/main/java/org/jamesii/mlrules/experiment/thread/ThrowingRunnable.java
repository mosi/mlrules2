/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.mlrules.experiment.thread;

/**
 * A {@link Runnable} that provides overridable methods to be called when it throws an exception or finishes correctly.
 * Use it like a standard runnable, but override {@code runPayload} instead of {@code run}.
 *
 * @author Tom Warnke
 */
public abstract class ThrowingRunnable implements Runnable {

	public abstract void runPayload();

	public abstract void onFinish();

	public abstract void onException(Throwable t);

	@Override
	public void run() {
		try {
			runPayload();
			onFinish();
		} catch (Exception e) {
			onException(e);
		}
	}
}
