/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.experiment;

import org.jamesii.mlrules.experiment.stop.StopCondition;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.experiment.thread.ThrowingRunnable;
import org.jamesii.mlrules.simulator.Simulator;

import java.util.logging.Logger;

/**
 * A simulation run executes one replication of an ML-Rules model with a given
 * stopping conditions.
 * 
 * @author Tobias Helms
 *
 */
public class SimulationRun extends ThrowingRunnable {

  private final Job job;

  private final StopConditionFactory stop;

  private final Simulator simulator;
  
  private long runtime = 0;

  public SimulationRun(Job job, Simulator simulator,
      StopConditionFactory stopFactory) {
    this.job = job;
    this.simulator = simulator;
    this.stop = stopFactory;
  }

  @Override
  public void runPayload() {
    Logger.getGlobal().info(String.format("Start simulation %s", job.getID()));
    long time = System.currentTimeMillis();
    StopCondition stopCondition = stop.create(simulator);
    boolean hasActiveObservers;
    boolean stopConditionFired;
    do {
      simulator.nextStep();
      hasActiveObservers = simulator.getObserver().stream()
              .flatMap(o -> o.getListener().stream()).anyMatch(l -> l.isActive());
      stopCondition.update();
      stopConditionFired = stopCondition.stop();
    } while (hasActiveObservers && !stopConditionFired);
    simulator.getObserver().stream().flatMap(o -> o.getListener().stream())
             .forEach(l -> l.finish(stopCondition));
    simulator.finish();
    runtime = System.currentTimeMillis() - time;
    Logger.getGlobal().info(
        String.format("Finished simulation %s in %s ms (%s steps executed)", job.getID(),
            runtime, simulator.getSteps()));
  }
  
  public long getRuntime() {
    return runtime;
  }

  @Override
  public void onFinish() {
    job.onSuccess(this);
  }

  @Override
  public void onException(Throwable t) {
    job.onFailure(t);
  }
}
