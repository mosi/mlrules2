/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.experiment;

import java.util.Map;

/**
 * A job is a parameter setting of a model for one simulation.
 * 
 * @author Tobias Helms
 *
 */
public abstract class Job {
	//TODO: rename variables

  private final int ID;

  private final Map<String, Object> parameter;

  public Job(int id, Map<String, Object> parameter) {
    this.ID = id;
    this.parameter = parameter;
  }

  public int getID() {
    return ID;
  }

  public Map<String, Object> getParameter() {
    return parameter;
  }

  protected abstract void onSuccess(SimulationRun run);

  protected abstract void onFailure(Throwable t);

}
