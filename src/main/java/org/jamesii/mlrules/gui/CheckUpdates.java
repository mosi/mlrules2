package org.jamesii.mlrules.gui;

import org.apache.maven.shared.invoker.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

/**
 * Use maven versions plugin to check for new updates of the ML-Rules dependency.
 * If new a new version is available, show a popup asking the user to update the pom
 * file. After updating the pom, exit the program.
 *
 * @author Tobias Helms
 */
public class CheckUpdates {

  private static void showUpdateDialog(List<String> updates) {
    StringJoiner joiner = new StringJoiner("\n","","\n");
    joiner.add("Updates are available for the ML-Rules editor: \n");
    updates.forEach(joiner::add);
    joiner.add("\nDo you want to update the editor (restart needed)?");

    Frame frame = new Frame();
    int n = JOptionPane.showConfirmDialog(frame, joiner.toString(), "Updates Available", JOptionPane.YES_NO_OPTION);
    frame.dispose();
    if (n == JOptionPane.YES_OPTION) {
      InvocationRequest request = new DefaultInvocationRequest();
      request.setPomFile(new File("pom.xml"));
      request.setGoals(Arrays.asList("versions:use-latest-releases", "versions:commit"));
      request.setBatchMode(true);

      Invoker invoker = new DefaultInvoker();
      try {
        InvocationResult result = invoker.execute(request);
        if (result.getExitCode() != 0) {
          throw new IllegalStateException(result.getExecutionException());
        }
        System.exit(0);
      } catch (MavenInvocationException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    InvocationRequest request = new DefaultInvocationRequest();
    request.setPomFile( new File( "pom.xml" ) );
    request.setGoals( Collections.singletonList( "versions:display-dependency-updates" ) );
    request.setBatchMode(true);

    Invoker invoker = new DefaultInvoker();
    try {
      MavenOutputHandler handler = new MavenOutputHandler();
      invoker.setOutputHandler(handler);
      InvocationResult result = invoker.execute( request );
      if (result.getExitCode() != 0) {
        throw new IllegalStateException(result.getExecutionException());
      }
      if (!handler.getOutdatedDependencies().isEmpty()) {
        showUpdateDialog(handler.getOutdatedDependencies());
      }
    } catch (MavenInvocationException e) {
      e.printStackTrace();
    }


  }

}
