package org.jamesii.mlrules.gui;

import org.apache.maven.shared.invoker.InvocationOutputHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Detect the lines of the plugins that can be updated.
 *
 * @author: Tobias Helms
 */
public class MavenOutputHandler implements InvocationOutputHandler {

  private boolean updates = false;

  private List<String> outdatedDependencies = new ArrayList<>();

  public void consumeLine( String line ) {
    if (line.endsWith("[INFO] ")) {
      updates = false;
    }
    if (updates) {
      outdatedDependencies.add(line.substring(9));
    }
    if (line.contains("[INFO] The following dependencies in Dependencies have newer versions:")) {
      updates = true;
    }
  }

  List<String> getOutdatedDependencies() {
    return outdatedDependencies;
  }

}
