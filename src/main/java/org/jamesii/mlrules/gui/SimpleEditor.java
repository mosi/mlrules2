/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.gui;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.Token;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.observation.IntervalObserver;
import org.jamesii.mlrules.observation.Listener;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.save.SaveAllListener;
import org.jamesii.mlrules.observation.visualization.DelayedJChart2DListener;
import org.jamesii.mlrules.observation.visualization.DelayedVisListener;
import org.jamesii.mlrules.observation.visualization.VisObserver;
import org.jamesii.mlrules.parser.exception.SemanticsException;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.grammar.MLRulesParser.ModelContext;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.syntax.Styles;
import org.jamesii.mlrules.parser.visitor.syntax.SyntaxHighlightVisitor;
import org.jamesii.mlrules.parser.visitor.syntax.VerboseErrorListener;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.advancedstatic.AdvancedStaticSimulator;
import org.jamesii.mlrules.simulator.hybrid.AdvancedHybridSimulator;
import org.jamesii.mlrules.simulator.hybrid.HybridSimulator;
import org.jamesii.mlrules.simulator.links.LinkSimulator;
import org.jamesii.mlrules.simulator.simple.SimpleSimulator;
import org.jamesii.mlrules.simulator.standard.StandardSimulator;
import org.jamesii.mlrules.simulator.tauleaping.TauLeapingSimulator;
import org.jamesii.mlrules.util.MLEnvironment;
import org.jamesii.mlrules.util.runtimeCompiling.CompileTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultStyledDocument;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The sandbox editor for ML-Rules in one class. It is a 'quick and dirty'
 * implementation. We do not intend to refactor it.
 *
 * @author Tobias Helms
 */
public class SimpleEditor {

  private final String VERSION = "2.2.12";

  private JFrame main;

  private JSplitPane top;

  private JTextPane logging;

  private JTextPane pane;

  private JScrollPane scroll;

  private JTextPane linenumbers;

  private final AtomicLong update;

  private final TextListener textListener = new TextListener();

  private final LineNumberListener lineListener = new LineNumberListener();

  private JTextField endtime;

  private JTextField interval;

  private JButton simulate;

  private JButton save;

  private JButton load;

  private JComboBox<String> listScroller;

  private String savePath = null;

  private boolean saveDirectly = false;

  private String loadPath = null;

  private boolean hierarchy;

  private boolean attributes;

  private boolean showReals;

  private boolean theme = false;

  // private boolean visualization = true;

  private double endtimeValue;

  private double intervalValue;

  private String tmpModelFile;

  private String initialText;

  private int initialHeight;

  private int initialWidth;

  private int initialDividerPosition;

  private int initialTextSize;

  private long timeStamp;

  //private boolean delayedPlotting;

  private boolean useRuntimeCompilation;

  private boolean useNewVis;


  private class ButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      Runnable doAssist = new Runnable() {
        @Override
        public void run() {
          new Thread() {
            @Override
            public void run() {
              double time = 0;
              double intervalV = 0;
              timeStamp = System.currentTimeMillis();

              try {
                time = Double.valueOf(endtime.getText());
                intervalV = Double.valueOf(interval.getText());

                if (Double.compare(time, 0.0) <= 0) {
                  throw new Exception("Time must be positive.");
                }
                if (intervalV <= 0) {
                  throw new Exception("Interval must be positive.");
                }

                String model = pane.getText();
                MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromString(model));

                CommonTokenStream tokens = new CommonTokenStream(lexer);
                MLRulesParser parser = new MLRulesParser(tokens);


                MLEnvironment env = new MLEnvironment();
                env.setGlobalValue(Model.RNG, new MersenneTwister(System.currentTimeMillis()));
                env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
                PredefinedFunctions.addAll(env);

                Model mlrulesModel = null;

                MLRulesLexer typeLexer = new MLRulesLexer(CharStreams.fromString(model));
                CommonTokenStream typeTokens = new CommonTokenStream(typeLexer);
                MLRulesParser typeParser = new MLRulesParser(typeTokens);
                TypeCheckVisitor typeChecker = new TypeCheckVisitor();
                typeChecker.doTypeCheck(typeParser.model());
                StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env,
                        Collections.emptyMap());
                mlrulesModel = visitor.create(parser.model());
                // RatePostProcessor postProcessor = new RatePostProcessor();
                // postProcessor.postProcess(result);
                // mlrulesModel = result;

                if (mlrulesModel == null) {
                  throw new IllegalArgumentException("could not create the model object");
                }

                if (useRuntimeCompilation) {
                  CompileTools.compileModel(mlrulesModel);
                }

                Logger.getGlobal()
                        .info(mlrulesModel.toString());

                // in case of too many observations, warn user and let him decide
                if ((Double.valueOf(endtime.getText()) / Double.valueOf(interval.getText())) * mlrulesModel
                        .getSpecies().getAmount() > 9999) {
                  JOptionPane warning = new JOptionPane();
                  int n = JOptionPane.showConfirmDialog(warning, "There are more than 10.000 observations, " +
                          "performance might be limited.\nDo you want to proceed?  ", "Warning!", JOptionPane
                          .YES_NO_OPTION);
                  if (n == JOptionPane.NO_OPTION) {
                    return;
                  }
                }

                Set<Observer> observers = new HashSet<>();
                //if (visualization) {
                Listener listener;
                if (useNewVis) {
                  //if (delayedPlotting)
                  listener = new DelayedJChart2DListener(time, attributes, showReals, hierarchy, timeStamp, model);
                    /*else
                      listener = new DelayedJChart2DListener(time, attributes, showReals, hierarchy, intervalV,
                      timeStamp);*/
                } else {
                  //if (delayedPlotting) {
                  listener = new DelayedVisListener(new File(loadPath));
                    /*} else {
                      listener = new VisListener(new File(loadPath));
                    }*/
                }
                VisObserver observer = new VisObserver((Model) mlrulesModel, time, intervalV, attributes, hierarchy,
                        !showReals);
                observer.register(listener);
                observers.add(observer);
                //}

                if (saveDirectly) {
                  IntervalObserver observer2 = new IntervalObserver(mlrulesModel, intervalV);
                  observer2.register(new SaveAllListener(new File(savePath),
                          new SimpleDateFormat("yyyy-MM-dd---HH-mm-ss").format(Calendar.getInstance()
                                  .getTime())));
                  observers.add(observer2);
                }

                Simulator simulator = null;

                switch (listScroller.getSelectedIndex()) {
                  case 0:
                    simulator = new StandardSimulator(mlrulesModel, true);
                    break;
                  case 1:
                    simulator = new SimpleSimulator(mlrulesModel, true);
                    break;
                  case 2:
                    simulator = new AdvancedStaticSimulator(mlrulesModel);
                    break;
                  case 3:
                    simulator = new LinkSimulator(mlrulesModel, true);
                    break;
                  case 4:
                    simulator = new HybridSimulator(mlrulesModel,
                            new DormandPrince853Integrator(1.0e-08, 10.0, 1.0e-8, 1.0e-8), 0.01, 0.03, 0.03, true);
                    break;
                  case 5:
                    simulator = new AdvancedHybridSimulator(mlrulesModel,
                            new DormandPrince853Integrator(1.0e-08, 10.0, 1.0e-8, 1.0e-8), false, 0.001, 0.001, 0.01,
                            0.05, 0.1);
                    break;
                  case 6:
                    simulator = new TauLeapingSimulator(mlrulesModel, 0.03, 10, 5, 1000);
                    break;
                  default:
                    // do nothing
                }

                if (simulator == null) {
                  throw new IllegalArgumentException("no simulator could be created");
                }
                simulator.getObserver()
                        .addAll(observers);

                Long start = System.currentTimeMillis();
                while (observers.stream()
                        .flatMap(o -> o.getListener()
                                .stream())
                        .anyMatch(l -> l.isActive())
                        && simulator.getCurrentTime() < time) {
                  simulator.nextStep();
                }
                Logger.getGlobal()
                        .log(Level.WARNING,
                                String.format("Finished simulation in %s ms (%s steps)", System.currentTimeMillis() -
                                        start, simulator.getSteps()));
                if (useRuntimeCompilation) {
                  CompileTools.deleteGeneratedClasses();
                }
                if (simulator instanceof TauLeapingSimulator) {
                  TauLeapingSimulator tls = (TauLeapingSimulator) simulator;
                  Logger.getGlobal()
                          .log(Level.WARNING, String.format("TauLeapingSimulator >> Steps: %s; Tau-Steps: %s; " +
                                  "Tau-Reactions: %s; SSA-Reactions: %s", tls.getSteps(), tls.getTauStepCounter(),
                                  tls.getTauFiringCounter(), tls.getSsaStepCounter()));
                }
                observers.stream()
                        .flatMap(o -> o.getListener()
                                .stream())
                        .forEach(l -> l.finish(null));
                simulator.finish();
              } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, e.getMessage());
              }
            }
          }.start();
        }
      };
      SwingUtilities.invokeLater(doAssist);
    }

  }

  private class TextListener implements DocumentListener {
    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent arg0) {
      update.set(System.currentTimeMillis());
    }

    @Override
    public void removeUpdate(DocumentEvent arg0) {
      update.set(System.currentTimeMillis());
    }
  }

  private class LineNumberListener implements DocumentListener {

    private boolean doUpdate = false;

    private int countLines() {
      return pane.getText()
              .split("\n", -1).length;
    }

    private void update() {
      doUpdate = true;
      Runnable doAssist = new Runnable() {
        @Override
        public void run() {
          linenumbers.setCaret(null);
          linenumbers.setText(IntStream.iterate(1, x -> x + 1)
                  .limit(countLines())
                  .boxed()
                  .map(i -> String.valueOf(i))
                  .collect(Collectors.joining("\r\n")));
          doUpdate = false;
        }
      };
      SwingUtilities.invokeLater(doAssist);
    }

    @Override
    public void changedUpdate(DocumentEvent arg0) {
      if (!doUpdate) {
        update();
      }
    }

    @Override
    public void insertUpdate(DocumentEvent arg0) {
      if (!doUpdate) {
        update();
      }
    }

    @Override
    public void removeUpdate(DocumentEvent arg0) {
      if (!doUpdate) {
        update();
      }
    }
  }

  private void loadProperties() {
    try {
      File file = new File("./editor.properties");
      if (!file.exists()) {
        file.createNewFile();
      }
      Properties prop = new Properties();
      InputStream inputStream = new FileInputStream("./editor.properties");
      prop.load(inputStream);

      savePath = prop.getProperty("save");
      loadPath = prop.getProperty("load");
      attributes = Boolean.valueOf(prop.getProperty("attributes"));
      hierarchy = Boolean.valueOf(prop.getProperty("hierarchy"));
      showReals = Boolean.valueOf(prop.getProperty("reals"));
      useNewVis = prop.containsKey("visualization") ? Boolean.valueOf(prop.getProperty("visualization")) : true;
      saveDirectly = Boolean.valueOf(prop.getProperty("saveDirectly"));
      endtimeValue = prop.containsKey("endTime") ? Double.valueOf(prop.getProperty("endTime")) : 100.0;
      intervalValue = prop.containsKey("interval") ? Double.valueOf(prop.getProperty("interval")) : 1.0;
      tmpModelFile = prop.getProperty("modelfile");
      theme = prop.containsKey("theme") ? Boolean.valueOf(prop.getProperty("theme")) : true;
      //delayedPlotting = Boolean.valueOf(prop.getProperty("delayedPlotting"));
      useRuntimeCompilation = Boolean.valueOf(prop.getProperty("optimization"));

      initialDividerPosition = prop.containsKey("logging") ? Integer.valueOf(prop.getProperty("logging")) : 500;
      initialHeight = prop.containsKey("height") ? Integer.valueOf(prop.getProperty("height")) : 700;
      initialWidth = prop.containsKey("width") ? Integer.valueOf(prop.getProperty("width")) : 1400;
      initialTextSize = prop.containsKey("textsize") ? Integer.valueOf(prop.getProperty("textsize")) : 12;

      if (tmpModelFile == null) {
        tmpModelFile = timeStamp + ".mlrj";
        this.initialText = "//constants\na1:0.1;\na2:0.05;\na3:0.01;\n\n//species\nA();B();C();\n\n//initial " +
                "solution\n>>INIT[100 A];\n\n//reaction rules\nA:a -> B @#a*a1;\nB:b -> A @#b*a2;\nB:b -> C@#b*a3;";
      } else {
        FileReader fr = new FileReader(tmpModelFile);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder builder = new StringBuilder();
        br.lines()
                .forEach(l -> builder.append(l + "\n"));
        br.close();
        fr.close();
        this.initialText = builder.toString();
      }

      if (savePath == null) {
        savePath = "./";
      }
      if (loadPath == null) {
        loadPath = "./";
      }

    } catch (

            IOException e)

    {
      e.printStackTrace();
    }

  }

  private void saveProperties() {
    try {
      Properties prop = new Properties();
      prop.setProperty("load", loadPath);
      prop.setProperty("save", savePath);
      prop.setProperty("attributes", Boolean.toString(attributes));
      prop.setProperty("hierarchy", Boolean.toString(hierarchy));
      prop.setProperty("reals", Boolean.toString(showReals));
      prop.setProperty("visualization", Boolean.toString(useNewVis));
      prop.setProperty("saveDirectly", Boolean.toString(saveDirectly));
      prop.setProperty("endTime", endtime.getText());
      prop.setProperty("interval", interval.getText());
      //prop.setProperty("delayedPlotting", Boolean.toString(delayedPlotting));
      prop.setProperty("optimization", Boolean.toString(useRuntimeCompilation));
      prop.setProperty("modelfile", tmpModelFile);
      prop.setProperty("width", Integer.toString((int) main.getSize()
              .getWidth()));
      prop.setProperty("height", Integer.toString((int) main.getSize()
              .getHeight()));
      prop.setProperty("logging", Integer.toString(top.getDividerLocation()));
      prop.setProperty("theme", Boolean.toString(!theme));
      prop.setProperty("textsize", Integer.toString(linenumbers.getFont()
              .getSize()));

      FileWriter fw = new FileWriter(tmpModelFile);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(pane.getText());
      bw.flush();
      bw.close();
      fw.close();

      File f = new File("./editor.properties");
      OutputStream out = new FileOutputStream(f);
      prop.store(out, "Current configuration of ML-Rules editor.");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public SimpleEditor() {

    loadProperties();

    this.update = new AtomicLong(System.currentTimeMillis());

    JPanel mainPanel = initWindowFrame();
    JScrollPane scroll2 = initScrollPane();
    initEditorPanel(mainPanel, scroll2);
    initRightClickMenu();
    initBottomPanel(mainPanel);

    main.add(mainPanel);
    main.setVisible(true);
  }

  private void initBottomPanel(JPanel mainPanel) {
    JPanel bottom = new JPanel(true);
    initButtons(bottom);
    initTextFields(bottom);
    initCheckboxes(bottom);

    listScroller = new JComboBox<String>(new String[]{"Standard SSA", "Static SSA", "Advanced Static SSA",
            "Link SSA", "Hybrid ODE/SSA", "Advanced Hybrid ODE/SSA", "Tau-Leaping"/* , "Static ODE" */});

    listScroller.setSelectedIndex(0);
    bottom.add(listScroller);

    simulate = new JButton("Start Simulation");
    simulate.addActionListener(new ButtonListener());
    bottom.add(simulate);

    mainPanel.add(bottom, BorderLayout.SOUTH);
  }

  private void initCheckboxes(JPanel bottom) {
    JCheckBox newVisCheckBox = new JCheckBox("New Visualization");
    newVisCheckBox.setSelected(useNewVis);
    newVisCheckBox.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          useNewVis = true;
        } else {
          useNewVis = false;
          new Thread() {
            @Override
            public void run() {
              JCheckBox checkbox = new JCheckBox("Attributes", attributes);
              JCheckBox checkbox2 = new JCheckBox("Hierarchies", hierarchy);
              JCheckBox checkbox3 = new JCheckBox("Reals", showReals);
              JOptionPane.showMessageDialog(main, new Object[]{checkbox, checkbox2, checkbox3}, "Parameter for old " +
                      "Visualization", JOptionPane.OK_OPTION);
              attributes = checkbox.isSelected();
              hierarchy = checkbox2.isSelected();
              showReals = checkbox3.isSelected();
            }
          }.start();
        }
      }
    });
    bottom.add(newVisCheckBox);

    /*JCheckBox delayedPrintingCheckBoy = new JCheckBox("Delayed Plotting");
    delayedPrintingCheckBoy.setSelected(delayedPlotting);
    delayedPrintingCheckBoy.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          delayedPlotting = true;
        } else {
          delayedPlotting = false;
        }
      }
    });*/


    JCheckBox optimization = new JCheckBox("Optimization");
    optimization.setSelected(useRuntimeCompilation);
    optimization.addItemListener((ItemListener) listener -> {
      if (ItemEvent.SELECTED == listener.getStateChange()) {
        useRuntimeCompilation = true;
      } else {
        useRuntimeCompilation = false;
      }
    });

    JCheckBox sd = new JCheckBox("Save Directly");
    sd.setMnemonic(KeyEvent.VK_C);
    sd.setSelected(saveDirectly);
    bottom.add(sd);
    sd.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (ItemEvent.SELECTED == e.getStateChange()) {
          if (saveDirectly) {
            return;
          }
          JFileChooser f = new JFileChooser();
          f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
          f.setDialogTitle("Where to save the results?");
          f.setCurrentDirectory(new File(savePath));
          int returnVal = f.showOpenDialog(null);
          if (returnVal == JFileChooser.APPROVE_OPTION) {
            saveDirectly = true;
            sd.setSelected(true);
            File file = f.getSelectedFile();
            savePath = file.getPath();
          }
        } else if (saveDirectly) {
          saveDirectly = false;
        }
      }
    });

    bottom.add(optimization);
    //bottom.add(delayedPrintingCheckBoy);
    bottom.add(sd);
  }

  private void initTextFields(JPanel bottom) {
    endtime = new JTextField(String.valueOf(endtimeValue), 4);
    interval = new JTextField(String.valueOf(intervalValue), 4);
    bottom.add(new JLabel("End Time:"));
    bottom.add(endtime);
    bottom.add(new JLabel("Interval:"));
    bottom.add(interval);
  }

  private void initButtons(JPanel bottom) {
    load = new JButton("Load");
    bottom.add(load);
    load.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        Runnable doAssist = new Runnable() {
          @Override
          public void run() {
            JFileChooser f = new JFileChooser();
            f.setCurrentDirectory(new File(loadPath));
            int returnVal = f.showOpenDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
              try {
                File file = f.getSelectedFile();
                loadPath = file.getParent();
                FileReader reader = new FileReader(file);
                BufferedReader buffer = new BufferedReader(reader);
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = buffer.readLine()) != null) {
                  builder.append(line);
                  builder.append("\n");
                }
                pane.setText(builder.toString());
                buffer.close();
                reader.close();
              } catch (IOException e) {
                // do nothing
              }
            }
          }
        };
        SwingUtilities.invokeLater(doAssist);
      }

    });
    save = new JButton("Save");
    save.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        Runnable doAssist = new Runnable() {
          @Override
          public void run() {
            JFileChooser f = new JFileChooser();
            f.setCurrentDirectory(new File(loadPath));
            int returnVal = f.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
              try {
                File file = f.getSelectedFile();
                loadPath = file.getParent();
                FileWriter writer = new FileWriter(file);
                BufferedWriter buffer = new BufferedWriter(writer);
                buffer.write(pane.getText());
                buffer.flush();
                buffer.close();
                writer.close();
              } catch (IOException e) {
                // do nothing
              }
            }
          }
        };
        SwingUtilities.invokeLater(doAssist);
      }

    });
    bottom.add(save);

  }

  private void initRightClickMenu() {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem switchTheme = new JMenuItem("switch color theme");
    popup.add(switchTheme);
    switchTheme.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (!theme) {
          pane.setBackground(Color.DARK_GRAY);
          linenumbers.setBackground(Color.DARK_GRAY);
          logging.setBackground(Color.DARK_GRAY);
          pane.setForeground(Color.LIGHT_GRAY);
          linenumbers.setForeground(Color.LIGHT_GRAY);
          logging.setForeground(Color.LIGHT_GRAY);
          pane.setCaretColor(Color.LIGHT_GRAY);
          logging.setCaretColor(Color.LIGHT_GRAY);
          theme = true;
        } else {
          pane.setBackground(Color.WHITE);
          linenumbers.setBackground(Color.WHITE);
          logging.setBackground(Color.WHITE);
          pane.setForeground(Color.BLACK);
          linenumbers.setForeground(Color.BLACK);
          logging.setForeground(Color.BLACK);
          pane.setCaretColor(Color.BLACK);
          logging.setCaretColor(Color.BLACK);
          theme = false;
        }
      }

    });
    switchTheme.doClick();
    JMenuItem changeTextSize = new JMenuItem("change text size");
    popup.add(changeTextSize);
    changeTextSize.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Object[] possibilities = {"4", "8", "12", "16", "20", "24", "28"};
        String s = (String) JOptionPane.showInputDialog(main,
                String.format("Current text size: %s \nSelect new text size:", pane.getFont()
                        .getSize()),
                "Select Text Size", JOptionPane.PLAIN_MESSAGE, null, possibilities,
                String.valueOf(pane.getFont()
                        .getSize()));
        if (s != null) {
          int size = Integer.valueOf(s);
          linenumbers.setFont(new Font("Courier New", Font.PLAIN, size));
          pane.setFont(new Font("Courier New", Font.PLAIN, size));
          logging.setFont(new Font("Courier New", Font.PLAIN, size));
        }
      }
    });

    MouseListener popupListener = new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
      }

      @Override
      public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
      }

      private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
          popup.show(e.getComponent(), e.getX(), e.getY());
        }
      }

    };
    pane.addMouseListener(popupListener);
    linenumbers.addMouseListener(popupListener);
    logging.addMouseListener(popupListener);
  }

  private void initEditorPanel(JPanel mainPanel, JScrollPane scroll2) {
    this.pane = new JTextPane(new DefaultStyledDocument()) {
      private static final long serialVersionUID = 1L;

      @Override
      public boolean getScrollableTracksViewportWidth() {
        return getUI().getPreferredSize(this).width <= getParent().getSize().width;
      }
    };

    Styles.addStyles(this.pane.getStyledDocument());
    JPanel editor = new JPanel(new BorderLayout());
    linenumbers = new JTextPane(new DefaultStyledDocument());
    editor.add(linenumbers, BorderLayout.WEST);
    editor.add(pane, BorderLayout.CENTER);
    linenumbers.setEditable(false);
    linenumbers.setCaret(null);
    linenumbers.setFont(new Font("Courier New", Font.PLAIN, initialTextSize));
    this.pane.getDocument()
            .addDocumentListener(lineListener);
    this.pane.setText(initialText);
    this.pane.getDocument()
            .addDocumentListener(textListener);
    this.pane.setFont(new Font("Courier New", Font.PLAIN, initialTextSize));
    scroll = new JScrollPane(editor);
    scroll.setBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll.setViewportBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    scroll.getVerticalScrollBar()
            .setUnitIncrement(20);

    scroll.setBorder(BorderFactory.createTitledBorder("Editor"));
    scroll2.setBorder(BorderFactory.createTitledBorder("Logger"));
    this.top = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scroll, scroll2);
    top.setDividerLocation(initialDividerPosition);
    top.setResizeWeight(1);
    mainPanel.add(top, BorderLayout.CENTER);
  }

  private JScrollPane initScrollPane() {
    this.logging = new JTextPane(new DefaultStyledDocument()) {
      private static final long serialVersionUID = 1L;

      @Override
      public boolean getScrollableTracksViewportWidth() {
        return getUI().getPreferredSize(this).width <= getParent().getSize().width;
      }
    };
    logging.setEditable(false);
    logging.setFont(new Font("Courier New", Font.PLAIN, initialTextSize));
    JScrollPane scroll2 = new JScrollPane(logging);
    scroll2.setBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll2.setViewportBorder(new EmptyBorder(new Insets(0, 0, 0, 0)));
    scroll2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    return scroll2;
  }

  private JPanel initWindowFrame() {
    this.main = new JFrame(
            "ML-Rules Sandbox 2 " + VERSION + " - Status: Alpha - For Demonstration Purposes Only - University of " +
                    "Rostock");
    main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    main.setSize(initialWidth, initialHeight);
    main.setMinimumSize(new Dimension(1500, 700));
    JPanel mainPanel = new JPanel(new BorderLayout(), true);
    main.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        saveProperties();
      }
    });
    return mainPanel;


  }

  public void update() {
    Runnable doAssist = new Runnable() {
      @Override
      public void run() {
        pane.getDocument()
                .removeDocumentListener(textListener);
        pane.getDocument()
                .removeDocumentListener(lineListener);
        simulate.setEnabled(true);
        logging.setText("");

        // Build Syntax
        MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromString(pane.getText()));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MLRulesParser parser = new MLRulesParser(tokens);

        TypeCheckVisitor typeChecker = new TypeCheckVisitor();

        VerboseErrorListener error = new VerboseErrorListener();
        parser.addErrorListener(error);
        parser.setErrorHandler(new DefaultErrorStrategy());
        // Format Text
        ModelContext context = parser.model();
        SyntaxHighlightVisitor syntaxVisitor = new SyntaxHighlightVisitor(pane);
        syntaxVisitor.doSyntaxHighlighting(context);
        // Format Comment
        tokens.getTokens()
                .stream()
                .filter(t -> t.getChannel() == Token.HIDDEN_CHANNEL)
                .forEach(t -> pane.getStyledDocument()
                        .setCharacterAttributes(t.getStartIndex(),
                                t.getStopIndex() - t.getStartIndex() + 1, pane.getStyle(Styles.COMMENT), true));

        // Show Syntax Errors
        if (!error.getSymbols()
                .isEmpty()) {
          error.getSymbols()
                  .forEach(o -> {
                    if (o instanceof Token) {
                      Token t = (Token) o;
                      pane.getStyledDocument()
                              .setCharacterAttributes(t.getStartIndex(), t.getStopIndex(),
                                      pane.getStyle(Styles.ERROR), true);
                    }
                    simulate.setEnabled(false);
                  });
          StringBuilder builder = new StringBuilder();
          error.getErrors()
                  .forEach(e -> builder.append(e + "\n"));
          logging.setText(builder.toString());
          simulate.setEnabled(false);
        } else {
          // Show Semantic Errors
          MLEnvironment env = new MLEnvironment();
          env.setGlobalValue(Model.RNG, new MersenneTwister(1));
          PredefinedFunctions.addAll(env);
          env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);


          boolean semanticsError = false;
          try {
            // // // type check
            lexer = new MLRulesLexer(CharStreams.fromString(pane.getText()));
            tokens = new CommonTokenStream(lexer);
            parser = new MLRulesParser(tokens);
            typeChecker.doTypeCheck(parser.model());
          } catch (SemanticsException e) {
            if (e.getCtx() != null && pane.getText()
                    .length() > e.getCtx()
                    .getStop()
                    .getStopIndex()) {
              pane.getStyledDocument()
                      .setCharacterAttributes(e.getCtx()
                                      .getStart()
                                      .getStartIndex(),
                              e.getCtx()
                                      .getStop()
                                      .getStopIndex() - e.getCtx()
                                      .getStart()
                                      .getStartIndex() + 1,
                              pane.getStyle(Styles.ERROR), true);
            }
            String txt = "SEMANTICS ERROR: Since the semantics checker is not perfect, you can try to start the " +
                    "simulation although semantics errors have been found.\nBe careful, the results are probably " +
                    "wrong or the simulation crashes.\n\n";
            logging.setText(txt + e.getMsg());
            e.printStackTrace();
            semanticsError = true;
          }
          try {


            // creation check
            lexer = new MLRulesLexer(CharStreams.fromString(pane.getText()));
            tokens = new CommonTokenStream(lexer);
            parser = new MLRulesParser(tokens);

            PredefinedFunctions.addAll(env);
            StandardMLRulesVisitor creationVisitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env,
                    Collections.emptyMap());
            creationVisitor.visitModel(parser.model());

            if (!semanticsError) {
              logging.setText("no errors");
            }
          } catch (SemanticsException e) {
            if (e.getCtx() != null && pane.getText()
                    .length() > e.getCtx()
                    .getStop()
                    .getStopIndex()) {
              pane.getStyledDocument()
                      .setCharacterAttributes(e.getCtx()
                                      .getStart()
                                      .getStartIndex(),
                              e.getCtx()
                                      .getStop()
                                      .getStopIndex() - e.getCtx()
                                      .getStart()
                                      .getStartIndex() + 1,
                              pane.getStyle(Styles.ERROR), true);
            }
            logging.setText(e.getMsg());
            e.printStackTrace();
            simulate.setEnabled(false);
          } catch (Exception e) {
            e.printStackTrace();
            logging.setText("no line available: " + e.getMessage());
            simulate.setEnabled(false);
          }

        }
        pane.getDocument()
                .addDocumentListener(textListener);
        pane.getDocument()
                .addDocumentListener(lineListener);
        update.set(Long.MAX_VALUE);

      }

    };
    SwingUtilities.invokeLater(doAssist);

  }

  public static void main(String[] args) throws IOException {
    Level level = Level.WARNING;
    if (args.length > 0 && Level.parse(args[0]) != null) {
      level = Level.parse(args[0]);
    }
    Logger.getGlobal()
            .setLevel(level);
    SimpleEditor e = new SimpleEditor();
    Thread worker = new Thread(new UpdateRunnable(e));
    worker.start();
  }

  public AtomicLong getUpdate() {
    return update;
  }

}
