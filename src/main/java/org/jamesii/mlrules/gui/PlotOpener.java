package org.jamesii.mlrules.gui;

import org.jamesii.mlrules.observation.visualization.ASimulationDataHandler;
import org.jamesii.mlrules.observation.visualization.chart.PlotWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;


/**
 * Created by Florian Thonig on 01.04.2017.
 */
public class PlotOpener {
    public static void main(String[] args) {
        // all that GUI stuff right in here
        JFrame window = new JFrame("MLRules2 Simulation Plot Viewer");
        JButton openButton = new JButton("Open Simulation Plot");
        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser fc = new JFileChooser();
                int returnVal = fc.showOpenDialog(openButton);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        FileInputStream fin = new FileInputStream(file);
                        ObjectInputStream oin = new ObjectInputStream(fin);
                        ASimulationDataHandler datahandler = (ASimulationDataHandler) oin.readObject();

                        PlotWindow chart = new PlotWindow(file.getName(), "Time", "Species Amount",
                                datahandler.getEndTime(), false, false, false, 0);
                        chart.setDataHandler(datahandler);
                        chart.recalculate();
                        chart.build_tree();
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                        JOptionPane exceptionMsg = new JOptionPane();
                        JOptionPane.showMessageDialog(exceptionMsg, "File not found.", "Error", JOptionPane.ERROR_MESSAGE);
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (ObjectStreamException e1) {
                        e1.printStackTrace();
                        JOptionPane exceptionMsg = new JOptionPane();
                        JOptionPane.showMessageDialog(exceptionMsg, "Wrong file type.", "Error", JOptionPane.ERROR_MESSAGE);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        JOptionPane exceptionMsg = new JOptionPane();
                        JOptionPane.showMessageDialog(exceptionMsg, "Unknown error.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        window.setPreferredSize(new Dimension(250, 100));
        window.setMinimumSize(new Dimension(250, 100));
        window.add(openButton);
        window.setVisible(true);
        //window.setResizable(false);
        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                window.dispose();
            }
        });
    }

}
