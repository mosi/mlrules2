/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.gui;

/**
 * This {@link Runnable} is used to delay the update of the syntax highlight and
 * error checking. Concretely, after 500ms of no activity, the updates are
 * executed. Without such a delay, the parser would be activated after each
 * keystroke which is not natural (and which is also not common, e.g., see
 * Eclipse).
 * 
 * @author Tobias Helms
 *
 */
public class UpdateRunnable implements Runnable {

  public final SimpleEditor editor;

  public UpdateRunnable(SimpleEditor editor) {
    this.editor = editor;
  }

  @Override
  public void run() {
    while (true) {
      try {
        Thread.sleep(100);
        if (System.currentTimeMillis() - editor.getUpdate().get() > 500) {
          editor.update();
          Thread.sleep(500);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
