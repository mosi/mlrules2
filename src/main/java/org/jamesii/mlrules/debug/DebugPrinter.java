/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.debug;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;

public class DebugPrinter {
  private MLRulesParser parser;

  private int offset = 0;

  public DebugPrinter() {
  };

  public DebugPrinter(MLRulesParser parser) {
    this.parser = parser;
  }

  public void in() {
    offset += 1;
  }

  public void out() {
    offset -= 1;
  }

  private String offsetString() {
    String result = "";
    for (int i = 0; i < offset; i++) {
      result += "-";
    }
    return result;
  }

  public void print(String msg) {
    System.out.println(offsetString() + msg);

  }

  public void print(RuleContext ctx) {
    if (this.parser != null) {
      System.out.println(ctx.toStringTree(this.parser));
    } else {
      System.out.println(ctx.toStringTree());
    }
  }

  public void print(String msg, RuleContext ctx) {
    if (this.parser != null) {
      System.out
          .println(offsetString() + msg + " " + ctx.toStringTree(this.parser));
    } else {
      System.out.println(offsetString() + msg + " " + ctx.toStringTree());
    }
  }

  public MLRulesParser getParser() {
    return parser;
  }

  public void print(ParseTree child) {
    if (this.parser != null) {
      System.out.println(offsetString() + child.toStringTree(this.parser));
    } else {
      System.out.println(offsetString() + child.toStringTree());
    }
  }
}
