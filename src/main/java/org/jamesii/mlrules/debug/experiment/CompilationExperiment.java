package org.jamesii.mlrules.debug.experiment;

import org.jamesii.mlrules.experiment.Experiment;
import org.jamesii.mlrules.experiment.SimpleJob;
import org.jamesii.mlrules.experiment.SimulationRun;
import org.jamesii.mlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.simulator.factory.StandardSimulatorFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A simple experiment to evaluate the runtime of the 'runtimeCompilation'.
 *
 * @author Tobias Helms
 */
public class CompilationExperiment {

  private static final int REPLICATIONS = 6;

  private static final String MODEL = "./runtimeCompilation/wnt.mlrj";

  private static final double ENDTIME = 100.0;

  /**
   * The path must exist - it will not be created. Exceptions are thrown if it does not exist.
   */
  private static final String RESULTS_PATH = "/home/tobi/MoSi/tmp/runtimeCompilation/";

  private static final double INTERVAL = 1.0;

  private static final int CORES = -1;

  private static List<Long> execute(boolean useOptimization) throws InterruptedException {
    int cores = CORES < 0 ? Runtime.getRuntime().availableProcessors() + CORES : CORES;

    StopConditionFactory stop = new SimTimeStopConditionFactory(ENDTIME);

    Experiment exp = new Experiment(MODEL, new StandardSimulatorFactory(true, useOptimization),
            new ExpInstrumenter(new File(RESULTS_PATH+ Boolean.toString(useOptimization)), INTERVAL), stop, cores);

    List<SimulationRun> runs = new ArrayList<>();

    for (int i = 0; i < REPLICATIONS; ++i) {
      try {
        exp.addJob(new SimpleJob(i, new HashMap<>()));
      } catch (Exception e) {
        e.printStackTrace();
      }
      runs.add(exp.getLastCreatedRun());
    }

    exp.finish();
    exp.getExecutor().awaitTermination(100, TimeUnit.SECONDS);
    return runs.stream().map(SimulationRun::getRuntime).collect(Collectors.toList());
  }

  public static void main(String[] args) {

    List<List<Long>> results = new ArrayList<>();
    for (boolean useOptimization : new Boolean[] {false, true}) {
      try {
        results.add(execute(useOptimization));
      } catch (InterruptedException e) {
        Logger.getGlobal().warning("Error occurred with optimization = " + useOptimization);
        e.printStackTrace();
      }
    }
    try {
      FileWriter writer = new FileWriter(RESULTS_PATH + "runtime.csv");
      BufferedWriter buffer = new BufferedWriter(writer);

      for (List<Long> configResults : results) {

      StringJoiner joiner = new StringJoiner(",", "", "");
      configResults.forEach(r -> joiner.add(Long.toString(r)));
      buffer.write(joiner.toString());
      buffer.newLine();
      }
      buffer.flush();
      buffer.close();
      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    }


  }

}
