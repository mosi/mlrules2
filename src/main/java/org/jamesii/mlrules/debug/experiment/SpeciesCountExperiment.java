package org.jamesii.mlrules.debug.experiment;

import org.jamesii.mlrules.experiment.Experiment;
import org.jamesii.mlrules.experiment.SimpleJob;
import org.jamesii.mlrules.experiment.stop.SchrubensSteadyStateConditionFactory;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.simulator.factory.ModelNotSupportedException;
import org.jamesii.mlrules.simulator.factory.StandardSimulatorFactory;

import java.io.IOException;
import java.util.HashMap;

public class SpeciesCountExperiment {

  public static void main(String[] args) throws IOException, ModelNotSupportedException {

    String modelLocation = "./speciescount.mlrj";
    int cores = 1;
    StopConditionFactory stop = new SchrubensSteadyStateConditionFactory(
            (f -> f.apply("*/B")), 10.0, 8, 4, 0.05);
    //StopConditionFactory stop = new SchrubensSteadyStateConditionFactory(
    //        (f -> f.apply("*/B")), 10.0);

    Experiment exp = new Experiment(modelLocation, new StandardSimulatorFactory(true,false),
            new SpeciesCountInstrumenter(), stop, cores);

    exp.addJob(new SimpleJob(0, new HashMap<>()));

    exp.finish();

  }

}
