package org.jamesii.mlrules.debug.experiment;

import org.jamesii.mlrules.experiment.Experiment;
import org.jamesii.mlrules.experiment.SimpleJob;
import org.jamesii.mlrules.experiment.SimulationRun;
import org.jamesii.mlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.simulator.factory.AdvancedHybridSimulatorFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A simple experiment to evaluate the runtime of the 'runtimeCompilation'.
 *
 * @author Tobias Helms
 */
public class HybridExperiment {

  private static final int REPLICATIONS = 2;

  private static final String MODEL = "./hybridComparison/dictycurrent.mlrj";

  private static final double ENDTIME = 100.0;

  /**
   * The path must exist - it will not be created. Exceptions are thrown if it does not exist.
   */
  private static final String RESULTS_PATH = "/home/tobi/MoSi/tmp/dictyComparison/";

  private static final double INTERVAL = 10.0;

  private static final int CORES = -1;

  private static List<Long> execute(double jumpRelation) throws Exception {
    int cores = CORES < 0 ? Runtime.getRuntime().availableProcessors() + CORES : CORES;

    StopConditionFactory stop = new SimTimeStopConditionFactory(ENDTIME);

    Experiment exp = new Experiment(MODEL, new AdvancedHybridSimulatorFactory(AdvancedHybridSimulatorFactory.Integrator.DORMAND_PRINCE, false, jumpRelation, 0.01, 0.1, 0.3, 0.5),
            new ExpInstrumenter(new File(RESULTS_PATH+ jumpRelation), INTERVAL), stop, cores);

    List<SimulationRun> runs = new ArrayList<>();

    for (int i = 0; i < REPLICATIONS; ++i) {
      exp.addJob(new SimpleJob(i, new HashMap<>()));
      runs.add(exp.getLastCreatedRun());
    }

    exp.finish();
    exp.getExecutor().awaitTermination(100, TimeUnit.SECONDS);
    return runs.stream().map(SimulationRun::getRuntime).collect(Collectors.toList());
  }

  public static void main(String[] args) {

    List<List<Long>> results = new ArrayList<>();
    for (double jumpRelation : new Double[] {0.01,0.05,0.1,0.2,0.5,1.0}) {
      try {
        results.add(execute(jumpRelation));
      } catch (Exception e) {
        Logger.getGlobal().warning("Error occurred with optimization = " + jumpRelation);
        e.printStackTrace();
      }
    }
    try {
      FileWriter writer = new FileWriter(RESULTS_PATH + "runtime.csv");
      BufferedWriter buffer = new BufferedWriter(writer);

      for (List<Long> configResults : results) {

        StringJoiner joiner = new StringJoiner(",", "", "");
        configResults.forEach(r -> joiner.add(Long.toString(r)));
        buffer.write(joiner.toString());
        buffer.newLine();
      }
      buffer.flush();
      buffer.close();
      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    }


  }

}
