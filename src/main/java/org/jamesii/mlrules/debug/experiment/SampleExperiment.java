/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.debug.experiment;

import org.jamesii.mlrules.experiment.Experiment;
import org.jamesii.mlrules.experiment.SimpleJob;
import org.jamesii.mlrules.experiment.stop.SimTimeStopConditionFactory;
import org.jamesii.mlrules.experiment.stop.StopConditionFactory;
import org.jamesii.mlrules.simulator.factory.ModelNotSupportedException;
import org.jamesii.mlrules.simulator.factory.StandardSimulatorFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Very simple experiment for debugging purposes.
 *
 * @author Tobias Helms
 *
 */
public class SampleExperiment {

  public static void main(String[] args) throws IOException, ModelNotSupportedException {

    //Logger.getGlobal().setLevel(Level.WARNING);

    String modelLocation = "./test.mlrj";
    int cores = 2;
    StopConditionFactory stop = new SimTimeStopConditionFactory(10);
    File directory = new File("./");

    System.out.println(directory.getAbsolutePath());

    Experiment exp = new Experiment(modelLocation, new StandardSimulatorFactory(true, false),
            new ExpInstrumenter(directory, 0.1), stop, cores);

    Map<String,Object> parameter = new HashMap<>();
    parameter.put("a", "1000 A + 1000 B");

    for (int i = 0; i < 20; ++i) {
      exp.addJob(new SimpleJob(i, parameter));
    }

    exp.finish();

  }

}
