package org.jamesii.mlrules.debug.experiment;

import org.jamesii.mlrules.experiment.Job;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.observation.Instrumenter;
import org.jamesii.mlrules.observation.IntervalObserver;
import org.jamesii.mlrules.observation.Observer;
import org.jamesii.mlrules.observation.SpeciesCountListener;

import java.util.HashSet;
import java.util.Set;

public class SpeciesCountInstrumenter  implements Instrumenter {

  @Override
  public Set<Observer> create(Job job, Model model) {
    Set<Observer> set = new HashSet<>();
    Observer o = new IntervalObserver(model, 10.0);
    o.register(new SpeciesCountListener("A", p -> System.out.println(p.fst() + " : " + p.snd())));
    set.add(o);
    return set;
  }
}
