/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.debug;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.jamesii.core.math.random.generators.mersennetwister.MersenneTwister;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;
import org.jamesii.mlrules.simulator.Simulator;
import org.jamesii.mlrules.simulator.factory.*;
import org.jamesii.mlrules.util.MLEnvironment;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyParserTest {
    static Logger logger = Logger.getGlobal();

    public enum SimulatorType{
        Simple,
        Standard,
        Hybrid,
        AdvancedStatic,
        TauLeaping,
        Link
    }

    public static void main(String[] args) throws IOException {
        run(SimulatorType.Standard, init(), false, 100);
        NullOutputStream.NULL_OUTPUT_STREAM.write(System.in.read());
    }

    public static Model init() throws IOException {
        return init("src/main/resources/debug.mlrj");
    }

    public static Model init(String modelPath) throws IOException {
        logger.info("INITIALIZING");
        String model = readModel(modelPath);

        TypeCheckVisitor typeChecker = new TypeCheckVisitor();
        typeChecker.doTypeCheck(initParser(model).model());

        MLRulesParser parser = initParser(model);
        MLEnvironment env = initEnvironment();
        StandardMLRulesVisitor visitor = new StandardMLRulesVisitor(typeChecker.getSymbolTable(), env, new HashMap<>());
        return visitor.create(parser.model());
    }

    private static MLRulesParser initParser(String model) {
        MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromString(model));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        return new MLRulesParser(tokens);
    }

    private static String readModel(String modelPath) throws IOException {
        File modelFile = new File(modelPath);
        return FileUtils.readFileToString(modelFile,Charset.defaultCharset());
    }

    private static MLEnvironment initEnvironment() {
        MLEnvironment env = new MLEnvironment();
        env.setGlobalValue(Model.RNG,
                new MersenneTwister(System.currentTimeMillis()));
        env.setValue(SpeciesType.ROOT_NAME, SpeciesType.ROOT);
        PredefinedFunctions.addAll(env);
        return env;



    }

    private static Simulator buildSimulator(SimulatorType type,  Model model, boolean useRuntimeCompilation) throws ModelNotSupportedException, InstantiationException {
        switch (type){
            case Link:{
                LinkSimulatorFactory factory = new LinkSimulatorFactory(true, useRuntimeCompilation);
                return factory.create(model);
            }
            case Hybrid:{
                HybridSimulatorFactory factory = new HybridSimulatorFactory(HybridSimulatorFactory.Integrator.DORMAND_PRINCE, 0.01, 0.1, 0.03, false, useRuntimeCompilation);
                return factory.create(model);
            }
            case Simple:{
                SimpleSimulatorFactory factory =  new SimpleSimulatorFactory(true, useRuntimeCompilation);
                return factory.create(model);
            }
            case Standard:{
                StandardSimulatorFactory factory =  new StandardSimulatorFactory(true, useRuntimeCompilation);
                return factory.create(model);
            }
            case TauLeaping:{
                TauLeapingSimulatorFactory factory =  new TauLeapingSimulatorFactory(useRuntimeCompilation);
                return factory.create(model);
            }
            case AdvancedStatic:{
                throw new InstantiationException("Simulator not supported");
            }
            default:{
                throw new InstantiationException("Simulator not supported");
            }
        }
    }

    public static void run(SimulatorType type, Model model, boolean useRuntimeCompilation, double durationInMilliSeconds) throws IOException {
        try {
            Simulator simulator = buildSimulator(type, model, useRuntimeCompilation);
            Long start = System.currentTimeMillis();

            logger.info("START");

            while (simulator.getCurrentTime() < durationInMilliSeconds) {
                simulator.nextStep();
            }
            Logger.getGlobal().log(Level.INFO, String.format(
                    "Finished simulation in %s ms", System.currentTimeMillis() - start));
            logger.info("End");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void run(SimulatorType type, Model model, boolean useRuntimeCompilation, int simulationSteps) throws IOException {
        try {
            Simulator simulator = buildSimulator(type, model, useRuntimeCompilation);
            logger.info("START");
            for (int i = 0;i<simulationSteps;i++) {
                simulator.nextStep();
            }
            logger.info("End");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

}
