package org.jamesii.mlrules.debug;

import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;

public class Gamma {

  public static void main(String[] args) {
    GammaDistribution g = new GammaDistribution(new JDKRandomGenerator(1),11,1.0/0.036);

    int n = 100;
    double sum = 0;
    for (int i = 0; i < n; ++i) {
      sum += g.sample();
    }
    System.out.println(sum / n);
  }

}
