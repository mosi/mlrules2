/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.rule;

import org.jamesii.mlrules.model.rule.timed.TimedRule;

import java.util.List;

/**
 * Container class for a list of {@link Rule}s and a list of {@link TimedRule}s.
 * 
 * @author Tobias Helms
 *
 */
public class Rules {

	  private final List<Rule> rules;
	  
	  private final List<TimedRule> timedRules;
	  
	  public Rules(List<Rule> rules, List<TimedRule> timedRules) {
	  	this.rules = rules;
	  	this.timedRules = timedRules;
	  }
	  
	  public List<Rule> getRules() {
	  	return rules;
	  }
	  
	  public List<TimedRule> getTimedRules() {
	  	return timedRules;
	  }
	
}
