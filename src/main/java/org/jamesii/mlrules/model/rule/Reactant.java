/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.rule;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.mlrules.model.species.SpeciesType;

import java.util.List;
import java.util.Optional;

/**
 * Reactants are defined on the left hand side of rules. They represent species
 * pattern, i.e., species are matched to reactants to create reactions.
 * Reactants can be nested.
 * 
 * @author Tobias Helms
 *
 */
public class Reactant {

  private final SpeciesType type;

  private final Node amount;

  private final List<Node> attributeNodes;

  /**
   * The rest name is used to describe all species within the matched species of
   * this reactant that are not matched to sub reactants. For example, the rest
   * of the species A()[B() + C()] with the reactant A()[B + rest?] is C(). If
   * no rest name is given, the optional is empty.
   */
  private final Optional<String> rest;

  /**
   * When a reactant pattern is matched to a concrete species, this species is
   * bound to the boundTo variable name. If no name for this variable is given
   * within the model file, the optional is empty.
   */
  private final Optional<String> boundTo;

  private final List<Reactant> subReactants;
  
  public Reactant(SpeciesType type, Node amount, List<Node> attributeNodes,
      Optional<String> rest, Optional<String> boundTo, List<Reactant> subReactants) {
    this.type = type;
    this.amount = amount;
    this.attributeNodes = attributeNodes;
    this.rest = rest;
    this.boundTo = boundTo;
    this.subReactants = subReactants;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(amount.toString());
    builder.append(" ");
    builder.append(type.getName());
    builder.append(attributeNodes.toString());

    if (!subReactants.isEmpty()) {
      builder.append(subReactants.toString());
    }
    rest.ifPresent(r -> builder.append(":" + rest));
    boundTo.ifPresent(b -> builder.append(":" + boundTo));

    return builder.toString();
  }

  public SpeciesType getType() {
    return type;
  }

  public List<Node> getAttributeNodes() {
    return attributeNodes;
  }

  public Optional<String> getBoundTo() {
    return boundTo;
  }

  public Node getAmount() {
    return amount;
  }

  public List<Reactant> getSubReactants() {
    return subReactants;
  }

  public Optional<String> getRest() {
    return rest;
  }

  public boolean isCompartment() {
    return type.isCompartment();
  }
  
}
