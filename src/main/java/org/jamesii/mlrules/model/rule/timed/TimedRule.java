/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.rule.timed;

import org.jamesii.mlrules.model.rule.Rule;

/**
 * 
 * Marker class for timed rules that should be executed each t time units.
 * 
 * @author Tobias Helms
 *
 */
public class TimedRule {

	private double nextTime;
	
  private final Rule rule;
  
  private final Timer timer;
  
  public TimedRule(Rule rule, Timer timer) {
    this(rule, timer, timer.nextTime());
  }
  
  public TimedRule(Rule rule, Timer timer, Double nextTime) {
    this.rule = rule;
    this.timer = timer;
    this.nextTime = nextTime;
  }
  
  public double getNextTime() {
    return nextTime;
  }

  public void updateNextTime() {
    nextTime = timer.nextTime();
  }
  
  public double getAndUpdateNextTime() {
    double tmp = getNextTime();
    updateNextTime();
    return tmp;
  }
  
  public Rule getRule() {
  	return rule;
  }
  
  public boolean done() {
    return Double.isInfinite(nextTime);
  }
  
}
