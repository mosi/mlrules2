/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.rule.timed;

/**
 * 
 * The {@link IntervalTimer} use an interval value to calculate a sequence of
 * reaction times for a {@link TimedReaction}.
 * 
 * @author Tobias Helms
 *
 */
public class IntervalTimer implements Timer {

  /**
   * The next firing time of the corresponding {@link TimedReaction}.
   */
  double nextTime;

  /**
   * The interval of the firing times.
   */
  double interval;

  public IntervalTimer(double nextTime) {
    this.nextTime = 0D;
    this.interval = nextTime;
  }

  @Override
  public double nextTime() {
    nextTime += interval;
    return nextTime;
  }

}
