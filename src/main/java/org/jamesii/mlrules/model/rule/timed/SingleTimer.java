/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.rule.timed;

/**
 * The {@link SingleTimer} returns infinity as the next reaction time, i.e., a
 * {@link TimedReaction} with this timer fires only once at the initially
 * computed time.
 * 
 * @author Tobias Helms
 *
 */
public class SingleTimer implements Timer {

  public static SingleTimer instance = new SingleTimer();

  private SingleTimer() {
  }

  @Override
  public double nextTime() {
    return Double.POSITIVE_INFINITY;
  }

}
