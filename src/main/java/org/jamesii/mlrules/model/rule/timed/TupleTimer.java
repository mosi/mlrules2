/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.rule.timed;

import org.jamesii.mlrules.parser.types.Tuple;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * The {@link TupleTimer} uses a predefined list of reaction times for a {@link TimedReaction}.
 * 
 * @author Tobias Helms
 *
 */
public class TupleTimer implements Timer {

  Queue<Double> times;
  
  public TupleTimer(Tuple tuple) {
    this.times = new PriorityQueue<Double>();
    for (int i = 0; i < tuple.size(); ++i) {
      times.add((Double) tuple.get(i));
    }
    times.add(Double.POSITIVE_INFINITY);
  }
  
  @Override
  public double nextTime() {
    return times.poll();
  }

}
