package org.jamesii.mlrules.model;

import org.jamesii.mlrules.model.rule.Rules;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.util.MLEnvironment;

/**
 * The ML-Rules model consists of a species tree, rule schemes, and an
 * environment.
 * 
 * @author Tobias Helms
 *
 */
public class Model {

  private final MLEnvironment env;

  /**
   * The current model state represented by the root species compartment.
   */
  private Compartment species;

  private final Rules rules;

  /** The keyword used for the random number generator in the environment. */
  public static final String RNG = "§rng";

  /**
   * The keyword used for the time (the current simulation time) entry in the
   * environment.
   */
  public static final String TIME = "§time";

  public Model(Compartment species, Rules rules, MLEnvironment env) {
    this.species = species;
    this.rules = rules;
    this.env = env;
  }

  @Override
  public String toString() {
    return String.format("%s \n %s \n %s", species.toString(), rules.toString(), env.toString());
  }

  /**
   * Return the environment of the model containing constants, functions, the
   * random number generator, etc.
   */
  public MLEnvironment getEnv() {
    return env;
  }

  public Compartment getSpecies() {
    return species;
  }

  public void setSpecies(Compartment species) {
    this.species = species;
  }

  public Rules getRules() {
    return rules;
  }

}
