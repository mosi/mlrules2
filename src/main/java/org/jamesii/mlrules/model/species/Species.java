/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.species;

import java.io.Serializable;
import java.util.Arrays;
import java.util.StringJoiner;

/**
 * {@link Species} in ML-Rules are the basic entities of the model. A
 * {@link Species} is defined by a type, a list of attributes and its context
 * species. If a species can contain other species, it is a {@link Compartment}.
 * If a species cannot contain other species, it is a {@link LeafSpecies}. A
 * concrete state of the model consisting of species defines a tree, whereby the
 * root of this tree is always of type {@link SpeciesType#ROOT}.
 * 
 * @author Tobias Helms
 *
 */
public abstract class Species implements Serializable {

  private final SpeciesType type;

  private final Object[] attributes;

  private final int hash;

  private Compartment context;

  protected int computeHash() {
    int result = getType().hashCode();
    for (int i = 0; i < attributes.length; ++i) {
      result = 31 * result + attributes[i].hashCode();
    }
    return result;
  }

  public Species(SpeciesType type, Object[] attributes, Compartment context) {
    this.type = type;
    if (attributes.length != type.getAttributesSize()) {
      throw new IllegalArgumentException(
          String.format("Could not create species %s because of wrong attribute numbers: %s vs. %s", type,
              attributes.length, type.getAttributesSize()));
    }
    this.attributes = Arrays.copyOf(attributes, attributes.length);
    this.context = context;
    this.hash = computeHash();
  }

  public SpeciesType getType() {
    return type;
  }

  public Object getAttribute(int i) {
    if (i < 0 || i > attributes.length - 1) {
      throw new IllegalArgumentException(String.format("Could not access attribute %s from species %s", i, toString()));
    }
    return attributes[i];
  }
 
  public Compartment getContext() {
    return context;
  }

  public void setContext(Compartment context) {
    this.context = context;
  }

  @Override
  public int hashCode() {
    return hash;
  }

  /**
   * Copy the given species and in case of compartments copy all compartments
   * (deep copy). SpecOptAttribute links are not changed, i.e., copies contain the same
   * link values as the originals.
   */
  public abstract Species copy();

  public abstract double getAmount();

  public String toString() {
    StringJoiner joiner = new StringJoiner(",", "(", ")");
    for (int i = 0; i < attributes.length; ++i) {
      joiner.add(attributes[i].toString());
    }
    return type.getName() + joiner.toString();
  }

}
