/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.model.species;

import java.io.Serializable;

/**
 * A type defines a name, a tuple of attribute types for species and
 * compartments.
 * 
 * @author Tobias Helms
 *
 */
public final class SpeciesType implements Serializable{

  public enum AttributeType {
    NUM, BOOL, STRING, LINK
  }

  private final String name;

  private final boolean isCompartment;

  private final AttributeType[] attributes;

  public static final String ROOT_NAME = "$$ROOT$$";

  public static final String UNKNOWN_NAME = "$$UNKNOWN$$";

  /**
   * The type of the species which is always the root of the species tree of an
   * ML-Rules model.
   */
  public static final SpeciesType ROOT = new SpeciesType(ROOT_NAME, new AttributeType[0], true);

  public static final SpeciesType UNKNOWN = new SpeciesType(UNKNOWN_NAME, new AttributeType[0], true);

  public SpeciesType(String name, AttributeType[] attributes, boolean isCompartment) {
    this.name = name;
    this.attributes = attributes;
    this.isCompartment = isCompartment;
  }

  public final String getName() {
    return name;
  }

  public final int getAttributesSize() {
    return attributes.length;
  }

  public final AttributeType getType(int i) {
    if (i < 0 || i > attributes.length - 1) {
      throw new IllegalArgumentException(String.format("Could not access species type %s from type %s", i, toString()));
    }
    return attributes[i];
  }

  public boolean isCompartment() {
    return isCompartment;
  }

  @Override
  public String toString() {
    return name + "(" + attributes.toString() + ")";
  }

}
