package org.jamesii.mlrules.model.species;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A {@link Compartment} is a {@link Species} that can contain other
 * {@link Species}. {@link Compartment}s are treated individual based, i.e., the
 * amount is always 1.
 *
 * @author Tobias Helms
 */
public class Compartment extends Species implements Serializable {

  private final Map<SpeciesType, Map<LeafSpecies, LeafSpecies>> subLeaves;

  private final Map<SpeciesType, Set<Compartment>> subCompartments;

  public boolean containsType(SpeciesType type) {
    if (type.isCompartment()) {
      return subCompartments.containsKey(type);
    } else {
      return subLeaves.containsKey(type);
    }
  }

  public void add(Species species) {
    if (species instanceof LeafSpecies) {
      subLeaves.computeIfAbsent(species.getType(), k -> new HashMap<>())
               .put((LeafSpecies) species,
                       (LeafSpecies) species);
    } else {
      subCompartments.computeIfAbsent(species.getType(), k -> new HashSet<>())
                     .add((Compartment) species);
    }
  }

  public void remove(Species species) {
    if (species instanceof LeafSpecies) {
      Map<LeafSpecies, LeafSpecies> subs = subLeaves.getOrDefault(species.getType(), Collections.emptyMap());
      subs.remove(species);
      if (subs.isEmpty()) {
        subLeaves.remove(species.getType());
      }
    } else {
      Set<Compartment> subs = subCompartments.getOrDefault(species.getType(), Collections.emptySet());
      subs.remove(species);
      if (subs.isEmpty()) {
        subCompartments.remove(species.getType());
      }
    }
  }

  public Compartment(SpeciesType type, Object[] attributes, Compartment context,
                     Map<SpeciesType, Map<LeafSpecies, LeafSpecies>> subLeaves, Map<SpeciesType, Set<Compartment>> subCompartments) {
    super(type, attributes, context);
    this.subLeaves = subLeaves;
    this.subCompartments = subCompartments;
  }

  private void getContextStreamInternal(Stream.Builder<Compartment> builder) {
    builder.add(this);
    if (getContext() != Compartment.UNKNOWN) {
      getContext().getContextStreamInternal(builder);
    }
  }

  public Stream<Compartment> getContextStream() {
    Stream.Builder<Compartment> builder = Stream.builder();
    getContextStreamInternal(builder);
    return builder.build();
  }

  public Optional<Species> getSpecies(Species species) {
    if (species instanceof LeafSpecies) {
      return Optional.ofNullable(subLeaves.getOrDefault(species.getType(), Collections.emptyMap())
                                          .get(species));
    } else {
      if (subCompartments.getOrDefault(species.getType(), Collections.emptySet())
                         .contains(species)) {
        return Optional.of(species);
      } else {
        return Optional.empty();
      }
    }
  }

  public Map<Species, Species> getAllSpecies() {
    Map<Species, Species> result = new HashMap<>();
    subLeaves.values()
             .forEach(m -> m.values()
                            .forEach(v -> result.put(v, v)));
    subCompartments.values()
                   .forEach(s -> s.forEach(v -> result.put(v, v)));
    return result;
  }

  public Map<SpeciesType, Set<Compartment>> getSubCompartments() {
    return subCompartments;
  }

  public Map<SpeciesType, Map<LeafSpecies, LeafSpecies>> getSubLeaves() {
    return subLeaves;
  }

  public Stream<Species> getAllSubSpeciesStream() {
    return Stream.concat(getSubLeavesStream(), getSubCompartmentsStream());
  }

  public Stream<LeafSpecies> getSubLeavesStream() {
    return subLeaves.values()
                    .stream()
                    .flatMap(v -> v.values()
                                   .stream());
  }

  public Stream<Compartment> getSubCompartmentsStream() {
    return subCompartments.values()
                          .stream()
                          .flatMap(v -> v.stream());
  }

  private void getSubCompartmentsRecursiveStreamInternal(Stream.Builder<Compartment> builder) {
    builder.add(this);
    getSubCompartmentsStream().forEach(c -> c.getSubCompartmentsRecursiveStreamInternal(builder));
  }

  /**
   * Return all compartments that are inside in the given species (including
   * itself) recursively, i.e., the stream also contains the compartments of the
   * compartments and their compartments etc.
   */
  public Stream<Compartment> getSubCompartmentsRecursiveStream() {
    Stream.Builder<Compartment> builder = Stream.builder();
    getSubCompartmentsRecursiveStreamInternal(builder);
    return builder.build();
  }

  private void getSubCompartmentsRecursiveInternal(Set<Compartment> set) {
    set.add(this);
    getSubCompartmentsStream().forEach(c -> c.getSubCompartmentsRecursiveInternal(set));
  }

  public Set<Compartment> getSubCompartmentsRecursive() {
    Set<Compartment> result = new HashSet<>();
    getSubCompartmentsRecursiveInternal(result);
    return result;
  }

  public int countAllCompartments() {
    return (int) getSubCompartmentsRecursiveStream().count();
  }

  public int countAllLeafSpecies() {
    Stream<Compartment> compartmentsStream = subCompartments.values()
                                                            .stream()
                                                            .flatMap(Collection::stream);
    int compartmentsLeaves = compartmentsStream.mapToInt(Compartment::countAllLeafSpecies)
                                               .sum();
    int leaves = (int) subLeaves.values()
                                .stream()
                                .flatMap(m -> m.values()
                                               .stream())
                                .count();
    return compartmentsLeaves + leaves;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(" ");
    builder.append(getType().getName());
    if (getType().getAttributesSize() > 0) {
      StringJoiner joiner = new StringJoiner(",", "(", ")");
      for (int i = 0; i < getType().getAttributesSize(); ++i) {
        joiner.add(getAttribute(i).toString());
      }
      builder.append(joiner.toString());
    }
    StringJoiner compJoiner = new StringJoiner("+", "[", "]");
    for (Set<Compartment> sc : subCompartments.values()) {
      for (Compartment c : sc) {
        compJoiner.add(c.toString());
      }
    }
    for (Map<LeafSpecies, LeafSpecies> ml : subLeaves.values()) {
      for (LeafSpecies l : ml.values()) {
        compJoiner.add(l.toString());
      }
    }
    builder.append(compJoiner.toString());
    return builder.toString();
  }

  @Override
  public boolean equals(Object o) {
    return o == this;
  }

  @Override
  public Species copy() {
    Object[] attributes = new Object[getType().getAttributesSize()];
    for (int i = 0; i < getType().getAttributesSize(); ++i) {
      attributes[i] = getAttribute(i);
    }
    Map<SpeciesType, Map<LeafSpecies, LeafSpecies>> copySubLeaves = new HashMap<>();
    for (Entry<SpeciesType, Map<LeafSpecies, LeafSpecies>> e : subLeaves.entrySet()) {
      copySubLeaves.put(e.getKey(), e.getValue()
                                     .keySet()
                                     .stream()
                                     .map(s -> s.copy())
                                     .collect(Collectors.toMap(s -> (LeafSpecies) s, s -> (LeafSpecies) s)));
    }
    Map<SpeciesType, Set<Compartment>> copySubCompartments = new HashMap<>();
    for (Entry<SpeciesType, Set<Compartment>> e : subCompartments.entrySet()) {
      copySubCompartments.put(e.getKey(),
              e.getValue()
               .stream()
               .map(s -> (Compartment) s.copy())
               .collect(Collectors.toSet()));
    }
    Compartment result = new Compartment(getType(), attributes, getContext(), copySubLeaves, copySubCompartments);
    copySubLeaves.values()
                 .forEach(v -> v.keySet()
                                .forEach(s -> s.setContext(result)));
    copySubCompartments.values()
                       .forEach(v -> v.forEach(s -> s.setContext(result)));
    return result;
  }

  @Override
  public double getAmount() {
    return 1;
  }

  public final static Compartment UNKNOWN = new Compartment(SpeciesType.UNKNOWN, new Object[0], null,
          Collections.emptyMap(), Collections.emptyMap()) {

    private final String msg = "Not supported by UNKNOWN compartment.";

    @Override
    public void add(Species species) {
      throw new RuntimeException(msg);
    }

    @Override
    public void remove(Species species) {
      throw new RuntimeException(msg);
    }

    @Override
    public boolean containsType(SpeciesType type) {
      throw new RuntimeException(msg);
    }

    @Override
    public Optional<Species> getSpecies(Species species) {
      throw new RuntimeException(msg);
    }

    @Override
    public Map<Species, Species> getAllSpecies() {
      throw new RuntimeException(msg);
    }

    @Override
    public String toString() {
      return "UNKNOWN";
    }

    @Override
    public Species copy() {
      throw new RuntimeException(msg);
    }

    @Override
    public Object getAttribute(int i) {
      throw new RuntimeException(msg);
    }

    @Override
    public Compartment getContext() {
      throw new RuntimeException(msg);
    }

    @Override
    public void setContext(Compartment context) {
      throw new RuntimeException(msg);
    }

  };

}
