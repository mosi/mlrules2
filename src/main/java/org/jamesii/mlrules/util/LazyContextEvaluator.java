/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.util;

import org.antlr.v4.runtime.ParserRuleContext;
import org.jamesii.mlrules.parser.visitor.modelcreator.StandardMLRulesVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Andreas Ruscheinski
 *
 */
public class LazyContextEvaluator {
  private List<ParserRuleContext> unhandeldContexts =
      new ArrayList<ParserRuleContext>();

  private StandardMLRulesVisitor visitor;

  public LazyContextEvaluator(StandardMLRulesVisitor visitor) {
    this.visitor = visitor;
  }

  public void addUnhandeldContext(ParserRuleContext ctx) {
    this.unhandeldContexts.add(ctx);
  }

  public List<ParserRuleContext> getUnhandeldContexts() {
    return this.unhandeldContexts;
  }

  public void update() {
  	int counter = unhandeldContexts.size();
    while (true) {
      List<ParserRuleContext> nUnhandeledContexts =
          new ArrayList<ParserRuleContext>();
      List<ParserRuleContext> copy =
          new ArrayList<ParserRuleContext>(this.unhandeldContexts);
      unhandeldContexts.clear();
      for (ParserRuleContext context : copy) {
        try {
          this.visitor.visit(context);
        } catch (Exception e) {
          nUnhandeledContexts.add(context);
        }
      }
      this.unhandeldContexts.addAll(nUnhandeledContexts);
      if (counter == unhandeldContexts.size()) {
      	return;
      }
      counter = unhandeldContexts.size();
    }
  }

  public boolean hasUnhandeldContexts() {
    if (unhandeldContexts.size() != 0) {
      return true;
    }
    return false;
  }
}
