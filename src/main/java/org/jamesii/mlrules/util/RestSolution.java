/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.util;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;

import java.util.HashMap;
import java.util.Map;

/**
 * The rest solution is similar to the {@link LazyInitialization}, i.e., rest
 * solutions are not evaluated until they are used.
 *
 * @author Tobias Helms
 */
public class RestSolution {

  private final String name;

  private final Compartment context;

  protected final Map<Species, Integer> removals;

  public RestSolution(String name, Compartment context,
                      Map<Species, Integer> removals) {
    this.name = name;
    this.context = context;
    this.removals = removals;
  }

  protected void fillRest(Species s, Map<Species, Species> rest) {
    Integer remove = removals.getOrDefault(s, 0);
    if (remove == 0) {
      rest.put(s, s);
    } else {
      if (s instanceof LeafSpecies) {
        Species copy = s.copy();
        LeafSpecies ls = (LeafSpecies) copy;
        ls.setAmount(ls.getAmount() - remove);
        if (Double.compare(ls.getAmount(), 0) > 0) {
          rest.put(ls, ls);
        }
      }
    }
  }

  /**
   * Compute the set of species of this rest solution and add the calculated
   * rest solution to the given environment.
   */
  public void computeValue(MLEnvironment env) {
    Map<Species, Species> rest = new HashMap<>();
    context.getAllSubSpeciesStream()
           .forEach(s -> fillRest(s, rest));
    env.setValue(name, rest);
  }

}
