package org.jamesii.mlrules.util.runtimeCompiling;

import javassist.*;
import org.jamesii.core.math.parsetree.BinaryNode;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.bool.*;
import org.jamesii.core.math.parsetree.control.IfThenElseNode;
import org.jamesii.core.math.parsetree.math.IMathNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.parser.functions.Function;
import org.jamesii.mlrules.parser.nodes.FunctionCallNode;
import org.jamesii.mlrules.parser.nodes.SpeciesAmountNode;
import org.jamesii.mlrules.parser.types.BaseType;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Used to generate new custom nodes from an {@link INode} root. Use the {@link JavassistNodeFactory#generateDynamicNode(INode, IEnvironment)}
 * function to get a new node. The generated node will have an overwritten calc() function which hard codes the prior
 * existing INode tree into a solid function.
 * <p></p>
 * All compiled class files are stored in a tmp folder inside pwd and are deleted on program termination (including
 * unintended termination)
 * <p></p>
 * Created by tm219 on 4/5/17.
 */
public class JavassistNodeFactory implements IDynamicNodeFactory{
    /**
     * Class which should be generated with the last call of {@link JavassistNodeFactory#generateDynamicNode(INode, IEnvironment)}
     */
    private CtClass currentClass = null;
    private boolean hasInitializer = false;
    private final String initializerFunctionName = "initialize";
    private Stack<STACK_TYPES> returnTypeStack = new Stack<>();
    private static Path tempFolder = null;

    private enum STACK_TYPES{
        BOOLEAN,
        DOUBLE,
        OTHER
    }

    public JavassistNodeFactory() throws IOException{
        if (tempFolder == null) {
            tempFolder = Files.createTempDirectory(new File(".").toPath(), "generatedFiles");
            Runtime.getRuntime().addShutdownHook(new Thread() {
                 public void run() {
                     try {
                         CompileTools.deleteGeneratedClasses(tempFolder.toString());
                     }catch (Exception e){}
                 }
             });
            tempFolder.toFile().deleteOnExit();
        }
    }

    /**
     * @param root The Node which' calculation should be generated anew
     * @param environment environment which should be used for the lookup of possible {@link Identifier}s
     *                    The environment will be hardcoded and cannot be changed once the Dynamic node is generated.
     *                    All identifiers which already have a value are treated as constants. Others will be evaluated
     *                    at runtime.
     * @return A new Object of the generated and compiled class on success, null otherwise.
     */
    public DynamicNode generateDynamicNode(INode root, IEnvironment environment){
        if (root instanceof DynamicNode){
            System.out.println("Info: skipping node to compile because it was compiled earlier");
            return (DynamicNode)root;
        }
        DynamicNode dynamicNode = null;
        try {
            createClass();
            interceptMethod(root, environment);
            compileClass();
            dynamicNode = instantiate();
        }catch (Exception e){
            System.out.println("Warning: compiling class with javassist failed: "+ e.getMessage());
        }
        this.currentClass = null;
        this.hasInitializer = false;
        this.returnTypeStack.clear();
        return dynamicNode;
    }



    /**
     *
     * @return An Object of the newly generated Class.
     * @throws CannotCompileException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private DynamicNode instantiate() throws CannotCompileException, IllegalAccessException, InstantiationException {
        Class<?> clazz = currentClass.toClass();
        return (DynamicNode) clazz.newInstance();
    }

    /**
     * Writes the byte code of the class
     * @throws NotFoundException
     * @throws IOException
     * @throws CannotCompileException
     */
    private void compileClass() throws NotFoundException, IOException, CannotCompileException {
        currentClass.writeFile(tempFolder.toString());
    }

    /**
     * overwrites the calculateValue() function of the base class
     * If the Class contains at least one species amount node, this method will also generate an initializer function for
     * each such node. The initializer will look up the Object of the species in the environment on first use, so that
     * the lookup can be skipped on all future uses of the species node.
     * @param root INode of the root node
     * @throws NotFoundException
     * @throws CannotCompileException
     * @throws IOException
     */
    private void interceptMethod(INode root, IEnvironment environment) throws NotFoundException, CannotCompileException, IOException {
        if (currentClass.getSuperclass().getDeclaredMethod("calculateValue") != null) {
            CtClass environmentType = ClassPool.getDefault().get(IEnvironment.class.getName());
            CtClass returnType = ClassPool.getDefault().get(ValueNode.class.getName());
            CtMethod calculateMethod = new CtMethod(
                    returnType,
                    "calculateValue",
                    new CtClass[]{environmentType},
                    currentClass
            );
            currentClass.addMethod(calculateMethod);//makes class abstract since method has no body yet
            StringBuilder body = new StringBuilder(String.format("{\n"+
                            "return (new %1$s(%2$s));\n" +
                            "}",
                    returnType.getName(),
                    nodeToString(root, environment)
            ));
            if ( hasInitializer){
                body.insert(0,
                        String.format("if(!initialized){%1$s($1);}",
                                initializerFunctionName
                        ));
            }
            body.insert(0,"{").append("}");
            calculateMethod.setBody(body.toString());
            currentClass.setModifiers(currentClass.getModifiers() & ~Modifier.ABSTRACT); // remove the abstract modifier from class
        }else{
            throw new IllegalStateException("Parent class does not declare calculateValue() method");
        }
    }

    /**
     * adds the new class to the javassist class pool and initializes basic parameters
     * @throws CannotCompileException
     * @throws NotFoundException
     */
    private void createClass() throws CannotCompileException, NotFoundException {
        ClassPool pool = ClassPool.getDefault();
        currentClass = pool.makeClass(
                "DN"+ randomString()
        );
        currentClass.setSuperclass(pool.get("org.jamesii.mlrules.util.runtimeCompiling.DynamicNode"));
    }

    /**
     * Generates a new string which represent the given tree as java code. The function traverses the tree recursively
     * Identifiers which are existing in the given environment are assumed to be constant and are hardcoded.
     * For Identifiers without a value in the given environment, a lookup function will be generated.
     * @param node Current node which should be converted
     * @return the string which represents the given node as javacode
     * @throws IllegalStateException
     * @throws NotFoundException
     * @throws CannotCompileException
     * @throws IOException
     */
    private String nodeToString(INode node, IEnvironment environment) throws IllegalStateException, NotFoundException, CannotCompileException, IOException {
        if (node instanceof Identifier){
            String key = (String)((Identifier) node).getIdent();
            boolean topOfStack = false;
            String returnString = "";
            if(returnTypeStack.isEmpty()){//assume double if top of stack
                topOfStack = true;
                returnTypeStack.push(STACK_TYPES.DOUBLE);
            }
            if(key.startsWith("§")){
                throw new IllegalStateException("got predefined function");
            }
            Object identifierObject = environment.getValue(key);
            if (identifierObject  instanceof Number){// if it is a constant: write the value
                return environment.getValue(((Identifier) node).getIdent()).toString();
            }else {
                String lookupMethodName = generateIdentifierEnvironmentLookup((Identifier) node, environment);
                if(returnTypeStack.lastElement() == STACK_TYPES.BOOLEAN) {
                    returnString = lookupMethodName + "($1).booleanValue()";//The "$1" is first parameter from the DynamicNode#calculateValue function in this context (aka the environment)
                }else if(returnTypeStack.lastElement() == STACK_TYPES.DOUBLE){
                    returnString = lookupMethodName + "($1).doubleValue()";//The "$1" is first parameter from the DynamicNode#calculateValue function in this context (aka the environment)
                }else{
                    throw new IllegalStateException("unexpected environment type");
                }
            }
            if(topOfStack){
                returnTypeStack.pop();
            }
            return returnString;
        }

        if (node instanceof BinaryNode) {
            List<? extends INode> children = node.getChildren();
            if (node instanceof IMathNode) {
                returnTypeStack.push(STACK_TYPES.DOUBLE);
                assert (children.size() == 2);
                // build code string like "(leftChild operator rightChild)"
                String returnString =String.format(
                        "(%1$s %2$s %3$s)",
                        nodeToString(children.get(0),environment),
                        node.getName(),
                        nodeToString(children.get(1),environment)
                        );
                returnTypeStack.pop();
                return returnString;
            }else{//assuming it is a boolean node
                if(nodeNeedsBooleanChild(node)) {
                    returnTypeStack.push(STACK_TYPES.BOOLEAN);
                }else{
                    returnTypeStack.push(STACK_TYPES.DOUBLE);
                }
                String returnString =String.format(
                        "(%1$s %2$s %3$s)",
                        nodeToString(children.get(0),environment),
                        getBooleanNodeOperator(node),
                        nodeToString(children.get(1),environment)
                );
                returnTypeStack.pop();
                return returnString;
            }
        }

        if(node instanceof NotNode){
            returnTypeStack.push(STACK_TYPES.BOOLEAN);
            assert (node.getChildren().size() == 1);
            String returnString =String.format(
                    "(!%1$s)",
                    nodeToString(node.getChildren().get(0), environment)
            );
            returnTypeStack.pop();
            return returnString;
        }

        if (node instanceof FunctionCallNode){
            List functionTypes = getFunction((FunctionCallNode)node).getType().getSubTypes();
            String returnString;
            if(functionTypes.toArray()[functionTypes.size()-1]  == BaseType.NUM){
                returnTypeStack.push(STACK_TYPES.DOUBLE);
                returnString = String.format("%1$s($1).doubleValue()",generateFunctionEnvironmentLookup((FunctionCallNode) node, environment));
            }else if(functionTypes.toArray()[functionTypes.size()-1]  == BaseType.BOOL){
                returnTypeStack.push(STACK_TYPES.BOOLEAN);
                returnString = String.format("%1$s($1).booleanValue()",generateFunctionEnvironmentLookup((FunctionCallNode) node, environment));
            }else{
                throw new IllegalStateException("Can't handle function return type");
            }
            returnTypeStack.pop();
            return returnString;
        }

        if (node instanceof IfThenElseNode){
            return String.format("(%1$s($1))", generateIfThenElseFunction((IfThenElseNode) node, environment));
        }

        if (node instanceof SpeciesAmountNode){
            String environmentKey = ((SpeciesAmountNode)node).getOtherName();
            if(environmentKey == null){
                throw new IllegalStateException("species name is null");
            }
            String speciesName = "species_"+environmentKey;
            try{
                currentClass.getField(speciesName);
            }catch (NotFoundException e) {
                CtClass speciesType = ClassPool.getDefault().get(Species.class.getName());
                generateFinalMemberVariable(
                        speciesType,
                        speciesName
                );
                addInitializer(speciesType, speciesName, environmentKey);
            }
            return speciesName+".getAmount()";
        }

        if (node instanceof ValueNode){
            return ((ValueNode)node).getValue().toString();
        }

        throw new IllegalStateException("Can't determine node type");
    }

    /**
     * @param booleanNode
     * @return A String representing a boolean operator in java code an success, throws an exception otherwise
     */
    private String getBooleanNodeOperator(INode booleanNode)throws IllegalStateException{
        if(booleanNode instanceof AndNode){
            return "&&";
        }
        if(booleanNode instanceof IsEqualNode){
            return "==";
        }
        if(booleanNode instanceof IsGreaterNode){
            return ">";
        }
        if(booleanNode instanceof IsGreaterOrEqualNode){
            return ">=";
        }
        if(booleanNode instanceof IsLowerNode){
            return "<";
        }
        if(booleanNode instanceof IsLowerOrEqualNode){
            return "<=";
        }
        if(booleanNode instanceof IsNotEqualNode){
            return "!=";
        }
        if(booleanNode instanceof NotNode){
            return "!";
        }
        if(booleanNode instanceof OrNode){
            return "||";
        }
        throw new IllegalStateException("Expected boolean node");
    }

    /**
     * @param node
     * @return true if the node is an AndNode, OrNode, or NotNode (which needs logic values to compute, not arithmetic ones)
     */
    private boolean nodeNeedsBooleanChild(INode node){
        if (node instanceof AndNode || node instanceof OrNode){
            return true;
        }
        if (node instanceof NotNode){
            return true;
        }
        return false;
    }

    /**
     * Adds an initializer for a speciesAmountNode to the class which is currently compiling, if none exists already.
     * Adds the new speciesAmount to the initializer if the function already exists.
     * @param memberType
     * @param memberName
     * @param environmentKey
     * @throws NotFoundException
     * @throws CannotCompileException
     */
    private void addInitializer(CtClass memberType, String memberName, String environmentKey) throws NotFoundException, CannotCompileException {
        CtMethod initializeMethod = null;
        try {
            initializeMethod = currentClass.getDeclaredMethod(initializerFunctionName);
        }catch (NotFoundException e){
            CtClass environmentType = ClassPool.getDefault().get(IEnvironment.class.getName());
            initializeMethod = new CtMethod(
                    CtClass.voidType,
                    initializerFunctionName,
                    new CtClass[]{environmentType},
                    currentClass
            );
            initializeMethod.setBody("{initialized = true;}");// initialized is declared in the DynamicNode super class
            currentClass.addMethod(initializeMethod);
            hasInitializer = true;
        }
        final String hashMapTypeName = ClassPool.getDefault().get(HashMap.class.getName()).getName();
        StringBuilder body = new StringBuilder("");
        body.append(String.format("Object species = $1.getValue(\"%s\");", environmentKey));
        body.append(String.format("if(species instanceof %s)", memberType.getName()));
        body.append(String.format("{%s = (%s)species;}", memberName, memberType.getName()));
        body.append(String.format("if(species instanceof %s)", hashMapTypeName));
        body.append(String.format("{if (((%s)species).size() != 1) throw new Exception(\"this HashMap is expected to have exactly one entry\");", hashMapTypeName));
        body.append(String.format("%s = (%s)((%s)species).keySet().toArray()[0];}", memberName, memberType.getName(), hashMapTypeName));

        //body.append(String.format("else{throw new Exception(\"Type is expected to be a Species or HasMap\");}"));
        initializeMethod.insertBefore(body.toString());
    }

    /**
     * Adds a function to the class which looks up the {@link Identifier} in the {@link IEnvironment} and returns its
     * randomly generated name.
     * @param identifier
     * @return
     * @throws NotFoundException
     * @throws CannotCompileException
     * @throws IllegalStateException
     * @throws IOException
     */
    private String generateIdentifierEnvironmentLookup(Identifier identifier, IEnvironment environment) throws NotFoundException, CannotCompileException, IllegalStateException, IOException {
        if (environment == null){
            throw new IllegalStateException("Empty Environment");
        }
        //Other identifiers than Strings are not supported because they are practically not used
        if (!(identifier.getIdent() instanceof String)){
            throw new IllegalStateException("Environment keys should only be Strings");
        }
        String methodName;
        Object value = environment.getValue(identifier.getIdent());
        if(value instanceof INode){
          throw new IllegalStateException("Identifiers of type INode are not supported in code generation");
        }else {
            methodName = "getEnv_"+identifier.getIdent();
            try {
                currentClass.getDeclaredMethod(methodName);
            }catch (NotFoundException e) {
                // get the type of the first parameter of the function for javassist
                CtClass environmentType = ClassPool.getDefault().get(IEnvironment.class.getName());
                CtMethod lookUp;
                if (returnTypeStack.lastElement() == STACK_TYPES.BOOLEAN){
                    lookUp = new CtMethod(
                            ClassPool.getDefault().get(Boolean.class.getName()),
                            methodName,
                            new CtClass[]{environmentType},
                            currentClass
                    );
                    //"$1" is the first parameter of the function
                    lookUp.setBody(String.format("return (Boolean)$1.getValue(\"%1$s\");",identifier.getIdent()));
                }else {
                    lookUp = new CtMethod(
                            ClassPool.getDefault().get(Double.class.getName()),
                            methodName,
                            new CtClass[]{environmentType},
                            currentClass
                    );
                    //"$1" is the first parameter of the function
                    lookUp.setBody(String.format("return (Double)$1.getValue(\"%1$s\");",identifier.getIdent()));
                }
                currentClass.addMethod(lookUp);
            }
        }
        return methodName;
    }

    /**
     * Generates a function which looks up the function of the FunctionCall in the environment
     * @param functionCall for the Function
     * @param environment environment in which the Function is located
     * @return the name of the generated function in the form: "functionCall_functionName_randomString"
     * @throws NotFoundException
     * @throws CannotCompileException
     * @throws IllegalStateException
     * @throws IOException
     */
    private String generateFunctionEnvironmentLookup(FunctionCallNode functionCall, IEnvironment environment) throws NotFoundException, CannotCompileException, IllegalStateException, IOException {
        if (environment == null){
            throw new IllegalStateException("Empty Environment");
        }
        String methodName;
        //Assuming the type is of numeric type
        methodName = "functionCall_"+getFunctionName(functionCall)+"_"+randomString();
        // get the type of the first parameter of the function for javassist
        CtClass environmentType = ClassPool.getDefault().get(IEnvironment.class.getName());
        CtClass returnType;
        if (returnTypeStack.lastElement() == STACK_TYPES.BOOLEAN){
            returnType = ClassPool.getDefault().get(Boolean.class.getName());
        }else{
            returnType = ClassPool.getDefault().get(Double.class.getName());
        }
        CtMethod lookUp = new CtMethod(
                returnType,
                methodName,
                new CtClass[]{environmentType},
                currentClass
        );
        StringBuilder functionBody = new StringBuilder("");
        final List<Node> arguments = getArguments(functionCall);
        boolean onlyConstantArguments = checkIfAllArgumentsAreConstants(arguments);
        if (onlyConstantArguments){
            functionBody.append(
                    String.format("{return new %1$s(%2$s);}",
                            returnType.getName(),
                            ((ValueNode)functionCall.calc(environment)).getValue().toString()
                    ));
        }else {
            buildDynamicFunctionBody(functionCall, environment, functionBody, returnType, arguments);
        }
        lookUp.setBody(functionBody.toString());
        currentClass.addMethod(lookUp);
        return methodName;
    }

    private void generateFinalMemberVariable(CtClass futureMemberType, String futureMemberName) throws CannotCompileException {
        CtField member = new CtField(futureMemberType, futureMemberName, currentClass);
        currentClass.addField(member);
    }

    /**
     * Generates a function which represent an {@link IfThenElseNode}. The Nodes return type is evaluated (only the then branch,
     * the else branch is assumed to have the same type) and hardcoded into the function definition. The result can be
     * used in a {@link ValueNode}.
     * @param node
     * @param environment
     * @return
     * @throws NotFoundException
     * @throws CannotCompileException
     * @throws IOException
     */
    private String generateIfThenElseFunction(IfThenElseNode node, IEnvironment environment) throws NotFoundException, CannotCompileException, IOException {
        String methodName = "ifThenElse_"+randomString();
       // get the type of the first parameter of the function for javassist
        CtClass environmentType = ClassPool.getDefault().get(IEnvironment.class.getName());
        CtClass returnType;
        CtMethod method;
        StringBuilder body = new StringBuilder("");
        final String returnVarName = "returnValue";
        boolean returnsBooleanType = false; //what type will the generated function return? boolean or double?
        //true if the statement is nonMath binary node, or NotNode or a ValueNode which stores a boolean
        if(node.getThenStmt() instanceof BinaryNode){
            if (node.getThenStmt() instanceof IMathNode){
                returnsBooleanType = false;
            }else{
                returnsBooleanType = true;
            }
        }else {
            if (node.getThenStmt() instanceof NotNode) {
                returnsBooleanType = true;
            }else {
                if (node.getThenStmt() instanceof ValueNode) {
                    if (((ValueNode)node.getThenStmt()).getValue() instanceof Boolean){
                        returnsBooleanType = true;
                    }
                }else{
                    returnsBooleanType = false;
                }
            }
        }
        //adjust function to the return type
        if (returnsBooleanType){
            returnType = ClassPool.getDefault().get(Boolean.class.getName());
            method = new CtMethod(
                    returnType,
                    methodName,
                    new CtClass[]{environmentType},
                    currentClass
            );
            returnTypeStack.push(STACK_TYPES.BOOLEAN);
        }else {
            returnType = ClassPool.getDefault().get(Double.class.getName());
            method = new CtMethod(
                    returnType,
                    methodName,
                    new CtClass[]{environmentType},
                    currentClass
            );
            returnTypeStack.push(STACK_TYPES.DOUBLE);
        }
        //evaluate the condition node and get a string representation
        returnTypeStack.push(STACK_TYPES.BOOLEAN);
        String condition = nodeToString(node.getCondition(), environment);
        returnTypeStack.pop();
        body.append(
                String.format(
                        "%3$s %2$s;\n" +
                        "if(%1$s)\n" +
                        "{%2$s = new %3$s(%4$s);}\n" +
                        "else\n" +
                        "{%2$s = new %3$s(%5$s);}\n" +
                        "return %2$s;",
                condition,
                returnVarName,
                returnType.getName(),
                nodeToString(node.getThenStmt(), environment),
                nodeToString(node.getElseStmt(), environment)
        ));
        returnTypeStack.pop();
        body.insert(0,"{").append("}");
        method.setBody(body.toString());
        currentClass.addMethod(method);
        return methodName;
    }

    /**
     *
     * @param arguments the list of the arguments
     * @return true if all arguments are {@link ValueNode}s, false otherwise
     */
    private boolean checkIfAllArgumentsAreConstants(List<Node> arguments) {
        boolean onlyConstantArguments = true;
        for (Node argument : arguments) {
            if (!(argument instanceof ValueNode)){
                onlyConstantArguments = false;
                break;
            }
        }
        return onlyConstantArguments;
    }

    /**
     * Builds the the code for a generic function call with variable parameters.
     * <p></p>
     * The resulting code should look something like this:
     *
     * {
     *    Function functionToCall = (Function)environment.getValue(getFunctionName(functionCall));
     *    JavassistNodeFactory nodeFactory = new JavassistNodeFactory();
     *    List<Node> arguments = new ArrayList();
     *    arguments.add(new ValueNode(new Double(a0)));arguments.add(new ValueNode(new Double(a1)));...
     *    FunctionCallNode functionCall = new FunctionCallNode(functionToCall, arguments);
     *    return ((ValueNode)functionCall.calc(environment).getValue();
     * }
     * @param functionCall
     * @param environment
     * @param functionBody
     * @param arguments
     * @throws NotFoundException
     * @throws CannotCompileException
     * @throws IOException
     */
    private void buildDynamicFunctionBody(FunctionCallNode functionCall, IEnvironment environment, StringBuilder functionBody, CtClass returnType, List<Node> arguments) throws NotFoundException, CannotCompileException, IOException {
        final String functionType = ClassPool.getDefault().get(Function.class.getName()).getName();
        final String listType = ClassPool.getDefault().get(List.class.getName()).getName();
        final String arrayListType = ClassPool.getDefault().get(ArrayList.class.getName()).getName();
        final String functionCallType = ClassPool.getDefault().get(FunctionCallNode.class.getName()).getName();
        final String valueNodeType = ClassPool.getDefault().get(ValueNode.class.getName()).getName();

        final String functionName = "functionToCall";
        final String argumentsName = "arguments";
        final String functionCallName = "functionCall";

        functionBody
                .append("{")
                //Function functionToCall = (Function)environment.getValue(getFunctionName(functionCall));
                .append(String.format("%1$s %2$s = (%1$s)$1.getValue(\"%3$s\");",
                        functionType,
                        functionName,
                        getFunctionName(functionCall)
                ))
                //List<Node> arguments = new ArrayList();
                .append(String.format("%1$s %2$s = new %3$s();",
                        listType,
                        argumentsName,
                        arrayListType
                ));
        //arguments.add(new ValueNode(new ValueNode(a0)));arguments.add(new ValueNode(a1));...
        if (!arguments.isEmpty()) {
            for (Node argument : arguments) {
                functionBody.append(String.format("%1$s.add(new %2$s(%3$s));",
                        argumentsName,
                        valueNodeType,
                        nodeToString(argument, environment)
                ));
            }
        }
        //FunctionCallNode functionCall = new FunctionCallNode(functionToCall, arguments);
        functionBody.append(String.format("%1$s %2$s = new %1$s(%3$s, %4$s);",
                functionCallType,
                functionCallName,
                functionName,
                argumentsName
        ));
        //return (Double)((ValueNode)functionCall.calc(environment).getValue();
        functionBody.append(String.format("return (%1$s)((%2$s)%3$s.calc($1)).getValue();",
                returnType.getName(),
                valueNodeType,
                functionCallName
        ));
        functionBody.append("}");
    }

    /**
     *
     * @param functionCall the Object of which the function name is of interest
     * @return the name of the function
     */
    static private String getFunctionName(FunctionCallNode functionCall){
        Node functionNode = functionCall.getChildren().get(0); //first child is function node by definition
        assert (functionNode instanceof ValueNode);
        assert (((ValueNode)functionNode).getValue() instanceof Function);
        return ((Function)((ValueNode)functionNode).getValue()).getName();//Cast it to the limit!!!
    }

    static private Function getFunction(FunctionCallNode functionCall){
        Node functionNode = functionCall.getChildren().get(0); //first child is function node by definition
        assert (functionNode instanceof ValueNode);
        assert (((ValueNode)functionNode).getValue() instanceof Function);
        return (Function)((ValueNode)functionNode).getValue();//Cast it to the limit!!!
    }

    /**
     *
     * @param functionCallNode the Object of which the arguments are of interest
     * @return all arguments of the function call
     */
    static private List<Node> getArguments(FunctionCallNode functionCallNode){
        List<Node> arguments = new ArrayList<>();
        for (int i = 1; i<functionCallNode.getChildren().size(); i++){//start by second child because the first is the Function
            arguments.add(functionCallNode.getChildren().get(i));
        }
        return arguments;
    }

    /**
     *
     * @return A UUID without '-' characters
     */
    private String randomString(){
        return UUID.randomUUID().toString().replace("-","");
    }
}
