package org.jamesii.mlrules.util.runtimeCompiling;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

/**
 * Created by tm219 on 4/5/17.
 */
public interface IDynamicNodeFactory {
    /**
     * Generates a {@link INode} equivalent to the given one. The nodes behaviour should be indistinguishable,
     * the only difference is that its {@link INode#calc} method is overridden with a more efficient implementation
     * generated individually at runtime.
     * WARNING: Only the INode interface is implemented. All information from custom nodes are lost!
     * @param root The Node which' calculation should be generated anew
     * @return A Node implementing ONLY the {@link INode} interface, with a custom compiled version of
     * the {@link INode#calc} function
     */
    public DynamicNode generateDynamicNode(INode root, IEnvironment environment);
}
