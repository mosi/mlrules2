package org.jamesii.mlrules.util.runtimeCompiling;

import javassist.CtClass;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.math.AddNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;

import java.util.LinkedList;
import java.util.List;

/**
 * Abstract class which is used as base class for runtime compiled classes.
 *
 * WARNING: Changing the signature of this class might (and probably will) interfere with the code generation!
 *
 * Created by tm219 on 4/5/17.
 */
public abstract class DynamicNode extends Node implements INode   {
    /**
     * Used in code generation to determine if all member variables where initialized.
     */
    protected boolean initialized = false;
    @Override
    public <N extends INode> N calc(IEnvironment<?> environment) {
        /*Double value = calculateValue(environment);
        return (N) new ValueNode<Double>(value);
        */
        return (N) calculateValue(environment);
    }

    @Override
    public List<? extends INode> getChildren() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    public abstract ValueNode calculateValue(IEnvironment environment);

    /**
     * Get a new instance of this object. All previously initialized variables will be uninitialized again in the returned
     * object. This makes it possible to rebind previously initialized species objects.
     * @return new uninitialized object of this class
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public DynamicNode cloneSelf() throws IllegalAccessException, InstantiationException {
        return this.getClass().newInstance();
    }
}

