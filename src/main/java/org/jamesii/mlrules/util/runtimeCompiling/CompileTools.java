package org.jamesii.mlrules.util.runtimeCompiling;

import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.parser.functions.Function;
import org.jamesii.mlrules.parser.functions.FunctionDefinition;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by tm219 on 5/17/17.
 */
public class CompileTools {
    public static void compileModel(Model model) throws IOException {
        Logger.getGlobal().info("---Compiling Model---");
        for (Object node : model.getEnv().getGlobal().values()) {
            if(node instanceof Function){
                Function compiledFunction = compileFunction((Function)node, model.getEnv());
                model.getEnv().getGlobal().put(((Function)node).getName(), compiledFunction);
            }
        }
        Logger.getGlobal().info("---Finished function compiling, starting with rules---");
        List<Rule>rules = model.getRules().getRules();
        for (int i = 0; i < rules.size(); i++) {
            rules.set(i, compileRule(rules.get(i),model.getEnv()));
        }
        Logger.getGlobal().info("---Done compiling Model---");
    }

    /**
     *
     * @param mlRulesFunction the source Function
     * @param environment the environment in which the source {@link Function} was located
     * @return a {@link Function} object which has a hard coded and compiled version of the source function
     */
    public static Function compileFunction(Function mlRulesFunction, IEnvironment environment) throws IOException {
        JavassistNodeFactory nodeFactory = new JavassistNodeFactory();
        Function generatedFunction = new Function(mlRulesFunction.getName(), mlRulesFunction.getType());
        List<FunctionDefinition> oldFunctionDefinitions = mlRulesFunction.getDefinitions();
        List<FunctionDefinition> newFunctionDefinitions = new ArrayList<>();
        for ( FunctionDefinition currentDefinition: oldFunctionDefinitions){
            if(!(currentDefinition.getFunction() instanceof DynamicNode)){
                DynamicNode newDefinitionNode = nodeFactory.generateDynamicNode(currentDefinition.getFunction(), environment);
                if (newDefinitionNode != null){
                    //TODO: Make the assignments hardcodable
                    newFunctionDefinitions.add(
                            new FunctionDefinition(currentDefinition.getParameter(),
                                    newDefinitionNode,
                                    currentDefinition.getAssignments())
                    );
                }else{
                    Logger.getGlobal().info("Unable to compile function definition");
                    newFunctionDefinitions.add(currentDefinition);
                }
            }else {
                Logger.getGlobal().info("Function has an already compiled node");
                newFunctionDefinitions.add(currentDefinition);
            }
        }
        generatedFunction.init(newFunctionDefinitions);
        return generatedFunction;
    }

    public static Rule compileRule(Rule mlRulululule, IEnvironment environment) throws IOException {
        JavassistNodeFactory nodeFactory = new JavassistNodeFactory();
        DynamicNode newRate = nodeFactory.generateDynamicNode(mlRulululule.getRate(), environment);
        //TODO: Make the assignments hardcodable
        if (newRate != null) {
            return new Rule(
                    mlRulululule.getReactants(),
                    mlRulululule.getProduct(),
                    newRate,
                    mlRulululule.getRate(),
                    mlRulululule.getAssignments()
            );
        }else{
            Logger.getGlobal().info("Unable to compile rule rate");
            return mlRulululule;
        }
    }

    public static void deleteGeneratedClasses() throws IOException {
        deleteGeneratedClasses("");
    }

    public static void deleteGeneratedClasses(String path) throws IOException {
        DirectoryStream<Path> targetDir = Files.newDirectoryStream(new File(path).toPath().toAbsolutePath());
        for (Path file : targetDir){
            Path current = file.getFileName();
            if( file.getFileName().toString().startsWith("DN")){
                Files.delete(file);
            }
        }
    }

    public static void deleteGeneratedClasses(Path path) throws IOException {
        deleteGeneratedClasses(path.toAbsolutePath());
    }
}
