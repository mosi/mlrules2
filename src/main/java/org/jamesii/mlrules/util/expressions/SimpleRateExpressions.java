/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.util.expressions;

/**
 * {@link SimpleRateExpressions} are an optimization for simple calculations
 * that shall be computed directly instead of using the node structure created
 * by the parser. For example, a rate expression <b>#a*k1</b> is complex to be
 * calculated with nodes, since one variable is used that has to be retrieved
 * from the environment and one function is used that must be called and
 * executed (including creating parameters etc.). Instead, a
 * {@link SimpleRateExpression} just saves the constant and the link to the
 * species directly used these to calculate the result directly.
 * 
 * @author Tobias Helms
 *
 */
public interface SimpleRateExpressions {

  public double calc();

}
