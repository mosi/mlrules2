/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.util.expressions;

import org.jamesii.mlrules.model.species.Species;

/**
 * 
 * Shortcut for simple rate calculations in the form of <b>#a*#b*k1</b>.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleRateExpressionTwoSpecies implements SimpleRateExpressions {

	private final Species species1;

	private final Species species2;

	private final double constant;

	public SimpleRateExpressionTwoSpecies(Species species1, Species species2, double constant) {
		this.species1 = species1;
		this.species2 = species2;
		this.constant = constant;
	}

	@Override
	public double calc() {
		return species1.getAmount() * species2.getAmount() * constant;
	}

}
