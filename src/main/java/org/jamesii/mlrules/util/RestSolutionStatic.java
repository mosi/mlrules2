package org.jamesii.mlrules.util;

import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;

import java.util.Map;

/**
 * This RestSolution does not remove LeafSpecies, if their amount is equal to zero
 */
public class RestSolutionStatic extends RestSolution {

  public RestSolutionStatic(String name, Compartment context, Map<Species, Integer> removals) {
    super(name, context, removals);
  }

  protected void fillRest(Species s, Map<Species, Species> rest) {
    Integer remove = removals.getOrDefault(s, 0);
    if (remove == 0) {
      rest.put(s, s);
    } else {
      if (s instanceof LeafSpecies) {
        Species copy = s.copy();
        LeafSpecies ls = (LeafSpecies) copy;
        ls.setAmount(ls.getAmount() - remove);
        if (Double.compare(ls.getAmount(), 0) >= 0) {
          rest.put(ls, ls);
        }
      }
    }
  }

}
