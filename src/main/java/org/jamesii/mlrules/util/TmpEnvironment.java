/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.util;

import org.jamesii.core.math.parsetree.variables.IEnvironment;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@link TmpEnvironment} is a simple version of the {@link MLEnvironment}
 * without a global fixed map of entries so that the retrieval of elements is
 * faster.
 * 
 * @author Tobias Helms
 *
 */
public class TmpEnvironment implements IEnvironment<String> {

  private static final long serialVersionUID = 1L;

  private final Map<String, Object> values = new HashMap<>(4);

  @Override
  public void setValue(String ident, Object value) {
    values.put(ident, value);
  }

  @Override
  public boolean containsIdent(String ident) {
    return values.containsKey(ident);
  }

  @Override
  public void removeValue(String ident) {
    if (values.remove(ident) == null) {
      throw new IllegalArgumentException(String.format(
          "could not remove %s from tmp env with values %s", ident, toString()));
    }
  }

  @Override
  public void removeAll() {
    values.clear();
  }

  @Override
  public Object getValue(String ident) {
    return values.get(ident);
  }

  /**
   * Copy the given environment, add all entries of this temporary environment
   * to the copy and return the copy.
   */
  public MLEnvironment create(MLEnvironment env) {
    MLEnvironment copy = env.copy();
    values.forEach((k, v) -> copy.setValue(k, v));
    return copy;
  }

}
