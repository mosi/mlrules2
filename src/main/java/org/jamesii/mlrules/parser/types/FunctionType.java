/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The function type represents arbitrary complex functions. The function
 * definition is specified by the sub types. Function types are not allowed as
 * sub types. The last sub type is always the return type, i.e., the sub types
 * are not allowed to be empty.
 * 
 * @author Tobias Helms
 *
 */
public class FunctionType implements Type {

  private final List<Type> subTypes;
  
  public static final FunctionType UNKNOWN = new FunctionType(Arrays.asList(BaseType.UNKNOWN));

  public FunctionType(List<Type> subTypes) {
    if (subTypes.isEmpty()) {
      throw new IllegalArgumentException(
          "Function types must have at least one sub type representing the return type.");
    }

    this.subTypes = new ArrayList<>(subTypes);
  }

  @Override
  public Primitives getType() {
    return Primitives.FUNCTION;
  }

  @Override
  public String toString() {
    return Primitives.FUNCTION.toString() + "(" + subTypes.toString() + ")";
  }
  
  public List<Type> getSubTypes() {
    return subTypes;
  }
  
  @Override
  public int hashCode() {
    int hash = subTypes.size();
    for (Type subType : subTypes) {
      hash = hash * 31 + subType.hashCode();
    }
    return hash;
  }
  
  @Override
  public boolean equals(Object o) {
    if (o instanceof FunctionType) {
      FunctionType f = (FunctionType) o;
      if (subTypes.size() != f.getSubTypes().size()) {
        return false;
      }
      for (int i = 0; i < subTypes.size(); ++i) {
        if (subTypes.get(i) != BaseType.UNKNOWN && f.getSubTypes().get(i) != BaseType.UNKNOWN && !subTypes.get(i).equals(f.getSubTypes().get(i))) {
          return false;
        }
      }
    }
    return true;
  }

}
