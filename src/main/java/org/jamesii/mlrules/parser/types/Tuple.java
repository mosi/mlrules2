/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.types;

import java.util.List;

/**
 * A tuple is a marker for lists.
 * 
 * @author Tobias Helms
 *
 */
public class Tuple {

  private final List<Object> values;

  public Tuple(List<Object> values) {
    this.values = values;
  }

  public int size() {
    return values.size();
  }

  public Object get(int i) {
    return values.get(i);
  }

}
