/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions;

import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.parser.functions.param.SimpleParameter;
import org.jamesii.mlrules.parser.nodes.*;
import org.jamesii.mlrules.parser.types.BaseType;
import org.jamesii.mlrules.parser.types.FunctionType;
import org.jamesii.mlrules.parser.visitor.typecheck.util.TypeScope;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Within this class useful functions which should be available in all ML-Rules
 * models are defined:
 * <ol>
 * amount(species) - return the amount of the given species
 * </ol>
 * <ol>
 * att(species) - return the attributes of the given species
 * </ol>
 * <ol>
 * sub(species) - return the sub species of the given species
 * </ol>
 * <ol>
 * name(species) - return the type name of the given species
 * </ol>
 * <ol>
 * norm(mean,var) - return a random number sampled from a normal distribution
 * with the given mean and variance
 * </ol>
 * <ol>
 * unif(min,max) - return a random number sampled from a uniformed distribution
 * with the given min and max
 * </ol>
 * <ol>
 * toInt(double) - return the int value of the given double value
 * </ol>
 * <ol>
 * nu() - return a new link value
 * </ol>
 * <ol>
 * simtime() - return the current simulation time
 * </ol>
 * <ol>
 * infinity() - return the double value infinity
 * </ol>
 * <ol>
 * count(sol,string) - return the amount of all species within the given
 * solution with the given name (no sub species considered)
 * </ol>
 * <ol>
 * dist(num,num,num,num) - return the distance of two coordinates (the first two
 * numbers are x1 and y1; the second two numbers are x2 and y2)
 * </ol>
 * <ol>
 * filter(sol,string) - return a solution no containing species with the given
 * name (no filtering of sub species)
 * </ol>
 * <ol>
 * sin(num) - compute the sine of the given angle
 * </ol>
 * <ol>
 * cos(num) - compute the cosine of the given angle
 * </ol>
 * <ol>
 * log(num) - compute the natural logarithm of the given value
 * </ol>
 * <ol>
 * round(num) - round the given number
 * </ol>
 * <ol>
 * new(num,string,tuple) - create a new species with the given amount, type and
 * attributes. No compartmental species are supported.
 *
 * @author Tobias Helms
 */
public class PredefinedFunctions {

  private PredefinedFunctions() {
  }

  /**
   * Add all predefined functions to the given environment.
   */
  public static final void addAll(MLEnvironment env) {
    att(env);
    amount(env);
    sub(env);
    name(env);
    norm(env);
    unif(env);
    toInt(env);
    nu(env);
    simtime(env);
    infinity(env);
    count(env);
    countTwoAtts(env);
    countAtt1(env);
    countAtt2(env);
    dist(env);
    filter(env);
    sin(env);
    cos(env);
    log(env);
    round(env);
    newSpecies(env);
    allCombinations(env);
    isEmpty(env);
  }

  public static final void addAll(TypeScope scope) {
    att(scope);
    amount(scope);
    sub(scope);
    name(scope);
    norm(scope);
    unif(scope);
    toInt(scope);
    nu(scope);
    simtime(scope);
    infinity(scope);
    count(scope);
    countTwoAtts(scope);
    countAtt1(scope);
    countAtt2(scope);
    dist(scope);
    filter(scope);
    sin(scope);
    cos(scope);
    log(scope);
    round(scope);
    newSpecies(scope);
    allCombinations(scope);
    isEmpty(scope);
  }

  private static void log(TypeScope scope) {
    scope.addFunction("log", Arrays.asList(BaseType.NUM, BaseType.NUM));
  }

  private static void newSpecies(TypeScope scope) {
    scope.addFunction("new", Arrays.asList(BaseType.NUM, BaseType.STRING, BaseType.TUPLE, BaseType.SPECIES));
  }

  private static void round(TypeScope scope) {
    scope.addFunction("round", Arrays.asList(BaseType.NUM, BaseType.NUM));
  }

  private static void cos(TypeScope scope) {
    scope.addFunction("cos", Arrays.asList(BaseType.NUM, BaseType.NUM));

  }

  private static void sin(TypeScope scope) {
    scope.addFunction("sin", Arrays.asList(BaseType.NUM, BaseType.NUM));
  }

  private static void filter(TypeScope scope) {
    scope.addFunction("filter", Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.SOL));

  }

  private static void dist(TypeScope scope) {
    scope.addFunction("dist", Arrays.asList(BaseType.NUM, BaseType.NUM, BaseType.NUM, BaseType.NUM, BaseType.NUM));

  }

  private static void count(TypeScope scope) {
    scope.addFunction("count", Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM));
  }

  private static void countTwoAtts(TypeScope scope) {
    scope.addFunction("countTwoAtts", Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM, BaseType.NUM, BaseType.NUM));
  }

  private static void countAtt1(TypeScope scope) {
    scope.addFunction("countAtt1", Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM, BaseType.WILDCARD, BaseType.NUM));
  }

  private static void countAtt2(TypeScope scope) {
    scope.addFunction("countAtt2", Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM, BaseType.WILDCARD, BaseType.NUM, BaseType.WILDCARD, BaseType.NUM));
  }

  private static void infinity(TypeScope scope) {
    scope.addFunction("infinity", Arrays.asList(BaseType.NUM));

  }

  private static void simtime(TypeScope scope) {
    scope.addFunction("simtime", Arrays.asList(BaseType.NUM));

  }

  private static void nu(TypeScope scope) {
    scope.addFunction("nu", Arrays.asList(BaseType.LINK));
  }

  private static void toInt(TypeScope scope) {
    scope.addFunction("toInt", Arrays.asList(BaseType.NUM, BaseType.NUM));
  }

  private static void unif(TypeScope scope) {
    scope.addFunction("unif", Arrays.asList(BaseType.NUM, BaseType.NUM, BaseType.NUM));
  }

  private static void norm(TypeScope scope) {
    scope.addFunction("norm", Arrays.asList(BaseType.NUM, BaseType.NUM, BaseType.NUM));

  }

  private static void name(TypeScope scope) {
    scope.addFunction("name", Arrays.asList(BaseType.SPECIES, BaseType.STRING));
  }

  private static void sub(TypeScope scope) {
    scope.addFunction("sub", Arrays.asList(BaseType.SPECIES, BaseType.SOL));
  }

  private static void att(TypeScope scope) {
    scope.addFunction("att", Arrays.asList(BaseType.SPECIES, BaseType.TUPLE));

  }

  private static void amount(TypeScope scope) {
    scope.addFunction("amount", Arrays.asList(BaseType.SPECIES, BaseType.NUM));

  }

  private static void allCombinations(TypeScope scope) {
    scope.addFunction("allCombinations", Arrays.asList(BaseType.STRING, BaseType.SOL));
  }

  private static void isEmpty(TypeScope scope) {
    scope.addFunction("isEmpty", Arrays.asList(BaseType.SOL, BaseType.BOOL));
  }

  public static final void amount(MLEnvironment env) {
    String name = "amount";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SPECIES, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(SpeciesAmountNode.NAME)),
            new SpeciesAmountNode(null), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void name(MLEnvironment env) {
    String name = "name";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SPECIES, BaseType.STRING)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(SpeciesNameNode.NAME)),
            new SpeciesNameNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void att(MLEnvironment env) {
    String name = "att";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SPECIES, BaseType.TUPLE)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(SpeciesAttributeNode.NAME)),
            new SpeciesAttributeNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void sub(MLEnvironment env) {
    String name = "sub";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SPECIES, BaseType.SOL)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(SpeciesSubNode.NAME)),
            new SpeciesSubNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void norm(MLEnvironment env) {
    String name = "norm";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Arrays.asList(new SimpleParameter(NormNode.MEAN), new SimpleParameter(NormNode.VAR)),
                    new NormNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void unif(MLEnvironment env) {
    String name = "unif";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Arrays.asList(new SimpleParameter(UnifNode.MIN), new SimpleParameter(UnifNode.MAX)),
                    new UnifNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void toInt(MLEnvironment env) {
    String name = "toInt";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(ToIntNode.VALUE)), new ToIntNode(),
            new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void nu(MLEnvironment env) {
    String name = "nu";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.LINK)));
    f.init(Arrays.asList(new FunctionDefinition(new ArrayList<>(), new NuNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void simtime(MLEnvironment env) {
    String name = "simtime";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Collections.emptyList(), new Identifier<String>(Model.TIME), Collections.emptyList())));
    env.setGlobalValue(name, f);
    env.setGlobalValue(Model.TIME, 0.0);
  }

  public static final void infinity(MLEnvironment env) {
    String name = "infinity";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Collections.emptyList(),
            new ValueNode<Double>(Double.POSITIVE_INFINITY), Collections.emptyList())));
    env.setGlobalValue(name, f);
  }

  public static final void count(MLEnvironment env) {
    String name = "count";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(
            Arrays.asList(new SimpleParameter(SolutionCountNode.SOL), new SimpleParameter(SolutionCountNode.NAME)),
            new SolutionCountNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void countTwoAtts(MLEnvironment env) {
    String name = "countTwoAtts";
    Function f = new Function(name, new FunctionType(
            Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM,
                    BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
            new SimpleParameter(SolutionCountTwoAttributesNode.SOL),
            new SimpleParameter(SolutionCountTwoAttributesNode.NAME),
            new SimpleParameter(SolutionCountTwoAttributesNode.ATT_1),
            new SimpleParameter(SolutionCountTwoAttributesNode.ATT_2)),
            new SolutionCountTwoAttributesNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void countAtt1(MLEnvironment env) {
    String name = "countAtt1";
    Function f = new Function(name, new FunctionType(
            Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM,
                    BaseType.WILDCARD, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
            new SimpleParameter(SolutionCountOneAttributeNode.SOL),
            new SimpleParameter(SolutionCountOneAttributeNode.NAME),
            new SimpleParameter(SolutionCountOneAttributeNode.POS),
            new SimpleParameter(SolutionCountOneAttributeNode.ATT)),
            new SolutionCountOneAttributeNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void countAtt2(MLEnvironment env) {
    String name = "countAtt2";
    Function f = new Function(name, new FunctionType(
            Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.NUM,
                    BaseType.WILDCARD, BaseType.NUM,
                    BaseType.WILDCARD, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(
            new SimpleParameter(SolutionCountTwoAttributeNode.SOL),
            new SimpleParameter(SolutionCountTwoAttributeNode.NAME),
            new SimpleParameter(SolutionCountTwoAttributeNode.POS1),
            new SimpleParameter(SolutionCountTwoAttributeNode.ATT1),
            new SimpleParameter(SolutionCountTwoAttributeNode.POS2),
            new SimpleParameter(SolutionCountTwoAttributeNode.ATT2)),
            new SolutionCountTwoAttributeNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void dist(MLEnvironment env) {
    String name = "dist";
    Function f = new Function(name,
            new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM, BaseType.NUM, BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(new FunctionDefinition(
            Arrays.asList(new SimpleParameter(DistanceNode.X1), new SimpleParameter(DistanceNode.Y1),
                    new SimpleParameter(DistanceNode.X2), new SimpleParameter(DistanceNode.Y2)),
            new DistanceNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void filter(MLEnvironment env) {
    String name = "filter";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SOL, BaseType.STRING, BaseType.SOL)));
    f.init(Arrays.asList(new FunctionDefinition(
            Arrays.asList(new SimpleParameter(SolutionFilterNode.SOL), new SimpleParameter(SolutionFilterNode.NAME)),
            new SolutionFilterNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void sin(MLEnvironment env) {
    String name = "sin";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Arrays.asList(new SimpleParameter(SinNode.VAL)), new SinNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void cos(MLEnvironment env) {
    String name = "cos";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Arrays.asList(new SimpleParameter(CosNode.VAL)), new CosNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void log(MLEnvironment env) {
    String name = "log";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Arrays.asList(new SimpleParameter(LogNode.VAL)), new LogNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void round(MLEnvironment env) {
    String name = "round";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.NUM, BaseType.NUM)));
    f.init(Arrays.asList(
            new FunctionDefinition(Arrays.asList(new SimpleParameter(RoundNode.VAL)), new RoundNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void newSpecies(MLEnvironment env) {
    String name = "new";
    Function f = new Function(name,
            new FunctionType(Arrays.asList(BaseType.NUM, BaseType.STRING, BaseType.TUPLE, BaseType.SPECIES)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(NewSpeciesNode.AMOUNT),
            new SimpleParameter(NewSpeciesNode.NAME), new SimpleParameter(NewSpeciesNode.ATTS)), new NewSpeciesNode(),
            new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void allCombinations(MLEnvironment env) {
    String name = "allCombinations";
    Function f = new Function(name,
            new FunctionType(Arrays.asList(BaseType.STRING, BaseType.SOL)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(AllBooleanCombinations.NAME)), new AllBooleanCombinations(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

  public static final void isEmpty(MLEnvironment env) {
    String name = "isEmpty";
    Function f = new Function(name, new FunctionType(Arrays.asList(BaseType.SOL, BaseType.BOOL)));
    f.init(Arrays.asList(new FunctionDefinition(Arrays.asList(new SimpleParameter(EmptyNode.SOL)), new EmptyNode(), new ArrayList<>())));
    env.setGlobalValue(name, f);
  }

}
