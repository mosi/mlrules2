/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.mlrules.parser.functions.param.Parameter;
import org.jamesii.mlrules.util.Assignment;

import java.util.List;

/**
 * A {@link FunctionDefinition} defines one case of a function. It is defined by
 * a list of parameters, a function node and a list of assignments (i.e.,
 * 'where' variables).
 * 
 * @author Tobias Helms
 *
 */
public class FunctionDefinition {

  private final List<Parameter> parameter;

  private final Node function;

  private final List<Assignment> assignments;

  public FunctionDefinition(List<Parameter> parameter, Node function, List<Assignment> assignments) {
    this.parameter = parameter;
    this.function = function;
    this.assignments = assignments;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(parameter.toString());
    builder.append(" = ");
    builder.append(function.toString());
    if (!assignments.isEmpty()) {
      builder.append(" where ");
      builder.append(assignments.toString());
    }
    return builder.toString();
  }

  public List<Parameter> getParameter() {
    return parameter;
  }

  public Node getFunction() {
    return function;
  }

  public List<Assignment> getAssignments() {
    return assignments;
  }

}
