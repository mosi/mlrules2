/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions;

import org.jamesii.mlrules.parser.types.FunctionType;
import org.jamesii.mlrules.parser.types.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * {@link Function}s are used to calculate results based on parameters, whereby
 * species and solutions are allowed to be parameters. {@link Function}s cannot
 * change parameter values, but they are more used in the sense of functional
 * programming and immutable objects. A {@link Function} is defined by a name, a
 * type definition, a list of cases ({@link FunctionDefinition}s.
 * 
 * @author Tobias Helms
 *
 */
public class Function {

  private final String name;

  private final FunctionType type;

  private final List<FunctionDefinition> definitions = new ArrayList<>();

  public Function(String name, Type type) {
    this.name = name;
    this.type = (FunctionType) type;
  }

  public String getName() {
    return name;
  }

  public List<FunctionDefinition> getDefinitions() {
    return definitions;
  }

  public FunctionType getType() {
    return type;
  }

  public void init(List<FunctionDefinition> definitions) {
    if (this.definitions.isEmpty()) {
      this.definitions.addAll(definitions);
    } else {
      throw new IllegalArgumentException(String.format("Function %s cannot be initialized twice.", name));
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(name);
    StringJoiner joiner = new StringJoiner(",", "[", "]");
    definitions.forEach(d -> joiner.add(d.toString()));
    builder.append(joiner.toString());
    return builder.toString();
  }
}
