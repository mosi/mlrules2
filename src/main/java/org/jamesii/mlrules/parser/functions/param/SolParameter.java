/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions.param;

import org.jamesii.mlrules.model.species.Species;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@link SolParameter} is used to access an individual species of a
 * solution. For example, in <b>f x + xs = x</b>, the parameter must be a
 * solution with at least one element. This notation is adapted from Haskells
 * x:xs notation to access the head of a list. One arbitrary element of this
 * solution is mapped to <b>x</b>, the rest is mapped to <b>xs</b>. For example,
 * in <b>f(3 A + 2 B + 6 C)</b>, one arbitrary species is mapped to <b>x</b>,
 * e.g., <b>x = 2 B</b>. Note that species that are treated population-based
 * (i.e., none compartments), are not treated individually here!
 * 
 * @author Tobias Helms
 *
 */
public class SolParameter implements Parameter {

  private final String head;

  private final String tail;

  public SolParameter(String head, String tail) {
    this.head = head;
    this.tail = tail;
  }

  @Override
  public String toString() {
    return head + ":" + tail;
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    if (value instanceof Map<?, ?>) {
      Map<?, ?> list = (Map<?, ?>) value;
      if (list.isEmpty()) {
        return false;
      }
      Object o = list.entrySet().iterator().next().getKey();
      if (o instanceof Species) {
        Map<Species, Species> headMap = new HashMap<>();
        headMap.put((Species) o, (Species) o);
        vars.put(head, headMap);
        Map<Species, Species> tailMap = new HashMap<>((Map<Species, Species>) list);
        tailMap.remove(o);
        vars.put(tail, tailMap);
        return true;
      }
    }
    return false;
  }

}
