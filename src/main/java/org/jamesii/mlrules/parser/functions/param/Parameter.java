/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions.param;

import java.util.Map;

/**
 * Parameters are used on the left hand side of function definitions to describe
 * pattern matchings. For example, the parameters in <b>f x [] = x</b> represent
 * a variable <b>x</b> and an empty solution <b>[]</b>. Thus, the call
 * <b>f(3,[])</b> would match the parameters and <b>x</b> would be bound to
 * <b>3</b>, whereas the call <b>f(3,[A+B])</b> would not match the parameters.
 * 
 * @author Tobias Helms
 *
 */
public interface Parameter {

  /**
   * Return true if the given object matches this parameter and put all matched
   * values to the given variables map.
   */
  boolean match(Object value, Map<String, Object> vars);

}
