/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions.param;

import java.util.Map;

/**
 * A {@link SimpleParameter} is just a variable name that is saves the given
 * parameter value. For example, in <b>f x = x</b>, <b>x</b> is a simple
 * parameter and within the call <b>f(3)</b>, <b>x</b> would be mapped to 3.
 * 
 * @author Tobias Helms
 *
 */
public class SimpleParameter implements Parameter {

  private final String name;

  public SimpleParameter(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    vars.put(name, value);
    return true;
  }

}
