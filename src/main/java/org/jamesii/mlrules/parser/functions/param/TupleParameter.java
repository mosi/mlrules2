/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.functions.param;

import org.jamesii.mlrules.parser.types.Tuple;

import java.util.List;
import java.util.Map;

/**
 * The {@link Tuple Parameter is used to directly access tuple values. For
 * example, <b>f <a,b,c></b> can be defined to directly map the three values of
 * the tuple. To match this parameter, a given tuple must exactly have the given
 * number of values, i.e., referring to the example, to match <b>f <a,b,c></b>,
 * a given tuple must have exactly three values.
 * 
 * @author Tobias Helms
 *
 */
public class TupleParameter implements Parameter {

  private final List<Parameter> subParameter;

  public TupleParameter(List<Parameter> subParameter) {
    this.subParameter = subParameter;
  }

  @Override
  public String toString() {
    return subParameter.toString();
  }

  @Override
  public boolean match(Object value, Map<String, Object> vars) {
    if (value instanceof Tuple) {
      Tuple tuple = (Tuple) value;
      if (tuple.size() != subParameter.size()) {
        return false;
      }
      for (int i = 0; i < tuple.size(); ++i) {
        if (!subParameter.get(i).match(tuple.get(i), vars)) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

}
