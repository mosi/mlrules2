// Generated from MLRules.g4 by ANTLR 4.7

package org.jamesii.mlrules.parser.grammar;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MLRulesParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT_SINGLE=1, COMMENT_MULTI=2, WS=3, AT=4, ATTIME=5, ATEACH=6, INIT=7, 
		IF=8, THEN=9, ELSE=10, FOR=11, WHILE=12, UNTIL=13, LENGTH=14, ADD=15, 
		WHERE=16, FREE=17, ASSIGN=18, DOT=19, COMMA=20, COLON=21, SEMI=22, L_PAREN=23, 
		R_PAREN=24, L_BRAKET=25, R_BRAKET=26, L_CURLY=27, R_CURLY=28, PLUS=29, 
		MINUS=30, MULT=31, DIV=32, ROOF=33, EQUALS=34, N_EQUALS=35, LT=36, LT_EQ=37, 
		GT=38, GT_EQ=39, TRUE=40, FALSE=41, AND=42, OR=43, NOT=44, LAMBDA=45, 
		TYPE_SEPERATOR=46, ARROW=47, TYPE_REAL=48, TYPE_BOOL=49, TYPE_STRING=50, 
		TYPE_SPECIES=51, TYPE_PATTERN=52, TYPE_TUPLE=53, TYPE_SOL=54, TYPE_LINK=55, 
		INT=56, REAL=57, BOOL=58, ID_SPECIES=59, ID=60, STRING=61, NU=62, COUNT=63, 
		QUESTION=64;
	public static final int
		RULE_model = 0, RULE_preamble = 1, RULE_constant = 2, RULE_speciesTypeParameters = 3, 
		RULE_speciesDefinition = 4, RULE_initialSolution = 5, RULE_mlrule = 6, 
		RULE_reactants = 7, RULE_products = 8, RULE_rate = 9, RULE_expression = 10, 
		RULE_species = 11, RULE_name = 12, RULE_attributes = 13, RULE_subSpecies = 14, 
		RULE_guard = 15, RULE_amount = 16, RULE_value = 17, RULE_where = 18, RULE_assign = 19, 
		RULE_assignName = 20, RULE_function = 21, RULE_typeDefinition = 22, RULE_type = 23, 
		RULE_functionDefinition = 24, RULE_parameter = 25, RULE_atomicParameter = 26;
	public static final String[] ruleNames = {
		"model", "preamble", "constant", "speciesTypeParameters", "speciesDefinition", 
		"initialSolution", "mlrule", "reactants", "products", "rate", "expression", 
		"species", "name", "attributes", "subSpecies", "guard", "amount", "value", 
		"where", "assign", "assignName", "function", "typeDefinition", "type", 
		"functionDefinition", "parameter", "atomicParameter"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, "'@'", "'@EXACT'", "'@EACH'", "'>>INIT'", "'if'", 
		"'then'", "'else'", "'for'", "'while'", "'until'", "'length'", "'add'", 
		"'where'", "'free'", "'='", "'.'", "','", "':'", "';'", "'('", "')'", 
		"'['", "']'", "'{'", "'}'", "'+'", "'-'", "'*'", "'/'", "'^'", "'=='", 
		"'!='", "'<'", "'<='", "'>'", "'>='", "'true'", "'false'", "'&&'", "'||'", 
		"'!'", "'\\'", "'::'", "'->'", "'num'", "'bool'", "'string'", "'species'", 
		"'pattern'", "'tuple'", "'sol'", "'link'", null, null, null, null, null, 
		null, null, "'#'", "'?'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMMENT_SINGLE", "COMMENT_MULTI", "WS", "AT", "ATTIME", "ATEACH", 
		"INIT", "IF", "THEN", "ELSE", "FOR", "WHILE", "UNTIL", "LENGTH", "ADD", 
		"WHERE", "FREE", "ASSIGN", "DOT", "COMMA", "COLON", "SEMI", "L_PAREN", 
		"R_PAREN", "L_BRAKET", "R_BRAKET", "L_CURLY", "R_CURLY", "PLUS", "MINUS", 
		"MULT", "DIV", "ROOF", "EQUALS", "N_EQUALS", "LT", "LT_EQ", "GT", "GT_EQ", 
		"TRUE", "FALSE", "AND", "OR", "NOT", "LAMBDA", "TYPE_SEPERATOR", "ARROW", 
		"TYPE_REAL", "TYPE_BOOL", "TYPE_STRING", "TYPE_SPECIES", "TYPE_PATTERN", 
		"TYPE_TUPLE", "TYPE_SOL", "TYPE_LINK", "INT", "REAL", "BOOL", "ID_SPECIES", 
		"ID", "STRING", "NU", "COUNT", "QUESTION"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "MLRules.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MLRulesParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ModelContext extends ParserRuleContext {
		public InitialSolutionContext initialSolution() {
			return getRuleContext(InitialSolutionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(MLRulesParser.EOF, 0); }
		public List<PreambleContext> preamble() {
			return getRuleContexts(PreambleContext.class);
		}
		public PreambleContext preamble(int i) {
			return getRuleContext(PreambleContext.class,i);
		}
		public List<MlruleContext> mlrule() {
			return getRuleContexts(MlruleContext.class);
		}
		public MlruleContext mlrule(int i) {
			return getRuleContext(MlruleContext.class,i);
		}
		public ModelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitModel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModelContext model() throws RecognitionException {
		ModelContext _localctx = new ModelContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_model);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID_SPECIES || _la==ID) {
				{
				{
				setState(54);
				preamble();
				}
				}
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(60);
			initialSolution();
			setState(62); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(61);
				mlrule();
				}
				}
				setState(64); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << L_PAREN) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID))) != 0) );
			setState(66);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreambleContext extends ParserRuleContext {
		public SpeciesDefinitionContext speciesDefinition() {
			return getRuleContext(SpeciesDefinitionContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public PreambleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preamble; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitPreamble(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PreambleContext preamble() throws RecognitionException {
		PreambleContext _localctx = new PreambleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_preamble);
		try {
			setState(75);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(68);
				speciesDefinition();
				setState(69);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(71);
				constant();
				setState(72);
				match(SEMI);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(74);
				function();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode COLON() { return getToken(MLRulesParser.COLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitConstant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_constant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			match(ID);
			setState(78);
			match(COLON);
			setState(79);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpeciesTypeParametersContext extends ParserRuleContext {
		public TerminalNode TYPE_LINK() { return getToken(MLRulesParser.TYPE_LINK, 0); }
		public TerminalNode TYPE_STRING() { return getToken(MLRulesParser.TYPE_STRING, 0); }
		public TerminalNode TYPE_BOOL() { return getToken(MLRulesParser.TYPE_BOOL, 0); }
		public TerminalNode TYPE_REAL() { return getToken(MLRulesParser.TYPE_REAL, 0); }
		public SpeciesTypeParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_speciesTypeParameters; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSpeciesTypeParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpeciesTypeParametersContext speciesTypeParameters() throws RecognitionException {
		SpeciesTypeParametersContext _localctx = new SpeciesTypeParametersContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_speciesTypeParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TYPE_REAL) | (1L << TYPE_BOOL) | (1L << TYPE_STRING) | (1L << TYPE_LINK))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpeciesDefinitionContext extends ParserRuleContext {
		public TerminalNode ID_SPECIES() { return getToken(MLRulesParser.ID_SPECIES, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public List<SpeciesTypeParametersContext> speciesTypeParameters() {
			return getRuleContexts(SpeciesTypeParametersContext.class);
		}
		public SpeciesTypeParametersContext speciesTypeParameters(int i) {
			return getRuleContext(SpeciesTypeParametersContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public SpeciesDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_speciesDefinition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSpeciesDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpeciesDefinitionContext speciesDefinition() throws RecognitionException {
		SpeciesDefinitionContext _localctx = new SpeciesDefinitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_speciesDefinition);
		int _la;
		try {
			setState(105);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(83);
				match(ID_SPECIES);
				setState(84);
				match(L_PAREN);
				setState(85);
				match(R_PAREN);
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==L_BRAKET) {
					{
					setState(86);
					match(L_BRAKET);
					setState(87);
					match(R_BRAKET);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(90);
				match(ID_SPECIES);
				setState(91);
				match(L_PAREN);
				setState(92);
				speciesTypeParameters();
				setState(97);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(93);
					match(COMMA);
					setState(94);
					speciesTypeParameters();
					}
					}
					setState(99);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(100);
				match(R_PAREN);
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==L_BRAKET) {
					{
					setState(101);
					match(L_BRAKET);
					setState(102);
					match(R_BRAKET);
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitialSolutionContext extends ParserRuleContext {
		public TerminalNode INIT() { return getToken(MLRulesParser.INIT, 0); }
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public InitialSolutionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initialSolution; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitInitialSolution(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitialSolutionContext initialSolution() throws RecognitionException {
		InitialSolutionContext _localctx = new InitialSolutionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_initialSolution);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(INIT);
			setState(108);
			match(L_BRAKET);
			setState(109);
			expression(0);
			setState(110);
			match(R_BRAKET);
			setState(111);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MlruleContext extends ParserRuleContext {
		public ReactantsContext reactants() {
			return getRuleContext(ReactantsContext.class,0);
		}
		public TerminalNode ARROW() { return getToken(MLRulesParser.ARROW, 0); }
		public ProductsContext products() {
			return getRuleContext(ProductsContext.class,0);
		}
		public RateContext rate() {
			return getRuleContext(RateContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public TerminalNode AT() { return getToken(MLRulesParser.AT, 0); }
		public TerminalNode ATTIME() { return getToken(MLRulesParser.ATTIME, 0); }
		public TerminalNode ATEACH() { return getToken(MLRulesParser.ATEACH, 0); }
		public WhereContext where() {
			return getRuleContext(WhereContext.class,0);
		}
		public MlruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mlrule; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitMlrule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MlruleContext mlrule() throws RecognitionException {
		MlruleContext _localctx = new MlruleContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_mlrule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			reactants();
			setState(114);
			match(ARROW);
			setState(115);
			products();
			setState(116);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AT) | (1L << ATTIME) | (1L << ATEACH))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(117);
			rate();
			setState(119);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(118);
				where();
				}
			}

			setState(121);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReactantsContext extends ParserRuleContext {
		public List<SpeciesContext> species() {
			return getRuleContexts(SpeciesContext.class);
		}
		public SpeciesContext species(int i) {
			return getRuleContext(SpeciesContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(MLRulesParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(MLRulesParser.PLUS, i);
		}
		public ReactantsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reactants; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitReactants(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReactantsContext reactants() throws RecognitionException {
		ReactantsContext _localctx = new ReactantsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_reactants);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			species();
			setState(128);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PLUS) {
				{
				{
				setState(124);
				match(PLUS);
				setState(125);
				species();
				}
				}
				setState(130);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProductsContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ProductsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_products; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitProducts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProductsContext products() throws RecognitionException {
		ProductsContext _localctx = new ProductsContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_products);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(131);
				expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RateContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rate; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitRate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RateContext rate() throws RecognitionException {
		RateContext _localctx = new RateContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_rate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolExprContext extends ExpressionContext {
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public TerminalNode LT_EQ() { return getToken(MLRulesParser.LT_EQ, 0); }
		public TerminalNode GT_EQ() { return getToken(MLRulesParser.GT_EQ, 0); }
		public TerminalNode EQUALS() { return getToken(MLRulesParser.EQUALS, 0); }
		public TerminalNode N_EQUALS() { return getToken(MLRulesParser.N_EQUALS, 0); }
		public BoolExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitBoolExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SpeciesExprContext extends ExpressionContext {
		public SpeciesContext species() {
			return getRuleContext(SpeciesContext.class,0);
		}
		public SpeciesExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSpeciesExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EmptySolutionContext extends ExpressionContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public EmptySolutionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitEmptySolution(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultDivContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MULT() { return getToken(MLRulesParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(MLRulesParser.DIV, 0); }
		public MultDivContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitMultDiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TupleContext extends ExpressionContext {
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public TupleContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitTuple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpValueContext extends ExpressionContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ExpValueContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitExpValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinusOneContext extends ExpressionContext {
		public TerminalNode MINUS() { return getToken(MLRulesParser.MINUS, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MinusOneContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitMinusOne(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotContext extends ExpressionContext {
		public TerminalNode NOT() { return getToken(MLRulesParser.NOT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NotContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitNot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RoofContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode ROOF() { return getToken(MLRulesParser.ROOF, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public RoofContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitRoof(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CountShortContext extends ExpressionContext {
		public TerminalNode COUNT() { return getToken(MLRulesParser.COUNT, 0); }
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public CountShortContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitCountShort(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfThenElseContext extends ExpressionContext {
		public TerminalNode IF() { return getToken(MLRulesParser.IF, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode THEN() { return getToken(MLRulesParser.THEN, 0); }
		public TerminalNode ELSE() { return getToken(MLRulesParser.ELSE, 0); }
		public IfThenElseContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitIfThenElse(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionCallContext extends ExpressionContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public FunctionCallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlusContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(MLRulesParser.PLUS, 0); }
		public PlusContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitPlus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ApplicationContext extends ExpressionContext {
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public AttributesContext attributes() {
			return getRuleContext(AttributesContext.class,0);
		}
		public ApplicationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitApplication(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndOrContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AND() { return getToken(MLRulesParser.AND, 0); }
		public TerminalNode OR() { return getToken(MLRulesParser.OR, 0); }
		public AndOrContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAndOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinusContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MINUS() { return getToken(MLRulesParser.MINUS, 0); }
		public MinusContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitMinus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenContext extends ExpressionContext {
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public ParenContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitParen(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				_localctx = new ApplicationContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(137);
				match(L_PAREN);
				setState(138);
				expression(0);
				setState(139);
				match(R_PAREN);
				setState(140);
				attributes();
				}
				break;
			case 2:
				{
				_localctx = new MinusOneContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(142);
				match(MINUS);
				setState(143);
				expression(16);
				}
				break;
			case 3:
				{
				_localctx = new NotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(144);
				match(NOT);
				setState(145);
				expression(15);
				}
				break;
			case 4:
				{
				_localctx = new IfThenElseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(146);
				match(IF);
				setState(147);
				expression(0);
				setState(148);
				match(THEN);
				setState(149);
				expression(0);
				setState(150);
				match(ELSE);
				setState(151);
				expression(10);
				}
				break;
			case 5:
				{
				_localctx = new BoolExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(153);
				match(L_PAREN);
				setState(154);
				expression(0);
				setState(155);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUALS) | (1L << N_EQUALS) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(156);
				expression(0);
				setState(157);
				match(R_PAREN);
				}
				break;
			case 6:
				{
				_localctx = new TupleContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(159);
				match(LT);
				setState(168);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
					{
					setState(160);
					expression(0);
					setState(165);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(161);
						match(COMMA);
						setState(162);
						expression(0);
						}
						}
						setState(167);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(170);
				match(GT);
				}
				break;
			case 7:
				{
				_localctx = new FunctionCallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(171);
				match(ID);
				setState(172);
				match(L_PAREN);
				setState(181);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
					{
					setState(173);
					expression(0);
					setState(178);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(174);
						match(COMMA);
						setState(175);
						expression(0);
						}
						}
						setState(180);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(183);
				match(R_PAREN);
				}
				break;
			case 8:
				{
				_localctx = new ParenContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(184);
				match(L_PAREN);
				setState(185);
				expression(0);
				setState(186);
				match(R_PAREN);
				}
				break;
			case 9:
				{
				_localctx = new SpeciesExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(188);
				species();
				}
				break;
			case 10:
				{
				_localctx = new EmptySolutionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(189);
				match(L_BRAKET);
				setState(190);
				match(R_BRAKET);
				}
				break;
			case 11:
				{
				_localctx = new ExpValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(191);
				value();
				}
				break;
			case 12:
				{
				_localctx = new CountShortContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(192);
				match(COUNT);
				setState(193);
				match(ID);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(216);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(214);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
					case 1:
						{
						_localctx = new MultDivContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(196);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(197);
						_la = _input.LA(1);
						if ( !(_la==MULT || _la==DIV) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(198);
						expression(14);
						}
						break;
					case 2:
						{
						_localctx = new PlusContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(199);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(200);
						match(PLUS);
						setState(201);
						expression(13);
						}
						break;
					case 3:
						{
						_localctx = new MinusContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(202);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(203);
						match(MINUS);
						setState(204);
						expression(12);
						}
						break;
					case 4:
						{
						_localctx = new AndOrContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(205);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(206);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(207);
						expression(10);
						}
						break;
					case 5:
						{
						_localctx = new RoofContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(208);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(209);
						match(ROOF);
						setState(210);
						match(L_PAREN);
						setState(211);
						expression(0);
						setState(212);
						match(R_PAREN);
						}
						break;
					}
					} 
				}
				setState(218);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SpeciesContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public AmountContext amount() {
			return getRuleContext(AmountContext.class,0);
		}
		public AttributesContext attributes() {
			return getRuleContext(AttributesContext.class,0);
		}
		public SubSpeciesContext subSpecies() {
			return getRuleContext(SubSpeciesContext.class,0);
		}
		public GuardContext guard() {
			return getRuleContext(GuardContext.class,0);
		}
		public TerminalNode COLON() { return getToken(MLRulesParser.COLON, 0); }
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public SpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_species; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSpecies(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpeciesContext species() throws RecognitionException {
		SpeciesContext _localctx = new SpeciesContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_species);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(219);
				amount();
				}
				break;
			}
			setState(222);
			name();
			setState(224);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(223);
				attributes();
				}
				break;
			}
			setState(227);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(226);
				subSpecies();
				}
				break;
			}
			setState(230);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(229);
				guard();
				}
				break;
			}
			setState(234);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(232);
				match(COLON);
				setState(233);
				match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode ID_SPECIES() { return getToken(MLRulesParser.ID_SPECIES, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_name);
		try {
			setState(241);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID_SPECIES:
				enterOuterAlt(_localctx, 1);
				{
				setState(236);
				match(ID_SPECIES);
				}
				break;
			case L_PAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(237);
				match(L_PAREN);
				setState(238);
				expression(0);
				setState(239);
				match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributesContext extends ParserRuleContext {
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public AttributesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributes; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAttributes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributesContext attributes() throws RecognitionException {
		AttributesContext _localctx = new AttributesContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_attributes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(243);
			match(L_PAREN);
			setState(252);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(244);
				expression(0);
				setState(249);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(245);
					match(COMMA);
					setState(246);
					expression(0);
					}
					}
					setState(251);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(254);
			match(R_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubSpeciesContext extends ParserRuleContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SubSpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subSpecies; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSubSpecies(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubSpeciesContext subSpecies() throws RecognitionException {
		SubSpeciesContext _localctx = new SubSpeciesContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_subSpecies);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256);
			match(L_BRAKET);
			setState(258);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(257);
				expression(0);
				}
			}

			setState(260);
			match(R_BRAKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GuardContext extends ParserRuleContext {
		public TerminalNode L_CURLY() { return getToken(MLRulesParser.L_CURLY, 0); }
		public TerminalNode R_CURLY() { return getToken(MLRulesParser.R_CURLY, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public GuardContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_guard; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitGuard(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GuardContext guard() throws RecognitionException {
		GuardContext _localctx = new GuardContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_guard);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			match(L_CURLY);
			setState(264);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IF) | (1L << FREE) | (1L << L_PAREN) | (1L << L_BRAKET) | (1L << MINUS) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << NOT) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING) | (1L << COUNT))) != 0)) {
				{
				setState(263);
				expression(0);
				}
			}

			setState(266);
			match(R_CURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AmountContext extends ParserRuleContext {
		public AmountContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_amount; }
	 
		public AmountContext() { }
		public void copyFrom(AmountContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AmountIDContext extends AmountContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public AmountIDContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAmountID(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AmountREALContext extends AmountContext {
		public TerminalNode REAL() { return getToken(MLRulesParser.REAL, 0); }
		public AmountREALContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAmountREAL(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AmountExprContext extends AmountContext {
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public AmountExprContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAmountExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AmountINTContext extends AmountContext {
		public TerminalNode INT() { return getToken(MLRulesParser.INT, 0); }
		public AmountINTContext(AmountContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAmountINT(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AmountContext amount() throws RecognitionException {
		AmountContext _localctx = new AmountContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_amount);
		try {
			setState(275);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new AmountINTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(268);
				match(INT);
				}
				break;
			case REAL:
				_localctx = new AmountREALContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(269);
				match(REAL);
				}
				break;
			case ID:
				_localctx = new AmountIDContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(270);
				match(ID);
				}
				break;
			case L_PAREN:
				_localctx = new AmountExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(271);
				match(L_PAREN);
				setState(272);
				expression(0);
				setState(273);
				match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	 
		public ValueContext() { }
		public void copyFrom(ValueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RealContext extends ValueContext {
		public TerminalNode REAL() { return getToken(MLRulesParser.REAL, 0); }
		public RealContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitReal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TrueContext extends ValueContext {
		public TerminalNode TRUE() { return getToken(MLRulesParser.TRUE, 0); }
		public TrueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitTrue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FalseContext extends ValueContext {
		public TerminalNode FALSE() { return getToken(MLRulesParser.FALSE, 0); }
		public FalseContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitFalse(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringContext extends ValueContext {
		public TerminalNode STRING() { return getToken(MLRulesParser.STRING, 0); }
		public StringContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdContext extends ValueContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public IdContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FreeContext extends ValueContext {
		public TerminalNode FREE() { return getToken(MLRulesParser.FREE, 0); }
		public FreeContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitFree(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntContext extends ValueContext {
		public TerminalNode INT() { return getToken(MLRulesParser.INT, 0); }
		public IntContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitInt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_value);
		try {
			setState(284);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new IntContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(277);
				match(INT);
				}
				break;
			case REAL:
				_localctx = new RealContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(278);
				match(REAL);
				}
				break;
			case TRUE:
				_localctx = new TrueContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(279);
				match(TRUE);
				}
				break;
			case FALSE:
				_localctx = new FalseContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(280);
				match(FALSE);
				}
				break;
			case STRING:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(281);
				match(STRING);
				}
				break;
			case FREE:
				_localctx = new FreeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(282);
				match(FREE);
				}
				break;
			case ID:
				_localctx = new IdContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(283);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereContext extends ParserRuleContext {
		public TerminalNode WHERE() { return getToken(MLRulesParser.WHERE, 0); }
		public List<AssignContext> assign() {
			return getRuleContexts(AssignContext.class);
		}
		public AssignContext assign(int i) {
			return getRuleContext(AssignContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public WhereContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_where; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitWhere(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhereContext where() throws RecognitionException {
		WhereContext _localctx = new WhereContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_where);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(286);
			match(WHERE);
			setState(287);
			assign();
			setState(292);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(288);
				match(COMMA);
				setState(289);
				assign();
				}
				}
				setState(294);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignContext extends ParserRuleContext {
		public AssignNameContext assignName() {
			return getRuleContext(AssignNameContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(MLRulesParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitAssign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignContext assign() throws RecognitionException {
		AssignContext _localctx = new AssignContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_assign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			assignName();
			setState(296);
			match(ASSIGN);
			setState(297);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignNameContext extends ParserRuleContext {
		public AssignNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignName; }
	 
		public AssignNameContext() { }
		public void copyFrom(AssignNameContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SingleAssignContext extends AssignNameContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public SingleAssignContext(AssignNameContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSingleAssign(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TupleAssignContext extends AssignNameContext {
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public List<TerminalNode> ID() { return getTokens(MLRulesParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MLRulesParser.ID, i);
		}
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public TupleAssignContext(AssignNameContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitTupleAssign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignNameContext assignName() throws RecognitionException {
		AssignNameContext _localctx = new AssignNameContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_assignName);
		int _la;
		try {
			setState(310);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new SingleAssignContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(299);
				match(ID);
				}
				break;
			case LT:
				_localctx = new TupleAssignContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(300);
				match(LT);
				setState(301);
				match(ID);
				setState(306);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(302);
					match(COMMA);
					setState(303);
					match(ID);
					}
					}
					setState(308);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(309);
				match(GT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public TypeDefinitionContext typeDefinition() {
			return getRuleContext(TypeDefinitionContext.class,0);
		}
		public List<FunctionDefinitionContext> functionDefinition() {
			return getRuleContexts(FunctionDefinitionContext.class);
		}
		public FunctionDefinitionContext functionDefinition(int i) {
			return getRuleContext(FunctionDefinitionContext.class,i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_function);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(312);
			typeDefinition();
			setState(314); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(313);
					functionDefinition();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(316); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode TYPE_SEPERATOR() { return getToken(MLRulesParser.TYPE_SEPERATOR, 0); }
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public List<TerminalNode> ARROW() { return getTokens(MLRulesParser.ARROW); }
		public TerminalNode ARROW(int i) {
			return getToken(MLRulesParser.ARROW, i);
		}
		public TypeDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeDefinition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitTypeDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeDefinitionContext typeDefinition() throws RecognitionException {
		TypeDefinitionContext _localctx = new TypeDefinitionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_typeDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(318);
			match(ID);
			setState(319);
			match(TYPE_SEPERATOR);
			setState(320);
			type();
			setState(325);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ARROW) {
				{
				{
				setState(321);
				match(ARROW);
				setState(322);
				type();
				}
				}
				setState(327);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(328);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	 
		public TypeContext() { }
		public void copyFrom(TypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BaseTypeFloatContext extends TypeContext {
		public TerminalNode TYPE_REAL() { return getToken(MLRulesParser.TYPE_REAL, 0); }
		public BaseTypeFloatContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitBaseTypeFloat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SpeciesTypeContext extends TypeContext {
		public TerminalNode TYPE_SPECIES() { return getToken(MLRulesParser.TYPE_SPECIES, 0); }
		public SpeciesTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSpeciesType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BaseTypeStringContext extends TypeContext {
		public TerminalNode TYPE_STRING() { return getToken(MLRulesParser.TYPE_STRING, 0); }
		public BaseTypeStringContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitBaseTypeString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BaseTypeLinkContext extends TypeContext {
		public TerminalNode TYPE_LINK() { return getToken(MLRulesParser.TYPE_LINK, 0); }
		public BaseTypeLinkContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitBaseTypeLink(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TupleTypeContext extends TypeContext {
		public TerminalNode TYPE_TUPLE() { return getToken(MLRulesParser.TYPE_TUPLE, 0); }
		public TupleTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitTupleType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionTypeContext extends TypeContext {
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<TerminalNode> ARROW() { return getTokens(MLRulesParser.ARROW); }
		public TerminalNode ARROW(int i) {
			return getToken(MLRulesParser.ARROW, i);
		}
		public FunctionTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitFunctionType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BaseTypeBoolContext extends TypeContext {
		public TerminalNode TYPE_BOOL() { return getToken(MLRulesParser.TYPE_BOOL, 0); }
		public BaseTypeBoolContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitBaseTypeBool(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SolutionTypeContext extends TypeContext {
		public TerminalNode TYPE_SOL() { return getToken(MLRulesParser.TYPE_SOL, 0); }
		public SolutionTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitSolutionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_type);
		int _la;
		try {
			setState(347);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TYPE_REAL:
				_localctx = new BaseTypeFloatContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(330);
				match(TYPE_REAL);
				}
				break;
			case TYPE_BOOL:
				_localctx = new BaseTypeBoolContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(331);
				match(TYPE_BOOL);
				}
				break;
			case TYPE_STRING:
				_localctx = new BaseTypeStringContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(332);
				match(TYPE_STRING);
				}
				break;
			case TYPE_SPECIES:
				_localctx = new SpeciesTypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(333);
				match(TYPE_SPECIES);
				}
				break;
			case TYPE_SOL:
				_localctx = new SolutionTypeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(334);
				match(TYPE_SOL);
				}
				break;
			case TYPE_TUPLE:
				_localctx = new TupleTypeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(335);
				match(TYPE_TUPLE);
				}
				break;
			case TYPE_LINK:
				_localctx = new BaseTypeLinkContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(336);
				match(TYPE_LINK);
				}
				break;
			case L_PAREN:
				_localctx = new FunctionTypeContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(337);
				match(L_PAREN);
				setState(338);
				type();
				setState(341); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(339);
					match(ARROW);
					setState(340);
					type();
					}
					}
					setState(343); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==ARROW );
				setState(345);
				match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MLRulesParser.ID, 0); }
		public TerminalNode ASSIGN() { return getToken(MLRulesParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(MLRulesParser.SEMI, 0); }
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public WhereContext where() {
			return getRuleContext(WhereContext.class,0);
		}
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitFunctionDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_functionDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(349);
			match(ID);
			setState(353);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << FREE) | (1L << L_BRAKET) | (1L << LT) | (1L << TRUE) | (1L << FALSE) | (1L << INT) | (1L << REAL) | (1L << ID_SPECIES) | (1L << ID) | (1L << STRING))) != 0)) {
				{
				{
				setState(350);
				parameter();
				}
				}
				setState(355);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(356);
			match(ASSIGN);
			setState(357);
			expression(0);
			setState(359);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(358);
				where();
				}
			}

			setState(361);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
	 
		public ParameterContext() { }
		public void copyFrom(ParameterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParamTupleContext extends ParameterContext {
		public TerminalNode LT() { return getToken(MLRulesParser.LT, 0); }
		public List<AtomicParameterContext> atomicParameter() {
			return getRuleContexts(AtomicParameterContext.class);
		}
		public AtomicParameterContext atomicParameter(int i) {
			return getRuleContext(AtomicParameterContext.class,i);
		}
		public TerminalNode GT() { return getToken(MLRulesParser.GT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public ParamTupleContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitParamTuple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParamSingleContext extends ParameterContext {
		public AtomicParameterContext atomicParameter() {
			return getRuleContext(AtomicParameterContext.class,0);
		}
		public ParamSingleContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitParamSingle(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParamSpeciesContext extends ParameterContext {
		public TerminalNode ID_SPECIES() { return getToken(MLRulesParser.ID_SPECIES, 0); }
		public TerminalNode L_PAREN() { return getToken(MLRulesParser.L_PAREN, 0); }
		public TerminalNode R_PAREN() { return getToken(MLRulesParser.R_PAREN, 0); }
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(MLRulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(MLRulesParser.COMMA, i);
		}
		public ParamSpeciesContext(ParameterContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitParamSpecies(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_parameter);
		int _la;
		try {
			setState(388);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FREE:
			case L_BRAKET:
			case TRUE:
			case FALSE:
			case INT:
			case REAL:
			case ID:
			case STRING:
				_localctx = new ParamSingleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(363);
				atomicParameter();
				}
				break;
			case LT:
				_localctx = new ParamTupleContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(364);
				match(LT);
				setState(365);
				atomicParameter();
				setState(370);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(366);
					match(COMMA);
					setState(367);
					atomicParameter();
					}
					}
					setState(372);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(373);
				match(GT);
				}
				break;
			case ID_SPECIES:
				_localctx = new ParamSpeciesContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(375);
				match(ID_SPECIES);
				setState(376);
				match(L_PAREN);
				setState(385);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << FREE) | (1L << TRUE) | (1L << FALSE) | (1L << INT) | (1L << REAL) | (1L << ID) | (1L << STRING))) != 0)) {
					{
					setState(377);
					value();
					setState(382);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(378);
						match(COMMA);
						setState(379);
						value();
						}
						}
						setState(384);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(387);
				match(R_PAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomicParameterContext extends ParserRuleContext {
		public AtomicParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomicParameter; }
	 
		public AtomicParameterContext() { }
		public void copyFrom(AtomicParameterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParamSolContext extends AtomicParameterContext {
		public List<TerminalNode> ID() { return getTokens(MLRulesParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(MLRulesParser.ID, i);
		}
		public TerminalNode PLUS() { return getToken(MLRulesParser.PLUS, 0); }
		public ParamSolContext(AtomicParameterContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitParamSol(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EmptySolContext extends AtomicParameterContext {
		public TerminalNode L_BRAKET() { return getToken(MLRulesParser.L_BRAKET, 0); }
		public TerminalNode R_BRAKET() { return getToken(MLRulesParser.R_BRAKET, 0); }
		public EmptySolContext(AtomicParameterContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitEmptySol(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParamSimpleContext extends AtomicParameterContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public ParamSimpleContext(AtomicParameterContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MLRulesVisitor ) return ((MLRulesVisitor<? extends T>)visitor).visitParamSimple(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomicParameterContext atomicParameter() throws RecognitionException {
		AtomicParameterContext _localctx = new AtomicParameterContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_atomicParameter);
		try {
			setState(396);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				_localctx = new ParamSimpleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(390);
				value();
				}
				break;
			case 2:
				_localctx = new ParamSolContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(391);
				match(ID);
				setState(392);
				match(PLUS);
				setState(393);
				match(ID);
				}
				break;
			case 3:
				_localctx = new EmptySolContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(394);
				match(L_BRAKET);
				setState(395);
				match(R_BRAKET);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 10:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 13);
		case 1:
			return precpred(_ctx, 12);
		case 2:
			return precpred(_ctx, 11);
		case 3:
			return precpred(_ctx, 9);
		case 4:
			return precpred(_ctx, 14);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3B\u0191\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\7\2:\n\2\f\2\16\2=\13\2\3\2\3\2\6\2"+
		"A\n\2\r\2\16\2B\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3N\n\3\3\4\3\4\3"+
		"\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6\5\6[\n\6\3\6\3\6\3\6\3\6\3\6\7\6b\n"+
		"\6\f\6\16\6e\13\6\3\6\3\6\3\6\5\6j\n\6\5\6l\n\6\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\b\3\b\3\b\3\b\3\b\3\b\5\bz\n\b\3\b\3\b\3\t\3\t\3\t\7\t\u0081\n\t"+
		"\f\t\16\t\u0084\13\t\3\n\5\n\u0087\n\n\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\7\f\u00a6\n\f\f\f\16\f\u00a9\13\f\5\f\u00ab\n\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\7\f\u00b3\n\f\f\f\16\f\u00b6\13\f\5\f\u00b8\n\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00c5\n\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u00d9\n\f"+
		"\f\f\16\f\u00dc\13\f\3\r\5\r\u00df\n\r\3\r\3\r\5\r\u00e3\n\r\3\r\5\r\u00e6"+
		"\n\r\3\r\5\r\u00e9\n\r\3\r\3\r\5\r\u00ed\n\r\3\16\3\16\3\16\3\16\3\16"+
		"\5\16\u00f4\n\16\3\17\3\17\3\17\3\17\7\17\u00fa\n\17\f\17\16\17\u00fd"+
		"\13\17\5\17\u00ff\n\17\3\17\3\17\3\20\3\20\5\20\u0105\n\20\3\20\3\20\3"+
		"\21\3\21\5\21\u010b\n\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\5\22\u0116\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u011f\n\23\3"+
		"\24\3\24\3\24\3\24\7\24\u0125\n\24\f\24\16\24\u0128\13\24\3\25\3\25\3"+
		"\25\3\25\3\26\3\26\3\26\3\26\3\26\7\26\u0133\n\26\f\26\16\26\u0136\13"+
		"\26\3\26\5\26\u0139\n\26\3\27\3\27\6\27\u013d\n\27\r\27\16\27\u013e\3"+
		"\30\3\30\3\30\3\30\3\30\7\30\u0146\n\30\f\30\16\30\u0149\13\30\3\30\3"+
		"\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\6\31\u0158"+
		"\n\31\r\31\16\31\u0159\3\31\3\31\5\31\u015e\n\31\3\32\3\32\7\32\u0162"+
		"\n\32\f\32\16\32\u0165\13\32\3\32\3\32\3\32\5\32\u016a\n\32\3\32\3\32"+
		"\3\33\3\33\3\33\3\33\3\33\7\33\u0173\n\33\f\33\16\33\u0176\13\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\7\33\u017f\n\33\f\33\16\33\u0182\13\33"+
		"\5\33\u0184\n\33\3\33\5\33\u0187\n\33\3\34\3\34\3\34\3\34\3\34\3\34\5"+
		"\34\u018f\n\34\3\34\2\3\26\35\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 "+
		"\"$&(*,.\60\62\64\66\2\7\4\2\62\6499\3\2\6\b\3\2$)\3\2!\"\3\2,-\2\u01bd"+
		"\2;\3\2\2\2\4M\3\2\2\2\6O\3\2\2\2\bS\3\2\2\2\nk\3\2\2\2\fm\3\2\2\2\16"+
		"s\3\2\2\2\20}\3\2\2\2\22\u0086\3\2\2\2\24\u0088\3\2\2\2\26\u00c4\3\2\2"+
		"\2\30\u00de\3\2\2\2\32\u00f3\3\2\2\2\34\u00f5\3\2\2\2\36\u0102\3\2\2\2"+
		" \u0108\3\2\2\2\"\u0115\3\2\2\2$\u011e\3\2\2\2&\u0120\3\2\2\2(\u0129\3"+
		"\2\2\2*\u0138\3\2\2\2,\u013a\3\2\2\2.\u0140\3\2\2\2\60\u015d\3\2\2\2\62"+
		"\u015f\3\2\2\2\64\u0186\3\2\2\2\66\u018e\3\2\2\28:\5\4\3\298\3\2\2\2:"+
		"=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<>\3\2\2\2=;\3\2\2\2>@\5\f\7\2?A\5\16\b\2"+
		"@?\3\2\2\2AB\3\2\2\2B@\3\2\2\2BC\3\2\2\2CD\3\2\2\2DE\7\2\2\3E\3\3\2\2"+
		"\2FG\5\n\6\2GH\7\30\2\2HN\3\2\2\2IJ\5\6\4\2JK\7\30\2\2KN\3\2\2\2LN\5,"+
		"\27\2MF\3\2\2\2MI\3\2\2\2ML\3\2\2\2N\5\3\2\2\2OP\7>\2\2PQ\7\27\2\2QR\5"+
		"\26\f\2R\7\3\2\2\2ST\t\2\2\2T\t\3\2\2\2UV\7=\2\2VW\7\31\2\2WZ\7\32\2\2"+
		"XY\7\33\2\2Y[\7\34\2\2ZX\3\2\2\2Z[\3\2\2\2[l\3\2\2\2\\]\7=\2\2]^\7\31"+
		"\2\2^c\5\b\5\2_`\7\26\2\2`b\5\b\5\2a_\3\2\2\2be\3\2\2\2ca\3\2\2\2cd\3"+
		"\2\2\2df\3\2\2\2ec\3\2\2\2fi\7\32\2\2gh\7\33\2\2hj\7\34\2\2ig\3\2\2\2"+
		"ij\3\2\2\2jl\3\2\2\2kU\3\2\2\2k\\\3\2\2\2l\13\3\2\2\2mn\7\t\2\2no\7\33"+
		"\2\2op\5\26\f\2pq\7\34\2\2qr\7\30\2\2r\r\3\2\2\2st\5\20\t\2tu\7\61\2\2"+
		"uv\5\22\n\2vw\t\3\2\2wy\5\24\13\2xz\5&\24\2yx\3\2\2\2yz\3\2\2\2z{\3\2"+
		"\2\2{|\7\30\2\2|\17\3\2\2\2}\u0082\5\30\r\2~\177\7\37\2\2\177\u0081\5"+
		"\30\r\2\u0080~\3\2\2\2\u0081\u0084\3\2\2\2\u0082\u0080\3\2\2\2\u0082\u0083"+
		"\3\2\2\2\u0083\21\3\2\2\2\u0084\u0082\3\2\2\2\u0085\u0087\5\26\f\2\u0086"+
		"\u0085\3\2\2\2\u0086\u0087\3\2\2\2\u0087\23\3\2\2\2\u0088\u0089\5\26\f"+
		"\2\u0089\25\3\2\2\2\u008a\u008b\b\f\1\2\u008b\u008c\7\31\2\2\u008c\u008d"+
		"\5\26\f\2\u008d\u008e\7\32\2\2\u008e\u008f\5\34\17\2\u008f\u00c5\3\2\2"+
		"\2\u0090\u0091\7 \2\2\u0091\u00c5\5\26\f\22\u0092\u0093\7.\2\2\u0093\u00c5"+
		"\5\26\f\21\u0094\u0095\7\n\2\2\u0095\u0096\5\26\f\2\u0096\u0097\7\13\2"+
		"\2\u0097\u0098\5\26\f\2\u0098\u0099\7\f\2\2\u0099\u009a\5\26\f\f\u009a"+
		"\u00c5\3\2\2\2\u009b\u009c\7\31\2\2\u009c\u009d\5\26\f\2\u009d\u009e\t"+
		"\4\2\2\u009e\u009f\5\26\f\2\u009f\u00a0\7\32\2\2\u00a0\u00c5\3\2\2\2\u00a1"+
		"\u00aa\7&\2\2\u00a2\u00a7\5\26\f\2\u00a3\u00a4\7\26\2\2\u00a4\u00a6\5"+
		"\26\f\2\u00a5\u00a3\3\2\2\2\u00a6\u00a9\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7"+
		"\u00a8\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00aa\u00a2\3\2"+
		"\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00c5\7(\2\2\u00ad"+
		"\u00ae\7>\2\2\u00ae\u00b7\7\31\2\2\u00af\u00b4\5\26\f\2\u00b0\u00b1\7"+
		"\26\2\2\u00b1\u00b3\5\26\f\2\u00b2\u00b0\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4"+
		"\u00b2\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b4\3\2"+
		"\2\2\u00b7\u00af\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9"+
		"\u00c5\7\32\2\2\u00ba\u00bb\7\31\2\2\u00bb\u00bc\5\26\f\2\u00bc\u00bd"+
		"\7\32\2\2\u00bd\u00c5\3\2\2\2\u00be\u00c5\5\30\r\2\u00bf\u00c0\7\33\2"+
		"\2\u00c0\u00c5\7\34\2\2\u00c1\u00c5\5$\23\2\u00c2\u00c3\7A\2\2\u00c3\u00c5"+
		"\7>\2\2\u00c4\u008a\3\2\2\2\u00c4\u0090\3\2\2\2\u00c4\u0092\3\2\2\2\u00c4"+
		"\u0094\3\2\2\2\u00c4\u009b\3\2\2\2\u00c4\u00a1\3\2\2\2\u00c4\u00ad\3\2"+
		"\2\2\u00c4\u00ba\3\2\2\2\u00c4\u00be\3\2\2\2\u00c4\u00bf\3\2\2\2\u00c4"+
		"\u00c1\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c5\u00da\3\2\2\2\u00c6\u00c7\f\17"+
		"\2\2\u00c7\u00c8\t\5\2\2\u00c8\u00d9\5\26\f\20\u00c9\u00ca\f\16\2\2\u00ca"+
		"\u00cb\7\37\2\2\u00cb\u00d9\5\26\f\17\u00cc\u00cd\f\r\2\2\u00cd\u00ce"+
		"\7 \2\2\u00ce\u00d9\5\26\f\16\u00cf\u00d0\f\13\2\2\u00d0\u00d1\t\6\2\2"+
		"\u00d1\u00d9\5\26\f\f\u00d2\u00d3\f\20\2\2\u00d3\u00d4\7#\2\2\u00d4\u00d5"+
		"\7\31\2\2\u00d5\u00d6\5\26\f\2\u00d6\u00d7\7\32\2\2\u00d7\u00d9\3\2\2"+
		"\2\u00d8\u00c6\3\2\2\2\u00d8\u00c9\3\2\2\2\u00d8\u00cc\3\2\2\2\u00d8\u00cf"+
		"\3\2\2\2\u00d8\u00d2\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3\2\2\2\u00da"+
		"\u00db\3\2\2\2\u00db\27\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00df\5\"\22"+
		"\2\u00de\u00dd\3\2\2\2\u00de\u00df\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e2"+
		"\5\32\16\2\u00e1\u00e3\5\34\17\2\u00e2\u00e1\3\2\2\2\u00e2\u00e3\3\2\2"+
		"\2\u00e3\u00e5\3\2\2\2\u00e4\u00e6\5\36\20\2\u00e5\u00e4\3\2\2\2\u00e5"+
		"\u00e6\3\2\2\2\u00e6\u00e8\3\2\2\2\u00e7\u00e9\5 \21\2\u00e8\u00e7\3\2"+
		"\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ec\3\2\2\2\u00ea\u00eb\7\27\2\2\u00eb"+
		"\u00ed\7>\2\2\u00ec\u00ea\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\31\3\2\2\2"+
		"\u00ee\u00f4\7=\2\2\u00ef\u00f0\7\31\2\2\u00f0\u00f1\5\26\f\2\u00f1\u00f2"+
		"\7\32\2\2\u00f2\u00f4\3\2\2\2\u00f3\u00ee\3\2\2\2\u00f3\u00ef\3\2\2\2"+
		"\u00f4\33\3\2\2\2\u00f5\u00fe\7\31\2\2\u00f6\u00fb\5\26\f\2\u00f7\u00f8"+
		"\7\26\2\2\u00f8\u00fa\5\26\f\2\u00f9\u00f7\3\2\2\2\u00fa\u00fd\3\2\2\2"+
		"\u00fb\u00f9\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00ff\3\2\2\2\u00fd\u00fb"+
		"\3\2\2\2\u00fe\u00f6\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\u0100\3\2\2\2\u0100"+
		"\u0101\7\32\2\2\u0101\35\3\2\2\2\u0102\u0104\7\33\2\2\u0103\u0105\5\26"+
		"\f\2\u0104\u0103\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0106\3\2\2\2\u0106"+
		"\u0107\7\34\2\2\u0107\37\3\2\2\2\u0108\u010a\7\35\2\2\u0109\u010b\5\26"+
		"\f\2\u010a\u0109\3\2\2\2\u010a\u010b\3\2\2\2\u010b\u010c\3\2\2\2\u010c"+
		"\u010d\7\36\2\2\u010d!\3\2\2\2\u010e\u0116\7:\2\2\u010f\u0116\7;\2\2\u0110"+
		"\u0116\7>\2\2\u0111\u0112\7\31\2\2\u0112\u0113\5\26\f\2\u0113\u0114\7"+
		"\32\2\2\u0114\u0116\3\2\2\2\u0115\u010e\3\2\2\2\u0115\u010f\3\2\2\2\u0115"+
		"\u0110\3\2\2\2\u0115\u0111\3\2\2\2\u0116#\3\2\2\2\u0117\u011f\7:\2\2\u0118"+
		"\u011f\7;\2\2\u0119\u011f\7*\2\2\u011a\u011f\7+\2\2\u011b\u011f\7?\2\2"+
		"\u011c\u011f\7\23\2\2\u011d\u011f\7>\2\2\u011e\u0117\3\2\2\2\u011e\u0118"+
		"\3\2\2\2\u011e\u0119\3\2\2\2\u011e\u011a\3\2\2\2\u011e\u011b\3\2\2\2\u011e"+
		"\u011c\3\2\2\2\u011e\u011d\3\2\2\2\u011f%\3\2\2\2\u0120\u0121\7\22\2\2"+
		"\u0121\u0126\5(\25\2\u0122\u0123\7\26\2\2\u0123\u0125\5(\25\2\u0124\u0122"+
		"\3\2\2\2\u0125\u0128\3\2\2\2\u0126\u0124\3\2\2\2\u0126\u0127\3\2\2\2\u0127"+
		"\'\3\2\2\2\u0128\u0126\3\2\2\2\u0129\u012a\5*\26\2\u012a\u012b\7\24\2"+
		"\2\u012b\u012c\5\26\f\2\u012c)\3\2\2\2\u012d\u0139\7>\2\2\u012e\u012f"+
		"\7&\2\2\u012f\u0134\7>\2\2\u0130\u0131\7\26\2\2\u0131\u0133\7>\2\2\u0132"+
		"\u0130\3\2\2\2\u0133\u0136\3\2\2\2\u0134\u0132\3\2\2\2\u0134\u0135\3\2"+
		"\2\2\u0135\u0137\3\2\2\2\u0136\u0134\3\2\2\2\u0137\u0139\7(\2\2\u0138"+
		"\u012d\3\2\2\2\u0138\u012e\3\2\2\2\u0139+\3\2\2\2\u013a\u013c\5.\30\2"+
		"\u013b\u013d\5\62\32\2\u013c\u013b\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u013c"+
		"\3\2\2\2\u013e\u013f\3\2\2\2\u013f-\3\2\2\2\u0140\u0141\7>\2\2\u0141\u0142"+
		"\7\60\2\2\u0142\u0147\5\60\31\2\u0143\u0144\7\61\2\2\u0144\u0146\5\60"+
		"\31\2\u0145\u0143\3\2\2\2\u0146\u0149\3\2\2\2\u0147\u0145\3\2\2\2\u0147"+
		"\u0148\3\2\2\2\u0148\u014a\3\2\2\2\u0149\u0147\3\2\2\2\u014a\u014b\7\30"+
		"\2\2\u014b/\3\2\2\2\u014c\u015e\7\62\2\2\u014d\u015e\7\63\2\2\u014e\u015e"+
		"\7\64\2\2\u014f\u015e\7\65\2\2\u0150\u015e\78\2\2\u0151\u015e\7\67\2\2"+
		"\u0152\u015e\79\2\2\u0153\u0154\7\31\2\2\u0154\u0157\5\60\31\2\u0155\u0156"+
		"\7\61\2\2\u0156\u0158\5\60\31\2\u0157\u0155\3\2\2\2\u0158\u0159\3\2\2"+
		"\2\u0159\u0157\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u015c"+
		"\7\32\2\2\u015c\u015e\3\2\2\2\u015d\u014c\3\2\2\2\u015d\u014d\3\2\2\2"+
		"\u015d\u014e\3\2\2\2\u015d\u014f\3\2\2\2\u015d\u0150\3\2\2\2\u015d\u0151"+
		"\3\2\2\2\u015d\u0152\3\2\2\2\u015d\u0153\3\2\2\2\u015e\61\3\2\2\2\u015f"+
		"\u0163\7>\2\2\u0160\u0162\5\64\33\2\u0161\u0160\3\2\2\2\u0162\u0165\3"+
		"\2\2\2\u0163\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166\3\2\2\2\u0165"+
		"\u0163\3\2\2\2\u0166\u0167\7\24\2\2\u0167\u0169\5\26\f\2\u0168\u016a\5"+
		"&\24\2\u0169\u0168\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u016b\3\2\2\2\u016b"+
		"\u016c\7\30\2\2\u016c\63\3\2\2\2\u016d\u0187\5\66\34\2\u016e\u016f\7&"+
		"\2\2\u016f\u0174\5\66\34\2\u0170\u0171\7\26\2\2\u0171\u0173\5\66\34\2"+
		"\u0172\u0170\3\2\2\2\u0173\u0176\3\2\2\2\u0174\u0172\3\2\2\2\u0174\u0175"+
		"\3\2\2\2\u0175\u0177\3\2\2\2\u0176\u0174\3\2\2\2\u0177\u0178\7(\2\2\u0178"+
		"\u0187\3\2\2\2\u0179\u017a\7=\2\2\u017a\u0183\7\31\2\2\u017b\u0180\5$"+
		"\23\2\u017c\u017d\7\26\2\2\u017d\u017f\5$\23\2\u017e\u017c\3\2\2\2\u017f"+
		"\u0182\3\2\2\2\u0180\u017e\3\2\2\2\u0180\u0181\3\2\2\2\u0181\u0184\3\2"+
		"\2\2\u0182\u0180\3\2\2\2\u0183\u017b\3\2\2\2\u0183\u0184\3\2\2\2\u0184"+
		"\u0185\3\2\2\2\u0185\u0187\7\32\2\2\u0186\u016d\3\2\2\2\u0186\u016e\3"+
		"\2\2\2\u0186\u0179\3\2\2\2\u0187\65\3\2\2\2\u0188\u018f\5$\23\2\u0189"+
		"\u018a\7>\2\2\u018a\u018b\7\37\2\2\u018b\u018f\7>\2\2\u018c\u018d\7\33"+
		"\2\2\u018d\u018f\7\34\2\2\u018e\u0188\3\2\2\2\u018e\u0189\3\2\2\2\u018e"+
		"\u018c\3\2\2\2\u018f\67\3\2\2\2-;BMZciky\u0082\u0086\u00a7\u00aa\u00b4"+
		"\u00b7\u00c4\u00d8\u00da\u00de\u00e2\u00e5\u00e8\u00ec\u00f3\u00fb\u00fe"+
		"\u0104\u010a\u0115\u011e\u0126\u0134\u0138\u013e\u0147\u0159\u015d\u0163"+
		"\u0169\u0174\u0180\u0183\u0186\u018e";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}