/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

grammar Functions;

import Expressions;

function: typeDefinition functionDefinition+;

typeDefinition
    :   ID TYPE_SEPERATOR type (ARROW type)* SEMI
    ;

type
    :   TYPE_REAL                               # baseTypeFloat
    |   TYPE_BOOL                               # baseTypeBool
    |   TYPE_STRING                             # baseTypeString
    |   TYPE_SPECIES							# speciesType
    |	TYPE_SOL								# solutionType
    |   TYPE_TUPLE								# tupleType
    |	TYPE_LINK								# baseTypeLink
    |	L_PAREN type (ARROW type)+ R_PAREN		# functionType                    
    ;
    
functionDefinition
    :   ID parameter* ASSIGN expression where? SEMI
    ; 
    
parameter
	: atomicParameter													# paramSingle
	| LT atomicParameter (COMMA atomicParameter)* GT					# paramTuple
	| ID_SPECIES L_PAREN (value (COMMA value)*)? R_PAREN 				# paramSpecies
	;

atomicParameter
	: value										# paramSimple
	| ID PLUS ID								# paramSol
	| L_BRAKET R_BRAKET							# emptySol
    ;