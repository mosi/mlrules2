// Generated from MLRules.g4 by ANTLR 4.7

package org.jamesii.mlrules.parser.grammar;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MLRulesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MLRulesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#model}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModel(MLRulesParser.ModelContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#preamble}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPreamble(MLRulesParser.PreambleContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(MLRulesParser.ConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#speciesTypeParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpeciesTypeParameters(MLRulesParser.SpeciesTypeParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#speciesDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpeciesDefinition(MLRulesParser.SpeciesDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#initialSolution}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitialSolution(MLRulesParser.InitialSolutionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#mlrule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMlrule(MLRulesParser.MlruleContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#reactants}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReactants(MLRulesParser.ReactantsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#products}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProducts(MLRulesParser.ProductsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#rate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRate(MLRulesParser.RateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolExpr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpr(MLRulesParser.BoolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SpeciesExpr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpeciesExpr(MLRulesParser.SpeciesExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EmptySolution}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptySolution(MLRulesParser.EmptySolutionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MultDiv}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultDiv(MLRulesParser.MultDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTuple(MLRulesParser.TupleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpValue}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpValue(MLRulesParser.ExpValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MinusOne}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusOne(MLRulesParser.MinusOneContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(MLRulesParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Roof}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoof(MLRulesParser.RoofContext ctx);
	/**
	 * Visit a parse tree produced by the {@code countShort}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCountShort(MLRulesParser.CountShortContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IfThenElse}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfThenElse(MLRulesParser.IfThenElseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(MLRulesParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(MLRulesParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Application}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitApplication(MLRulesParser.ApplicationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AndOr}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndOr(MLRulesParser.AndOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(MLRulesParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Paren}
	 * labeled alternative in {@link MLRulesParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParen(MLRulesParser.ParenContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#species}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecies(MLRulesParser.SpeciesContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(MLRulesParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#attributes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributes(MLRulesParser.AttributesContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#subSpecies}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubSpecies(MLRulesParser.SubSpeciesContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#guard}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGuard(MLRulesParser.GuardContext ctx);
	/**
	 * Visit a parse tree produced by the {@code amountINT}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmountINT(MLRulesParser.AmountINTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code amountREAL}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmountREAL(MLRulesParser.AmountREALContext ctx);
	/**
	 * Visit a parse tree produced by the {@code amountID}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmountID(MLRulesParser.AmountIDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code amountExpr}
	 * labeled alternative in {@link MLRulesParser#amount}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAmountExpr(MLRulesParser.AmountExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt(MLRulesParser.IntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Real}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReal(MLRulesParser.RealContext ctx);
	/**
	 * Visit a parse tree produced by the {@code True}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrue(MLRulesParser.TrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code False}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalse(MLRulesParser.FalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code String}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(MLRulesParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Free}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFree(MLRulesParser.FreeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Id}
	 * labeled alternative in {@link MLRulesParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(MLRulesParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere(MLRulesParser.WhereContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#assign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(MLRulesParser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code singleAssign}
	 * labeled alternative in {@link MLRulesParser#assignName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleAssign(MLRulesParser.SingleAssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tupleAssign}
	 * labeled alternative in {@link MLRulesParser#assignName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupleAssign(MLRulesParser.TupleAssignContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(MLRulesParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#typeDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeDefinition(MLRulesParser.TypeDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code baseTypeFloat}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBaseTypeFloat(MLRulesParser.BaseTypeFloatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code baseTypeBool}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBaseTypeBool(MLRulesParser.BaseTypeBoolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code baseTypeString}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBaseTypeString(MLRulesParser.BaseTypeStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code speciesType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpeciesType(MLRulesParser.SpeciesTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code solutionType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSolutionType(MLRulesParser.SolutionTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tupleType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupleType(MLRulesParser.TupleTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code baseTypeLink}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBaseTypeLink(MLRulesParser.BaseTypeLinkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionType}
	 * labeled alternative in {@link MLRulesParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionType(MLRulesParser.FunctionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MLRulesParser#functionDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDefinition(MLRulesParser.FunctionDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code paramSingle}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamSingle(MLRulesParser.ParamSingleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code paramTuple}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamTuple(MLRulesParser.ParamTupleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code paramSpecies}
	 * labeled alternative in {@link MLRulesParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamSpecies(MLRulesParser.ParamSpeciesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code paramSimple}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamSimple(MLRulesParser.ParamSimpleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code paramSol}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamSol(MLRulesParser.ParamSolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptySol}
	 * labeled alternative in {@link MLRulesParser#atomicParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptySol(MLRulesParser.EmptySolContext ctx);
}