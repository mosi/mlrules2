// Generated from MLRules.g4 by ANTLR 4.7

package org.jamesii.mlrules.parser.grammar;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MLRulesLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT_SINGLE=1, COMMENT_MULTI=2, WS=3, AT=4, ATTIME=5, ATEACH=6, INIT=7, 
		IF=8, THEN=9, ELSE=10, FOR=11, WHILE=12, UNTIL=13, LENGTH=14, ADD=15, 
		WHERE=16, FREE=17, ASSIGN=18, DOT=19, COMMA=20, COLON=21, SEMI=22, L_PAREN=23, 
		R_PAREN=24, L_BRAKET=25, R_BRAKET=26, L_CURLY=27, R_CURLY=28, PLUS=29, 
		MINUS=30, MULT=31, DIV=32, ROOF=33, EQUALS=34, N_EQUALS=35, LT=36, LT_EQ=37, 
		GT=38, GT_EQ=39, TRUE=40, FALSE=41, AND=42, OR=43, NOT=44, LAMBDA=45, 
		TYPE_SEPERATOR=46, ARROW=47, TYPE_REAL=48, TYPE_BOOL=49, TYPE_STRING=50, 
		TYPE_SPECIES=51, TYPE_PATTERN=52, TYPE_TUPLE=53, TYPE_SOL=54, TYPE_LINK=55, 
		INT=56, REAL=57, BOOL=58, ID_SPECIES=59, ID=60, STRING=61, NU=62, COUNT=63, 
		QUESTION=64;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"COMMENT_SINGLE", "COMMENT_MULTI", "WS", "AT", "ATTIME", "ATEACH", "INIT", 
		"IF", "THEN", "ELSE", "FOR", "WHILE", "UNTIL", "LENGTH", "ADD", "WHERE", 
		"FREE", "ASSIGN", "DOT", "COMMA", "COLON", "SEMI", "L_PAREN", "R_PAREN", 
		"L_BRAKET", "R_BRAKET", "L_CURLY", "R_CURLY", "PLUS", "MINUS", "MULT", 
		"DIV", "ROOF", "EQUALS", "N_EQUALS", "LT", "LT_EQ", "GT", "GT_EQ", "TRUE", 
		"FALSE", "AND", "OR", "NOT", "LAMBDA", "TYPE_SEPERATOR", "ARROW", "TYPE_REAL", 
		"TYPE_BOOL", "TYPE_STRING", "TYPE_SPECIES", "TYPE_PATTERN", "TYPE_TUPLE", 
		"TYPE_SOL", "TYPE_LINK", "INT", "REAL", "EXPONENT", "BOOL", "ID_SPECIES", 
		"ID", "CAPITAL", "SMALL", "CHAR", "STRING", "ESCAPE", "UNICODE_ESCAPE", 
		"OCTAL_ESCAPE", "DIGIT", "HEX_DIGIT", "NU", "COUNT", "QUESTION"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, "'@'", "'@EXACT'", "'@EACH'", "'>>INIT'", "'if'", 
		"'then'", "'else'", "'for'", "'while'", "'until'", "'length'", "'add'", 
		"'where'", "'free'", "'='", "'.'", "','", "':'", "';'", "'('", "')'", 
		"'['", "']'", "'{'", "'}'", "'+'", "'-'", "'*'", "'/'", "'^'", "'=='", 
		"'!='", "'<'", "'<='", "'>'", "'>='", "'true'", "'false'", "'&&'", "'||'", 
		"'!'", "'\\'", "'::'", "'->'", "'num'", "'bool'", "'string'", "'species'", 
		"'pattern'", "'tuple'", "'sol'", "'link'", null, null, null, null, null, 
		null, null, "'#'", "'?'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMMENT_SINGLE", "COMMENT_MULTI", "WS", "AT", "ATTIME", "ATEACH", 
		"INIT", "IF", "THEN", "ELSE", "FOR", "WHILE", "UNTIL", "LENGTH", "ADD", 
		"WHERE", "FREE", "ASSIGN", "DOT", "COMMA", "COLON", "SEMI", "L_PAREN", 
		"R_PAREN", "L_BRAKET", "R_BRAKET", "L_CURLY", "R_CURLY", "PLUS", "MINUS", 
		"MULT", "DIV", "ROOF", "EQUALS", "N_EQUALS", "LT", "LT_EQ", "GT", "GT_EQ", 
		"TRUE", "FALSE", "AND", "OR", "NOT", "LAMBDA", "TYPE_SEPERATOR", "ARROW", 
		"TYPE_REAL", "TYPE_BOOL", "TYPE_STRING", "TYPE_SPECIES", "TYPE_PATTERN", 
		"TYPE_TUPLE", "TYPE_SOL", "TYPE_LINK", "INT", "REAL", "BOOL", "ID_SPECIES", 
		"ID", "STRING", "NU", "COUNT", "QUESTION"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MLRulesLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MLRules.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2B\u01fe\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\3\2\3\2\3\2\3\2\7\2\u009a\n\2\f\2\16\2\u009d\13\2\3\2\5\2\u00a0"+
		"\n\2\3\2\5\2\u00a3\n\2\3\2\3\2\3\3\3\3\3\3\3\3\7\3\u00ab\n\3\f\3\16\3"+
		"\u00ae\13\3\3\3\3\3\3\3\3\3\3\3\3\4\6\4\u00b6\n\4\r\4\16\4\u00b7\3\4\3"+
		"\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3"+
		"\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3"+
		"\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\21\3"+
		"\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\24\3\24\3"+
		"\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3"+
		"\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3#\3"+
		"$\3$\3$\3%\3%\3&\3&\3&\3\'\3\'\3(\3(\3(\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*"+
		"\3*\3+\3+\3+\3,\3,\3,\3-\3-\3.\3.\3/\3/\3/\3\60\3\60\3\60\3\61\3\61\3"+
		"\61\3\61\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3"+
		"\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65\3\65\3"+
		"\65\3\65\3\66\3\66\3\66\3\66\3\66\3\66\3\67\3\67\3\67\3\67\38\38\38\3"+
		"8\38\39\69\u0180\n9\r9\169\u0181\3:\6:\u0185\n:\r:\16:\u0186\3:\3:\7:"+
		"\u018b\n:\f:\16:\u018e\13:\3:\5:\u0191\n:\3:\6:\u0194\n:\r:\16:\u0195"+
		"\3:\3:\5:\u019a\n:\3;\3;\5;\u019e\n;\3;\6;\u01a1\n;\r;\16;\u01a2\3<\3"+
		"<\5<\u01a7\n<\3=\3=\3=\7=\u01ac\n=\f=\16=\u01af\13=\3>\3>\3>\7>\u01b4"+
		"\n>\f>\16>\u01b7\13>\3>\5>\u01ba\n>\3?\3?\3@\3@\3A\3A\3B\3B\3B\7B\u01c5"+
		"\nB\fB\16B\u01c8\13B\3B\3B\3C\3C\3C\3C\5C\u01d0\nC\3C\3C\3C\3C\7C\u01d6"+
		"\nC\fC\16C\u01d9\13C\3C\3C\3C\3C\5C\u01df\nC\3D\3D\3D\3D\3D\3D\3D\3D\3"+
		"E\3E\3E\3E\3E\3E\3E\5E\u01f0\nE\5E\u01f2\nE\3F\3F\3G\3G\3H\3H\3H\3I\3"+
		"I\3J\3J\4\u00ac\u01d7\2K\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f"+
		"\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63"+
		"\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62"+
		"c\63e\64g\65i\66k\67m8o9q:s;u\2w<y={>}\2\177\2\u0081\2\u0083?\u0085\2"+
		"\u0087\2\u0089\2\u008b\2\u008d\2\u008f@\u0091A\u0093B\3\2\17\4\2\f\f\17"+
		"\17\5\2\13\f\17\17\"\"\4\2GGgg\4\2--//\3\2C\\\3\2c|\6\2C\\aac|~~\4\2)"+
		")^^\b\2))ddhhppttvv\3\2\62\65\3\2\629\3\2\62;\t\2))\62;CCHHcchh~~\2\u0210"+
		"\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2"+
		"\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2"+
		"\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2"+
		"\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2"+
		"\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3"+
		"\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2"+
		"\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2"+
		"U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3"+
		"\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2"+
		"\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2"+
		"\u0083\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2\2\3\u0095"+
		"\3\2\2\2\5\u00a6\3\2\2\2\7\u00b5\3\2\2\2\t\u00bb\3\2\2\2\13\u00bd\3\2"+
		"\2\2\r\u00c4\3\2\2\2\17\u00ca\3\2\2\2\21\u00d1\3\2\2\2\23\u00d4\3\2\2"+
		"\2\25\u00d9\3\2\2\2\27\u00de\3\2\2\2\31\u00e2\3\2\2\2\33\u00e8\3\2\2\2"+
		"\35\u00ee\3\2\2\2\37\u00f5\3\2\2\2!\u00f9\3\2\2\2#\u00ff\3\2\2\2%\u0104"+
		"\3\2\2\2\'\u0106\3\2\2\2)\u0108\3\2\2\2+\u010a\3\2\2\2-\u010c\3\2\2\2"+
		"/\u010e\3\2\2\2\61\u0110\3\2\2\2\63\u0112\3\2\2\2\65\u0114\3\2\2\2\67"+
		"\u0116\3\2\2\29\u0118\3\2\2\2;\u011a\3\2\2\2=\u011c\3\2\2\2?\u011e\3\2"+
		"\2\2A\u0120\3\2\2\2C\u0122\3\2\2\2E\u0124\3\2\2\2G\u0127\3\2\2\2I\u012a"+
		"\3\2\2\2K\u012c\3\2\2\2M\u012f\3\2\2\2O\u0131\3\2\2\2Q\u0134\3\2\2\2S"+
		"\u0139\3\2\2\2U\u013f\3\2\2\2W\u0142\3\2\2\2Y\u0145\3\2\2\2[\u0147\3\2"+
		"\2\2]\u0149\3\2\2\2_\u014c\3\2\2\2a\u014f\3\2\2\2c\u0153\3\2\2\2e\u0158"+
		"\3\2\2\2g\u015f\3\2\2\2i\u0167\3\2\2\2k\u016f\3\2\2\2m\u0175\3\2\2\2o"+
		"\u0179\3\2\2\2q\u017f\3\2\2\2s\u0199\3\2\2\2u\u019b\3\2\2\2w\u01a6\3\2"+
		"\2\2y\u01a8\3\2\2\2{\u01b0\3\2\2\2}\u01bb\3\2\2\2\177\u01bd\3\2\2\2\u0081"+
		"\u01bf\3\2\2\2\u0083\u01c1\3\2\2\2\u0085\u01de\3\2\2\2\u0087\u01e0\3\2"+
		"\2\2\u0089\u01f1\3\2\2\2\u008b\u01f3\3\2\2\2\u008d\u01f5\3\2\2\2\u008f"+
		"\u01f7\3\2\2\2\u0091\u01fa\3\2\2\2\u0093\u01fc\3\2\2\2\u0095\u0096\7\61"+
		"\2\2\u0096\u0097\7\61\2\2\u0097\u009b\3\2\2\2\u0098\u009a\n\2\2\2\u0099"+
		"\u0098\3\2\2\2\u009a\u009d\3\2\2\2\u009b\u0099\3\2\2\2\u009b\u009c\3\2"+
		"\2\2\u009c\u00a2\3\2\2\2\u009d\u009b\3\2\2\2\u009e\u00a0\7\17\2\2\u009f"+
		"\u009e\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a3\7\f"+
		"\2\2\u00a2\u009f\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4"+
		"\u00a5\b\2\2\2\u00a5\4\3\2\2\2\u00a6\u00a7\7\61\2\2\u00a7\u00a8\7,\2\2"+
		"\u00a8\u00ac\3\2\2\2\u00a9\u00ab\13\2\2\2\u00aa\u00a9\3\2\2\2\u00ab\u00ae"+
		"\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ad\u00af\3\2\2\2\u00ae"+
		"\u00ac\3\2\2\2\u00af\u00b0\7,\2\2\u00b0\u00b1\7\61\2\2\u00b1\u00b2\3\2"+
		"\2\2\u00b2\u00b3\b\3\2\2\u00b3\6\3\2\2\2\u00b4\u00b6\t\3\2\2\u00b5\u00b4"+
		"\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8"+
		"\u00b9\3\2\2\2\u00b9\u00ba\b\4\3\2\u00ba\b\3\2\2\2\u00bb\u00bc\7B\2\2"+
		"\u00bc\n\3\2\2\2\u00bd\u00be\7B\2\2\u00be\u00bf\7G\2\2\u00bf\u00c0\7Z"+
		"\2\2\u00c0\u00c1\7C\2\2\u00c1\u00c2\7E\2\2\u00c2\u00c3\7V\2\2\u00c3\f"+
		"\3\2\2\2\u00c4\u00c5\7B\2\2\u00c5\u00c6\7G\2\2\u00c6\u00c7\7C\2\2\u00c7"+
		"\u00c8\7E\2\2\u00c8\u00c9\7J\2\2\u00c9\16\3\2\2\2\u00ca\u00cb\7@\2\2\u00cb"+
		"\u00cc\7@\2\2\u00cc\u00cd\7K\2\2\u00cd\u00ce\7P\2\2\u00ce\u00cf\7K\2\2"+
		"\u00cf\u00d0\7V\2\2\u00d0\20\3\2\2\2\u00d1\u00d2\7k\2\2\u00d2\u00d3\7"+
		"h\2\2\u00d3\22\3\2\2\2\u00d4\u00d5\7v\2\2\u00d5\u00d6\7j\2\2\u00d6\u00d7"+
		"\7g\2\2\u00d7\u00d8\7p\2\2\u00d8\24\3\2\2\2\u00d9\u00da\7g\2\2\u00da\u00db"+
		"\7n\2\2\u00db\u00dc\7u\2\2\u00dc\u00dd\7g\2\2\u00dd\26\3\2\2\2\u00de\u00df"+
		"\7h\2\2\u00df\u00e0\7q\2\2\u00e0\u00e1\7t\2\2\u00e1\30\3\2\2\2\u00e2\u00e3"+
		"\7y\2\2\u00e3\u00e4\7j\2\2\u00e4\u00e5\7k\2\2\u00e5\u00e6\7n\2\2\u00e6"+
		"\u00e7\7g\2\2\u00e7\32\3\2\2\2\u00e8\u00e9\7w\2\2\u00e9\u00ea\7p\2\2\u00ea"+
		"\u00eb\7v\2\2\u00eb\u00ec\7k\2\2\u00ec\u00ed\7n\2\2\u00ed\34\3\2\2\2\u00ee"+
		"\u00ef\7n\2\2\u00ef\u00f0\7g\2\2\u00f0\u00f1\7p\2\2\u00f1\u00f2\7i\2\2"+
		"\u00f2\u00f3\7v\2\2\u00f3\u00f4\7j\2\2\u00f4\36\3\2\2\2\u00f5\u00f6\7"+
		"c\2\2\u00f6\u00f7\7f\2\2\u00f7\u00f8\7f\2\2\u00f8 \3\2\2\2\u00f9\u00fa"+
		"\7y\2\2\u00fa\u00fb\7j\2\2\u00fb\u00fc\7g\2\2\u00fc\u00fd\7t\2\2\u00fd"+
		"\u00fe\7g\2\2\u00fe\"\3\2\2\2\u00ff\u0100\7h\2\2\u0100\u0101\7t\2\2\u0101"+
		"\u0102\7g\2\2\u0102\u0103\7g\2\2\u0103$\3\2\2\2\u0104\u0105\7?\2\2\u0105"+
		"&\3\2\2\2\u0106\u0107\7\60\2\2\u0107(\3\2\2\2\u0108\u0109\7.\2\2\u0109"+
		"*\3\2\2\2\u010a\u010b\7<\2\2\u010b,\3\2\2\2\u010c\u010d\7=\2\2\u010d."+
		"\3\2\2\2\u010e\u010f\7*\2\2\u010f\60\3\2\2\2\u0110\u0111\7+\2\2\u0111"+
		"\62\3\2\2\2\u0112\u0113\7]\2\2\u0113\64\3\2\2\2\u0114\u0115\7_\2\2\u0115"+
		"\66\3\2\2\2\u0116\u0117\7}\2\2\u01178\3\2\2\2\u0118\u0119\7\177\2\2\u0119"+
		":\3\2\2\2\u011a\u011b\7-\2\2\u011b<\3\2\2\2\u011c\u011d\7/\2\2\u011d>"+
		"\3\2\2\2\u011e\u011f\7,\2\2\u011f@\3\2\2\2\u0120\u0121\7\61\2\2\u0121"+
		"B\3\2\2\2\u0122\u0123\7`\2\2\u0123D\3\2\2\2\u0124\u0125\7?\2\2\u0125\u0126"+
		"\7?\2\2\u0126F\3\2\2\2\u0127\u0128\7#\2\2\u0128\u0129\7?\2\2\u0129H\3"+
		"\2\2\2\u012a\u012b\7>\2\2\u012bJ\3\2\2\2\u012c\u012d\7>\2\2\u012d\u012e"+
		"\7?\2\2\u012eL\3\2\2\2\u012f\u0130\7@\2\2\u0130N\3\2\2\2\u0131\u0132\7"+
		"@\2\2\u0132\u0133\7?\2\2\u0133P\3\2\2\2\u0134\u0135\7v\2\2\u0135\u0136"+
		"\7t\2\2\u0136\u0137\7w\2\2\u0137\u0138\7g\2\2\u0138R\3\2\2\2\u0139\u013a"+
		"\7h\2\2\u013a\u013b\7c\2\2\u013b\u013c\7n\2\2\u013c\u013d\7u\2\2\u013d"+
		"\u013e\7g\2\2\u013eT\3\2\2\2\u013f\u0140\7(\2\2\u0140\u0141\7(\2\2\u0141"+
		"V\3\2\2\2\u0142\u0143\7~\2\2\u0143\u0144\7~\2\2\u0144X\3\2\2\2\u0145\u0146"+
		"\7#\2\2\u0146Z\3\2\2\2\u0147\u0148\7^\2\2\u0148\\\3\2\2\2\u0149\u014a"+
		"\7<\2\2\u014a\u014b\7<\2\2\u014b^\3\2\2\2\u014c\u014d\7/\2\2\u014d\u014e"+
		"\7@\2\2\u014e`\3\2\2\2\u014f\u0150\7p\2\2\u0150\u0151\7w\2\2\u0151\u0152"+
		"\7o\2\2\u0152b\3\2\2\2\u0153\u0154\7d\2\2\u0154\u0155\7q\2\2\u0155\u0156"+
		"\7q\2\2\u0156\u0157\7n\2\2\u0157d\3\2\2\2\u0158\u0159\7u\2\2\u0159\u015a"+
		"\7v\2\2\u015a\u015b\7t\2\2\u015b\u015c\7k\2\2\u015c\u015d\7p\2\2\u015d"+
		"\u015e\7i\2\2\u015ef\3\2\2\2\u015f\u0160\7u\2\2\u0160\u0161\7r\2\2\u0161"+
		"\u0162\7g\2\2\u0162\u0163\7e\2\2\u0163\u0164\7k\2\2\u0164\u0165\7g\2\2"+
		"\u0165\u0166\7u\2\2\u0166h\3\2\2\2\u0167\u0168\7r\2\2\u0168\u0169\7c\2"+
		"\2\u0169\u016a\7v\2\2\u016a\u016b\7v\2\2\u016b\u016c\7g\2\2\u016c\u016d"+
		"\7t\2\2\u016d\u016e\7p\2\2\u016ej\3\2\2\2\u016f\u0170\7v\2\2\u0170\u0171"+
		"\7w\2\2\u0171\u0172\7r\2\2\u0172\u0173\7n\2\2\u0173\u0174\7g\2\2\u0174"+
		"l\3\2\2\2\u0175\u0176\7u\2\2\u0176\u0177\7q\2\2\u0177\u0178\7n\2\2\u0178"+
		"n\3\2\2\2\u0179\u017a\7n\2\2\u017a\u017b\7k\2\2\u017b\u017c\7p\2\2\u017c"+
		"\u017d\7m\2\2\u017dp\3\2\2\2\u017e\u0180\5\u008bF\2\u017f\u017e\3\2\2"+
		"\2\u0180\u0181\3\2\2\2\u0181\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182r"+
		"\3\2\2\2\u0183\u0185\5\u008bF\2\u0184\u0183\3\2\2\2\u0185\u0186\3\2\2"+
		"\2\u0186\u0184\3\2\2\2\u0186\u0187\3\2\2\2\u0187\u0188\3\2\2\2\u0188\u018c"+
		"\5\'\24\2\u0189\u018b\5\u008bF\2\u018a\u0189\3\2\2\2\u018b\u018e\3\2\2"+
		"\2\u018c\u018a\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c"+
		"\3\2\2\2\u018f\u0191\5u;\2\u0190\u018f\3\2\2\2\u0190\u0191\3\2\2\2\u0191"+
		"\u019a\3\2\2\2\u0192\u0194\5\u008bF\2\u0193\u0192\3\2\2\2\u0194\u0195"+
		"\3\2\2\2\u0195\u0193\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0197\3\2\2\2\u0197"+
		"\u0198\5u;\2\u0198\u019a\3\2\2\2\u0199\u0184\3\2\2\2\u0199\u0193\3\2\2"+
		"\2\u019at\3\2\2\2\u019b\u019d\t\4\2\2\u019c\u019e\t\5\2\2\u019d\u019c"+
		"\3\2\2\2\u019d\u019e\3\2\2\2\u019e\u01a0\3\2\2\2\u019f\u01a1\5\u008bF"+
		"\2\u01a0\u019f\3\2\2\2\u01a1\u01a2\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a2\u01a3"+
		"\3\2\2\2\u01a3v\3\2\2\2\u01a4\u01a7\5Q)\2\u01a5\u01a7\5S*\2\u01a6\u01a4"+
		"\3\2\2\2\u01a6\u01a5\3\2\2\2\u01a7x\3\2\2\2\u01a8\u01ad\5}?\2\u01a9\u01ac"+
		"\5\u0081A\2\u01aa\u01ac\5\u008bF\2\u01ab\u01a9\3\2\2\2\u01ab\u01aa\3\2"+
		"\2\2\u01ac\u01af\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ad\u01ae\3\2\2\2\u01ae"+
		"z\3\2\2\2\u01af\u01ad\3\2\2\2\u01b0\u01b5\5\177@\2\u01b1\u01b4\5\u0081"+
		"A\2\u01b2\u01b4\5\u008bF\2\u01b3\u01b1\3\2\2\2\u01b3\u01b2\3\2\2\2\u01b4"+
		"\u01b7\3\2\2\2\u01b5\u01b3\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6\u01b9\3\2"+
		"\2\2\u01b7\u01b5\3\2\2\2\u01b8\u01ba\5\u0093J\2\u01b9\u01b8\3\2\2\2\u01b9"+
		"\u01ba\3\2\2\2\u01ba|\3\2\2\2\u01bb\u01bc\t\6\2\2\u01bc~\3\2\2\2\u01bd"+
		"\u01be\t\7\2\2\u01be\u0080\3\2\2\2\u01bf\u01c0\t\b\2\2\u01c0\u0082\3\2"+
		"\2\2\u01c1\u01c6\7)\2\2\u01c2\u01c5\5\u0085C\2\u01c3\u01c5\n\t\2\2\u01c4"+
		"\u01c2\3\2\2\2\u01c4\u01c3\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c4\3\2"+
		"\2\2\u01c6\u01c7\3\2\2\2\u01c7\u01c9\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c9"+
		"\u01ca\7)\2\2\u01ca\u0084\3\2\2\2\u01cb\u01cf\7^\2\2\u01cc\u01d0\t\n\2"+
		"\2\u01ce\u01d0\7^\2\2\u01cf\u01cc\3\2\2\2\u01cf\u01cd\3\2\2\2\u01cf\u01ce"+
		"\3\2\2\2\u01d0\u01df\3\2\2\2\u01d1\u01d2\7\61\2\2\u01d2\u01d3\7,\2\2\u01d3"+
		"\u01d7\3\2\2\2\u01d4\u01d6\13\2\2\2\u01d5\u01d4\3\2\2\2\u01d6\u01d9\3"+
		"\2\2\2\u01d7\u01d8\3\2\2\2\u01d7\u01d5\3\2\2\2\u01d8\u01da\3\2\2\2\u01d9"+
		"\u01d7\3\2\2\2\u01da\u01db\7,\2\2\u01db\u01df\7\61\2\2\u01dc\u01df\5\u0087"+
		"D\2\u01dd\u01df\5\u0089E\2\u01de\u01cb\3\2\2\2\u01de\u01d1\3\2\2\2\u01de"+
		"\u01dc\3\2\2\2\u01de\u01dd\3\2\2\2\u01df\u0086\3\2\2\2\u01e0\u01e1\7^"+
		"\2\2\u01e1\u01e2\7w\2\2\u01e2\u01e3\3\2\2\2\u01e3\u01e4\5\u008dG\2\u01e4"+
		"\u01e5\5\u008dG\2\u01e5\u01e6\5\u008dG\2\u01e6\u01e7\5\u008dG\2\u01e7"+
		"\u0088\3\2\2\2\u01e8\u01e9\7^\2\2\u01e9\u01ea\t\13\2\2\u01ea\u01eb\t\f"+
		"\2\2\u01eb\u01f2\t\f\2\2\u01ec\u01ed\7^\2\2\u01ed\u01ef\t\f\2\2\u01ee"+
		"\u01f0\t\f\2\2\u01ef\u01ee\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01f2\3\2"+
		"\2\2\u01f1\u01e8\3\2\2\2\u01f1\u01ec\3\2\2\2\u01f2\u008a\3\2\2\2\u01f3"+
		"\u01f4\t\r\2\2\u01f4\u008c\3\2\2\2\u01f5\u01f6\t\16\2\2\u01f6\u008e\3"+
		"\2\2\2\u01f7\u01f8\7&\2\2\u01f8\u01f9\5{>\2\u01f9\u0090\3\2\2\2\u01fa"+
		"\u01fb\7%\2\2\u01fb\u0092\3\2\2\2\u01fc\u01fd\7A\2\2\u01fd\u0094\3\2\2"+
		"\2\35\2\u009b\u009f\u00a2\u00ac\u00b7\u0181\u0186\u018c\u0190\u0195\u0199"+
		"\u019d\u01a2\u01a6\u01ab\u01ad\u01b3\u01b5\u01b9\u01c4\u01c6\u01cf\u01d7"+
		"\u01de\u01ef\u01f1\4\2\3\2\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}