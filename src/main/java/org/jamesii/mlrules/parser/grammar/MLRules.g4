/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

grammar MLRules;

import ExpressionsLex, Expressions, Functions;

@header{
package org.jamesii.mlrules.parser.grammar;
}


model : preamble* initialSolution mlrule+ EOF;

preamble
	: speciesDefinition SEMI
	| constant SEMI
	| function
	;

constant : ID COLON expression;


speciesTypeParameters : (TYPE_LINK|TYPE_STRING|TYPE_BOOL|TYPE_REAL);

speciesDefinition 
	: ID_SPECIES L_PAREN R_PAREN (L_BRAKET R_BRAKET)?
	| ID_SPECIES L_PAREN speciesTypeParameters (COMMA speciesTypeParameters)* R_PAREN (L_BRAKET R_BRAKET)?;
initialSolution : INIT L_BRAKET expression R_BRAKET SEMI;

mlrule : reactants ARROW products (AT | ATTIME | ATEACH) rate where? SEMI;

reactants : species (PLUS species)*;

products : expression?;

rate : expression;
