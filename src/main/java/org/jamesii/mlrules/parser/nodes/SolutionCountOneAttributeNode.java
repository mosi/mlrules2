package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.Map;

public class SolutionCountOneAttributeNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String SOL = "§sol";

  public static final String NAME = "§name";
  
  public static final String ATT = "§att";
  
  public static final String POS = "§pos";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object s = env.getValue(SOL);
      Object n = env.getValue(NAME);
      Object att = env.getValue(ATT);
      Object pos = env.getValue(POS);
      if (s instanceof Map<?, ?> && n instanceof String) {
        Map<?, ?> list = (Map<?, ?>) s;
        String name = (String) n;
        int position = ((Number) pos).intValue();
        double num = 0D;
        for (Object species : list.keySet()) {
          Species tmp = (Species) species;
          if (tmp.getType().getName().equals(name) && tmp.getAttribute(position).equals(att)) {
            num += ((Species) species).getAmount();
          }
        }
        return (N) new ValueNode<Double>(num);
    }
    throw new IllegalArgumentException(String.format(
        "Could not compute the number of species %s with attribute %s at position %s", n, att, pos));
      }
      throw new IllegalArgumentException("could not count number of species with specific attribute");
  }

  @Override
  public String toString() {
    return "countAtt1()";
  }

}
