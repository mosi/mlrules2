/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.math.AddNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@link MLRulesAddNode} is responsible for the summation of solutions.
 * Note that the arguments are immutable and a new result solution is created.
 * 
 * @author Tobias Helms
 *
 */
public class MLRulesAddNode extends AddNode {

  private static final long serialVersionUID = 1L;

  public MLRulesAddNode(Node left, Node right) {
    super(left, right);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    Node leftResult = getLeft().calc(cEnv);
    Node rightResult = getRight().calc(cEnv);

    if (leftResult instanceof ValueNode<?> && rightResult instanceof ValueNode<?>) {
      if (((ValueNode<?>) leftResult).getValue() instanceof Map<?, ?>
          && ((ValueNode<?>) rightResult).getValue() instanceof Map<?, ?>) {
        Map<Species, Species> leftSpecies = (Map<Species, Species>) ((ValueNode<?>) leftResult).getValue();
        Map<Species, Species> rightSpecies = (Map<Species, Species>) ((ValueNode<?>) rightResult).getValue();
        Map<Species, Species> resultSpecies = new HashMap<>(leftSpecies);
        rightSpecies.keySet().stream().forEach(s -> {
          Species species = resultSpecies.computeIfAbsent(s, s2 -> s2);
          if (species != s) {
            ((LeafSpecies) species).setAmount(species.getAmount() + s.getAmount());
          }
        });
        return (N) new ValueNode<Map<Species, Species>>(resultSpecies);
      }
      return (N) super.calc((ValueNode<?>) leftResult, (ValueNode<?>) rightResult);
    }
    return super.calc(cEnv);
  }

}
