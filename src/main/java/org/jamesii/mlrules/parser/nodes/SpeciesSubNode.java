/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.Collections;
import java.util.Map;

/**
 * Return the sub solution of the given species. None compartments return empty solutions.
 * 
 * @author Tobias Helms
 *
 */
public class SpeciesSubNode extends Node {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "§species";

	@SuppressWarnings("unchecked")
	@Override
	public <N extends INode> N calc(IEnvironment<?> cEnv) {
		if (cEnv instanceof MLEnvironment) {
			MLEnvironment env = (MLEnvironment) cEnv;
			Object n = env.getValue(NAME);
			if (n instanceof Map<?, ?>) {
				Map<?, ?> species = (Map<?, ?>) n;
				if (species.size() != 1) {
					throw new IllegalArgumentException(
					    String.format("The name function is only applicable for one species and not for %s.", species));
				}
				Object o = species.entrySet().iterator().next().getKey();
				if (o instanceof LeafSpecies) {
					return (N) new ValueNode<Map<Species, Species>>(Collections.emptyMap());
				} else if (o instanceof Compartment) {
					return (N) new ValueNode<Map<Species, Species>>(((Compartment) o).getAllSpecies());
				}
			}
		}
		throw new IllegalArgumentException(
		    String.format("Could not compute the sub species of the species bound to %s", NAME));

	}

	@Override
	public String toString() {
		return "sub()";
	}

}
