package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.Map;

public class EmptyNode extends Node {

  public static final String SOL = "§sol";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object s = env.getValue(SOL);
      if (s instanceof Map<?, ?>) {
        Map<?, ?> list = (Map<?, ?>) s;
        return (N) new ValueNode<>(list.isEmpty());
      }
    }
    throw new IllegalArgumentException(
            "the given environment to calculate whether a solution is empty is not an MLEnvironment");
  }


}
