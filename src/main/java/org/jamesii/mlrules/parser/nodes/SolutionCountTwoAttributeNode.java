package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.Map;

public class SolutionCountTwoAttributeNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String SOL = "§sol";

  public static final String NAME = "§name";
  
  public static final String ATT1 = "§att1";
  
  public static final String POS1 = "§pos1";

  public static final String ATT2 = "§att2";

  public static final String POS2 = "§pos2";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object s = env.getValue(SOL);
      Object n = env.getValue(NAME);
      Object att1 = env.getValue(ATT1);
      Object pos1 = env.getValue(POS1);
      Object att2 = env.getValue(ATT2);
      Object pos2 = env.getValue(POS2);
      if (s instanceof Map<?, ?> && n instanceof String) {
        Map<?, ?> list = (Map<?, ?>) s;
        String name = (String) n;
        int position1 = ((Number) pos1).intValue();
        int position2 = ((Number) pos2).intValue();
        double num = 0D;
        for (Object species : list.keySet()) {
          Species tmp = (Species) species;
          if (tmp.getType().getName().equals(name) && tmp.getAttribute(position1).equals(att1) && tmp.getAttribute(position2).equals(att2)) {
            num += ((Species) species).getAmount();
          }
        }
        return (N) new ValueNode<Double>(num);
    }
    throw new IllegalArgumentException(String.format(
        "Could not compute the number of species %s with attribute %s at position %s and with attribute %s at position %s", n, att1, pos1, att2, pos2));
      }
      throw new IllegalArgumentException("could not count number of species with specific attribute");
  }

  @Override
  public String toString() {
    return "countAtt2()";
  }

}
