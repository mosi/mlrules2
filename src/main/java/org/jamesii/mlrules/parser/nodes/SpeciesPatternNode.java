/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.types.Tuple;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.*;

/**
 * A {@link SpeciesPatternNode} represents the pattern for a species consisting
 * of an amount node, a type node, nodes for the attributes of the species, and
 * nodes for the sub solution of the species. Further, an optional string for
 * the bound variable used in reactants can be set. The
 * {@link SpeciesPatternNode} contains a method to transform itself to a
 * {@link Reactant}.
 *
 * @author Tobias Helms
 */
public class SpeciesPatternNode extends Node {

  private static final long serialVersionUID = 1L;

  private final Node amountNode;

  private final Node typeNode;

  private final Node[] attributeNodes;

  private final Node subNode;

  private final Optional<String> boundTo;

  @SuppressWarnings("unchecked")
  private String createReactants(MLRulesAddNode node, List<Reactant> reactants, MLEnvironment env) {
    if (node.getLeft() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getLeft()).toReactant(env));
    } else if (node.getLeft() instanceof MLRulesAddNode) {
      createReactants((MLRulesAddNode) node.getLeft(), reactants, env);
    }
    if (node.getRight() instanceof SpeciesPatternNode) {
      reactants.add(((SpeciesPatternNode) node.getRight()).toReactant(env));
    } else if (node.getRight() instanceof MLRulesAddNode) {
      return createReactants((MLRulesAddNode) node.getRight(), reactants, env);
    } else if (node.getRight() instanceof Identifier<?>) {
      return ((Identifier<String>) node.getRight()).getIdent();
    }
    return "$sol?";
  }

  public SpeciesPatternNode(Node amount, Node type, Node[] attributes, Node subNode, Optional<String> boundTo) {
    this.amountNode = amount;
    this.typeNode = type;
    this.attributeNodes = attributes;
    this.subNode = subNode;
    this.boundTo = boundTo;
  }

  @Override
  public List<Node> getChildren() {
    List<Node> result = Arrays.asList(amountNode);
    if (subNode != null) {
      result.add(subNode);
    }
    result.add(typeNode);
    result.addAll(Arrays.asList(attributeNodes));
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    MLEnvironment env = (MLEnvironment) cEnv;

    // if (!boundTo.startsWith("$")) {
    // return (N) this;
    // }

    Node tmp = typeNode.calc(cEnv);
    String name = null;
    if (tmp instanceof ValueNode<?> && ((ValueNode<?>) tmp).getValue() instanceof String) {
      name = (String) ((ValueNode<?>) tmp).getValue();
    }
    SpeciesType type = (SpeciesType) env.getValue(name);
    if (type == null) {
      throw new IllegalArgumentException(String.format("Unknown species %s cannot be created.", name));
    }

    tmp = amountNode.calc(cEnv);
    double amount;
    if (tmp instanceof ValueNode<?>) {
      if (((ValueNode<?>) tmp).getValue() instanceof Double) {
        amount = ((ValueNode<Double>) tmp).getValue();
        if (type.isCompartment() && Math.rint(amount) != amount) {
          throw new IllegalArgumentException(String.format("Compartment %s is not allowed having real valued amount %f", type.toString(), amount));
        }
      } else {
        amount = ((ValueNode<Integer>) tmp).getValue();
      }
    } else {
      return (N) this;
    }

    if (amount < 0) {
      return (N) new ValueNode<Map<Species, Species>>(new HashMap<>());
    }


    Object[] attributes = new Object[type.getAttributesSize()];
    for (int i = 0; i < attributeNodes.length; ++i) {
      tmp = attributeNodes[i].calc(cEnv);
      if (tmp instanceof ValueNode<?> && ((ValueNode<?>) tmp).getValue() != null
              && ((ValueNode<?>) tmp).getValue() instanceof Tuple) {
        Tuple values = (Tuple) ((ValueNode<?>) tmp).getValue();
        if (attributeNodes.length != 1 || values.size() != attributes.length) {
          throw new IllegalArgumentException(String.format("invalid number of attributes for %s", type.getName()));
        }
        for (int j = 0; j < values.size(); ++j) {
          attributes[j] = values.get(j);
          if (attributes[j] == null) {
            throw new IllegalArgumentException(
                    String.format("attribute %s of species type %s is null", j, type.getName()));
          }
        }
      } else if (tmp instanceof ValueNode<?>) {
        attributes[i] = ((ValueNode<?>) tmp).getValue();
        if (attributes[i] == null) {
          throw new IllegalArgumentException(
                  String.format("attribute %s of species type %s is null", i, type.getName()));
        }
      } else {
        return (N) this;
      }
    }

    Map<SpeciesType, Map<LeafSpecies, LeafSpecies>> subSpecies = new HashMap<>();
    Map<SpeciesType, Set<Compartment>> subCompartments = new HashMap<>();
    if (subNode != null) {
      Node result = subNode.calc(cEnv);
      if (result instanceof ValueNode<?> && ((ValueNode<?>) result).getValue() instanceof Map<?, ?>) {
        ((Map<Species, Species>) ((ValueNode<?>) result).getValue()).forEach((k, v) -> {
          Species copy = k.copy();
          if (copy.getType()
                  .isCompartment()) {
            subCompartments.computeIfAbsent(copy.getType(), key -> new HashSet<>())
                           .add((Compartment) copy);
          } else {
            subSpecies.computeIfAbsent(copy.getType(), key -> new HashMap<>())
                      .put((LeafSpecies) copy,
                              (LeafSpecies) copy);
          }
        });
        if (!type.isCompartment() && (!subCompartments.isEmpty() || !subSpecies.isEmpty())) {
          throw new IllegalArgumentException(
                  String.format("leaf species %s is not allowed to have sub species", type.getName()));
        }
      } else {
        return (N) this;
      }
    }

    Map<Species, Species> resultMap = new HashMap<>();
    Species newSpecies;
    if (type.isCompartment()) {
      newSpecies = new Compartment(type, attributes, Compartment.UNKNOWN, subSpecies, subCompartments);
      ((Compartment) newSpecies).getAllSubSpeciesStream()
                                .forEach(s -> s.setContext((Compartment) newSpecies));
      for (int i = 0; i < amount; ++i) {
        Species copy = newSpecies.copy();
        resultMap.put(copy, copy);
      }
    } else {
      newSpecies = new LeafSpecies(type, attributes, Compartment.UNKNOWN, amount);
      resultMap.put(newSpecies, newSpecies);
    }

    return (N) new ValueNode<Map<Species, Species>>(resultMap);
  }

  @SuppressWarnings("unchecked")
  public Reactant toReactant(MLEnvironment env) {
    Node tmp = typeNode.calc(env);
    String name = null;
    if (tmp instanceof ValueNode<?> && ((ValueNode<?>) tmp).getValue() instanceof String) {
      name = (String) ((ValueNode<?>) tmp).getValue();
    }
    SpeciesType type = (SpeciesType) env.getValue(name);

    List<Reactant> subReactants = new ArrayList<>();
    String rest = null;
    if (subNode instanceof SpeciesPatternNode) {
      subReactants.add(((SpeciesPatternNode) subNode).toReactant(env));
    } else if (subNode instanceof MLRulesAddNode) {
      rest = createReactants((MLRulesAddNode) subNode, subReactants, env);
    } else if (subNode instanceof Identifier<?>) {
      rest = ((Identifier<String>) subNode).getIdent();
    }

    if (type == null) {
      throw new IllegalArgumentException("Unknown species type " + name);
    }

    if (!type.isCompartment() && rest != null) {
      throw new IllegalArgumentException(
              String.format("leaf species type %s cannot have a rest solution", type.getName()));
    }

    // test if amount is double and throw exception for compartments
    Node result = amountNode.calc(env);
    if (result instanceof ValueNode<?>) {
      ValueNode<?> vn = (ValueNode<?>) result;
      if (vn.getValue() instanceof Double && type.isCompartment()) {
        double amount = (Double) vn.getValue();
        if (Math.rint(amount) != amount) {
          throw new IllegalArgumentException(String.format("compartment %s is not allowed to have real valued amount", type.getName()));
        }
      }
    }

    return new Reactant(type, amountNode, Arrays.asList(attributeNodes), Optional.ofNullable(rest), boundTo,
            subReactants);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(amountNode.toString());
    builder.append(typeNode.toString());
    StringJoiner joiner = new StringJoiner(",", "(", ")");
    for (Node n : attributeNodes) {
      joiner.add(n.toString());
    }
    builder.append(joiner.toString());
    if (subNode != null) {
      builder.append("[");
      builder.append(subNode.toString());
      builder.append("]");
    }
    builder.append(boundTo.isPresent() ? ":" + boundTo.get() : "");
    return builder.toString();
  }
}
