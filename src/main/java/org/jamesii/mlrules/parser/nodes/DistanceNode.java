/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.util.MLEnvironment;

/**
 * Utility function to calculate the distance of two points (x1,y1) and (x2,y2).
 * 
 * @author Tobias Helms
 *
 */
public class DistanceNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String X1 = "§x1";

  public static final String X2 = "§x2";

  public static final String Y1 = "§y1";

  public static final String Y2 = "§y2";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Double x1 = (Double) env.getValue(X1);
      Double x2 = (Double) env.getValue(X2);
      Double y1 = (Double) env.getValue(Y1);
      Double y2 = (Double) env.getValue(Y2);
      if (x1 == null || x2 == null || y1 == null || y2 == null) {
        throw new IllegalArgumentException(String
            .format("invalid coordinates (%s,%s) and (%s,%s)", x1, y1, x2, y2));
      }
      return (N) new ValueNode<Double>(
          Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2)));
    }
    throw new IllegalArgumentException(
        "the given environment to calculate the distance of two coordinates is not an MLEnvironment");
  }

}
