package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tobias Helms
 */
public class AllBooleanCombinations extends Node {

  private static final long serialVersionUID = 1L;

  public static final String NAME = "§name";

  private Species calc(int i, SpeciesType type) {
    String b = String.format("%" + type.getAttributesSize() + "s", Integer.toBinaryString(i)).replace(' ', '0');
    Object[] atts = new Object[type.getAttributesSize()];
    for (int j = 0; j < type.getAttributesSize(); ++j) {
      if (j < b.length()) {
        atts[j] = b.charAt(j) != '0';
      } else {
        atts[j] = false;
      }
    }
    return new LeafSpecies(type, atts, null, 0D);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      String n = (String) env.getValue(NAME);
      SpeciesType x = (SpeciesType) env.getValue(n);
      if (!x.isCompartment()) {
        Map<Species,Species> result = new HashMap<>();
      for (int i = 0; i < Math.pow(2,x.getAttributesSize()); ++i) {
        Species s = calc(i, x);
        result.put(s,s);
      }
      return (N) new ValueNode<>(result);
      }
    }
    throw new IllegalArgumentException(
            "the given environment to calculate the cosine of an angle is not an MLEnvironment");
  }


}
