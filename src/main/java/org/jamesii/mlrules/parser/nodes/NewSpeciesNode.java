/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.parser.types.Tuple;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.HashMap;
import java.util.Map;

/**
 * Create a new species with the given amount, name and attributes. Sub
 * solutions are not supported.
 * 
 * @author Tobias Helms
 *
 */
public class NewSpeciesNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String AMOUNT = "§amount";

  public static final String NAME = "§name";

  public static final String ATTS = "§atts";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object n = env.getValue(NAME);
      if (n instanceof String) {
        String name = (String) n;
        n = env.getValue(name);
        if (n instanceof SpeciesType) {
          SpeciesType type = (SpeciesType) n;
          n = env.getValue(AMOUNT);
          if (n instanceof Number) {
            double amount = ((Number) n).doubleValue();
            if (amount > 0) {
              n = env.getValue(ATTS);
              if (n instanceof Tuple) {
                Tuple atts = (Tuple) n;
                if (atts.size() == type.getAttributesSize()) {
                  Object[] attsArray = new Object[atts.size()];
                  for (int i = 0; i < attsArray.length; ++i) {
                    attsArray[i] = atts.get(i);
                  }
                  Map<Species, Species> result = new HashMap<>();
                  Species s = new LeafSpecies(type, attsArray, Compartment.UNKNOWN, amount);
                  result.put(s, s);
                  return (N) new ValueNode<Map<Species, Species>>(result);
                }
              }
            } else if (Double.compare(amount, 0D) == 0) {
              return (N) new ValueNode<Map<Species, Species>>(new HashMap<>());
            }
          }
        }
      }
    }
    throw new IllegalArgumentException(
        String.format("Could not compute the name of the species bound to %s %s %s", AMOUNT, NAME, ATTS));

  }

  public String toString() {
    return "new()";
  }

}
