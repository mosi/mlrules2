/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.Map;

/**
 * Return the amount of the given species.
 *
 * @author Tobias Helms
 */
public class SpeciesAmountNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String NAME = "§species";

  private String otherName = null;

  public SpeciesAmountNode(String otherName) {
    this.otherName = otherName;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Object n = env.getValue(otherName == null ? NAME : otherName);
      if (n instanceof Map<?, ?>) {
        Map<?, ?> list = (Map<?, ?>) n;
        double sum = 0;
        for (Object o : list.values()) {
          if (o instanceof Species) {
            sum += ((Species) o).getAmount();
          }
        }
        return (N) new ValueNode<>(sum);

      } else if (n instanceof LeafSpecies) {
        return (N) new ValueNode<>(((LeafSpecies) n).getAmount());
      } else if (n instanceof Compartment) {
        return (N) new ValueNode<>(1D);
      }
    }
    throw new IllegalArgumentException(String.format("Could not compute the amount of the species bound to %s", NAME));
  }

  @Override
  public String toString() {
    return "num()";
  }

  public String getOtherName() {
    return otherName;
  }

}
