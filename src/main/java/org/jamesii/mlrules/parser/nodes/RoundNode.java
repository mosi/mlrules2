/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.nodes;

import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.variables.IEnvironment;
import org.jamesii.mlrules.util.MLEnvironment;

/**
 * Call {@link Math#round(double)} with the given value.
 * 
 * @author Tobias Helms
 */
public class RoundNode extends Node {

  private static final long serialVersionUID = 1L;

  public static final String VAL = "§val";

  @SuppressWarnings("unchecked")
  @Override
  public <N extends INode> N calc(IEnvironment<?> cEnv) {
    if (cEnv instanceof MLEnvironment) {
      MLEnvironment env = (MLEnvironment) cEnv;
      Double x1 = ((Number) env.getValue(VAL)).doubleValue();
      return (N) new ValueNode<Double>(Double.valueOf(Math.round(x1)));
    }
    throw new IllegalArgumentException("the given environment to round the given value is not an MLEnvironment");
  }

}
