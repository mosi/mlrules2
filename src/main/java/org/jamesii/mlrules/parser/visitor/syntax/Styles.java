/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.visitor.syntax;

import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import java.awt.*;

/**
 * The styles of the syntax highlight.
 * 
 * @author Tobias Helms
 *
 */
public class Styles {

  public static final String BOLD = "bold";

  public static final String BOOL = "bool";

  public static final String KEY = "key";
  
  public static final String STRING = "string";
  
  public static final String CONST = "const";
  
  public static final String FUNCTION = "function";
  
  public static final String SPECIES = "species";
  
  public static final String COMMENT = "comment";
  
  public static final String ERROR = "error";

  private Styles() {
  }

  private static void addBold(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(BOLD, null);
    StyleConstants.setBold(style, true);
    doc.addStyle(BOLD, style);
  }
  
  private static void addBool(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(BOOL, null);
    StyleConstants.setForeground(style, Color.MAGENTA);
    StyleConstants.setBold(style, true);
    doc.addStyle(BOOL, style);
  }
  
  private static void addKey(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(KEY, null);
    StyleConstants.setForeground(style, Color.BLUE);
    StyleConstants.setBold(style, true);
    doc.addStyle(KEY, style);
  }
  
  private static void addString(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(STRING, null);
    StyleConstants.setForeground(style, Color.BLUE);
    doc.addStyle(STRING, style);
  }
  
  private static void addConst(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(CONST, null);
    StyleConstants.setForeground(style, new Color(0,100,0));
    doc.addStyle(CONST, style);
  }
  
  private static void addFunction(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(FUNCTION, null);
    StyleConstants.setForeground(style, Color.RED);
    doc.addStyle(FUNCTION, style);
  }
  
  private static void addSpecies(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(SPECIES, null);
    StyleConstants.setForeground(style, new Color(172,14,14));
    StyleConstants.setBold(style, true);
    doc.addStyle(SPECIES, style);
  }
  
  private static void addComment(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(COMMENT, null);
    StyleConstants.setForeground(style, Color.GRAY);
//    StyleConstants.setBold(style, true);
    doc.addStyle(COMMENT, style);
  }
  
  private static void addError(StyledDocument doc) {
    StyleContext sc = new StyleContext();
    Style style = sc.addStyle(ERROR, null);
    StyleConstants.setBold(style, true);
    StyleConstants.setForeground(style, Color.RED);
    StyleConstants.setUnderline(style, true);
    doc.addStyle(ERROR, style);
  }

  public static void addStyles(StyledDocument doc) {
    addBold(doc);
    addBool(doc);
    addKey(doc);
    addString(doc);
    addConst(doc);
    addFunction(doc);
    addSpecies(doc);
    addComment(doc);
    addError(doc);
  }

}
