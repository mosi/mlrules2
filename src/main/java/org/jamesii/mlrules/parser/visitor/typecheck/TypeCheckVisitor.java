/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.visitor.typecheck;

import org.antlr.v4.runtime.ParserRuleContext;
import org.jamesii.mlrules.parser.exception.DelayedEvaluationException;
import org.jamesii.mlrules.parser.exception.SemanticsException;
import org.jamesii.mlrules.parser.grammar.MLRulesBaseVisitor;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.types.BaseType;
import org.jamesii.mlrules.parser.types.FunctionType;
import org.jamesii.mlrules.parser.types.Type;
import org.jamesii.mlrules.parser.visitor.typecheck.util.TypeSymbolTable;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Andreas Ruscheinski
 *
 */
public class TypeCheckVisitor extends MLRulesBaseVisitor<Type> {
  TypeSymbolTable symbolTable = null;

  private boolean inPreamble = false;

  private boolean inSpecies = false;

  private boolean inProduct = false;

  private boolean inSubSpecies = false;

  public TypeCheckVisitor() {
    this.symbolTable = new TypeSymbolTable(this);
  }

  public TypeSymbolTable getSymbolTable() {
    return symbolTable;
  }

  @Override
  public Type visitModel(MLRulesParser.ModelContext ctx) {
    // visit preambles
    inPreamble = true;
    for (MLRulesParser.PreambleContext preamble : ctx.preamble()) {
      this.visitPreamble(preamble);
    }
    inPreamble = false;
    inSubSpecies = false;
    inSpecies = false;
    // try to evaluate all delayed expressions
    symbolTable.update();
    // System.out.println(symbolTable);
    // revisit left delayed expressions for throwing exception
    for (ParserRuleContext unhandeldContext : symbolTable.getUnhandeldContexts()) {
      this.visit(unhandeldContext);
    }
    // visit initial solution
    this.visitInitialSolution(ctx.initialSolution());
    // visit every rule
    for (MLRulesParser.MlruleContext rule : ctx.mlrule()) {
      this.visitMlrule(rule);
    }
    return null;
  }

  @Override
  public Type visitPreamble(MLRulesParser.PreambleContext ctx) {
    if (ctx.constant() != null) {
      this.visitConstant(ctx.constant());
    } else if (ctx.function() != null) {
      this.visitFunction(ctx.function());
    } else if (ctx.speciesDefinition() != null) {
      this.visitSpeciesDefinition(ctx.speciesDefinition());
    } else {
      throw new SemanticsException(ctx, String.format("can't handle preamble %s", ctx.getText()));
    }
    return null;
  }

  @Override
  public Type visitConstant(MLRulesParser.ConstantContext ctx) {
    String constantName = ctx.ID().getText();
    // check if constantName is already defined
    if (this.symbolTable.containsVariable(constantName)
        || this.symbolTable.containsPossibleDefinedVariable(constantName)) {
      throw new SemanticsException(ctx, String.format("constant %s is already defineed", constantName));
    }
    Type constantType = null;
    try {
      constantType = this.visit(ctx.expression());
    } catch (DelayedEvaluationException e) {
      // expression can't be evaluated, try delayed evaluation after all
      // preambles
      symbolTable.addUnhandeldContext(ctx);
      this.symbolTable.addPossibleDefinedVariable(constantName);
      return null;
    }
    // can't get constant type or type is unknown
    if (constantType == null || constantType == BaseType.UNKNOWN) {
      throw new SemanticsException(ctx.expression(), String.format("can't get type of %s", ctx.expression().getText()));
    }
    this.symbolTable.defineVariable(constantName, constantType);
    return null;
  }

  @Override
  public Type visitSpeciesTypeParameters(MLRulesParser.SpeciesTypeParametersContext ctx) {
    Type result = null;
    if (ctx.TYPE_BOOL() != null) {
      result = BaseType.BOOL;
    } else if (ctx.TYPE_LINK() != null) {
      result = BaseType.LINK;
    } else if (ctx.TYPE_REAL() != null) {
      result = BaseType.NUM;
    } else if (ctx.TYPE_STRING() != null) {
      result = BaseType.STRING;
      // }
      // else if (ctx.TYPE_STRING_SET() != null) {
      // String text = ctx.TYPE_STRING_SET().getText();
      // // find elements for string set
      // List<String> elems = new ArrayList<String>();
      // Pattern pattern = Pattern.compile("\\p{Alnum}*");
      // Matcher matcher = pattern.matcher(text);
      // while (matcher.find()) {
      // String found = matcher.group();
      // if (!found.equals("")) {
      // elems.add(matcher.group());
      // }
      // }
      // result = new StringSetType(elems);
    } else {
      throw new SemanticsException(ctx, String.format("invalid type %s", ctx.getText()));
    }
    return result;
  }

  @Override
  public Type visitSpeciesDefinition(MLRulesParser.SpeciesDefinitionContext ctx) {
    String speciesName = ctx.ID_SPECIES().getText();
    List<Type> speciesTypes = new ArrayList<Type>();
    if (this.symbolTable.containsSpecies(speciesName)) {
      throw new SemanticsException(ctx, String.format("species %s is already defined", speciesName));
    }
    // visit species types
    for (MLRulesParser.SpeciesTypeParametersContext typeParameter : ctx.speciesTypeParameters()) {
      Type parameterType = this.visit(typeParameter);
      speciesTypes.add(parameterType);
    }
    this.symbolTable.defineSpecies(speciesName, speciesTypes);
    return null;

  }

  @Override
  public Type visitInitialSolution(MLRulesParser.InitialSolutionContext ctx) {
    Type expType = this.visit(ctx.expression());
    if (expType != BaseType.SPECIES && expType != BaseType.SOL) {
      throw new SemanticsException(ctx,
          String.format("initial solution must be Species or Solution but %s given", expType));
    }
    return null;
  }

  @Override
  public Type visitMlrule(MLRulesParser.MlruleContext ctx) {
    // System.out.println("VIsit Ml Rule: " + ctx.getText());
    // TODO: ask Tobi for scope of where expression
    this.symbolTable.pushScope();
    this.visit(ctx.reactants());
    if (ctx.where() != null) {
      this.visit(ctx.where());
    }
    inProduct = true;
    this.visit(ctx.products());
    inProduct = false;

    this.visit(ctx.rate());
    this.symbolTable.popScope();
    return null;
  }

  @Override
  public Type visitReactants(MLRulesParser.ReactantsContext ctx) {
    for (MLRulesParser.SpeciesContext species : ctx.species()) {
      // System.out.println("Visit Reactant: " + ctx.getText());
      this.visit(species);
    }
    return null;
  }

  @Override
  public Type visitProducts(MLRulesParser.ProductsContext ctx) {
    // TODO
    if (ctx.expression() != null) {
      Type expType = this.visit(ctx.expression());
    }
    return null;
  }

  @Override
  public Type visitRate(MLRulesParser.RateContext ctx) {
    // TODO
    Type rateType = this.visit(ctx.expression());
    return null;
  }

  @Override
  public Type visitBoolExpr(MLRulesParser.BoolExprContext ctx) {
    Type left = this.visit(ctx.getChild(1));
    Type right = this.visit(ctx.getChild(3));
    Type result = null;
    try {
      result = Type.comparison(left, right);
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
    return result;
  }

  @Override
  public Type visitSpeciesExpr(MLRulesParser.SpeciesExprContext ctx) {
    boolean oldInSpecies = inSpecies;
    boolean oldinSubSpceies = inSubSpecies;
    Type speciesExpressionType = this.visit(ctx.species());
    if (speciesExpressionType != BaseType.SPECIES) {
      throw new SemanticsException(ctx, String.format("wrong species type %s", speciesExpressionType));
    }
    inSpecies = oldInSpecies;
    inSubSpecies = oldinSubSpceies;
    // TODO TESt
    return BaseType.SOL;
  }

  @Override
  public Type visitEmptySolution(MLRulesParser.EmptySolutionContext ctx) {
    return BaseType.SOL;
  }

  @Override
  public Type visitMultDiv(MLRulesParser.MultDivContext ctx) {
    Type left = this.visit(ctx.getChild(0));
    Type right = this.visit(ctx.getChild(2));
    Type result = null;
    try {
      result = Type.arithmetic(left, right);
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
    return result;
  }

  @Override
  public Type visitTuple(MLRulesParser.TupleContext ctx) {
    // System.out.println(ctx.getText());
    return BaseType.TUPLE;
  }

  @Override
  public Type visitExpValue(MLRulesParser.ExpValueContext ctx) {
    return this.visit(ctx.value());
  }

  @Override
  public Type visitMinusOne(MLRulesParser.MinusOneContext ctx) {
    Type inner = this.visit(ctx.expression());
    return inner;
  }

  @Override
  public Type visitNot(MLRulesParser.NotContext ctx) {
    Type inner = this.visit(ctx.expression());
    if (inner != BaseType.BOOL && inner != BaseType.UNKNOWN) {
      throw new SemanticsException(ctx, String.format("wrong type %s for not operator", inner.toString()));
    }
    return inner;
  }

  @Override
  public Type visitRoof(MLRulesParser.RoofContext ctx) {
    Type left = this.visit(ctx.getChild(0));
    Type right = this.visit(ctx.getChild(3));
    Type result = null;
    try {
      result = Type.arithmetic(left, right);
    } catch (IllegalArgumentException ex) {
      throw new SemanticsException(ctx, ex.getMessage());
    }
    return result;

  }

  @Override
  public Type visitCountShort(MLRulesParser.CountShortContext ctx) {
    Type result = this.symbolTable.getVariableType(ctx.ID().getText());
    if (result == null) {
      throw new SemanticsException(ctx, String.format("count expression %s not defined", ctx.ID().getText()));
    }
    if (result != BaseType.SPECIES) {
      throw new SemanticsException(ctx, String.format("wrong type of count expresion %s", ctx.ID().getText()));
    }
    return BaseType.NUM;
  }

  @Override
  public Type visitIfThenElse(MLRulesParser.IfThenElseContext ctx) {
    Type ifType = this.visit(ctx.getChild(1));
    Type thenType = this.visit(ctx.getChild(3));
    Type elseType = this.visit(ctx.getChild(5));
    // check for wrong type of if expression
    if (ifType != BaseType.BOOL && ifType != BaseType.UNKNOWN) {
      throw new SemanticsException(ctx, String.format("invalid condition type %s", ifType));
    }
    // makes type of else- and then-expression equal
    if ((elseType == BaseType.SOL && thenType == BaseType.SPECIES)
        || (elseType == BaseType.SPECIES && thenType == BaseType.SOL)) {
      elseType = BaseType.SOL;
      thenType = BaseType.SOL;
    }
    // check for type of else- and then-expressio is unknown or not equal
    if (thenType != BaseType.UNKNOWN && elseType != BaseType.UNKNOWN && !thenType.equals(elseType)) {
      throw new SemanticsException(ctx,
          String.format("types of then and else part are not equal: %s vs. %s", thenType, elseType));
    }
    Type result = (thenType == BaseType.UNKNOWN || elseType == BaseType.UNKNOWN) ? BaseType.UNKNOWN : thenType;
    return result;

  }

  @Override
  public Type visitFunctionCall(MLRulesParser.FunctionCallContext ctx) {
    List<Type> givenParameterTypes = new ArrayList<Type>();
    for (MLRulesParser.ExpressionContext expression : ctx.expression()) {
      Type givenParameter = this.visit(expression);
      givenParameterTypes.add(givenParameter);
    }
    List<Type> expectedParameter = this.symbolTable.getFunctionParameterTypes(ctx.ID().getText());
    if (expectedParameter == null) {
      if (inPreamble) {
        throw new DelayedEvaluationException();
      }
      throw new SemanticsException(ctx, String.format("function %s is not defined", ctx.ID().getText()));
    } else if (!isTypeListEqual(givenParameterTypes, expectedParameter)) {
      // System.out.println("Failure: " + ctx.getText());
      throw new SemanticsException(ctx,
          String.format("cant match the given parameter %s types with the expected parameter types %s",
              givenParameterTypes, expectedParameter));
    }
    return this.symbolTable.getFunctionResultType(ctx.ID().getText());

  }

  @Override
  public Type visitPlus(MLRulesParser.PlusContext ctx) {
    Type left = this.visit(ctx.getChild(0));
    Type right = this.visit(ctx.getChild(2));
    Type result = null;
    try {
      result = Type.plus(left, right);
    } catch (IllegalArgumentException ex) {
      throw new SemanticsException(ctx, ex.getMessage());
    }
    return result;
  }

  @Override
  public Type visitApplication(MLRulesParser.ApplicationContext ctx) {
    // TODO Auto-generated method stub
    throw new SemanticsException(ctx, "cant handle " + ctx.getClass());
  }

  @Override
  public Type visitAndOr(MLRulesParser.AndOrContext ctx) {
    Type left = this.visit(ctx.getChild(0));
    Type right = this.visit(ctx.getChild(2));
    Type result = null;
    try {
      result = Type.bool(left, right);
    } catch (IllegalArgumentException e) {
      throw new SemanticsException(ctx, e.getMessage());
    }
    return result;
  }

  @Override
  public Type visitMinus(MLRulesParser.MinusContext ctx) {
    Type left = this.visit(ctx.getChild(0));
    Type right = this.visit(ctx.getChild(2));
    Type result = null;
    try {
      result = Type.plus(left, right);
    } catch (IllegalArgumentException ex) {
      throw new SemanticsException(ctx, ex.getMessage());
    }
    return result;
  }

  @Override
  public Type visitParen(MLRulesParser.ParenContext ctx) {
    return this.visit(ctx.expression());
  }

  @Override
  public Type visitSpecies(MLRulesParser.SpeciesContext ctx) {
    // System.out.println("Visit Species: " + ctx.getText());
    if (!inPreamble) {
      this.inSpecies = true;
    }
    Type amounType = BaseType.NUM;
    if (ctx.amount() != null) {
      amounType = this.visit(ctx.amount());
    }
    if (amounType != BaseType.NUM) {
      throw new SemanticsException(ctx.amount(), "wrong amount type " + amounType);
    }
    Type nameType = this.visit(ctx.name());
    if (nameType == BaseType.STRING) {
      // species depend on function evaluation
    }
    if (ctx.attributes() != null) {
      this.visit(ctx.attributes());
    } else {
      if (this.symbolTable.getActiveSpeciesTypes() != null && !this.symbolTable.getActiveSpeciesTypes().isEmpty()) {
        throw new SemanticsException(ctx, String.format("No attributes found! Expected attribute parameter %s", symbolTable.getActiveSpeciesTypes()));
      }
    }
    if (ctx.guard() != null) {
      this.visit(ctx.guard());
    }

    this.symbolTable.deactivateSpecies();
    if (ctx.subSpecies() != null) {
      this.visit(ctx.subSpecies());
    }
    if (ctx.ID() != null) {
      // System.out.println(ctx.ID().getText());
      if (this.symbolTable.containsVariable(ctx.ID().getText())) {
        throw new SemanticsException(ctx, String.format("variable %s is already defined", ctx.ID().getText()));
      }
      this.symbolTable.defineVariable(ctx.ID().getText(), BaseType.SPECIES);
    }
    if (!inPreamble) {
      this.inSpecies = false;
    }
    return BaseType.SPECIES;
  }

  @Override
  public Type visitName(MLRulesParser.NameContext ctx) {
    // TODO: checks
    Type result = null;
    if (ctx.ID_SPECIES() != null) {
      String name = ctx.ID_SPECIES().getText();
      if (!this.symbolTable.containsSpecies(name)) {
        if (inPreamble) {
          throw new DelayedEvaluationException();
        }
        throw new SemanticsException(ctx, String.format("unknown species type %s", ctx.ID_SPECIES().getText()));
      }

      this.symbolTable.activateSpecies(name);
    } else {
      result = this.visit(ctx.expression());
    }
    return result;
  }

  @Override
  public Type visitAttributes(MLRulesParser.AttributesContext ctx) {
    List<Type> parameterTypes = new ArrayList<Type>();
    List<Type> expectedTypes = this.symbolTable.getActiveSpeciesTypes();
    int paramCounter = 0;
    for (MLRulesParser.ExpressionContext exp : ctx.expression()) {
      if (expectedTypes != null && expectedTypes.size() == parameterTypes.size()) {
        throw new SemanticsException(ctx,String.format("too many attribute values; expected %s", expectedTypes));
      }
      Type t = this.visit(exp);
      // if (t == BaseType.STRING
      // && expectedTypes.get(paramCounter) instanceof StringSetType) {
      // StringSetType stringSet =
      // (StringSetType) expectedTypes.get(paramCounter);
      // String stringText = exp.getText().replace("'", "");
      // if (!stringSet.containsString(stringText)) {
      // throw new SemanticsException(ctx,
      // String.format("string %s is not in the defined string set %s",
      // stringText, stringSet));
      // }
      // }
      parameterTypes.add(t);
      paramCounter++;
      if (this.symbolTable.getSpeciesParamCounter() < paramCounter) {
        this.symbolTable.shiftSpeciesAttributeCounter();;
      }
    }
    try {
      if (expectedTypes == null) {
        // TODO
        return null;
      }
      if (!isTypeListEqual(parameterTypes, expectedTypes)) {
        throw new SemanticsException(ctx,
            String.format("cant match given parameter types %s with expected types %s", parameterTypes, expectedTypes));
      }
    } catch (NullPointerException ex) {
      throw new SemanticsException(ctx, "cant check types");
    }
    return null;
  }

  @Override
  public Type visitSubSpecies(MLRulesParser.SubSpeciesContext ctx) {
    this.inSubSpecies = true;
    // System.out.println("Sub Species: " + ctx.getText());
    if (ctx.expression() != null) {
      Type t = this.visit(ctx.expression());
    }
    this.inSubSpecies = false;

    return null;
  }

  @Override
  public Type visitGuard(MLRulesParser.GuardContext ctx) {
    if (ctx.expression() != null) {
      Type type = this.visit(ctx.expression());
      if (type != BaseType.BOOL) {
        throw new SemanticsException(ctx, "wrong type of guard expression");
      }
    }
    return null;
  }

  @Override
  public Type visitAmountINT(MLRulesParser.AmountINTContext ctx) {
    return BaseType.NUM;
  }

  @Override
  public Type visitAmountREAL(MLRulesParser.AmountREALContext ctx) {
    return BaseType.NUM;
  }

  @Override
  public Type visitAmountID(MLRulesParser.AmountIDContext ctx) {
    String varName = ctx.getText();
    Type result = symbolTable.getVariableType(varName);
    if (result != BaseType.UNKNOWN && result != BaseType.NUM) {
      throw new SemanticsException(ctx, String.format("invalid amount type %s", result));
    }
    return result;
  }

  @Override
  public Type visitAmountExpr(MLRulesParser.AmountExprContext ctx) {
    Type result = this.visit(ctx.expression());
    if (result != BaseType.UNKNOWN && result != BaseType.NUM) {
      throw new SemanticsException(ctx, String.format("invalid amount type %s", result));
    }
    return result;
  }

  @Override
  public Type visitInt(MLRulesParser.IntContext ctx) {
    return BaseType.NUM;
  }

  @Override
  public Type visitReal(MLRulesParser.RealContext ctx) {
    return BaseType.NUM;

  }

  @Override
  public Type visitTrue(MLRulesParser.TrueContext ctx) {
    return BaseType.BOOL;

  }

  @Override
  public Type visitFalse(MLRulesParser.FalseContext ctx) {
    return BaseType.BOOL;

  }

  @Override
  public Type visitString(MLRulesParser.StringContext ctx) {
    return BaseType.STRING;

  }

  @Override
  public Type visitFree(MLRulesParser.FreeContext ctx) {
    return BaseType.LINK;
  }

  @Override
  public Type visitId(MLRulesParser.IdContext ctx) {
    String variableName = ctx.ID().getText();
    Type variableType = null;
    if (symbolTable.containsFunction(variableName)) {
      throw new SemanticsException(ctx, String.format("function %s cannot be used as variable", variableName));
    }
    // TODO: other checks
    if (this.symbolTable.containsVariable(variableName) && !inSpecies) {
      // variable is known
      variableType = this.symbolTable.getVariableType(variableName);
    } else if (inSpecies && !this.symbolTable.getActiveSpecies().equals("")) {
      // variable is in ml rule species expression
      variableType = this.symbolTable.getVariableType(variableName);
      if (variableType == null) {
        if (inProduct) {
          throw new SemanticsException(ctx, String.format("variable %s not defined", variableName));
        }
        // variable is species attribute and needs to be interpolated
        variableType = this.symbolTable.defineSpeciesVariable(variableName);
      }
      // otherwise variable is already defined
      this.symbolTable.shiftSpeciesAttributeCounter();
    } else if (inSpecies && !inSubSpecies) {
      // variable is in species
      variableType = this.symbolTable.getVariableType(variableName);
    } else if (inSubSpecies && !this.symbolTable.containsVariable(variableName)) {
      variableType = BaseType.SOL;
      this.symbolTable.defineVariable(variableName, variableType);
    } else if (inSubSpecies && this.symbolTable.containsVariable(variableName)) {
      variableType = this.symbolTable.getVariableType(variableName);
    } else {
      // variable is unknown
      if (inPreamble) {
        throw new DelayedEvaluationException();
      } else if (!this.symbolTable.getActiveSpecies().equals("")) {
        // variable is in species expression so needs to be interpolated
      }
      throw new SemanticsException(ctx, String.format("can't get type of variable %s", ctx.ID().getText()));
    }
    return variableType;

  }

  @Override
  public Type visitWhere(MLRulesParser.WhereContext ctx) {
    for (MLRulesParser.AssignContext assign : ctx.assign()) {
      this.visit(assign);
    }
    return null;
  }

  @Override
  public Type visitAssign(MLRulesParser.AssignContext ctx) {
    // System.out.println("Visit Assign: " + ctx.getText());
    String assignName = null;
    if (ctx.assignName() instanceof MLRulesParser.SingleAssignContext) {
      MLRulesParser.SingleAssignContext name = (MLRulesParser.SingleAssignContext) ctx.assignName();
      assignName = name.ID().getText();
    }
    // TODO: Problem variable name is in lazy eval: so i can use it here
    if (this.symbolTable.containsVariable(assignName)) {
      throw new SemanticsException(ctx, String.format("variable %s is already defined", assignName));
    }
    Type assignType = this.visit(ctx.expression());
    this.symbolTable.defineWhereVariable(assignName, assignType);
    return null;
  }

  @Override
  public Type visitSingleAssign(MLRulesParser.SingleAssignContext ctx) {
    // TODO Auto-generated method stub
    throw new SemanticsException(ctx, "cant handle " + ctx.getClass());
  }

  @Override
  public Type visitTupleAssign(MLRulesParser.TupleAssignContext ctx) {
    // TODO Auto-generated method stub
    throw new SemanticsException(ctx, "cant handle " + ctx.getClass());
  }

  @Override
  public Type visitFunction(MLRulesParser.FunctionContext ctx) {
    // define function with corresponding types
    this.visitTypeDefinition(ctx.typeDefinition());
    if (ctx.functionDefinition().size() == 0) {
      throw new SemanticsException(ctx, "function definition is missing");
    }
    // check function definition for matching the expected types
    for (MLRulesParser.FunctionDefinitionContext definition : ctx.functionDefinition()) {
      try {
        this.visitFunctionDefinition(definition);
      } catch (DelayedEvaluationException e) {
        // handle function with unknown variables
        this.symbolTable.addUnhandeldContext(definition);
      }
    }
    return null;
  }

  @Override
  public Type visitTypeDefinition(MLRulesParser.TypeDefinitionContext ctx) {
    String functionName = ctx.ID().getText();
    // check if function is already defined
    if (this.symbolTable.containsFunction(functionName)) {
      throw new SemanticsException(ctx, String.format("function %s is already defined", functionName));
    }
    // get function types
    List<Type> functionTypes = new ArrayList<Type>();
    for (MLRulesParser.TypeContext type : ctx.type()) {
      Type innerType = this.visit(type);
      functionTypes.add(innerType);
    }
    // define function
    this.symbolTable.defineFunction(functionName, functionTypes);
    return null;
  }

  @Override
  public Type visitBaseTypeFloat(MLRulesParser.BaseTypeFloatContext ctx) {
    return BaseType.NUM;
  }

  @Override
  public Type visitBaseTypeBool(MLRulesParser.BaseTypeBoolContext ctx) {
    return BaseType.BOOL;
  }

  @Override
  public Type visitBaseTypeString(MLRulesParser.BaseTypeStringContext ctx) {
    return BaseType.STRING;
  }

  @Override
  public Type visitSpeciesType(MLRulesParser.SpeciesTypeContext ctx) {
    return BaseType.SPECIES;
  }

  @Override
  public Type visitSolutionType(MLRulesParser.SolutionTypeContext ctx) {
    return BaseType.SOL;
  }

  @Override
  public Type visitTupleType(MLRulesParser.TupleTypeContext ctx) {
    return BaseType.TUPLE;
  }

  @Override
  public Type visitBaseTypeLink(MLRulesParser.BaseTypeLinkContext ctx) {
    return BaseType.LINK;
  }

  @Override
  public Type visitFunctionType(MLRulesParser.FunctionTypeContext ctx) {
    List<Type> functionTypes = new ArrayList();
    for (MLRulesParser.TypeContext type : ctx.type()) {
      Type t = this.visit(type);
      functionTypes.add(t);
    }
    FunctionType f = new FunctionType(functionTypes);
    return f;
  }

  @Override
  public Type visitFunctionDefinition(MLRulesParser.FunctionDefinitionContext ctx) {
    // create new local scope
    this.symbolTable.pushScope();
    // activate function for later matching function variables with expected
    // types
    try {
      String functionName = ctx.ID().getText();
      this.symbolTable.activateFunction(functionName);
      List<Type> expectedTypes = this.symbolTable.getFunctionTypes(functionName);
      List<Type> functionTypes = new ArrayList<Type>();
      // visit function parameters for creating function variables with
      // corresponding
      // type
      for (MLRulesParser.ParameterContext parameter : ctx.parameter()) {
        Type parameterType = this.visit(parameter);
        functionTypes.add(parameterType);
        if (parameterType instanceof FunctionType) {
          FunctionType functionType = (FunctionType) parameterType;
          symbolTable.defineFunction(parameter.getText(), functionType.getSubTypes());
        }
        // increment parameter counter for matching the next parameter with
        // expected type
        symbolTable.shiftParamCounter();

      }
      // visit where for creating the local variables
      if (ctx.where() != null) {
        this.visit(ctx.where());
      }
      // calculate function expression result
      Type expressionType = null;
      expressionType = this.visit(ctx.expression());
      functionTypes.add(expressionType);
      // TOD: checks
      this.symbolTable.deactivateFunction();
      if (!isTypeListEqual(functionTypes, expectedTypes)) {
        throw new SemanticsException(ctx,
            String.format("Cant match function types %s with parameter types %s", functionTypes, expectedTypes));
      }
      symbolTable.popScope();
    } catch (DelayedEvaluationException ex) {
      this.symbolTable.popScope();
      throw ex;
    }
    return null;
  }

  @Override
  public Type visitParamSingle(MLRulesParser.ParamSingleContext ctx) {
    // System.out.println("Single: " + ctx.getText());
    return this.visit(ctx.atomicParameter());

  }

  @Override
  public Type visitParamTuple(MLRulesParser.ParamTupleContext ctx) {
    for (MLRulesParser.AtomicParameterContext parameter : ctx.atomicParameter()) {
      this.visit(parameter);
    }
    return BaseType.TUPLE;
  }

  @Override
  public Type visitParamSpecies(MLRulesParser.ParamSpeciesContext ctx) {
    String speciesName = ctx.ID_SPECIES().getText();
    if (!symbolTable.containsSpecies(speciesName)) {
      if (inPreamble) {
        throw new DelayedEvaluationException();
      }
      throw new SemanticsException(ctx, String.format("species %s is not defined", speciesName));
    }
    List<Type> speciesTypes = new ArrayList<>();
    List<Type> expectedTypes = this.symbolTable.getSpeciesType(speciesName);
    int paramCounter = 0;
    for (MLRulesParser.ValueContext val : ctx.value()) {
      Type paramType = null;
      try {
        paramType = this.visit(val);
      } catch (Exception ex) {
        // parameter is variable
        String variableName = val.getText();
        Type variableType = expectedTypes.get(paramCounter);
        paramType = variableType;
        this.symbolTable.defineVariable(variableName, variableType);
      }
      speciesTypes.add(paramType);
      paramCounter++;
    }
    if (!isTypeListEqual(speciesTypes, expectedTypes)) {
      throw new SemanticsException(ctx,
          String.format("cant match expected types %s with given types %s", expectedTypes, speciesTypes));
    }
    return BaseType.SPECIES;
  }

  @Override
  public Type visitParamSimple(MLRulesParser.ParamSimpleContext ctx) {
    String variableName = ctx.getText();
    Type result = null;

    if (this.symbolTable.containsVariable(variableName)) {
      throw new SemanticsException(ctx, String.format("variable %s is already defined", variableName));
    }
    try {
      // try to check if parameter is value, should evaluated without exception
      result = this.visit(ctx.value());
    } catch (Exception e) {
      // parameter is variable
      if (ctx.parent instanceof MLRulesParser.ParamTupleContext) {
        // param is in tuple -> tuple elements are wildcards
        result = BaseType.WILDCARD;
        this.symbolTable.defineVariable(variableName, result);
      } else {
        // define local variable depending on param counter
        result = this.symbolTable.defineLocalVariable(variableName);
      }
    }
    return result;
  }

  @Override
  public Type visitParamSol(MLRulesParser.ParamSolContext ctx) {
    String left = ctx.ID(0).getText();
    String right = ctx.ID(1).getText();
    if (this.symbolTable.containsVariable(left)) {
      throw new SemanticsException(ctx, String.format("variable %s is already defined", left));
    } else if (this.symbolTable.containsVariable(right)) {
      throw new SemanticsException(ctx, String.format("variable %s is already defined", right));
    }
    this.symbolTable.defineVariable(left, BaseType.SPECIES);
    this.symbolTable.defineVariable(right, BaseType.SOL);
    return BaseType.SOL;
  }

  @Override
  public Type visitEmptySol(MLRulesParser.EmptySolContext ctx) {
    return BaseType.SOL;
  }

  public void doTypeCheck(MLRulesParser.ModelContext model) {
    this.visit(model);
  }

  private boolean isTypeListEqual(List<Type> l1, List<Type> l2) {
    if (l1.size() != l2.size()) {
      return false;
    } else {
      for (int i = 0; i < l1.size(); i++) {
        Type l1Type = l1.get(i);
        Type l2Type = l2.get(i);
        if (l1Type == BaseType.WILDCARD || l2Type == BaseType.WILDCARD) {
          continue;
        } else if (l1Type == BaseType.SOL && l2Type == BaseType.SPECIES) {
          continue;
        } else if (l1Type == BaseType.SPECIES && l2Type == BaseType.SOL) {
          continue;
          // } else if (l1Type == BaseType.STRING
          // && l2Type instanceof StringSetType) {
          // return true;
          // } else if (l1Type instanceof StringSetType
          // && l2Type == BaseType.STRING) {
          // return true;
          // } else if (l1Type instanceof StringSetType
          // && l2Type instanceof StringSetType) {
          // StringSetType l1t = (StringSetType) l1Type;
          // StringSetType l2t = (StringSetType) l2Type;
          // return l1t.equals(l2t);
        } else if (l1Type != l2Type) {
          return false;
        }
      }
    }
    return true;
  }
}
