/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.visitor.modelcreator;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.jamesii.core.math.parsetree.INode;
import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.core.math.parsetree.bool.*;
import org.jamesii.core.math.parsetree.control.IfThenElseNode;
import org.jamesii.core.math.parsetree.math.DivNode;
import org.jamesii.core.math.parsetree.math.MinusNode;
import org.jamesii.core.math.parsetree.math.MultNode;
import org.jamesii.core.math.parsetree.math.PowerNode;
import org.jamesii.core.math.parsetree.variables.Identifier;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Reactant;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.model.rule.RuleComparator;
import org.jamesii.mlrules.model.rule.Rules;
import org.jamesii.mlrules.model.rule.timed.IntervalTimer;
import org.jamesii.mlrules.model.rule.timed.SingleTimer;
import org.jamesii.mlrules.model.rule.timed.TimedRule;
import org.jamesii.mlrules.model.rule.timed.TupleTimer;
import org.jamesii.mlrules.model.species.Compartment;
import org.jamesii.mlrules.model.species.LeafSpecies;
import org.jamesii.mlrules.model.species.Species;
import org.jamesii.mlrules.model.species.SpeciesType;
import org.jamesii.mlrules.model.species.SpeciesType.AttributeType;
import org.jamesii.mlrules.parser.exception.DelayedEvaluationException;
import org.jamesii.mlrules.parser.exception.SemanticsException;
import org.jamesii.mlrules.parser.functions.Function;
import org.jamesii.mlrules.parser.functions.FunctionDefinition;
import org.jamesii.mlrules.parser.functions.param.*;
import org.jamesii.mlrules.parser.grammar.MLRulesBaseVisitor;
import org.jamesii.mlrules.parser.grammar.MLRulesLexer;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;
import org.jamesii.mlrules.parser.nodes.*;
import org.jamesii.mlrules.parser.types.BaseType;
import org.jamesii.mlrules.parser.types.FunctionType;
import org.jamesii.mlrules.parser.types.Tuple;
import org.jamesii.mlrules.parser.types.Type;
import org.jamesii.mlrules.parser.visitor.typecheck.util.TypeSymbolTable;
import org.jamesii.mlrules.simulator.standard.StandardSimulator;
import org.jamesii.mlrules.util.*;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class SimpleVisitor creates an model which is executeable
 * by the {@link StandardSimulator}.
 * 
 * Note: This implementation can be used for implementing all other sorts of
 * {@link MLRulesModelCreater}s using overriding of methods. This implementation
 * is also used for semantic checking.
 * 
 * @author Andreas Ruscheinski
 */
public class StandardMLRulesVisitor extends MLRulesBaseVisitor<Node> implements MLRulesModelCreater {
  private Compartment initialSolution;

  private List<Rule> rules = new ArrayList<>();

  private final List<TimedRule> timedRules = new ArrayList<>();

  private LazyContextEvaluator lazyEvaluator;

  /** The enviroment. */
  private MLEnvironment enviroment;

  private boolean inPreamble = false;

  private boolean lazyEval = true;

  private Map<String, Object> parameter;

  private final TypeSymbolTable typeSymbolTable;

  /**
   * Instantiates a new simple visitor.
   *
   * @param enviroment
   *          the enviroment
   */
  public StandardMLRulesVisitor(TypeSymbolTable typeSymbolTable, MLEnvironment enviroment, Map<String, Object> parameter) {
    this.typeSymbolTable = typeSymbolTable;
    this.enviroment = enviroment;
    this.parameter = parameter;
    this.lazyEvaluator = new LazyContextEvaluator(this);
  }

  @Override
  public Node visitModel(MLRulesParser.ModelContext ctx) {
    inPreamble = true;
    for (MLRulesParser.PreambleContext preamble : ctx.preamble()) {
      this.visitPreamble(preamble);
    }
    inPreamble = false;
    this.lazyEvaluator.update();
    lazyEval = false;
    // TODO: report all exceptions at once
    if (this.lazyEvaluator.hasUnhandeldContexts()) {
      for (ParserRuleContext uctx : new ArrayList<>(this.lazyEvaluator.getUnhandeldContexts())) {
        this.visit(uctx);
      }
    }
    this.visitInitialSolution(ctx.initialSolution());
    for (MLRulesParser.MlruleContext mlrule : ctx.mlrule()) {
      this.visitMlrule(mlrule);
    }
    return null;
  }

  @Override
  public Node visitPreamble(MLRulesParser.PreambleContext ctx) {
    if (ctx.speciesDefinition() != null) {
      this.visitSpeciesDefinition(ctx.speciesDefinition());
    } else if (ctx.constant() != null) {
      this.visitConstant(ctx.constant());
    } else if (ctx.function() != null) {
      this.visitFunction(ctx.function());
    } else {
      throw new SemanticsException(ctx, "cant resolve preamble");
    }
    return null;
  }

  @Override
  public Node visitConstant(MLRulesParser.ConstantContext ctx) {
    String nodeID = ctx.ID().getText();
    // Error if constant already defined
    if (this.enviroment.containsIdent(nodeID) && !(this.enviroment.getValue(nodeID) instanceof Function
        && ((Function) this.enviroment.getValue(nodeID)).getDefinitions().isEmpty())) {
      throw new SemanticsException(ctx, String.format("constant name %s is already used", nodeID));
    }

    if (parameter.containsKey(nodeID)) {
      if (typeSymbolTable.containsVariable(nodeID)) {
        if (typeSymbolTable.getVariableType(nodeID).equals(BaseType.SOL)) {
          Object param = parameter.get(nodeID);
          if (param instanceof String) {
            String sParam = (String) param;
            MLRulesLexer lexer = new MLRulesLexer(CharStreams.fromString(sParam));

            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MLRulesParser parser = new MLRulesParser(tokens);
            MLRulesParser.ExpressionContext exp = parser.expression();
            Node value = this.visit(exp);
            ValueNode result = null;
            // if value cant be calculated try lazy evaluation
            try {
              result = value.calc(this.enviroment);
            } catch (Exception ex) {
              if (lazyEval) {
                this.lazyEvaluator.addUnhandeldContext(ctx);
                return null;
              } else {
                throw new SemanticsException(ctx, String.format("can't compute value of constant %s", nodeID));
              }
            }
            this.enviroment.setGlobalValue(nodeID, result.getValue());
            return null;
          } else {
            throw new SemanticsException(ctx, String.format("can't create a solution for %s", nodeID));
          }

        }
      }

      this.enviroment.setGlobalValue(nodeID, parameter.get(nodeID));
    } else {

      Node value = this.visit(ctx.expression());
      ValueNode result = null;
      // if value cant be calculated try lazy evaluation
      try {
        result = value.calc(this.enviroment);
      } catch (Exception ex) {
        if (lazyEval) {
          this.lazyEvaluator.addUnhandeldContext(ctx);
          return null;
        } else {
          throw new SemanticsException(ctx, String.format("can't compute value of constant %s", nodeID));
        }
      }
      this.enviroment.setGlobalValue(nodeID, result.getValue());
    }
    return null;
  }

  @Override
  public Node visitBaseTypeString(MLRulesParser.BaseTypeStringContext ctx) {
    Node result = new ValueNode<Type>(BaseType.STRING);
    return result;
  }

  @Override
  public Node visitTrue(MLRulesParser.TrueContext ctx) {
    Node trueNode = new ValueNode<Boolean>(true);
    return trueNode;
  }

  @Override
  public Node visitTupleType(MLRulesParser.TupleTypeContext ctx) {
    Node result = new ValueNode<Type>(BaseType.TUPLE);
    return result;
  }

  @Override
  public Node visitFalse(MLRulesParser.FalseContext ctx) {
    Node falseNode = new ValueNode<Boolean>(false);
    return falseNode;
  }

  @Override
  public Node visitString(MLRulesParser.StringContext ctx) {
    String text = ctx.getText();
    String value = text.substring(1, text.length() - 1);
    Node stringNode = new ValueNode<String>(value);
    return stringNode;
  }

  @Override
  public Node visitParamSpecies(MLRulesParser.ParamSpeciesContext ctx) {
    List<Parameter> subParameter = new ArrayList<>();
    // visit subParameters
    for (MLRulesParser.ValueContext inner : ctx.value()) {
      Node innerResult = this.visit(inner);
      if (innerResult instanceof Identifier<?>) {
        subParameter.add(new SimpleParameter(((Identifier<String>) innerResult).getIdent()));
      } else {
        subParameter.add(new ValueParameter((ValueNode<?>) innerResult));
      }
    }
    SpeciesType type = (SpeciesType) enviroment.getValue(ctx.ID_SPECIES().getText());
    if (type == null) {
      if (lazyEval) {
        this.lazyEvaluator.addUnhandeldContext(ctx);
        return null;
      } else {
        throw new SemanticsException(ctx, String.format("can't determine species type %s", ctx.ID_SPECIES().getText()));
      }
    }
    // ValueNode<Parameter> result =
    // new ValueNode<Parameter>(new SpeciesParameter(type, subParameter));
    // if (type.getSpecOptAttributes() != subParameter.size()) {
    // throw new SemanticsException(ctx,
    // String.format("Species %s has %s attributes and not %s attributes.",
    // type.getName(), type.getSpecOptAttributes(), subParameter.size()));
    // }
    // return result;
    return new ValueNode<Parameter>(new SpeciesParameter(type, subParameter));
  }

  @Override
  public Node visitParamSol(MLRulesParser.ParamSolContext ctx) {
    SolParameter parameter = new SolParameter(ctx.ID(0).getText(), ctx.ID(1).getText());
    Node result = new ValueNode<Parameter>(parameter);
    return result;
  }

  @Override
  public Node visitInitialSolution(MLRulesParser.InitialSolutionContext ctx) {
    Node node = null;
    try {
      node = this.visit(ctx.expression());
      @SuppressWarnings("unchecked")
      Map<Species, Species> species = (Map<Species, Species>) ((ValueNode<?>) node.calc(this.enviroment)).getValue();
      Map<SpeciesType, Map<LeafSpecies, LeafSpecies>> leafSpecies = new HashMap<>();
      Map<SpeciesType, Set<Compartment>> compartments = new HashMap<>();
      for (Species s : species.values()) {
        if (s.getType().isCompartment()) {
          compartments.computeIfAbsent(s.getType(), s2 -> new HashSet<>()).add((Compartment) s);
        } else {
          leafSpecies.computeIfAbsent(s.getType(), s2 -> new HashMap<>()).put((LeafSpecies) s, (LeafSpecies) s);
        }
      }
      initialSolution = new Compartment(SpeciesType.ROOT, new Object[0], Compartment.UNKNOWN, leafSpecies,
          compartments);
      initialSolution.getAllSubSpeciesStream().forEach(s -> s.setContext(initialSolution));
    } catch (Exception e) {
      if (e instanceof SemanticsException) {
        throw e;
      }
      throw new SemanticsException(ctx,
          String.format("Could not create the initial solution %s: %s", node.toString(), e.getMessage()));
    }
    return null;
  }

  @Override
  public Node visitAmountID(MLRulesParser.AmountIDContext ctx) {
    Node result = new Identifier<String>(ctx.getText());
    return result;
  }

  @Override
  public Node visitEmptySol(MLRulesParser.EmptySolContext ctx) {
    Parameter parameter = new EmptySolParameter();
    Node result = new ValueNode<Parameter>(parameter);
    return result;
  }

  @Override
  public Node visitTypeDefinition(MLRulesParser.TypeDefinitionContext ctx) {
    if (this.enviroment.containsIdent(ctx.ID().getText())) {
      throw new SemanticsException(ctx, String.format("name %s is used ambigously", ctx.ID().getText()));
    }

    List<Type> subTypes = new ArrayList<Type>();
    for (MLRulesParser.TypeContext type : ctx.type()) {
      ValueNode<Type> subType = (ValueNode<Type>) this.visit(type);
      subTypes.add(subType.getValue());
    }
    if (subTypes.isEmpty()) {
      throw new SemanticsException(ctx, "types of function are empty");
    }
    FunctionType type = new FunctionType(subTypes);
    Function function = new Function(ctx.ID().getText(), type);
    this.enviroment.setGlobalValue(ctx.ID().getText(), function);
    return null;
  }

  @Override
  public Node visitFunction(MLRulesParser.FunctionContext ctx) {
    String name = ((MLRulesParser.TypeDefinitionContext) ctx.getChild(0)).ID().getText();
    List<FunctionDefinition> definitions = new ArrayList<FunctionDefinition>();
    try {
      this.visitTypeDefinition(ctx.typeDefinition());
      Function function = (Function) this.enviroment.getValue(name);
      for (MLRulesParser.FunctionDefinitionContext definition : ctx.functionDefinition()) {
        ValueNode<FunctionDefinition> definitionNode = (ValueNode<FunctionDefinition>) this
            .visitFunctionDefinition(definition);
        definitions.add(definitionNode.getValue());
      }
      function.init(definitions);
    } catch (DelayedEvaluationException ex) {
      this.enviroment.removeGlobal(name);
      if (lazyEval) {
        this.lazyEvaluator.addUnhandeldContext(ctx);
      } else {
        throw new SemanticsException(ctx, "could not handle function " + name);
      }
    }
    return null;
  }

  @Override
  public Node visitReal(MLRulesParser.RealContext ctx) {
    Double value = Double.valueOf(ctx.getText());
    Node realNode = new ValueNode<Double>(value);
    return realNode;
  }

  @Override
  public Node visitPlus(MLRulesParser.PlusContext ctx) {
    Node left = this.visit(ctx.getChild(0));
    Node right = this.visit(ctx.getChild(2));
    Node result = new MLRulesAddNode(left, right);
    return result;
  }

  @Override
  public Node visitBaseTypeBool(MLRulesParser.BaseTypeBoolContext ctx) {
    Node result = new ValueNode<Type>(BaseType.BOOL);
    return result;
  }

  @Override
  public Node visitSolutionType(MLRulesParser.SolutionTypeContext ctx) {
    Node result = new ValueNode<Type>(BaseType.SOL);
    return result;
  }

  @Override
  public Node visitSpeciesType(MLRulesParser.SpeciesTypeContext ctx) {
    Node result = new ValueNode<Type>(BaseType.SPECIES);
    return result;
  }

  @Override
  public Node visitSubSpecies(MLRulesParser.SubSpeciesContext ctx) {
    Node result = null;
    if (ctx.expression() != null) {
      result = this.visit(ctx.expression());
    }
    return result;
  }

  @Override
  public Node visitSpeciesExpr(MLRulesParser.SpeciesExprContext ctx) {
    Node result = this.visit(ctx.species());
    return result;
  }

  @Override
  public Node visitSingleAssign(MLRulesParser.SingleAssignContext ctx) {
    String id = ctx.ID().getText();
    Node result = new ValueNode<List<String>>(Arrays.asList(id));
    return result;
  }

  @Override
  public Node visitFree(MLRulesParser.FreeContext ctx) {
    Node freeNode = new ValueNode<Nu>(Nu.FREE);
    return freeNode;
  }

  @Override
  public Node visitEmptySolution(MLRulesParser.EmptySolutionContext ctx) {
    Node result = new EmptySolutionNode();
    return result;
  }

  @Override
  public Node visitMultDiv(MLRulesParser.MultDivContext ctx) {
    Node left = this.visit(ctx.getChild(0));
    Node right = this.visit(ctx.getChild(2));
    Node result = ctx.MULT() != null ? new MultNode(left, right) : new DivNode(left, right);
    return result;
  }

  @Override
  public Node visitNot(MLRulesParser.NotContext ctx) {
    Node inner = this.visit(ctx.getChild(1));
    Node result = new NotNode(inner);
    return result;
  }

  @Override
  public Node visitCountShort(MLRulesParser.CountShortContext ctx) {
    return new SpeciesAmountNode(ctx.ID().getText());
  }

  @Override
  public Node visitName(MLRulesParser.NameContext ctx) {
    Node result = null;
    if (ctx.ID_SPECIES() != null) {
      result = new ValueNode<String>(ctx.ID_SPECIES().getText());
    } else {
      result = this.visit(ctx.expression());
    }
    return result;
  }

  @Override
  public Node visitId(MLRulesParser.IdContext ctx) {
    Node result = new Identifier<Serializable>(ctx.getText());
    return result;
  }

  @Override
  public Node visitMlrule(MLRulesParser.MlruleContext ctx) {
    ValueNode<List<Reactant>> reactantNode = (ValueNode<List<Reactant>>) this.visitReactants(ctx.reactants());
    List<Reactant> reactants = reactantNode.getValue();
    Node productsNode = this.visitProducts(ctx.products());
    Node rateNode = this.visitRate(ctx.rate());
    ValueNode<List<Assignment>> whereNode = null;
    List<Assignment> assignments = null;
    if (ctx.where() != null) {
      whereNode = (ValueNode<List<Assignment>>) this.visitWhere(ctx.where());
      assignments = whereNode.getValue();
    }

    if (ctx.AT() != null) {
      Rule rule = new Rule(reactants, productsNode, rateNode, assignments == null ? new ArrayList<>() : assignments);
      rules.add(rule);
    } else {
      try {
        Rule rule = new Rule(reactants, productsNode, new ValueNode<Double>(1D),
            assignments == null ? new ArrayList<>() : assignments);
        if (ctx.ATEACH() != null) {
          if (rateNode instanceof TupleNode) {
            TupleNode tn = (TupleNode) rateNode;
            Node n = tn.calc(enviroment);
            if (n instanceof ValueNode<?>) {
              ValueNode<?> vn = (ValueNode<?>) n;
              if (vn.getValue() instanceof Tuple) {
                Tuple tuple = (Tuple) vn.getValue();
                timedRules.add(new TimedRule(rule, new TupleTimer(tuple)));
              } else {
                throw new Exception("tuple of timed reaction has wrong types");
              }
            } else {
              throw new Exception("the tuple of rates could not be computed");
            }
          } else {
            double time = NodeHelper.getDouble(rateNode, this.enviroment);
            timedRules.add(new TimedRule(rule, new IntervalTimer(time)));
          }
        } else {
          double time = NodeHelper.getDouble(rateNode, this.enviroment);
          timedRules.add(new TimedRule(rule, SingleTimer.instance, time));
        }
      } catch (Exception e) {
        throw new SemanticsException(ctx, "Could not compute timed reaction!\n" + e.getMessage());
      }
    }
    return null;
  }

  @Override
  public Node visitTupleAssign(MLRulesParser.TupleAssignContext ctx) {
    List<String> names = ctx.ID().stream().map(id -> id.getText()).collect(Collectors.toList());
    Node result = new ValueNode<List<String>>(names);
    return result;
  }

  @Override
  public Node visitApplication(MLRulesParser.ApplicationContext ctx) {
    Node function = this.visit(ctx.expression());
    List<Node> arguments = ((ValueNode<List<Node>>) this.visit(ctx.attributes())).getValue();
    Node result = new FunctionCallNode(function, arguments);
    return result;
  }

  @Override
  public Node visitAndOr(MLRulesParser.AndOrContext ctx) {
    Node left = this.visit(ctx.getChild(0));
    Node right = this.visit(ctx.getChild(2));
    Node result = ctx.AND() != null ? new AndNode(left, right) : new OrNode(left, right);
    return result;
  }

  @Override
  public Node visitMinus(MLRulesParser.MinusContext ctx) {
    Node left = this.visit(ctx.getChild(0));
    Node right = this.visit(ctx.getChild(2));
    Node result = new MinusNode(left, right);
    return result;
  }

  @Override
  public Node visitBaseTypeFloat(MLRulesParser.BaseTypeFloatContext ctx) {
    Node result = new ValueNode<Type>(BaseType.NUM);
    return result;
  }

  @Override
  public Node visitBoolExpr(MLRulesParser.BoolExprContext ctx) {
    Node left = this.visit(ctx.getChild(1));
    Node right = this.visit(ctx.getChild(3));
    Node result;
    if (ctx.EQUALS() != null) {
      result = new IsEqualNode(left, right);
    } else if (ctx.N_EQUALS() != null) {
      result = new IsNotEqualNode(left, right);
    } else if (ctx.GT() != null) {
      result = new IsGreaterNode(left, right);
    } else if (ctx.GT_EQ() != null) {
      result = new IsGreaterOrEqualNode(left, right);
    } else if (ctx.LT() != null) {
      result = new IsLowerNode(left, right);
    } else if (ctx.LT_EQ() != null) {
      result = new IsLowerOrEqualNode(left, right);
    } else {
      throw new SemanticsException(ctx, "boolean expression was not able to determine correct operation");
    }
    return result;
  }

  @Override
  public Node visitGuard(MLRulesParser.GuardContext ctx) {
    Node result = null;
    if (ctx.expression() != null) {
      result = this.visit(ctx.expression());
    }
    return result;
  }

  @Override
  public Node visitInt(MLRulesParser.IntContext ctx) {
    Double value = Double.valueOf(ctx.getText());
    Node intNode = new ValueNode<Double>(value);
    return intNode;
  }

  @Override
  public Node visitParamSimple(MLRulesParser.ParamSimpleContext ctx) {
    Node node = this.visit(ctx.value());
    Parameter parameter = null;
    if (node instanceof Identifier<?>) {
      parameter = new SimpleParameter(((Identifier<String>) node).getIdent());
    } else {
      parameter = new ValueParameter((ValueNode<?>) node);
    }
    Node result = new ValueNode<Parameter>(parameter);
    return result;
  }

  @Override
  public Node visitSpeciesDefinition(MLRulesParser.SpeciesDefinitionContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      String name = ctx.ID_SPECIES().getText();
      List<AttributeType> attributeTypes = new ArrayList<AttributeType>();
      for (MLRulesParser.SpeciesTypeParametersContext speciesType : ctx.speciesTypeParameters()) {
        ValueNode<AttributeType> result = (ValueNode<AttributeType>) this.visit(speciesType);
        attributeTypes.add(result.getValue());
      }
      if (this.enviroment.containsIdent(ctx.ID_SPECIES().getText())) {
        throw new SemanticsException(ctx, String.format("name %s is used ambigously", ctx.ID_SPECIES().getText()));
      }
      AttributeType[] types = new AttributeType[attributeTypes.size()];
      for (int i = 0; i < types.length; ++i) {
        types[i] = attributeTypes.get(i);
      }
      this.enviroment.setGlobalValue(name, new SpeciesType(name, types, (ctx.R_BRAKET() != null) ? true : false));
    } else {
      throw new SemanticsException(ctx, "invalid species definition");
    }
    return null;
  }

  @Override
  public Node visitSpeciesTypeParameters(MLRulesParser.SpeciesTypeParametersContext ctx) {
    Node result = null;
    switch (ctx.getChild(0).getText()) {
    case "num":
      result = new ValueNode<AttributeType>(AttributeType.NUM);
      break;
    case "link":
      result = new ValueNode<AttributeType>(AttributeType.LINK);
      break;
    case "string":
      result = new ValueNode<AttributeType>(AttributeType.STRING);
      break;
    case "bool":
      result = new ValueNode<AttributeType>(AttributeType.BOOL);
      break;
    default:
      break;
    }
    return result;
  }

  @Override
  public Node visitProducts(MLRulesParser.ProductsContext ctx) {
    Node result = ctx.expression() != null ? this.visit(ctx.expression())
        : new ValueNode<Map<Species, Species>>(new HashMap<>());
    return result;
  }

  @Override
  public Node visitTuple(MLRulesParser.TupleContext ctx) {
    List<INode> tupleElements = new ArrayList<INode>();
    for (MLRulesParser.ExpressionContext element : ctx.expression()) {
      Node innerElement = this.visit(element);
      tupleElements.add(innerElement);
    }
    Node result = new TupleNode(tupleElements);
    return result;
  }

  @Override
  public Node visitExpValue(MLRulesParser.ExpValueContext ctx) {
    Node value = this.visit(ctx.value());
    return value;
  }

  @Override
  public Node visitRoof(MLRulesParser.RoofContext ctx) {
    Node base = this.visit(ctx.getChild(0));
    Node exponent = this.visit(ctx.getChild(3));
    Node result = new PowerNode(base, exponent);
    return result;
  }

  @Override
  public Node visitParamTuple(MLRulesParser.ParamTupleContext ctx) {
    List<Parameter> subParameter = new ArrayList<>();
    for (MLRulesParser.AtomicParameterContext parameter : ctx.atomicParameter()) {
      ValueNode<Parameter> node = (ValueNode<Parameter>) this.visit(parameter);
      subParameter.add(node.getValue());
    }
    TupleParameter parameter = new TupleParameter(subParameter);
    Node result = new ValueNode<Parameter>(parameter);
    return result;
  }

  @Override
  public Node visitRate(MLRulesParser.RateContext ctx) {
    Node result = this.visit(ctx.expression());
    return result;
  }

  @Override
  public Node visitWhere(MLRulesParser.WhereContext ctx) {
    List<Assignment> assignments = new ArrayList<Assignment>();
    for (MLRulesParser.AssignContext assign : ctx.assign()) {
      ValueNode<Assignment> assigment = (ValueNode<Assignment>) this.visitAssign(assign);
      assignments.add(assigment.getValue());
    }
    Node result = new ValueNode<List<Assignment>>(assignments);
    return result;
  }

  @Override
  public Node visitFunctionCall(MLRulesParser.FunctionCallContext ctx) {
    String name = ctx.ID().getText();
    Function function = (Function) this.enviroment.getValue(name);
    if (function == null) {
      function = new Function(name, null);
    }
    List<Node> nodes = new ArrayList<Node>();
    for (MLRulesParser.ExpressionContext inner : ctx.expression()) {
      Node innerResult = this.visit(inner);
      nodes.add(innerResult);
    }
    Node result = new FunctionCallNode(new ValueNode<Function>(function), nodes);
    return result;
  }

  @Override
  public Node visitFunctionType(MLRulesParser.FunctionTypeContext ctx) {
    List<Type> subTypes = new ArrayList<>();
    for (MLRulesParser.TypeContext type : ctx.type()) {
      ValueNode<Type> innerType = (ValueNode<Type>) this.visit(type);
      subTypes.add(innerType.getValue());
    }
    if (subTypes.isEmpty()) {
      throw new SemanticsException(ctx, "types of function are empty");
    }
    Node result = new ValueNode<>(new FunctionType(subTypes));
    return result;
  }

  @Override
  public Node visitAmountExpr(MLRulesParser.AmountExprContext ctx) {
    Node amount = this.visit(ctx.expression());
    return amount;
  }

  @Override
  public Node visitFunctionDefinition(MLRulesParser.FunctionDefinitionContext ctx) {
    if (ctx.ID() == null) {
      throw new SemanticsException(ctx, "function definition error");
    }
    List<Parameter> parameters = new ArrayList<Parameter>();
    Node result = null;
    try {
      for (MLRulesParser.ParameterContext parameter : ctx.parameter()) {
        ValueNode<Parameter> param = (ValueNode<Parameter>) this.visit(parameter);
        parameters.add(param.getValue());
      }
      List<Assignment> assignments = ctx.where() != null
          ? ((ValueNode<List<Assignment>>) this.visitWhere(ctx.where())).getValue() : new ArrayList<>();
      ;
      Node rule = this.visit(ctx.expression());
      FunctionDefinition definition = new FunctionDefinition(parameters, rule, assignments);
      result = new ValueNode<FunctionDefinition>(definition);

    } catch (Exception ex) {
      throw new DelayedEvaluationException();
    }
    return result;
  }

  @Override
  public Node visitReactants(MLRulesParser.ReactantsContext ctx) {
    List<Reactant> reactants = new ArrayList<>();
    for (MLRulesParser.SpeciesContext species : ctx.species()) {
      SpeciesPatternNode innerSpecies = (SpeciesPatternNode) this.visitSpecies(species);
      reactants.add(innerSpecies.toReactant(this.enviroment));
    }
    Node result = new ValueNode<List<Reactant>>(reactants);
    return result;
  }

  @Override
  public Node visitParamSingle(MLRulesParser.ParamSingleContext ctx) {
    Node result = this.visit(ctx.atomicParameter());
    return result;
  }

  @Override
  public Node visitBaseTypeLink(MLRulesParser.BaseTypeLinkContext ctx) {
    Node result = new ValueNode<Type>(BaseType.LINK);
    return result;
  }

  @Override
  public Node visitMinusOne(MLRulesParser.MinusOneContext ctx) {
    Node inner = this.visit(ctx.getChild(1));
    Node minusOne = new ValueNode<Integer>(-1);
    Node result = new MultNode(minusOne, inner);
    return result;
  }

  @Override
  public Node visitSpecies(MLRulesParser.SpeciesContext ctx) {
    Node amount = ctx.amount() != null ? this.visit(ctx.amount()) : new ValueNode<>(1);
    Node name = this.visit(ctx.name()); // TODO
    List<INode> attributes = ctx.attributes() != null
        ? ((ValueNode<List<INode>>) this.visit(ctx.attributes())).getValue() : new ArrayList<INode>();
    Node subSpecies = ctx.subSpecies() != null ? this.visit(ctx.subSpecies())
        : new ValueNode<Map<Species, Species>>(new HashMap<>()); // TODO
    Node guard = ctx.guard() != null ? this.visit(ctx.guard()) : null;

    // check if amount is double and species type is a compartment
    // also check if attributes are available
    try {
      Node tmp = name.calc(enviroment);
      String name2 = null;
      if (tmp instanceof ValueNode<?> && ((ValueNode<?>) tmp).getValue() instanceof String) {
        name2 = (String) ((ValueNode<?>) tmp).getValue();
      }
      SpeciesType type = (SpeciesType) enviroment.getValue(name2);
      if (type != null) {



        tmp = amount.calc(enviroment);
        double amount2;
        if (tmp instanceof ValueNode<?>) {
          if (((ValueNode<?>) tmp).getValue() instanceof Double) {
            amount2 = ((ValueNode<Double>) tmp).getValue();
            if (type.isCompartment() && Math.rint(amount2) != amount2) {
              throw new SemanticsException(ctx, String.format("Compartment %s is not allowed having real valued amount %f", type.toString(), amount2));
            }
          }
        }
      }
    } catch (Exception e) {
      // do nothing
    }

    Node result = new SpeciesPatternNode(amount, name, attributes.toArray(new Node[attributes.size()]), subSpecies,
        Optional.ofNullable(ctx.ID() != null ? ctx.ID().getText() : null));
    return result;

  }

  @Override
  public Node visitAttributes(MLRulesParser.AttributesContext ctx) {
    List<INode> attributes = new ArrayList<INode>();
    // TODO: Check Tuple
    for (MLRulesParser.ExpressionContext expression : ctx.expression()) {
      Node attribute = this.visit(expression);
      attributes.add(attribute);
    }
    ValueNode<List<INode>> result = new ValueNode<List<INode>>(attributes);
    return result;
  }

  @Override
  public Node visitIfThenElse(MLRulesParser.IfThenElseContext ctx) {
    Node condition = this.visit(ctx.getChild(1));
    Node thenAction = this.visit(ctx.getChild(3));
    Node elseAction = this.visit(ctx.getChild(5));
    Node result = new IfThenElseNode(condition, thenAction, elseAction);
    return result;
  }

  @Override
  public Node visitAmountINT(MLRulesParser.AmountINTContext ctx) {
    Node result = new ValueNode<>(Double.valueOf(ctx.getText()));
    return result;
  }

  @Override
  public Node visitAmountREAL(MLRulesParser.AmountREALContext ctx) {
    Node result = new ValueNode<>(Double.valueOf(ctx.getText()));
    return result;
  }

  @Override
  public Node visitParen(MLRulesParser.ParenContext ctx) {
    Node inner = this.visit(ctx.expression());
    return inner;
  }

  @Override
  public Node visitAssign(MLRulesParser.AssignContext ctx) {
    ValueNode<List<String>> names = (ValueNode<List<String>>) this.visit(ctx.assignName());
    Node expression = this.visit(ctx.expression());
    Assignment assignment = new Assignment(names.getValue(), expression);
    ValueNode<Assignment> result = new ValueNode<Assignment>(assignment);
    return result;
  }

  @Override
  public Model create(MLRulesParser.ModelContext modelContext) {
    this.visit(modelContext);
    timedRules.sort(RuleComparator.instance);
    return new Model(initialSolution, new Rules(rules, timedRules), this.enviroment);
  }
}
