/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.visitor.typecheck.util;

import org.antlr.v4.runtime.ParserRuleContext;
import org.jamesii.mlrules.parser.exception.DelayedEvaluationException;
import org.jamesii.mlrules.parser.functions.PredefinedFunctions;
import org.jamesii.mlrules.parser.types.Type;
import org.jamesii.mlrules.parser.visitor.typecheck.TypeCheckVisitor;

import java.util.*;

/**
 * 
 * @author Andreas Ruscheinski
 *
 */
public class TypeSymbolTable {

  private Stack<TypeScope> scopes = new Stack<TypeScope>();

  private TypeScope currentScope = null;

  private String activeFunction = "";

  private String activeSpecies = "";

  private List<Type> activeFunctionTypes = null;

  private List<Type> activeSpeciesTypes = null;

  private List<ParserRuleContext> unhandeldContexts =
      new ArrayList<ParserRuleContext>();

  private Set<String> possibleDefinedVariables = new HashSet<String>();

  private int paramCounter = 0;

  private int speciesParamCounter = 0;

  private TypeCheckVisitor visitor;

  public TypeSymbolTable(TypeCheckVisitor visitor) {
    TypeScope rootScope = new TypeScope();
    PredefinedFunctions.addAll(rootScope);
    scopes.push(rootScope);
    currentScope = rootScope;
    this.visitor = visitor;
  }

  public void pushScope() {
    TypeScope newScope = currentScope.clone();
    scopes.push(newScope);
    this.currentScope = newScope;
  }

  public void popScope() {
    scopes.pop();
    this.currentScope = scopes.peek();
  }

  public void defineFunction(String functionName, List<Type> functionTypes) {
    this.currentScope.addFunction(functionName, functionTypes);
  }

  public void defineVariable(String variable, Type type) {
    this.currentScope.addVariable(variable, type);
  }

  public boolean containsVariable(String variable) {
    return currentScope.containsVariable(variable);
  }

  public boolean containsFunction(String functionName) {
    return currentScope.containsFunction(functionName);
  }

  public Type getVariableType(String variable) {
    return currentScope.getVariabletype(variable);
  }

  public List<Type> getFunctionTypes(String functionName) {
    return currentScope.getFunctionVariables(functionName);
  }

  public List<Type> getFunctionParameterTypes(String functionName) {
    List<Type> tmp = getFunctionTypes(functionName);
    if (tmp == null) {
      return null;
    }
    int lastIndex = tmp.size() - 1;
    return tmp.subList(0, lastIndex);
  }

  public Type getFunctionResultType(String functionName) {
    List<Type> tmp = getFunctionTypes(functionName);
    // System.out.println(functionName);
    int lastIndex = tmp.size() - 1;
    return tmp.get(lastIndex);
  }

  public Type defineLocalVariable(String variableName) {
    if (this.activeFunctionTypes != null) {
      Type variableType = this.activeFunctionTypes.get(paramCounter);
      defineVariable(variableName, variableType);
    }
    return getVariableType(variableName);
  }

  public void defineWhereVariable(String variableName, Type variableType) {
    this.currentScope.addVariable(variableName, variableType);
  }

  public void defineSpecies(String speciesName, List<Type> speciesTypes) {
    this.currentScope.addSpecies(speciesName, speciesTypes);
  }

  public void activateFunction(String functionName) {
    if (containsFunction(functionName)) {
      this.activeFunction = functionName;
      this.activeFunctionTypes = getFunctionTypes(functionName);
      this.paramCounter = 0;
    }
  }

  public void activateSpecies(String speciesName) {
    if (containsSpecies(speciesName)) {
      this.activeSpecies = speciesName;
      this.activeSpeciesTypes = getSpeciesType(speciesName);
    }
  }

  public boolean containsSpecies(String speciesName) {
    return this.currentScope.containsSpecies(speciesName);
  }

  public void deactivateFunction() {
    this.activeFunction = "";
    this.activeFunctionTypes = null;
    this.paramCounter = 0;
  }

  public void deactivateSpecies() {
    this.activeSpecies = "";
    this.activeSpeciesTypes = null;
    this.speciesParamCounter = 0;
  }

  public void addUnhandeldContext(ParserRuleContext ctx) {
    // //System.out
    // .println(String.format("Add %s to unhandeld ctxs", ctx.getText()));
    if (this.unhandeldContexts.contains(ctx)) {
      throw new DelayedEvaluationException();
    }
    this.unhandeldContexts.add(ctx);
  }

  public List<ParserRuleContext> getUnhandeldContexts() {
    return this.unhandeldContexts;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(currentScope);
    builder.append("\nUnhandeld Contexts\n==============\n");
    for (ParserRuleContext ctx : unhandeldContexts) {
      builder.append(ctx.getText() + "\n");
    }
    return builder.toString();
  }

  public boolean hasUnhandeldContexts() {
    if (unhandeldContexts.size() != 0) {
      return true;
    }
    return false;
  }

  public void update() {
    int counter = 0;
    this.rollBack();
    this.possibleDefinedVariables.clear();
    while (true) {
      List<ParserRuleContext> nUnhandeledContexts =
          new ArrayList<ParserRuleContext>();
      List<ParserRuleContext> copy =
          new ArrayList<ParserRuleContext>(this.unhandeldContexts);
      for (ParserRuleContext context : copy) {
        try {
          // System.out.println("Symbol-Table visit: " + context.getText());
          this.visitor.visit(context);
        } catch (Exception e) {
          nUnhandeledContexts.add(context);
        } finally {
          this.rollBack();
        }
      }
      this.unhandeldContexts = nUnhandeledContexts;
      if (nUnhandeledContexts.size() == counter) {
        return;
      }
      counter = this.unhandeldContexts.size();
    }
  }

  public String getActiveSpecies() {
    return activeSpecies;
  }

  public List<Type> getActiveSpeciesTypes() {
    return this.activeSpeciesTypes;
  }

  public List<Type> getSpeciesType(String speciesName) {
    return this.currentScope.getSpeciesTypes(speciesName);
  }

  public void shiftParamCounter() {
    paramCounter++;
  }

  private void rollBack() {
    this.deactivateFunction();
    this.deactivateSpecies();
  }

  public void addPossibleDefinedVariable(String variable) {
    this.possibleDefinedVariables.add(variable);
  }

  public boolean containsPossibleDefinedVariable(String variable) {
    return this.possibleDefinedVariables.contains(variable);
  }

  public void shiftSpeciesAttributeCounter() {
    speciesParamCounter++;
  }

  public int getSpeciesParamCounter() {
    return speciesParamCounter;
  }
  
  public Type defineSpeciesVariable(String variable) {
    Type type = this.activeSpeciesTypes.get(speciesParamCounter);
    this.defineVariable(variable, type);
    return type;

  }
}
