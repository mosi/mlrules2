/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.visitor.typecheck.util;

import org.jamesii.mlrules.parser.types.Type;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * 
 * @author Andreas Ruscheinski
 *
 */
public class TypeScope {
  private HashMap<String, Type> variables = new HashMap<String, Type>();

  private HashMap<String, List<Type>> functionTypes =
      new HashMap<String, List<Type>>();

  private HashMap<String, List<Type>> speciesTypes =
      new HashMap<String, List<Type>>();

  private HashMap<String, String> variableToSpecies =
      new HashMap<String, String>();

  public TypeScope() {

  }

  public TypeScope(HashMap<String, Type> variables,
      HashMap<String, List<Type>> functionTypes,
      HashMap<String, List<Type>> speciesTypes,
      HashMap<String, String> variableToSpecies) {
    this.variables = variables;
    this.functionTypes = functionTypes;
    this.speciesTypes = speciesTypes;
    this.variableToSpecies = variableToSpecies;
  }

  @Override
  public TypeScope clone() {
    HashMap<String, Type> clonedVariables =
        (HashMap<String, Type>) this.variables.clone();
    HashMap<String, List<Type>> clonedFunctions =
        (HashMap<String, List<Type>>) this.functionTypes.clone();
    HashMap<String, List<Type>> clonedSpecies =
        (HashMap<String, List<Type>>) this.speciesTypes.clone();
    HashMap<String, String> clonedVariableToSpecies =
        (HashMap<String, String>) this.variableToSpecies.clone();
    return new TypeScope(clonedVariables, clonedFunctions, clonedSpecies,
        clonedVariableToSpecies);
  }

  public Type getVariabletype(String variable) {
    return this.variables.get(variable);
  }

  public List<Type> getFunctionVariables(String variable) {
    return this.functionTypes.get(variable);
  }

  public void addVariable(String variable, Type type) {
    this.variables.put(variable, type);
  }

  public void addFunction(String functionName, List<Type> functionTypes) {
    this.functionTypes.put(functionName, functionTypes);
  }

  public void addSpecies(String speciesName, List<Type> speciesTypes) {
    this.speciesTypes.put(speciesName, speciesTypes);
  }

  public boolean containsFunction(String functionName) {
    return functionTypes.containsKey(functionName);
  }

  public boolean containsVariable(String variable) {
    return variables.containsKey(variable);
  }

  public List<Type> getSpeciesTypes(String speciesName) {
    return this.speciesTypes.get(speciesName);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Variables\n=========\n");
    for (Entry<String, Type> entry : variables.entrySet()) {
      builder
          .append(String.format("%s - %s\n", entry.getKey(), entry.getValue()));
    }
    builder.append("Functions\n=========\n");
    for (Entry<String, List<Type>> entry : functionTypes.entrySet()) {
      builder
          .append(String.format("%s - %s\n", entry.getKey(), entry.getValue()));
    }
    builder.append("Species\n=========\n");
    for (Entry<String, List<Type>> entry : speciesTypes.entrySet()) {
      builder
          .append(String.format("%s - %s\n", entry.getKey(), entry.getValue()));
    }
    return builder.toString();
  }

  public boolean containsSpecies(String speciesName) {
    return this.speciesTypes.containsKey(speciesName);
  }

}
