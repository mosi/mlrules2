/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.visitor.syntax;

import org.jamesii.mlrules.parser.grammar.MLRulesBaseVisitor;
import org.jamesii.mlrules.parser.grammar.MLRulesParser;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyledDocument;

/**
 * Add syntax highlight to the text of the model editor.
 *
 * @author Tobias Helms
 */
public class SyntaxHighlightVisitor extends MLRulesBaseVisitor<Object> {
  private JTextPane pane;

  private int pos;

  private String text;

  private StyledDocument doc;

  private int currentIndex = 0;

  public SyntaxHighlightVisitor(JTextPane pane) {
    this.pane = pane;
    this.pos = this.pane.getCaretPosition();
    this.text = this.pane.getText();
    this.doc = pane.getStyledDocument();
    try {
      this.doc.remove(0, this.doc.getLength());
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  private void add(int start, int stop, Style style) {
    try {
      if (currentIndex > start) {
        doc.setCharacterAttributes(start, stop - start + 1, style, true);
      } else {
        if (currentIndex < start) {
          doc.insertString(currentIndex, text.substring(currentIndex, start),
                  new SimpleAttributeSet());
        }
        doc.insertString(start, text.substring(start, stop + 1), style);
        currentIndex = stop + 1;
      }
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Object visitTrue(MLRulesParser.TrueContext ctx) {
    add(ctx.getStart()
           .getStartIndex(), ctx.getStart()
                                .getStopIndex(),
            doc.getStyle(Styles.BOOL));
    return super.visitTrue(ctx);
  }

  @Override
  public Object visitConstant(MLRulesParser.ConstantContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitConstant(ctx);
  }

  @Override
  public Object visitFalse(MLRulesParser.FalseContext ctx) {
    add(ctx.getStart()
           .getStartIndex(), ctx.getStart()
                                .getStopIndex(),
            doc.getStyle(Styles.BOOL));
    return super.visitFalse(ctx);
  }

  @Override
  public Object visitSingleAssign(MLRulesParser.SingleAssignContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitSingleAssign(ctx);
  }

  @Override
  public Object visitTupleAssign(MLRulesParser.TupleAssignContext ctx) {
    if (ctx.ID() != null) {
      ctx.ID()
         .forEach(i -> add(i.getSymbol()
                            .getStartIndex(),
                 i.getSymbol()
                  .getStopIndex(), doc.getStyle(Styles.CONST)));
    }
    return super.visitTupleAssign(ctx);
  }

  @Override
  public Object visitString(MLRulesParser.StringContext ctx) {
    add(ctx.getStart()
           .getStartIndex(), ctx.getStart()
                                .getStopIndex(),
            doc.getStyle(Styles.STRING));
    return super.visitString(ctx);
  }

  @Override
  public Object visitFree(MLRulesParser.FreeContext ctx) {
    add(ctx.getStart()
           .getStartIndex(), ctx.getStart()
                                .getStopIndex(),
            doc.getStyle(Styles.KEY));
    return super.visitFree(ctx);
  }

  @Override
  public Object visitAmountID(MLRulesParser.AmountIDContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitAmountID(ctx);
  }

  @Override
  public Object visitCountShort(MLRulesParser.CountShortContext ctx) {
    if (ctx.COUNT() != null) {
      add(ctx.COUNT()
             .getSymbol()
             .getStartIndex(),
              ctx.COUNT()
                 .getSymbol()
                 .getStopIndex(),
              doc.getStyle(Styles.FUNCTION));
    }
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitCountShort(ctx);
  }

  @Override
  public Object visitName(MLRulesParser.NameContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      add(ctx.ID_SPECIES()
             .getSymbol()
             .getStartIndex(),
              ctx.ID_SPECIES()
                 .getSymbol()
                 .getStopIndex(),
              doc.getStyle(Styles.SPECIES));
    }
    return super.visitName(ctx);
  }

  @Override
  public Object visitId(MLRulesParser.IdContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitId(ctx);
  }

  @Override
  public Object visitSpeciesDefinition(MLRulesParser.SpeciesDefinitionContext ctx) {
    if (ctx.ID_SPECIES() != null) {
      add(ctx.ID_SPECIES()
             .getSymbol()
             .getStartIndex(),
              ctx.ID_SPECIES()
                 .getSymbol()
                 .getStopIndex(),
              doc.getStyle(Styles.SPECIES));
    }
    return super.visitSpeciesDefinition(ctx);
  }

  @Override
  public Object visitParamSol(MLRulesParser.ParamSolContext ctx) {
    if (ctx.ID(0) != null && ctx.ID(1) != null) {
      add(ctx.ID(0)
             .getSymbol()
             .getStartIndex(),
              ctx.ID(0)
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
      add(ctx.ID(1)
             .getSymbol()
             .getStartIndex(),
              ctx.ID(1)
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitParamSol(ctx);
  }

  @Override
  public Object visitWhere(MLRulesParser.WhereContext ctx) {
    if (ctx.WHERE() != null) {
      add(ctx.WHERE()
             .getSymbol()
             .getStartIndex(),
              ctx.WHERE()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    return super.visitWhere(ctx);
  }

  @Override
  public Object visitFunctionCall(MLRulesParser.FunctionCallContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
    return super.visitFunctionCall(ctx);
  }

  @Override
  public Object visitSpecies(MLRulesParser.SpeciesContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.CONST));
    }
    return super.visitSpecies(ctx);
  }

  @Override
  public Object visitInitialSolution(MLRulesParser.InitialSolutionContext ctx) {
    if (ctx.INIT() != null) {
      add(ctx.INIT()
             .getSymbol()
             .getStartIndex(),
              ctx.INIT()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.BOLD));
    }
    return super.visitInitialSolution(ctx);
  }

  @Override
  public Object visitIfThenElse(MLRulesParser.IfThenElseContext ctx) {
    if (ctx.IF() != null) {
      add(ctx.IF()
             .getSymbol()
             .getStartIndex(),
              ctx.IF()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    if (ctx.THEN() != null) {
      add(ctx.THEN()
             .getSymbol()
             .getStartIndex(),
              ctx.THEN()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    if (ctx.ELSE() != null) {
      add(ctx.ELSE()
             .getSymbol()
             .getStartIndex(),
              ctx.ELSE()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    return super.visitIfThenElse(ctx);
  }

  @Override
  public Object visitBaseTypeString(MLRulesParser.BaseTypeStringContext ctx) {
    if (ctx.TYPE_STRING() != null) {
      add(ctx.TYPE_STRING()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_STRING()
                 .getSymbol()
                 .getStopIndex(),
              doc.getStyle(Styles.KEY));
    }
    return super.visitBaseTypeString(ctx);
  }

  @Override
  public Object visitBaseTypeBool(MLRulesParser.BaseTypeBoolContext ctx) {
    if (ctx.TYPE_BOOL() != null) {
      add(ctx.TYPE_BOOL()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_BOOL()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    return super.visitBaseTypeBool(ctx);
  }

  @Override
  public Object visitBaseTypeFloat(MLRulesParser.BaseTypeFloatContext ctx) {
    if (ctx.TYPE_REAL() != null) {
      add(ctx.TYPE_REAL()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_REAL()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    return super.visitBaseTypeFloat(ctx);
  }

  @Override
  public Object visitBaseTypeLink(MLRulesParser.BaseTypeLinkContext ctx) {
    if (ctx.TYPE_LINK() != null) {
      add(ctx.TYPE_LINK()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_LINK()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    return super.visitBaseTypeLink(ctx);
  }

  @Override
  public Object visitTupleType(MLRulesParser.TupleTypeContext ctx) {
    if (ctx.TYPE_TUPLE() != null) {
      add(ctx.TYPE_TUPLE()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_TUPLE()
                 .getSymbol()
                 .getStopIndex(),
              doc.getStyle(Styles.KEY));
    }
    return super.visitTupleType(ctx);
  }

  @Override
  public Object visitSolutionType(MLRulesParser.SolutionTypeContext ctx) {
    if (ctx.TYPE_SOL() != null) {
      add(ctx.TYPE_SOL()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_SOL()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.KEY));
    }
    return super.visitSolutionType(ctx);
  }

  @Override
  public Object visitSpeciesType(MLRulesParser.SpeciesTypeContext ctx) {
    if (ctx.TYPE_SPECIES() != null) {
      add(ctx.TYPE_SPECIES()
             .getSymbol()
             .getStartIndex(),
              ctx.TYPE_SPECIES()
                 .getSymbol()
                 .getStopIndex(),
              doc.getStyle(Styles.KEY));
    }
    return super.visitSpeciesType(ctx);
  }

  @Override
  public Object visitTypeDefinition(MLRulesParser.TypeDefinitionContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
    return super.visitTypeDefinition(ctx);
  }

  @Override
  public Object visitFunctionDefinition(MLRulesParser.FunctionDefinitionContext ctx) {
    if (ctx.ID() != null) {
      add(ctx.ID()
             .getSymbol()
             .getStartIndex(),
              ctx.ID()
                 .getSymbol()
                 .getStopIndex(), doc.getStyle(Styles.FUNCTION));
    }
    return super.visitFunctionDefinition(ctx);
  }

  public void doSyntaxHighlighting(MLRulesParser.ModelContext context) {
    this.visit(context);
    try {
      if (currentIndex < text.length()) {
        doc.insertString(currentIndex,
                text.substring(currentIndex, text.length()),
                new SimpleAttributeSet());
        if (doc.getLength() > text.length()) {
          throw new IllegalArgumentException(String.format(
                  "The syntax checker created new characters.\nBefore:%s\nAfter:%s",
                  text, doc.getText(0, doc.getLength())));
        }
      }
      pane.setStyledDocument(doc);
      if (pos < pane.getText()
                    .length()) {
        pane.setCaretPosition(pos);
      }
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

}
