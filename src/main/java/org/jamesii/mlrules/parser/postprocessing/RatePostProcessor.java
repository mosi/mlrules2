/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.postprocessing;

import org.jamesii.core.math.parsetree.Node;
import org.jamesii.core.math.parsetree.ValueNode;
import org.jamesii.mlrules.model.Model;
import org.jamesii.mlrules.model.rule.Rule;
import org.jamesii.mlrules.util.Assignment;
import org.jamesii.mlrules.util.MLEnvironment;

import java.util.List;

/**
 * 
 * @author Andreas Ruscheinski
 *
 */
public class RatePostProcessor extends AbstractPostProcessor {

  @Override
  public void postProcess(Model model) {
    MLEnvironment env = model.getEnv();
    String oldRate = "";
    for (Rule normalRule : model.getRules().getRules()) {
      Node rateNode = normalRule.getRate();
      oldRate = rateNode.toString();
      try {
        MLEnvironment local = env.newLevel();
        List<Assignment> assignments = normalRule.getAssignments();
        for (Assignment assignment : assignments) {
          ValueNode result = assignment.getExpression().calc(local);
          for (String name : assignment.getNames()) {
            local.setValue(name, result);
          }
        }

        Node evalRateNode = rateNode.calc(local);
        normalRule.setRate(evalRateNode);
        String newRate = normalRule.getRate().toString();
      } catch (Exception e) {
        e.printStackTrace();
        break;
      }
    }
  }

}
