/*
 *   Copyright 2015 University of Rostock
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.jamesii.mlrules.parser.exception;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * This exception is thrown if the semantics of an ML-Rules model is invalid,
 * e.g., a variable is used that has not been defined or a string and a link
 * shall be summed.
 * 
 * @author Tobias Helms
 *
 */
public class SemanticsException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final String msg;

  private final ParserRuleContext ctx;

  public SemanticsException(ParserRuleContext ctx, String msg) {
    this.msg = String.format("line %s: %s", ctx.getStart().getLine(), msg);
    this.ctx = ctx;
  }

  public String getMsg() {
    return msg;
  }

  public ParserRuleContext getCtx() {
    return ctx;
  }

  @Override
  public String toString() {
    return msg;
  }

}
