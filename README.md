# The official ML-Rules repository

ML-Rules is a multi-level rule-based modeling formalism to model biochemical reaction networks. This repository contains the current version of ML-Rules supporting not only nested and attributed species, but also functions on attributes and functions on solutions. Further, ML-Rules supports arbitrary reaction kinetics. ML-Rules is implemented as an external domain-specific language using ANTLR4. Various simulators are available to execute models stochastically (using the SSA or tailored simulators), deterministically (if possible), or in a hybrid manner.

For more information, see the following paper (sorted from new to old). A complete list of publications related to ML-Rules can be found [**here**](https://eprints.mosi.informatik.uni-rostock.de/view/projects/ESCeMMo.html).

-  [Handling Dynamic Sets of Reactions in Stochastic Simulation Algorithms] (https://dl.acm.org/citation.cfm?id=3200943) by Till Köster and Adelinde M. Uhrmacher, Proceedings of the ACM SIGSIM Conference on Principles of Advanced Discrete Simulation (PADS), 2018
 - Stochastic simulation algorithms with different strategies dealing with dynamics sets of reactions are compared. The artifacts to reproduce the experiments can be found [**here**](https://git.informatik.uni-rostock.de/mosi/dynamic_reaction_container).
-  [Hybrid Simulation of Dynamic Reaction Networks in Multi-Level Models] (https://dl.acm.org/citation.cfm?id=3200926) by Tobias Helms, Pia Wilsdorf, and Adelinde M. Uhrmacher, Proceedings of the ACM SIGSIM Conference on Principles of Advanced Discrete Simulation (PADS), 2018
 - A hybrid simulator combining deterministic, approximate and stochastic approaches is presented. The artifacts to reproduce the experiments can be found [**here**](https://git.informatik.uni-rostock.de/mosi/pads2018/hybrid-simulation).
-  [Semantics and Efficient Simulation Algorithms of an Expressive Multi-Level Modeling Language] (https://dl.acm.org/citation.cfm?id=2998499) by Tobias Helms, Tom Warnke, Carsten Maus, and Adelinde M. Uhrmacher, ACM Transactions on Modeling and Computer Simulation (TOMACS) - Special Issue on PADS 2015, Volume 27, Issue 2, Article No. 8, July 2017
 - The formal syntax and semantics of ML-Rules are used to develop tailored simulators for sub classes of ML-Rules models. The source code, models, and executables used to execute the experiments are available [**here**](https://git.informatik.uni-rostock.de/mosi/mlrules-tomacs).
-  [Syntax and Semantics of a Multi-Level Modeling Language](http://dl.acm.org/citation.cfm?id=2769467) by Tom Warnke, Tobias Helms, and Adelinde M. Uhrmacher, Proceedings of the 3rd ACM SIGSIM Conference on Principles of Advanced Discrete Simulation (PADS), 2015
 - A formal syntax and semantics of ML-Rules and implications for its simulation algorithm are presented in this paper.
-  [Multi-level modeling and simulation of cell biological systems with ML-Rules - A tutorial](http://ieeexplore.ieee.org/document/7019887/?arnumber=7019887) by Tobias Helms, Carsten Maus, Fiete Haack, and Adelinde M. Uhrmacher, Proceedings of the Winter Simulation Conference (WSC), 2014
 - A tutorial about ML-Rules and its opportunities.
-  [An Approximate Execution of Rule-Based Multi-level Models](http://link.springer.com/chapter/10.1007/978-3-642-40708-6_3) by Tobias Helms, Martin Luboschik, Heidrun Schumann, and Adelinde M. Uhrmacher, Proceedings of the 11th International Conference on Computational Methods in Systems Biology (CMSB), 2013
 - In this paper, we present a tau-leaping variant for ML-Rules.
-  [Toward Accessible Multilevel Modeling in Systems Biology: A Rule-based Language Concept](http://rosdok.uni-rostock.de/file/rosdok_disshab_0000001060/rosdok_derivate_0000005181/Dissertation_Maus_2013.pdf), Carsten Maus, PhD Thesis, University of Rostock, 2013
 - PHD Thesis about ML-Rules written by its creator Carsten Maus.
-  [Toward a language for the flexible observation of simulations](http://dl.acm.org/citation.cfm?id=2430301) by Tobias Helms, Jan Himmelspach, Carsten Maus, Oliver Röwer, Johannes Schützel, and Adelinde M. Uhrmacher, Proceedings of the Winter Simulation Conference, 2012
 - An SQL-like instrumentation language is presented, which is exemplarily applied to ML-Rules.
- [Rule-based multi-level modeling of cell biological systems](http://bmcsystbiol.biomedcentral.com/articles/10.1186/1752-0509-5-166) by Carsten Maus, Stefan Rybacki, and Adelinde M. Uhrmacher, BMC Systems Biology 2011
 - One of the first ML-Rules papers describing its basic syntax and features. The sandbox to start experiments with the presented version is available [here](http://www.biomedcentral.com/content/supplementary/1752-0509-5-166-s1.zip).




# Repository Structure

The repository contains the current source code of ML-Rules (*./src/*) and a documentation (*./documentation/*) including several example models. A sandbox is available (org.jamesii.mlrules.gui.SimpleEditor) that can directly be started. Alternatively, a quickstart repository exists [here](https://git.informatik.uni-rostock.de/mosi/mlrules2-quickstart), which can be used to start the editor with the current released version of ML-Rules. The grammar files of ML-Rules defining its concrete syntax are saved in ./src/main/java/org/jamesii/mlrules2/parser/grammar.



